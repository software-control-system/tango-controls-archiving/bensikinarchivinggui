/*	Synchrotron Soleil
 *
 *   File          :  StateViewer.java
 *
 *   Project       :  Bensikin_CVS
 *
 *   Description   :
 *
 *   Author        :  SOLEIL
 *
 *   Original      :  16 mars 2006
 *
 *   Revision:  					Author:
 *   Date: 							State:
 *
 *   Log: StateViewer.java,v
 *
 */
package fr.soleil.bensikin.components;

import java.awt.Color;

import javax.swing.JLabel;

import fr.esrf.TangoDs.TangoConst;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.tango.data.adapter.StateAdapter;

public class StateViewer extends JLabel {

    private static final long serialVersionUID = 3090334740049922790L;

    private final MyStateAdapter stateAdapter;

    public StateViewer() {
        super();
        stateAdapter = new MyStateAdapter();
    }

    public StateViewer(int stateCode) {
        this();
        setState(stateCode);
    }

    public void setState(int stateCode) {
        this.setText(TangoConst.Tango_DevStateName[stateCode]);
        this.setToolTipText(this.getText());
        Color color = null;
        // XXX en attendant correction dans TangoDAO

        // TODO avisto
        CometeColor cometeColor = stateAdapter.getColor(this.getText());

        if (cometeColor != null) {
            color = ColorTool.getColor(cometeColor);
        }
        cometeColor = null;
        if (color != null) {
            this.setBackground(color);
            this.setOpaque(true);
        } else {
            this.setOpaque(false);
        }
    }

    protected class MyStateAdapter extends StateAdapter<String> {
        public MyStateAdapter() {
            super(null, String.class);
        }

        public CometeColor getColor(String name) {
            return adaptSourceData(name);
        }
    };
}
