//+======================================================================
//$Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/components/editors/SnapshotDetailEditor.java,v $
//
//Project:      Tango Archiving Service
//
//Description:  Java source code for the class  SnapshotDetailEditor.
//						(Claisse Laurent) - 16 juin 2005
//
//$Author: ounsy $
//
//$Revision: 1.10 $
//
//$Log: SnapshotDetailEditor.java,v $
//Revision 1.10  2006/11/29 09:58:31  ounsy
//minor changes
//
//Revision 1.9  2006/06/28 12:46:25  ounsy
//image support
//
//Revision 1.8  2006/03/16 16:40:30  ounsy
//taking care of String formating
//
//Revision 1.7  2006/02/15 09:15:42  ounsy
//small spectrum bug correcion
//
//Revision 1.6  2005/12/14 16:11:41  ounsy
//minor changes
//
//Revision 1.5  2005/11/29 18:25:27  chinkumo
//no message
//
//Revision 1.1.1.2  2005/08/22 11:58:35  chinkumo
//First commit
//
//
//copyleft :		Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.components.editors;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.tools.StringFormater;
import fr.soleil.bensikin.components.BensikinBooleanViewer;
import fr.soleil.bensikin.components.snapshot.detail.SnapshotDetailTable;
import fr.soleil.bensikin.data.snapshot.SnapshotAttributeValue;

/**
 * Cell editor used by SnapshotDetailTable. The used component for scalar
 * attributes is a TextField.
 * 
 * @author CLAISSE
 */
public class SnapshotDetailEditor extends AbstractCellEditor implements TableCellEditor {

    private static final long serialVersionUID = -5430693511896256940L;

    private JComponent buffer;
    private final JTable correspondingTable;
    private final CoordinateCheckBoxAdapter<JCheckBox> settableCheckBoxAdapter;
    private final CoordinateCheckBoxAdapter<BensikinBooleanViewer> checkBoxAdapter;
    private final JTextField textField;

    /**
     * Default constructor
     */
    public SnapshotDetailEditor(JTable table) {
        super();
        buffer = null;
        correspondingTable = table;
        checkBoxAdapter = new CoordinateCheckBoxAdapter<BensikinBooleanViewer>(new BensikinBooleanViewer());
        checkBoxAdapter.getCheckBox().addActionListener(
                new CheckBoxCanSetListener<BensikinBooleanViewer>(checkBoxAdapter));
        settableCheckBoxAdapter = new CoordinateCheckBoxAdapter<JCheckBox>(new JCheckBox());
        settableCheckBoxAdapter.getCheckBox().addActionListener(
                new CheckBoxCanSetListener<JCheckBox>(settableCheckBoxAdapter));
        textField = new JTextField();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        return getScalarTableCellEditorComponent(table, value, isSelected, row, column);
    }

    /**
     * Used for scalar values.
     * 
     * @param table
     * @param value
     * @param isSelected
     * @param row
     * @param column
     * @return A JTextField which content is filled with the String
     *         representation of value, or a JCheckBox if value is Boolean.
     */
    private Component getScalarTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row,
            int column) {
        if (value instanceof Boolean) {
            settableCheckBoxAdapter.setRow(row);
            settableCheckBoxAdapter.setColumn(column);
            settableCheckBoxAdapter.getCheckBox().setSelected(((Boolean) value).booleanValue());
            buffer = settableCheckBoxAdapter.getCheckBox();
        } else {
            if (value instanceof SnapshotAttributeValue) {
                if ((((SnapshotAttributeValue) value).getDataFormat() == SnapshotAttributeValue.SCALAR_DATA_FORMAT)
                        && (((SnapshotAttributeValue) value).getDataType() == TangoConst.Tango_DEV_BOOLEAN)) {
                    Boolean val = (Boolean) ((SnapshotAttributeValue) value).getScalarValue();
                    checkBoxAdapter.getCheckBox().setSelected(val);
                    checkBoxAdapter.setRow(row);
                    checkBoxAdapter.setColumn(column);
                    buffer = checkBoxAdapter.getCheckBox();
                    buffer.setOpaque(true);
                    if (isSelected) {
                        buffer.setBackground(new Color(0, 0, 120));
                        buffer.setForeground(Color.WHITE);
                    } else {
                        buffer.setBackground(Color.WHITE);
                        buffer.setForeground(Color.BLACK);
                    }
                } else {
                    Object val = ((SnapshotAttributeValue) value).getValue();
                    if (val == null) {
                        textField.setText("");
                    } else if (val instanceof String) {
                        textField.setText(StringFormater.formatStringToRead(new String((String) val)));
                    } else {
                        textField.setText(val.toString());
                    }
                    buffer = textField;
                }
            } else {
                textField.setText(value == null ? "" : value.toString());
                buffer = textField;
            }
        }
        return buffer;
    }

    @Override
    public Object getCellEditorValue() {
        Object value;
        if (buffer instanceof JCheckBox) {
            value = Boolean.valueOf(((JCheckBox) buffer).isSelected());
        } else if (buffer instanceof JTextField) {
            value = ((JTextField) buffer).getText();
        } else {
            value = null;
        }
        return value;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private class CoordinateCheckBoxAdapter<T extends JCheckBox> {
        private int row;
        private int column;
        private final T checkBox;

        public CoordinateCheckBoxAdapter(T checkBox) {
            this.checkBox = checkBox;
            row = -1;
            column = -1;
        }

        public int getRow() {
            return row;
        }

        public void setRow(int row) {
            this.row = row;
        }

        public int getColumn() {
            return column;
        }

        public void setColumn(int column) {
            this.column = column;
        }

        public T getCheckBox() {
            return checkBox;
        }
    }

    private class CheckBoxListener<T extends JCheckBox> implements ActionListener {

        protected final CoordinateCheckBoxAdapter<T> checkboxAdapter;

        public CheckBoxListener(CoordinateCheckBoxAdapter<T> checkboxAdapter) {
            this.checkboxAdapter = checkboxAdapter;
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            correspondingTable.setValueAt(Boolean.valueOf(checkboxAdapter.getCheckBox().isSelected()),
                    checkboxAdapter.getRow(), checkboxAdapter.getColumn());
            correspondingTable.editingCanceled(null);
        }
    }

    private class CheckBoxCanSetListener<T extends JCheckBox> extends CheckBoxListener<T> {

        public CheckBoxCanSetListener(CoordinateCheckBoxAdapter<T> checkboxAdapter) {
            super(checkboxAdapter);
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            super.actionPerformed(evt);
            if (correspondingTable instanceof SnapshotDetailTable) {
                ((SnapshotDetailTable) correspondingTable).updateSnapshotCanSetProperties(checkboxAdapter.getRow(),
                        checkboxAdapter.getCheckBox().isSelected());
            }
        }

    }

}
