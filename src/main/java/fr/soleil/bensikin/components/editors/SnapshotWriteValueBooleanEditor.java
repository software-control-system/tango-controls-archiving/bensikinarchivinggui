package fr.soleil.bensikin.components.editors;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

import fr.soleil.bensikin.components.BensikinBooleanViewer;

public class SnapshotWriteValueBooleanEditor extends AbstractCellEditor implements TableCellEditor {

    private static final long serialVersionUID = 1008083035722990139L;

    private static final String TRUE = "true";
    private static final String FALSE = "false";

    private Component comp;
    private final BooleanAdapter booleanAdapter;
    private final JTextField textField;

    public SnapshotWriteValueBooleanEditor() {
        super();
        booleanAdapter = new BooleanAdapter(new BensikinBooleanViewer());
        booleanAdapter.getCheckbox().addActionListener(new BooleanListener(booleanAdapter));
        textField = new JTextField();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        if (value instanceof String) {
            String strValue = value.toString().trim();
            if (TRUE.equalsIgnoreCase(strValue)) {
                comp = getCheckedComponent(row, column, table, Boolean.TRUE);
            } else if (FALSE.equalsIgnoreCase(strValue)) {
                comp = getCheckedComponent(row, column, table, Boolean.FALSE);
            } else {
                textField.setText(value.toString());
                comp = textField;
            }
        } else {
            textField.setText(value.toString());
            comp = textField;
        }
        return comp;
    }

    protected Component getCheckedComponent(int row, int column, JTable table, Boolean value) {
        synchronized (booleanAdapter) {
            booleanAdapter.update(row, column, table);
        }
        booleanAdapter.getCheckbox().setSelected(value);
        booleanAdapter.getCheckbox().setOpaque(false);
        return booleanAdapter.getCheckbox();
    }

    @Override
    public Object getCellEditorValue() {
        if (comp instanceof BensikinBooleanViewer) {
            return "" + ((BensikinBooleanViewer) comp).isSelected();
        } else
            return ((JTextField) comp).getText();
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private class BooleanAdapter {
        private final BensikinBooleanViewer checkbox;
        private int row;
        private int column;
        private JTable table;

        public BooleanAdapter(BensikinBooleanViewer checkbox) {
            this.checkbox = checkbox;
            row = -1;
            column = -1;
            table = null;
        }

        public BensikinBooleanViewer getCheckbox() {
            return checkbox;
        }

        public int getRow() {
            return row;
        }

        public int getColumn() {
            return column;
        }

        public JTable getTable() {
            return table;
        }

        public void update(int row, int column, JTable table) {
            this.row = row;
            this.column = column;
            this.table = table;
        }
    }

    private class BooleanListener implements ActionListener {

        private BooleanAdapter adapter;

        public BooleanListener(BooleanAdapter adapter) {
            this.adapter = adapter;
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            synchronized (adapter) {
                adapter.getCheckbox().setSelected(Boolean.valueOf(adapter.getCheckbox().isSelected()));
                adapter.getTable().setValueAt("" + adapter.getCheckbox().isSelected(), adapter.getRow(),
                        adapter.getColumn());
                adapter.getTable().editingCanceled(null);
            }
        }
    }

}
