/*	Synchrotron Soleil 
 *  
 *   File          :  SnapSpectrumWriteTableEditor.java
 *  
 *   Project       :  Bensikin_CVS
 *  
 *   Description   :  
 *  
 *   Author        :  SOLEIL
 *  
 *   Original      :  14 f�vr. 2006 
 *  
 *   Revision:  					Author:  
 *   Date: 							State:  
 *  
 *   Log: SnapSpectrumWriteTableEditor.java,v 
 *
 */
package fr.soleil.bensikin.components.editors;

import java.awt.Component;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

/**
 * 
 * @author SOLEIL
 */
public class SnapSpectrumWriteTableEditor extends AbstractCellEditor implements TableCellEditor {

    private static final long serialVersionUID = 7149852079457164308L;

    private final JTextField buffer;

    /**
     * Default constructor
     */
    public SnapSpectrumWriteTableEditor() {
        super();
        buffer = new JTextField();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        buffer.setText(value.toString());
        return buffer;
    }

    @Override
    public Object getCellEditorValue() {
        return buffer.getText();
    }

}
