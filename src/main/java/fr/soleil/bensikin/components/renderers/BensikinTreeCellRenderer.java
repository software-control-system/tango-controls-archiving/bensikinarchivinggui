//+======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/components/renderers/BensikinTreeCellRenderer.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  BensikinTreeCellRenderer.
//						(GIRARDOT Raphael) - oct. 2005
//
// $Author: chinkumo $
//
// $Revision: 1.1 $
//
// $Log: BensikinTreeCellRenderer.java,v $
// Revision 1.1  2005/11/29 18:25:08  chinkumo
// no message
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.components.renderers;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreePath;

import fr.soleil.bensikin.Bensikin;

public class BensikinTreeCellRenderer extends DefaultTreeCellRenderer {

    private static final long serialVersionUID = 9136062957615500232L;

    private static final ImageIcon ATTRIBUTE_ICON = new ImageIcon(Bensikin.class.getResource("icons/attribute.gif"));
    private static final ImageIcon HOST_ICON = new ImageIcon(Bensikin.class.getResource("icons/host.gif"));
    private static final ImageIcon DEVICE_ICON = new ImageIcon(Bensikin.class.getResource("icons/device.gif"));
    private static final ImageIcon BIG_DEVICE_ICON = new ImageIcon(Bensikin.class.getResource("icons/big_device.gif"));
    private static final ImageIcon FOLDER_ICON = new ImageIcon(Bensikin.class.getResource("icons/folder.gif"));
    private static final ImageIcon FOLDER_OPENED_ICON = new ImageIcon(
            Bensikin.class.getResource("icons/folder_opened.gif"));

    private final JLabel label;

    public BensikinTreeCellRenderer() {
        super();
        label = new JLabel();
        label.setOpaque(true);
        label.setFont(new Font("Arial", Font.PLAIN, 11));
        label.setBackground(Color.WHITE);
        label.setForeground(Color.BLACK);
        label.setIgnoreRepaint(false);
        label.setIconTextGap(2);
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded,
            boolean leaf, int row, boolean hasFocus) {
        Component comp = null;
        TreePath path = tree.getPathForRow(row);
        if (path != null) {
            Object[] thePath = path.getPath();
            if (thePath != null) {
                label.setText(value.toString().trim());
                label.setBackground(Color.WHITE);
                label.setForeground(Color.BLACK);
                if (sel) {
                    label.setBackground(new Color(0, 0, 120));
                    label.setForeground(Color.WHITE);
                }
                switch (thePath.length) {
                case 1:
                    label.setIcon(HOST_ICON);
                    break;
                case 4:
                    if (sel || expanded) {
                        label.setIcon(BIG_DEVICE_ICON);
                    } else {
                        label.setIcon(DEVICE_ICON);
                    }
                    break;
                case 5:
                    label.setIcon(ATTRIBUTE_ICON);
                    break;
                default:
                    if (sel || expanded) {
                        label.setIcon(FOLDER_OPENED_ICON);
                    } else {
                        label.setIcon(FOLDER_ICON);
                    }
                    break;
                }
                label.repaint();
                comp = label;
            }
        }
        if (comp == null) {
            comp = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        }
        return comp;
    }

}
