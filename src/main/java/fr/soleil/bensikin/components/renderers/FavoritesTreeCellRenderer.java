package fr.soleil.bensikin.components.renderers;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreePath;

import fr.soleil.bensikin.Bensikin;

public class FavoritesTreeCellRenderer extends DefaultTreeCellRenderer {

    private static final long serialVersionUID = 1807559047998843170L;

    private static final ImageIcon ATTRIBUTE_ICON = new ImageIcon(Bensikin.class.getResource("icons/attribute.gif"));
    private static final ImageIcon FOLDER_ICON = new ImageIcon(Bensikin.class.getResource("icons/folder.gif"));
    private static final ImageIcon FOLDER_OPENED_ICON = new ImageIcon(
            Bensikin.class.getResource("icons/folder_opened.gif"));

    private final JLabel label;

    public FavoritesTreeCellRenderer() {
        super();
        label = new JLabel();
        label.setOpaque(true);
        label.setFont(new Font("Arial", Font.PLAIN, 11));
        label.setBackground(Color.WHITE);
        label.setForeground(Color.BLACK);
        label.setIgnoreRepaint(false);
        label.setIconTextGap(2);
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded,
            boolean leaf, int row, boolean hasFocus) {
        Component comp = null;
        TreePath path = tree.getPathForRow(row);
        if (path != null) {
            boolean isContextNode = false;
            Object[] path_o = path.getPath();
            for (int i = 0; i < path_o.length - 1; i++) {
                DefaultMutableTreeNode currentPathElement = (DefaultMutableTreeNode) path_o[i + 1];
                if (currentPathElement.getUserObject().getClass().equals(DefaultMutableTreeNode.class)) {
                    isContextNode = true;
                }
            }

            label.setText(value.toString().trim());
            if (sel) {
                label.setBackground(new Color(0, 0, 120));
                label.setForeground(Color.WHITE);
            }

            if (isContextNode) {
                label.setIcon(ATTRIBUTE_ICON);
            } else {
                if (sel || expanded) {
                    label.setIcon(FOLDER_OPENED_ICON);
                } else {
                    label.setIcon(FOLDER_ICON);
                }
            }

            label.repaint();
            comp = label;
        }
        if (comp == null) {
            comp = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        }
        return comp;
    }

}
