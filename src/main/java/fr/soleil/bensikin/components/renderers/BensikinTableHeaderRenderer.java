package fr.soleil.bensikin.components.renderers;

import java.awt.Component;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;

import fr.soleil.bensikin.components.BensikinComparator;
import fr.soleil.bensikin.models.SortableTableModel;

public class BensikinTableHeaderRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = -3756718459626064980L;

    private static final ImageIcon UP_ICON = new ImageIcon(
            BensikinTableHeaderRenderer.class.getResource("/fr/soleil/bensikin/icons/sort_up.gif"));
    private static final ImageIcon DOWN_ICON = new ImageIcon(
            BensikinTableHeaderRenderer.class.getResource("/fr/soleil/bensikin/icons/sort_down.gif"));

    public BensikinTableHeaderRenderer() {
        super();
        setBorder(UIManager.getBorder("TableHeader.cellBorder"));
        setHorizontalAlignment(JLabel.CENTER);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        if (table != null) {
            int columnToUse = table.convertColumnIndexToModel(column);
            JTableHeader header = table.getTableHeader();
            if (header != null) {
                setForeground(header.getForeground());
                setBackground(header.getBackground());
                setFont(header.getFont());
            }
            int sortedColumn = -1;
            if (table.getModel() instanceof SortableTableModel) {
                SortableTableModel model = (SortableTableModel) table.getModel();
                sortedColumn = model.getSortedColumn();
                int idSort = model.getIdSort();
                if (columnToUse == sortedColumn) {
                    switch (idSort) {
                    case BensikinComparator.SORT_UP:
                        setIcon(UP_ICON);
                        break;
                    case BensikinComparator.SORT_DOWN:
                        setIcon(DOWN_ICON);
                        break;
                    default:
                        setIcon(null);
                        break;
                    }
                } else {
                    setIcon(null);
                }
            }
            if (sortedColumn < 0) {
                RowSorter<?> sorter = table.getRowSorter();
                if (sorter != null) {
                    List<? extends RowSorter.SortKey> keys = sorter.getSortKeys();
                    if ((keys != null) && (keys.size() > 0)) {
                        RowSorter.SortKey key = keys.get(0);
                        if (key != null) {
                            sortedColumn = key.getColumn();
                            if (columnToUse == sortedColumn) {
                                switch (key.getSortOrder()) {
                                case ASCENDING:
                                    setIcon(UP_ICON);
                                    break;
                                case DESCENDING:
                                    setIcon(DOWN_ICON);
                                    break;
                                default:
                                    setIcon(null);
                                    break;
                                }
                            }
                        }

                    }
                }
            }
        }
        setText((value == null) ? "" : value.toString());
        return this;
    }
}
