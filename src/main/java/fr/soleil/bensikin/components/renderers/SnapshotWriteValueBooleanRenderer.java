package fr.soleil.bensikin.components.renderers;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;

import fr.soleil.bensikin.components.BensikinBooleanViewer;

public class SnapshotWriteValueBooleanRenderer extends BensikinTableCellRenderer {

    private static final long serialVersionUID = -5651354000408235700L;

    private static final String TRUE = "true";
    private static final String FALSE = "false";

    private final BensikinBooleanViewer viewer;

    public SnapshotWriteValueBooleanRenderer() {
        super();
        viewer = new BensikinBooleanViewer();
        viewer.setOpaque(true);
        viewer.setBackground(Color.WHITE);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        Component comp;
        if (value instanceof String) {
            String strValue = value.toString().trim();
            if (TRUE.equalsIgnoreCase(strValue)) {
                viewer.setSelected(Boolean.TRUE);
                comp = viewer;
            } else if (FALSE.equalsIgnoreCase(strValue)) {
                viewer.setSelected(Boolean.FALSE);
                comp = viewer;
            } else {
                comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        } else {
            comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
        return comp;
    }
}
