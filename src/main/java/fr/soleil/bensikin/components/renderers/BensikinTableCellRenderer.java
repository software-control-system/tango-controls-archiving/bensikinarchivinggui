// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/bensikin/bensikin/components/renderers/BensikinTableCellRenderer.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class BensikinTableCellRenderer.
// (GIRARDOT Raphael) - oct. 2005
//
// $Author: ounsy $
//
// $Revision: 1.8 $
//
// $Log: BensikinTableCellRenderer.java,v $
// Revision 1.8 2007/06/29 09:18:58 ounsy
// devLong represented as Integer + SnapshotCompareTable sorting
//
// Revision 1.7 2007/02/08 15:06:19 ounsy
// corrected a bug for State scalar attributes
//
// Revision 1.6 2006/10/31 16:54:08 ounsy
// milliseconds and null values management
//
// Revision 1.5 2006/04/13 12:37:33 ounsy
// new spectrum types support
//
// Revision 1.4 2006/03/16 15:34:14 ounsy
// State scalar support
//
// Revision 1.3 2006/03/15 15:07:41 ounsy
// renders boolean scalars
//
// Revision 1.2 2005/12/14 16:12:04 ounsy
// minor changes
//
// Revision 1.1 2005/11/29 18:25:08 chinkumo
// no message
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.bensikin.components.renderers;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;

import fr.esrf.TangoDs.TangoConst;
import fr.soleil.bensikin.components.BensikinBooleanViewer;
import fr.soleil.bensikin.components.StateViewer;
import fr.soleil.bensikin.data.snapshot.SnapshotAttributeValue;
import fr.soleil.bensikin.tools.Messages;

public class BensikinTableCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 8887998946423013523L;

    public static final Color HIGHLIGHTED = Color.YELLOW;
    public static final Color HIGHLIGHTED_SELECTED = new Color(230, 230, 50);
    public static final Color HIGHLIGHTED_TITLE = new Color(212, 203, 142);
    public static final Color HIGHLIGHTED_SELECTED_TITLE = new Color(150, 144, 117);
    public static final Color HIGHLIGHTED_SELECTED_TITLE_DARK = new Color(110, 104, 77);

    private List<Integer> attributesNumRowList;
    private boolean hightlight;
    private final JCheckBox checkbox;
    private final JLabel label;
    private final Font labelFont;
    private final StateViewer stateViewer;
    private final BensikinBooleanViewer buffer;
    private final DefaultTableCellRenderer renderer;

    public BensikinTableCellRenderer() {
        super();
        attributesNumRowList = new ArrayList<Integer>();
        hightlight = false;
        checkbox = new JCheckBox();
        label = new JLabel();
        labelFont = label.getFont();
        label.setOpaque(true);
        stateViewer = new StateViewer();
        buffer = new BensikinBooleanViewer();
        buffer.setOpaque(true);
        renderer = new DefaultTableCellRenderer();
    }

    public List<Integer> getAttributesNumRowList() {
        return attributesNumRowList;
    }

    public void setAttributesNumRowList(List<Integer> attributesNumRowList) {
        this.attributesNumRowList = (attributesNumRowList == null ? new ArrayList<Integer>() : attributesNumRowList);
    }

    public boolean isHightlight() {
        return hightlight;
    }

    public void setHightlight(boolean hightlight) {
        this.hightlight = hightlight;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean hasFocus,
            int row, int column) {
        setBackground(Color.WHITE);
        boolean highlighted = ((getAttributesNumRowList() != null)
                && getAttributesNumRowList().contains(table.convertRowIndexToModel(row)) && isHightlight());
        JLabel ref = (JLabel) renderer.getTableCellRendererComponent(table, value, selected, hasFocus, row, column);
        label.setBorder(ref.getBorder());
        label.setBackground(ref.getBackground());
        label.setFont(labelFont);
        Component comp = super.getTableCellRendererComponent(table, value, selected, hasFocus, row, column);
        if (value instanceof Boolean) {
            checkbox.setSelected(((Boolean) value).booleanValue());
            checkbox.setOpaque(false);
            checkbox.setBackground(null);
            if (selected) {
                checkbox.setOpaque(true);
                if (highlighted) {
                    checkbox.setBackground(HIGHLIGHTED_SELECTED);
                } else {
                    checkbox.setBackground(ref.getBackground());
                }
            } else if (highlighted) {
                checkbox.setOpaque(true);
                checkbox.setBackground(HIGHLIGHTED);
            }
            comp = checkbox;
        } else if (value instanceof SnapshotAttributeValue
                && (((SnapshotAttributeValue) value).getDataFormat() == SnapshotAttributeValue.SCALAR_DATA_FORMAT)) {
            SnapshotAttributeValue attrValue = (SnapshotAttributeValue) value;
            Object scalarValue = attrValue.getScalarValue();
            if (attrValue.getDataType() == TangoConst.Tango_DEV_BOOLEAN) {
                Boolean val = (Boolean) scalarValue;
                buffer.setSelected(val);
                setColors(buffer, ref, highlighted, selected);
                comp = buffer;
            } else {
                if (scalarValue == null) {
                    label.setText(Messages.getMessage("SNAPSHOT_DETAIL_NO_DATA"));
                    setColors(label, highlighted, selected);
                    comp = label;
                } else if (attrValue.getDataType() == TangoConst.Tango_DEV_STATE) {
                    try {
                        Number valS = (Number) scalarValue;
                        if (valS != null) {
                            int val = valS.intValue();
                            stateViewer.setState(val);
                            comp = stateViewer;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        // do nothing
                    }
                } else if (attrValue.getDataType() == TangoConst.Tango_DEV_STRING) {
                    String val = (String) scalarValue;
                    if (val != null) {
                        label.setText(val);
                        setColors(label, ref, highlighted, selected);
                        comp = label;
                    }
                }
            }
        } else if (value == null) {
            label.setText(Messages.getMessage("SNAPSHOT_DETAIL_NO_DATA"));
            setColors(label, highlighted, selected);
            comp = label;
        } else {
            JTableHeader header = table.getTableHeader();
            if (header != null) {
                TableCellRenderer renderer = header.getDefaultRenderer();
                if (renderer != null) {
                    JLabel comp2 = (JLabel) renderer.getTableCellRendererComponent(table, value, selected, hasFocus, 0,
                            0);
                    if ((comp2 != null) && (row > -1) && (table.convertColumnIndexToModel(column) == 0)) {
                        if ((comp instanceof JComponent) && (comp2 instanceof JComponent)) {
                            ((JComponent) comp).setBorder(((JComponent) comp2).getBorder());
                        }
                        label.setText(comp2.getText());
                        label.setFont(comp2.getFont());
                        label.setBorder(comp2.getBorder());
                        setColorsTitle(label, comp2, highlighted, selected);
                    } else {
                        label.setText(ref.getText());
                        label.setFont(ref.getFont());
                        setColors(label, ref, highlighted, selected);
                    }
                    comp = label;
                }
            }
        }
        return comp;
    }

    public static void setColors(Component comp, boolean highlighted, boolean selected) {
        if (highlighted) {
            if (selected) {
                comp.setBackground(HIGHLIGHTED_SELECTED);
            } else {
                comp.setBackground(HIGHLIGHTED);
            }
        }
    }

    public static void setColors(Component comp, Component ref, boolean highlighted, boolean selected) {
        if (selected) {
            if (highlighted) {
                comp.setBackground(HIGHLIGHTED_SELECTED);
            } else {
                comp.setBackground(ref.getBackground());
            }
            comp.setForeground(ref.getForeground());
        } else {
            if (highlighted) {
                comp.setBackground(HIGHLIGHTED);
            } else {
                comp.setBackground(Color.WHITE);
            }
            comp.setForeground(Color.BLACK);
        }
    }

    public static void setColorsTitle(Component comp, Component ref, boolean highlighted, boolean selected) {
        if (selected) {
            if (highlighted) {
                comp.setBackground(HIGHLIGHTED_SELECTED_TITLE);
            } else {
                comp.setBackground(Color.GRAY);
            }
            comp.setForeground(Color.WHITE);
        } else {
            if (highlighted) {
                comp.setBackground(HIGHLIGHTED_TITLE);
            } else {
                comp.setBackground(ref.getBackground());
            }
            comp.setForeground(ref.getForeground());
        }
    }
}
