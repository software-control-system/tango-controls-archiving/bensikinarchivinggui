// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/bensikin/bensikin/components/renderers/SnapshotDetailRenderer.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class SnapshotDetailRenderer.
// (Claisse Laurent) - 16 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.15 $
//
// $Log: SnapshotDetailRenderer.java,v $
// Revision 1.15 2007/03/26 08:07:53 ounsy
// *** empty log message ***
//
// Revision 1.14 2006/10/31 16:54:08 ounsy
// milliseconds and null values management
//
// Revision 1.13 2006/07/24 07:38:59 ounsy
// better image support
//
// Revision 1.12 2006/06/28 12:46:41 ounsy
// image support
//
// Revision 1.11 2006/06/16 08:52:57 ounsy
// ready for images
//
// Revision 1.10 2006/03/20 15:51:04 ounsy
// added the case of Snapshot delta value for spectrums which
// read and write parts are the same length.
//
// Revision 1.9 2006/03/16 15:34:32 ounsy
// State scalar support
//
// Revision 1.8 2006/03/15 15:08:07 ounsy
// renders boolean scalars
//
// Revision 1.7 2006/02/23 10:04:52 ounsy
// notice when modified
//
// Revision 1.6 2006/02/15 09:16:26 ounsy
// spectrums rw management
//
// Revision 1.5 2005/11/29 18:25:08 chinkumo
// no message
//
// Revision 1.1.1.2 2005/08/22 11:58:35 chinkumo
// First commit
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.bensikin.components.renderers;

import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import fr.esrf.TangoApi.StateUtilities;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.bensikin.components.snapshot.ImageButton;
import fr.soleil.bensikin.components.snapshot.ImageWriteButton;
import fr.soleil.bensikin.components.snapshot.SpectrumButton;
import fr.soleil.bensikin.components.snapshot.SpectrumDeltaValueButton;
import fr.soleil.bensikin.components.snapshot.SpectrumWriteValueButton;
import fr.soleil.bensikin.data.snapshot.SnapshotAttributeDeltaValue;
import fr.soleil.bensikin.data.snapshot.SnapshotAttributeReadValue;
import fr.soleil.bensikin.data.snapshot.SnapshotAttributeValue;
import fr.soleil.bensikin.data.snapshot.SnapshotAttributeWriteValue;
import fr.soleil.bensikin.models.SnapshotDetailTableModel;
import fr.soleil.bensikin.tools.Messages;

/**
 * A cell renderer used for SnapshotDetailTable. It paints cells containing "Not
 * applicable" values in grey, and modified values in red. In all other cases,
 * it paints the cell with the default component.
 * 
 * @author CLAISSE
 */
public class SnapshotDetailRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = -6180337780570297335L;

    private List<Integer> attributesNumRowList;
    private boolean hightlight;
    private final JPanel panel;
    private final JLabel label;
    private final BensikinTableCellRenderer cellRenderer;

    public SnapshotDetailRenderer() {
        super();
        attributesNumRowList = new ArrayList<Integer>();
        panel = new JPanel();
        label = new JLabel();
        cellRenderer = new BensikinTableCellRenderer();
    }

    public List<Integer> getAttributesNumRowList() {
        return attributesNumRowList;
    }

    public void setAttributesNumRowList(List<Integer> attributesNumRowList) {
        this.attributesNumRowList = attributesNumRowList;
    }

    public boolean isHightlight() {
        return hightlight;
    }

    public void setHightlight(boolean hightlight) {
        this.hightlight = hightlight;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        Component ret;
        boolean highlighted = ((getAttributesNumRowList() != null)
                && getAttributesNumRowList().contains(table.convertRowIndexToModel(row)) && isHightlight());
        resetBensikinCellRenderer();
        if (value instanceof SnapshotAttributeValue) {
            if (((SnapshotAttributeValue) value).isNotApplicable()) {
                if (isSelected) {
                    if (highlighted) {
                        panel.setBackground(BensikinTableCellRenderer.HIGHLIGHTED_SELECTED_TITLE_DARK);
                    } else {
                        panel.setBackground(Color.DARK_GRAY);
                    }
                } else {
                    if (highlighted) {
                        panel.setBackground(BensikinTableCellRenderer.HIGHLIGHTED_SELECTED_TITLE);
                    } else {
                        panel.setBackground(Color.GRAY);
                    }
                }
                ret = panel;
            } else {
                int dataFormat = ((SnapshotAttributeValue) value).getDataFormat();
                switch (dataFormat) {
                    case SnapshotAttributeValue.SCALAR_DATA_FORMAT:
                        if (((SnapshotAttributeValue) value).getScalarValue() == null) {
                            label.setText(Messages.getMessage("SNAPSHOT_DETAIL_NO_DATA"));
                            JLabel ref = (JLabel) super.getTableCellRendererComponent(table, label.getText(),
                                    isSelected, hasFocus, row, column);
                            label.setBorder(ref.getBorder());
                            label.setOpaque(true);
                            BensikinTableCellRenderer.setColors(label, ref, highlighted, isSelected);
                            ret = label;
                        } else if (((SnapshotAttributeValue) value).getDataType() == TangoConst.Tango_DEV_BOOLEAN
                                || ((SnapshotAttributeValue) value).getDataType() == TangoConst.Tango_DEV_STATE) {
                            ret = cellRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
                                    column);
                        } else {
                            ret = cellRenderer.getTableCellRendererComponent(table,
                                    ((SnapshotAttributeValue) value).toFormatedString(), isSelected, hasFocus, row,
                                    column);
                        }
                        if (((SnapshotAttributeValue) value).isModified()) {
                            ret.setBackground(Color.RED);
                        }
                        break;
                    case SnapshotAttributeValue.SPECTRUM_DATA_FORMAT:
                        JComponent ret2 = (JComponent) cellRenderer.getTableCellRendererComponent(table,
                                ((SnapshotAttributeValue) value).toFormatedString(), isSelected, hasFocus, row, column);
                        String name = "";
                        if (table.getColumnName(column).equals(Messages.getMessage("SNAPSHOT_DETAIL_COLUMNS_R"))) {
                            name += Messages.getMessage("DIALOGS_SPECTRUM_ATTRIBUTE_READ");
                        } else if (table.getColumnName(column)
                                .equals(Messages.getMessage("SNAPSHOT_DETAIL_COLUMNS_W"))) {
                            name += Messages.getMessage("DIALOGS_SPECTRUM_ATTRIBUTE_WRITE");
                        } else if (table.getColumnName(column)
                                .equals(Messages.getMessage("SNAPSHOT_DETAIL_COLUMNS_DELTA"))) {
                            name += Messages.getMessage("DIALOGS_SPECTRUM_ATTRIBUTE_DELTA");
                        }
                        JLabel button;
                        SnapshotDetailTableModel model = null;
                        if (table != null && table.getModel() instanceof SnapshotDetailTableModel) {
                            model = (SnapshotDetailTableModel) table.getModel();
                        }
                        if (value instanceof SnapshotAttributeWriteValue) {
                            button = new SpectrumWriteValueButton(table.getValueAt(row, 0).toString() + " " + name,
                                    (SnapshotAttributeValue) value, table, table.convertRowIndexToModel(row),
                                    table.convertColumnIndexToModel(column));
                        } else if (value instanceof SnapshotAttributeReadValue) {
                            button = new SpectrumButton(table.getValueAt(row, 0).toString() + " " + name,
                                    table.convertRowIndexToModel(row), table.convertColumnIndexToModel(column), model);
                        } else {
                            button = new SpectrumDeltaValueButton(table.getValueAt(row, 0).toString() + " " + name,
                                    (SnapshotAttributeValue) value, table, table.convertRowIndexToModel(row),
                                    table.convertColumnIndexToModel(column));
                        }
                        button.setOpaque(true);
                        button.setBorder(ret2.getBorder());
                        BensikinTableCellRenderer.setColors(button, ret2, highlighted, isSelected);
                        if (((SnapshotAttributeValue) value).isModified()) {
                            button.setBackground(Color.RED);
                        }
                        ret = button;
                        break;

                    case SnapshotAttributeValue.IMAGE_DATA_FORMAT:
                        JComponent ret3 = (JComponent) cellRenderer.getTableCellRendererComponent(table,
                                ((SnapshotAttributeValue) value).toFormatedString(), isSelected, hasFocus, row, column);
                        // XXX New components creation --> risk of memory leak
                        JLabel imageButton;
                        if (value instanceof SnapshotAttributeWriteValue) {
                            imageButton = new ImageWriteButton(table,
                                    table.getModel().getValueAt(table.convertRowIndexToModel(row), 0).toString(),
                                    ((SnapshotAttributeWriteValue) value).getImageValue(),
                                    ((SnapshotAttributeWriteValue) value).getDataType(),
                                    ((SnapshotAttributeWriteValue) value).getDisplayFormat());
                        } else {
                            imageButton = new ImageButton(
                                    table.getModel().getValueAt(table.convertRowIndexToModel(row), 0).toString(),
                                    ((SnapshotAttributeReadValue) value).getImageValue(),
                                    ((SnapshotAttributeReadValue) value).getDataType(),
                                    ((SnapshotAttributeReadValue) value).getDisplayFormat());
                        }
                        imageButton.setOpaque(true);
                        imageButton.setBorder(ret3.getBorder());
                        BensikinTableCellRenderer.setColors(imageButton, ret3, highlighted, isSelected);
                        ret = imageButton;
                        break;

                    default:
                        ret = null;
                        break;
                }
            }
        } else {
            ret = cellRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
        ret.repaint();
        return ret;
    }

    private void resetBensikinCellRenderer() {
        cellRenderer.setAttributesNumRowList(attributesNumRowList);
        cellRenderer.setHightlight(isHightlight());
    }

    public static String getTextFor(SnapshotAttributeValue value) {
        String text;
        if ((value == null) || value.isNotApplicable()) {
            text = "";
        } else {
            int dataFormat = value.getDataFormat();
            switch (dataFormat) {
                case SnapshotAttributeValue.SCALAR_DATA_FORMAT:
                    if (value.getScalarValue() == null) {
                        text = Messages.getMessage("SNAPSHOT_DETAIL_NO_DATA");
                    } else if (value.getDataType() == TangoConst.Tango_DEV_STATE) {
                        short val = ((Number) value.getScalarValue()).shortValue();
                        text = StateUtilities.getNameForState(val);
                    } else if (value.getDataType() == TangoConst.Tango_DEV_BOOLEAN) {
                        text = value.getScalarValue().toString();
                    } else {
                        text = value.toFormatedString();
                    }
                    break;
                case SnapshotAttributeValue.SPECTRUM_DATA_FORMAT:
                    if (value instanceof SnapshotAttributeReadValue) {
                        text = Messages.getMessage("DIALOGS_SPECTRUM_ATTRIBUTE_READ");
                    } else if (value instanceof SnapshotAttributeWriteValue) {
                        text = Messages.getMessage("DIALOGS_SPECTRUM_ATTRIBUTE_WRITE");
                    } else if (value instanceof SnapshotAttributeDeltaValue) {
                        text = Messages.getMessage("DIALOGS_SPECTRUM_ATTRIBUTE_DELTA");
                    } else {
                        text = value.toFormatedString();
                    }
                    break;
                case SnapshotAttributeValue.IMAGE_DATA_FORMAT:
                    if (value instanceof SnapshotAttributeWriteValue) {
                        text = Messages.getMessage("DIALOGS_IMAGE_ATTRIBUTE_VIEW");
                    } else {
                        text = Messages.getMessage("DIALOGS_IMAGE_ATTRIBUTE_VIEW");
                    }
                    break;
                default:
                    text = "";
                    break;
            }
        }
        return text;
    }

}
