package fr.soleil.bensikin.components.snapshot.detail;

import org.jdesktop.swingx.table.TableColumnExt;

import fr.soleil.bensikin.data.snapshot.Snapshot;
import fr.soleil.bensikin.models.SnapshotDetailPrintTableModel;
import fr.soleil.bensikin.models.SnapshotDetailTableModel.SnapshotColumns;

public class SnapshotDetailPrintTable extends SnapshotDetailTable {

    private static final long serialVersionUID = 4891176588729880824L;

    private boolean showDelta;

    public SnapshotDetailPrintTable(Snapshot snapshot) {
        super(snapshot);
    }

    /**
     * @return the showDelta
     */
    public boolean isShowDelta() {
	return showDelta;
    }

    /**
     * @param showDelta
     *            the showDelta to set
     */
    public void setShowDelta(boolean showDelta) {
	this.showDelta = showDelta;

	TableColumnExt columnExt = getColumnExt(SnapshotColumns.DELTA);
	columnExt.setVisible(showDelta);
    }

    @Override
    protected void initializeModel() {
        SnapshotDetailPrintTableModel model = new SnapshotDetailPrintTableModel(
                snapshot);
        this.setModel(model);
    }

    @Override
    protected void initializeTableColumns() {
        if (getColumnCount() > 0) {
	    TableColumnExt nameColumn = this.getColumnExt(SnapshotColumns.NAME);
	    nameColumn.setMinWidth(200);
	    nameColumn.setPreferredWidth(200);
        }

	setShowDelta(false);
	setShowCanSet(false);
	setShowAbsValues(false);
    }

    @Override
    protected void initializeEditor() {
        // No editor;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

}
