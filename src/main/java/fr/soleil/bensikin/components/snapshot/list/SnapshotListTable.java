//+======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/components/snapshot/list/SnapshotListTable.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  SnapshotListTable.
//						(Claisse Laurent) - 16 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.6 $
//
// $Log: SnapshotListTable.java,v $
// Revision 1.6  2006/01/10 13:28:15  ounsy
// minor changes
//
// Revision 1.5  2005/11/29 18:25:27  chinkumo
// no message
//
// Revision 1.1.1.2  2005/08/22 11:58:35  chinkumo
// First commit
//
//
// copyleft :		Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.components.snapshot.list;

import javax.swing.JTable;
import javax.swing.table.JTableHeader;

import fr.soleil.bensikin.actions.listeners.SnapshotListTableListener;
import fr.soleil.bensikin.actions.listeners.SnapshotTableHeaderListener;
import fr.soleil.bensikin.components.renderers.BensikinTableHeaderRenderer;
import fr.soleil.bensikin.models.SnapshotListTableModel;

/**
 * A singleton class containing the current list of snapshots. The table's cells
 * are not editable. A SnapshotTableListener is added that listens to line
 * selection events, and a SnapshotTableHeaderListener is added that listens to
 * column double-clicks to sort them.
 * 
 * @author CLAISSE
 */
public class SnapshotListTable extends JTable {

    private static final long serialVersionUID = 3490058261536305013L;

    private static SnapshotListTable snapshotListTableInstance = null;

    /**
     * Instantiates itself if necessary, returns the instance.
     * 
     * @return The instance
     */
    public static SnapshotListTable getInstance() {
        if (snapshotListTableInstance == null) {
            snapshotListTableInstance = new SnapshotListTable();
        }

        return snapshotListTableInstance;
    }

    /**
     * Default constructor.
     * <UL>
     * <LI>Instantiates its table model
     * <LI>Adds a selection listener on its table body
     * (SnapshotListTableListener)
     * <LI>Adds a sort request listener on its table header
     * (SnapshotTableHeaderListener)
     * <LI>Sets its columns sizes and row height
     * <LI>Disables the columns auto resize mode
     * </UL>
     */
    private SnapshotListTable() {
        super();

        this.addMouseListener(new SnapshotListTableListener());
        JTableHeader header = this.getTableHeader();
        header.addMouseListener(new SnapshotTableHeaderListener());
        header.setDefaultRenderer(new BensikinTableHeaderRenderer());

        setModel(SnapshotListTableModel.getInstance());

        this.getColumn("ID").setPreferredWidth(20);
        this.getColumn("Time").setPreferredWidth(40);
        this.getColumn("Comment").setPreferredWidth(160);

        this.setRowHeight(20);

        // this.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        this.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    @Override
    public void repaint() {
        try {
            if (this.getColumn("Time") != null) {
                this.getColumn("Time").setPreferredWidth(160);
            }
            if (this.getColumn("ID") != null) {
                this.getColumn("ID").setPreferredWidth(40);
            }
        } catch (IllegalArgumentException iae) {
            // the columns don't exist yet : do nothing
        }
        super.repaint();
    }
}
