package fr.soleil.bensikin.components.snapshot;

import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.bensikin.actions.BensikinAction;
import fr.soleil.bensikin.components.renderers.SnapshotDetailRenderer;
import fr.soleil.bensikin.components.snapshot.detail.SnapshotDetailTable;
import fr.soleil.bensikin.containers.snapshot.SnapshotDetailTabbedPane;
import fr.soleil.bensikin.data.snapshot.Snapshot;
import fr.soleil.bensikin.data.snapshot.SnapshotAttribute;
import fr.soleil.bensikin.models.SnapshotDetailTableModel;
import fr.soleil.bensikin.tools.Messages;

/**
 * Match the selected Snapshot detail panel
 * 
 * @author guest
 * 
 */
public class MatchSnapshotBox extends BensikinAction {

    private static final long serialVersionUID = -6173083238614847496L;

    private JTextField regexField;
    private static MatchSnapshotBox instance;

    private JRadioButton filterRButton;
    private JRadioButton highlightRButton;
    private ButtonGroup buttonGroup;
    private JButton submitButton;
    List<Integer> attributesNbRowList = new ArrayList<Integer>();

    private Snapshot snapshot;
    private SnapshotDetailTable snapshotDetailTable;

    public static MatchSnapshotBox getInstance() {
        return instance;
    }

    public List<Integer> getAttributesNbRowList() {
        return attributesNbRowList;
    }

    public void setAttributesNbRowList(List<Integer> attributesNbRowList) {
        this.attributesNbRowList = attributesNbRowList;
    }

    public Box createMatchSnapshot(SnapshotDetailTable snapshotDetailTable) {

        Font font = new Font("Arial", Font.PLAIN, 11);

        this.snapshotDetailTable = snapshotDetailTable;
        regexField = new JTextField(10);
        regexField.setToolTipText(Messages.getMessage("SNAPSHOP_DETAIL_MATCH_TOOLTIP"));
        regexField.addActionListener(this);

        buttonGroup = new ButtonGroup();

        filterRButton = new JRadioButton("Filter");
        buttonGroup.add(filterRButton);
        GUIUtilities.setObjectBackground(filterRButton, GUIUtilities.SNAPSHOT_COLOR);

        highlightRButton = new JRadioButton("Hightlight");
        highlightRButton.setSelected(true);
        buttonGroup.add(highlightRButton);
        GUIUtilities.setObjectBackground(highlightRButton, GUIUtilities.SNAPSHOT_COLOR);

        submitButton = new JButton("MATCH");
        submitButton.setMargin(new Insets(2, 2, 2, 2));
        submitButton.setToolTipText(Messages.getMessage("SNAPSHOP_DETAIL_MATCH_TOOLTIP"));
        submitButton.setFocusPainted(false);
        submitButton.setFocusable(false);
        submitButton.setFont(font);
        submitButton.addActionListener(this);
        GUIUtilities.setObjectBackground(submitButton, GUIUtilities.SNAPSHOT_COLOR);

        Box box = new Box(BoxLayout.X_AXIS);
        box.add(regexField);
        box.add(Box.createHorizontalStrut(7));
        box.add(highlightRButton);
        box.add(Box.createHorizontalStrut(5));
        box.add(filterRButton);
        box.add(Box.createHorizontalStrut(7));
        box.add(submitButton);

        return box;

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        // Attribute list to set the attributes list
        List<String> attributesList = new ArrayList<String>();
        List<Integer> attributesNbRowList = new ArrayList<Integer>();

        // Pattern is the matching text field
        String pattern = regexField.getText();

        // Get all the snapshot of the opened tabbed
        snapshot = SnapshotDetailTabbedPane.getInstance().getSelectedSnapshotDetailTabbedPaneContent().getSnapshot();

        SnapshotAttribute snapshotAttribute[] = snapshot.getSnapshotAttributes().getSnapshotAttributes();
        int rowCount = snapshotAttribute.length;

        for (int i = 0; i < rowCount; i++) {
            String attributeName = snapshotAttribute[i].getAttributeCompleteName();
            attributesList.add(attributeName);
        }

        // Use lower case to avoid case sensitivity (TANGOARCH-493)
        pattern = pattern.replace("?", ".").toLowerCase();
        pattern = pattern.replace("*", ".*");

        // Match the pattern and attributeName get from attributesList
        SnapshotDetailRenderer defaultRenderer = (SnapshotDetailRenderer) snapshotDetailTable
                .getDefaultRenderer(Object.class);

        SnapshotDetailTableModel snapshotDetailModel = SnapshotDetailTableModel.getInstance(snapshot);

        for (int i = 0; i < rowCount; i++) {
            String attributeNameMatch = attributesList.get(i);
            // Use lower case to avoid case sensitivity (TANGOARCH-493)
            if (Pattern.matches(pattern, attributeNameMatch.toLowerCase())) {
                attributesNbRowList.add(i);
            }
        }
        if (highlightRButton.isSelected()) {
            snapshotDetailModel.setFilter(false);
            defaultRenderer.setHightlight(true);
            snapshotDetailModel.load(snapshot);
            defaultRenderer.setAttributesNumRowList(attributesNbRowList);
            snapshotDetailTable.repaint();
        } else if (filterRButton.isSelected()) {
            snapshotDetailModel.setFilter(true);
            defaultRenderer.setHightlight(false);
            snapshotDetailModel.setAttributesNumRowList(attributesNbRowList);
            snapshotDetailModel.reloadMatchingValues(snapshot);
            snapshotDetailTable.repaint();
        }

    }
}
