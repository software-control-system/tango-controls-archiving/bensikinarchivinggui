// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/bensikin/bensikin/components/snapshot/detail/SnapshotDetailTable.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class SnapshotDetailTable.
// (Claisse Laurent) - 16 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.13 $
//
// $Log: SnapshotDetailTable.java,v $
// Revision 1.13 2007/08/28 07:39:13 ounsy
// bug with R-->W correction
//
// Revision 1.12 2007/08/23 12:58:26 ounsy
// toUserFriendlyString() available in SnapshotDetailTable
//
// Revision 1.11 2007/08/21 15:13:18 ounsy
// Print Snapshot as table or text (Mantis bug 3913)
//
// Revision 1.10 2007/08/21 08:44:39 ounsy
// Snapshot Detail : Transfer Read To Write (Mantis bug 5543)
//
// Revision 1.9 2007/06/29 09:18:58 ounsy
// devLong represented as Integer + SnapshotCompareTable sorting
//
// Revision 1.8 2006/04/13 12:21:41 ounsy
// added a sort on the snapshot detail table
//
// Revision 1.7 2006/02/15 09:17:24 ounsy
// spectrums rw management
//
// Revision 1.6 2005/12/14 16:18:03 ounsy
// added the selectAllOrNone method
//
// Revision 1.5 2005/11/29 18:25:27 chinkumo
// no message
//
// Revision 1.1.1.2 2005/08/22 11:58:35 chinkumo
// First commit
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================

package fr.soleil.bensikin.components.snapshot.detail;

import java.util.HashMap;

import javax.swing.DefaultRowSorter;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.ColumnFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.bensikin.actions.listeners.SnapshotDetailTableHeaderListener;
import fr.soleil.bensikin.actions.listeners.SnapshotDetailTableListener;
import fr.soleil.bensikin.components.editors.SnapshotDetailEditor;
import fr.soleil.bensikin.components.renderers.BensikinTableHeaderRenderer;
import fr.soleil.bensikin.components.renderers.SnapshotDetailRenderer;
import fr.soleil.bensikin.data.snapshot.Snapshot;
import fr.soleil.bensikin.data.snapshot.SnapshotAttributeValue;
import fr.soleil.bensikin.data.snapshot.SnapshotAttributeValue.SnapshotValueComparator;
import fr.soleil.bensikin.data.snapshot.SnapshotAttributeWriteValue;
import fr.soleil.bensikin.models.SnapshotDetailTableModel;
import fr.soleil.bensikin.models.SnapshotDetailTableModel.SnapshotColumns;
import fr.soleil.bensikin.options.Options;
import fr.soleil.bensikin.tools.Messages;

/**
 * A JTable used to view/edit the detail of a given snapshot.
 * 
 * @author CLAISSE
 */
public class SnapshotDetailTable extends JXTable {

    private static final long serialVersionUID = 3743671742277849037L;

    private static final boolean DEFAULT_SHOW_CAN_SET = true;
    private static final boolean DEFAULT_SHOW_ABS = false;

    protected Snapshot snapshot;

    private boolean showCanSet;
    private boolean showAbsValues;

    /**
     * The comparator used with columns having a {@link SnapshotAttributeValue}
     * object to handle
     */
    private final SnapshotValueComparator comparator;

    /**
     * Builds its model from the given snapshot, makes the 1st column the same
     * color as the header, and adds a cell renderer and editor to itself.
     * 
     * @param snapshot
     *            The snapshot we wish to view/edit
     */
    public SnapshotDetailTable(Snapshot snapshot) {
        super();
        // setSortable(false);// for the JXTable not to interact with the sort
        // mechanism already in place
        comparator = new SnapshotValueComparator();
        this.snapshot = snapshot;
        initialize();
    }

    protected void initialize() {
        // gives each column its identifier
        setColumnFactory(new ColumnFactory() {
            private final SnapshotColumns[] columns = SnapshotColumns.values();

            @Override
            public void configureTableColumn(TableModel model, TableColumnExt columnExt) {
                super.configureTableColumn(model, columnExt);
                // follow the enum order!
                columnExt.setIdentifier(columns[columnExt.getModelIndex()]);
            }
        });
        getTableHeader().setDefaultRenderer(new BensikinTableHeaderRenderer());
        initializeModel();
        initializeRenderer();
        initializeEditor();
        initializeTableColumns();
        initializeSortModel();
        this.setRowHeight(this.getRowHeight() + 2);
        this.addMouseListener(new SnapshotDetailTableListener());

        // Do not allow column reordering because it has a bad impact on data
        // sorting
        // getTableHeader().setReorderingAllowed(false);
    }

    protected void initializeSortModel() {
        setSortOrder(SnapshotColumns.NAME.ordinal(), SortOrder.ASCENDING);
        RowSorter<?> rowSorter = getRowSorter();
        if (rowSorter instanceof DefaultRowSorter) {
            for (int i = 0; i < getModel().getColumnCount(); ++i) {
                // String columnName = getModel().getColumnName(i);
                if ((i != SnapshotColumns.NAME.ordinal()) && (i != SnapshotColumns.CAN_SET.ordinal())) {
                    ((DefaultRowSorter<?, ?>) rowSorter).setComparator(i, comparator);
                }
            }
        }
    }

    protected void initializeTableColumns() {
        this.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

        TableColumnExt nameColumn = this.getColumnExt(SnapshotColumns.NAME);
        nameColumn.setMinWidth(200);
        nameColumn.setPreferredWidth(200);

        TableColumnExt canSetColumn = this.getColumnExt(SnapshotColumns.CAN_SET);
        JCheckBox checkBox = new JCheckBox();
        canSetColumn.setMinWidth(checkBox.getMinimumSize().width);

        setShowCanSet(DEFAULT_SHOW_CAN_SET);
        setShowAbsValues(DEFAULT_SHOW_ABS);
    }

    protected void initializeRenderer() {
        this.setDefaultRenderer(Object.class, new SnapshotDetailRenderer());
    }

    protected void initializeEditor() {
        this.setDefaultEditor(Object.class, new SnapshotDetailEditor(this));
    }

    protected void initializeModel() {
        SnapshotDetailTableModel model = SnapshotDetailTableModel.getInstance(snapshot);
        JTableHeader header = this.getTableHeader();
        header.addMouseListener(new SnapshotDetailTableHeaderListener(model));
        header.setDefaultRenderer(new BensikinTableHeaderRenderer());
        this.setModel(model);
    }

    public void updateSnapshotCanSetProperties(int row, boolean value) {
        int column = getModel().findColumn(SnapshotDetailTableModel.NAME_COLUMN);
        Object cellValue = getValueAt(row, column);
        if (cellValue instanceof String) {
            snapshot.updateSnapshotCanSetProperty((String) cellValue, value);
        }
    }

    /**
     * @return true if checkbox column is visible, false otherwise
     */
    public boolean isShowCanSet() {
        return showCanSet;
    }

    /**
     * Allow to show or hide checkbox column.
     * 
     * @param showCanSet
     *            true to show the column, false to hide it
     */
    public void setShowCanSet(boolean showCanSet) {
        this.showCanSet = showCanSet;

        TableColumnExt columnExt = getColumnExt(SnapshotColumns.CAN_SET);
        columnExt.setVisible(showCanSet);
    }

    /**
     * @return true if absolute columns are visible, false otherwise
     */
    public boolean isShowAbsValues() {
        return showAbsValues;
    }

    /**
     * Allow to show or hide absolute columns.
     * 
     * @param showAbsValues
     *            true to show the columns, false to hide it
     */
    public void setShowAbsValues(boolean showAbsValues) {
        this.showAbsValues = showAbsValues;

        getColumnExt(SnapshotColumns.WRITE_ABS).setVisible(showAbsValues);
        getColumnExt(SnapshotColumns.READ_ABS).setVisible(showAbsValues);
        getColumnExt(SnapshotColumns.DELTA_ABS).setVisible(showAbsValues);
    }

    @Override
    public void setModel(TableModel newModel) {
        if (newModel instanceof SnapshotDetailTableModel) {
            super.setModel(newModel);
        }
    }

    @Override
    public SnapshotDetailTableModel getModel() {
        return (SnapshotDetailTableModel) super.getModel();
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        /*
         * if (column != 1)//the write column is the only editable value {
         * return false; } SnapshotAttributeValue objectAt =
         * (SnapshotAttributeValue) this.getValueAt(row, column); return
         * !objectAt.isNotApplicable();
         */
        Object value = this.getValueAt(row, column);
        if (value instanceof SnapshotAttributeWriteValue) {
            if (((SnapshotAttributeWriteValue) value).getDataFormat() == SnapshotAttributeValue.SPECTRUM_DATA_FORMAT) {
                return false;
            }
            return !((SnapshotAttributeWriteValue) value).isNotApplicable();
        } else if (value instanceof Boolean) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean isColumnSelected(int column) {
        if (column == 0) {
            return false;
        }
        return super.isColumnSelected(column);
    }

    /**
     * @param b
     */
    public void selectAllOrNone(boolean b) {
        SnapshotDetailTableModel model = this.getModel();
        model.selectAllOrNone(b);
    }

    public void transferSelectedReadToWrite() {
        SnapshotDetailTableModel model = this.getModel();
        model.transferSelectedReadToWrite();
        repaint();
    }

    /**
     *
     */
    public void applyChange() {
        SnapshotDetailTableModel model = this.getModel();
        SnapshotDetailEditor editor = (SnapshotDetailEditor) this.getCellEditor();
        if (this.isEditing()) {
            model.setValueAt(editor.getCellEditorValue(), this.getEditingRow(), this.getEditingColumn());
        }
    }

    /**
     * @return the snapshot
     */
    public Snapshot getSnapshot() {
        return snapshot;
    }

    /**
     * Called when printing snapshot as text
     * 
     * @return
     */
    // TODO refactor to use columns id
    // TODO this is specific to the print table!
    public String toUserFriendlyString() {
        StringBuilder buffer = new StringBuilder();
        int rowCount = getRowCount();
        int columnCount = 3;// don't export canset and abs columns
        for (int column = 0; column < columnCount; column++) {
            if (column > 0) {
                buffer.append(Options.getInstance().getSnapshotOptions().getCSVSeparator());
            }
            // getColumnName refers to visible columns, ie {name, write, read}!!
            buffer.append(getColumnName(column));
        }
        buffer.append(GUIUtilities.CRLF);

        for (int row = 0; row < rowCount; row++) {
            for (int column = 0; column < columnCount; column++) {
                if (column > 0) {
                    buffer.append(Options.getInstance().getSnapshotOptions().getCSVSeparator());
                }
                Object value = getValueAt(row, column);
                if (value instanceof SnapshotAttributeValue) {
                    ((SnapshotAttributeValue) value).appendToString(buffer);
                } else {
                    buffer.append(value);
                }
            }
            buffer.append(GUIUtilities.CRLF);
        }
        // TODO à revoir, ça duplique l'affichage des attributs
        // if (snapshot != null) {
        // // get columns without considering visible columns
        // //in print mode, delta is not visible and should not appear here
        // buffer.append(snapshot.toUserFriendlyString());
        // }
        return buffer.toString();
    }

    public String snapshotToCsvString() {
        StringBuilder outputBuffer = new StringBuilder();
        if (snapshot != null) {
            outputBuffer = snapshot.snapshotInfoToUserFriendlyStringBuilder(outputBuffer);
            if (outputBuffer.length() > 0) {
                outputBuffer.append(GUIUtilities.CRLF).append(GUIUtilities.CRLF);
            }
            outputBuffer = snapshotAttributesToStringBuilder(outputBuffer);
        }
        return outputBuffer.toString();
    }

    private StringBuilder snapshotAttributesToStringBuilder(StringBuilder buffer) {
        if (buffer == null) {
            buffer = new StringBuilder();
        }
        HashMap<Integer, Boolean> columnMap = computeExportableColumnsMap();
        int columnCount = 0;
        for (int col = 0; col < getColumnCount(); col++) {
            if (columnMap.get(col)) {
                // only add separator before an item
                if (columnCount > 0) {
                    buffer.append(Options.getInstance().getSnapshotOptions().getCSVSeparator());
                }
                columnCount++;
                buffer.append(getColumnName(col));
            }
        }
        for (int row = 0; row < getRowCount(); row++) {
            columnCount = 0;
            // ... And add carriage return
            buffer.append(GUIUtilities.CRLF);
            for (int col = 0; col < getColumnCount(); col++) {
                if (columnMap.get(col)) {
                    if (columnCount > 0) {
                        buffer.append(Options.getInstance().getSnapshotOptions().getCSVSeparator());
                    }
                    columnCount++;
                    Object value = getValueAt(row, col);
                    if (value instanceof SnapshotAttributeValue) {
                        buffer.append(SnapshotDetailRenderer.getTextFor((SnapshotAttributeValue) value));
                    } else {
                        buffer.append(value);
                    }
                }
            }
        }
        columnMap.clear();
        return buffer;
    }

    private HashMap<Integer, Boolean> computeExportableColumnsMap() {
        HashMap<Integer, Boolean> columnMap = new HashMap<Integer, Boolean>();
        for (int i = 0; i < getColumnCount(); i++) {
            if (Messages.getMessage("SNAPSHOT_DETAIL_COLUMNS_NAME").equals(getColumnName(i))) {
                columnMap.put(i, true);
            } else if (Messages.getMessage("SNAPSHOT_DETAIL_COLUMNS_R").equals(getColumnName(i))) {
                columnMap.put(i, Options.getInstance().getSnapshotOptions().isCSVRead());
            } else if (Messages.getMessage("SNAPSHOT_DETAIL_COLUMNS_W").equals(getColumnName(i))) {
                columnMap.put(i, Options.getInstance().getSnapshotOptions().isCSVWrite());
            } else if (Messages.getMessage("SNAPSHOT_DETAIL_COLUMNS_DELTA").equals(getColumnName(i))) {
                columnMap.put(i, Options.getInstance().getSnapshotOptions().isCSVDelta());
            } else {
                columnMap.put(i, false);
            }
        }
        return columnMap;
    }
}
