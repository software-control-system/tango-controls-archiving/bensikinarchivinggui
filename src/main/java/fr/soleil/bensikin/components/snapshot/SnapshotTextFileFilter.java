package fr.soleil.bensikin.components.snapshot;

import fr.soleil.bensikin.components.DataFileFilter;
import fr.soleil.bensikin.tools.Messages;

public class SnapshotTextFileFilter extends DataFileFilter {

    public static final String FILE_EXTENSION = "txt";

    /**
     * Builds with FILE_EXTENSION extension and a description found in
     * resources.
     */
    public SnapshotTextFileFilter() {
	super(FILE_EXTENSION);
	description = Messages.getMessage("FILE_CHOOSER_SNAPSHOT_TEXT");
    }

}
