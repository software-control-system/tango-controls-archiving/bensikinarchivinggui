//+======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/components/BensikinBooleanViewer.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  BensikinBooleanViewer.
//						(GIRARDOT Raphael) - nov. 2005
//
// $Author: ounsy $
//
// $Revision: 1.3 $
//
// $Log: BensikinBooleanViewer.java,v $
// Revision 1.3  2006/04/13 12:37:33  ounsy
// new spectrum types support
//
// Revision 1.2  2006/03/15 15:07:00  ounsy
// adapted to receive no data
//
// Revision 1.1  2005/11/29 18:25:08  chinkumo
// no message
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.components;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;

import fr.soleil.bensikin.Bensikin;
import fr.soleil.bensikin.tools.Messages;

/**
 * A JCheckbox that looks like a couple of LEDs
 * 
 * @author SOLEIL
 */
public class BensikinBooleanViewer extends JCheckBox {

    private static final long serialVersionUID = 7321765898890349381L;

    private static final ImageIcon TRUE_ICON = new ImageIcon(Bensikin.class.getResource("icons/true.gif"));
    private static final ImageIcon FALSE_ICON = new ImageIcon(Bensikin.class.getResource("icons/false.gif"));
    private static final ImageIcon OFF_ICON = new ImageIcon(Bensikin.class.getResource("icons/double_off.gif"));

    /**
     * Constructor
     */
    public BensikinBooleanViewer() {
        super();
        setIcon(FALSE_ICON);
        setPressedIcon(OFF_ICON);
        setSelectedIcon(TRUE_ICON);
    }

    /**
     * Constructor
     * 
     * @param value
     *            a Boolean to select/unselect the checkbox
     */
    public BensikinBooleanViewer(Boolean value) {
        this();
        setSelected(value);
    }

    public void setSelected(Boolean value) {
        if (value == null) {
            setText(Messages.getMessage("SNAPSHOT_DETAIL_NO_DATA"));
            setSelected(false);
            setIcon(OFF_ICON);
        } else {
            setText(value.toString());
            setIcon(FALSE_ICON);
            setSelected(value.booleanValue());
        }
        repaint();
    }

}
