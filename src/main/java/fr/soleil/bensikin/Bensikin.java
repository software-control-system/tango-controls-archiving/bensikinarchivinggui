// +======================================================================
// $Source: $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class Bensikin.
// (Claisse Laurent) - 10 juin 2005
//
// $Author: ounsy $
//
// $Revision: $
//
// $Log: Bensikin.java,v $
// Revision 1.12 2007/03/14 15:49:11 ounsy
// the user/password is no longer hard-coded in the APIs, Bensikin takes two
// more parameters
//
// Revision 1.11 2007/02/01 13:59:15 pierrejoseph
// Connection profiles are now viewer, expert
//
// Revision 1.10 2006/12/12 13:17:49 ounsy
// minor changees
//
// Revision 1.9 2006/04/26 14:41:45 ounsy
// splash with soleil logo
//
// Revision 1.8 2006/04/26 12:38:01 ounsy
// splash added
//
// Revision 1.7 2006/04/10 08:48:38 ounsy
// added the launch parameter that describes user rights
//
// Revision 1.6 2005/12/14 15:56:22 ounsy
// added session management
//
// Revision 1.5 2005/11/29 18:25:08 chinkumo
// no message
//
// Revision 1.1.1.2 2005/08/22 11:58:32 chinkumo
// First commit
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.bensikin;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.io.File;
import java.util.Locale;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.gui.tools.AccountDelegate;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.snap.api.manager.SnapManagerApi;
import fr.soleil.archiving.snap.api.tools.SnapshotingException;
import fr.soleil.bensikin.containers.BensikinFrame;
import fr.soleil.bensikin.lifecycle.LifeCycleManager;
import fr.soleil.bensikin.lifecycle.LifeCycleManagerFactory;
import fr.soleil.bensikin.options.Options;
import fr.soleil.bensikin.options.manager.IOptionsManager;
import fr.soleil.bensikin.options.manager.OptionsManagerFactory;
import fr.soleil.bensikin.tools.Messages;
import fr.soleil.comete.swing.image.ImageJManager;
import fr.soleil.lib.project.application.user.manager.AccountManager;
import fr.soleil.lib.project.swing.Splash;

/**
 * The Main class of Bensikin.
 * 
 * @author CLAISSE
 */
public class Bensikin {

    private final static Logger LOGGER = LoggerFactory.getLogger(Bensikin.class);
    public static final String DEFAULT_LOG_ID = ImageJManager.generateApplicationId(Bensikin.class.getName());

    private static String pathToResources;
    private static boolean hires;
    private static final ImageIcon APPLI_START = new ImageIcon(Bensikin.class.getResource("icons/splash-11986-2.png"));

    private static Splash splash;

    private static int userRights;
    private static final String USER_PROFILE_VIEWER = "Viewer";
    private static final String USER_PROFILE_EXPERT = "Expert";
    public static final int USER_RIGHTS_RESTRICTED = 1;
    public static final int USER_RIGHTS_ALL = 2;

    public static final String USER_PARAMETER_KEY = "USER";
    public static final String PASSWORD_PARAMETER_KEY = "PASSWORD";

    private static AccountManager accountManager;

    private static String optionsPath;

    /**
     * Creates the main frame and the LifeCycleManager, launches the application
     * 
     * @param password
     * @param user
     */
    private static void createAndShowGUI() {
        try {
            // String title = "Bensikin v" + Messages.getMessage("ABOUT_RELEASE") +
            // " (" + Messages.getMessage("ABOUT_RELEASE_DATE") + ")";
            splash.progress(1);

            splash.setMessage("Building environment...");
            // LifeCycleManagerFactory.setUser(user);
            // LifeCycleManagerFactory.setPassword(password);
            final LifeCycleManager lifeCycleManager = LifeCycleManagerFactory.getCurrentImpl();
            splash.progress(2);
            splash.setMessage("Setting up environment...");

            lifeCycleManager.applicationWillStart(splash);

            // Create and set up the window.
            splash.progress(16);
            splash.setMessage("Building frame...");
            final JFrame frame = BensikinFrame.getInstance(lifeCycleManager);
            frame.pack();
            // final GraphicsEnvironment env =
            // GraphicsEnvironment.getLocalGraphicsEnvironment();
            // frame.setMaximizedBounds(env.getMaximumWindowBounds());
            frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);
            frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

            // frame.setUndecorated(false);
            splash.progress(17);
            splash.setMessage("Setting up frame...");

            // Display the window.
            splash.progress(18);
            splash.setMessage("Preparing to display application...");

            splash.setVisible(false);
            splash.dispose();
            // frame.setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());

            frame.setVisible(true);
            LOGGER.debug(Messages.getLogMessage("APPLICATION_STARTED_OK"));
        } catch (Exception e) {
            Bensikin.treatError(e, splash);
        }
    }

    /**
     * Returns the working directory.
     * 
     * @return Returns the pathToResources.
     */
    public static String getPathToResources() {
        return pathToResources;
    }

    public static String getOptionPath() {
        return optionsPath;
    }

    public static void treatError(Exception e, Splash splash) {
        treatError(e, null, splash);
    }

    public static void treatError(Exception e, String message, Splash splash) {
        String msg = "Bensikin encountered an undesired error and will close:\n" + e.getMessage();
        if (!GraphicsEnvironment.isHeadless()) {
            JOptionPane.showMessageDialog(splash, msg, "Bensikin Error!", JOptionPane.ERROR_MESSAGE);
        }
        if (message == null) {
            message = msg;
        }
        LOGGER.error(message, e);
        System.exit(1);
    }

    public static void loadOptions() {
        // load options
        try {
            optionsPath = Bensikin.getPathToResources() + "/options";
            final File path = new File(optionsPath);
            if (!path.exists()) {
                final boolean created = path.mkdirs();
                if (!created) {
                    LOGGER.error("Could not create file {}", path);
                }
            }
            optionsPath += "/options.xml";

            final IOptionsManager optionsManager = OptionsManagerFactory.getCurrentImpl();
            final Options options = optionsManager.loadOptions(optionsPath);
            Options.setOptionsInstance(options);
            options.push();

            final String msg = Messages.getLogMessage("APPLICATION_WILL_START_LOAD_OPTIONS_OK");
            LOGGER.debug(msg);
        } catch (final Exception e) {
            final String msg = Messages.getLogMessage("APPLICATION_WILL_START_LOAD_OPTIONS_KO");
            treatError(e, msg, splash);
        }
    }

    /**
     * Sets the working directory.
     * 
     * @param pathToResources
     */
    public static void setPathToResources(final String pathToResources) {
        Bensikin.pathToResources = pathToResources;
    }

    /**
     * Returns true if Bensikin is running in hires.
     * 
     * @return Returns the hires.
     */
    public static boolean isHires() {
        return hires;
    }

    /**
     * Detects the screen resolution and sets the hires attribute consequently
     */
    private static void initResolutionMode() {
        final Toolkit toolkit = Toolkit.getDefaultToolkit();
        final Dimension dim = toolkit.getScreenSize();
//        System.out.println("Resolution-------------------------------------------------------------");
//        System.out.println("Resolution: " + dim.width + "*" + dim.height);

        hires = !(dim.width <= 1024);
    }

    protected static boolean launchAccountSelection() {
        boolean canceled;
        AccountDelegate delegate = new AccountDelegate();
        canceled = delegate.launchAccountSelector(Bensikin.class.getSimpleName(), accountManager, splash, LOGGER);
        if (!canceled) {
            pathToResources = delegate.getSelectedAccountPath();
        }
        return canceled;
    }

    public static boolean isRestricted() {
        return userRights != Bensikin.USER_RIGHTS_ALL;
    }

    public static AccountManager getAccountManager() {
        return accountManager;
    }

    public static void setAccountManager(final AccountManager accountManager) {
        Bensikin.accountManager = accountManager;
    }

    /**
     * Launches Bensikin.
     * 
     * @param args
     *            Must have 1 parameter, the working directory, which will be
     *            created if it doesn't exist
     */
    public static void main(final String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        if (args == null || args.length < 1 || args.length > 4) {
            System.out.println("Incorrect arguments. Correct syntax: java Bensikin userdb passdb userRights");
            System.exit(1);
        }
        pathToResources = System.getProperty(GUIUtilities.WORKING_DIR);
        try {
            final File path = new File(pathToResources);
            if (!path.isDirectory()) {
                path.mkdirs();
                // throw new Exception("Not a directory");
            }
            if (!path.canRead()) {
                throw new ArchivingException("Invalid path");
            }
            if (!path.canWrite()) {
                throw new ArchivingException("The path is read only");
            }
        } catch (final Exception e) {
            System.out.println("Incorrect arguments. The parameter " + pathToResources + " is not a valid directory");
            e.printStackTrace();
            System.exit(1);
        }

        final String user = args[0];
        final String password = args[1];

        final String userRights_s = args.length == 3 ? args[2] : null;
        setUserRights(userRights_s);

        setAccountManager(new AccountManager(pathToResources, "Bensikin", null, false));

        final Locale currentLocale = new Locale("en", "US");
        try {
            Messages.initResourceBundle(currentLocale);
        } catch (final Exception e) {
            e.printStackTrace();
            // we refuse to launch the application without at least those
            // resources
            System.exit(1);
        }
        if (launchAccountSelection()) {
            System.exit(0);
        } else {
            System.out.println("Bensikin start");
            initResolutionMode();

            // Show splash before connections initialization (TANGOARCH-457)
            final String title = new StringBuilder(Messages.getAppMessage("project.name")).append(" v")
                    .append(Messages.getAppMessage("project.version")).append(" (")
                    .append(Messages.getAppMessage("build.date")).append(")").toString();
            splash = new Splash(APPLI_START, Color.BLACK);
            splash.setTitle(title);
            splash.setCopyright("Synchrotron SOLEIL");
            splash.setMaxProgress(18);
            splash.progress(0);
            splash.setMessage("Initializing connections...");

            javax.swing.SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    try {
                        SnapManagerApi.initFullSnapConnection("", "", "", user, password, "");
                    } catch (final SnapshotingException e) {
//                        LOGGER.error("", e);
                        treatError(e, splash);
                    }
                    createAndShowGUI();
                }
            });
        }
    }

    public static void setUserRights(final String userRights_s) {
        try {
            if (userRights_s == null || userRights_s.equalsIgnoreCase(USER_PROFILE_VIEWER)) {
                userRights = USER_RIGHTS_RESTRICTED;
            } else if (userRights_s.equalsIgnoreCase(USER_PROFILE_EXPERT)) {
                userRights = USER_RIGHTS_ALL;
            } else {
                throw new Exception("The userRights parameter has to be either " + USER_PROFILE_VIEWER + " or "
                        + USER_PROFILE_EXPERT);
            }
        } catch (final Exception e) {
            System.out.println("Incorrect arguments. The userRights parameter has to be either " + USER_PROFILE_VIEWER
                    + " or " + USER_PROFILE_EXPERT);
            e.printStackTrace();
            System.exit(1);
        }
    }

}
