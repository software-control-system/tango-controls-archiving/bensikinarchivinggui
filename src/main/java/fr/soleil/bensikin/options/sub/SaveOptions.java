//+======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/options/sub/SaveOptions.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  SaveOptions.
//						(Claisse Laurent) - 5 juil. 2005
//
// $Author: chinkumo $
//
// $Revision: 1.4 $
//
// $Log: SaveOptions.java,v $
// Revision 1.4  2005/11/29 18:25:13  chinkumo
// no message
//
// Revision 1.1.1.2  2005/08/22 11:58:41  chinkumo
// First commit
//
//
// copyleft :		Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.options.sub;

import javax.swing.ButtonModel;

import fr.soleil.bensikin.containers.sub.dialogs.options.OptionsSaveOptionsTab;
import fr.soleil.bensikin.lifecycle.LifeCycleManager;
import fr.soleil.bensikin.lifecycle.LifeCycleManagerFactory;
import fr.soleil.bensikin.options.PushPullOptionBook;
import fr.soleil.bensikin.options.ReadWriteOptionBook;

/**
 * The save options of the application. Contains: -whether the application saves
 * on shutdown/loads on startup
 * 
 * @author CLAISSE
 */
public class SaveOptions extends ReadWriteOptionBook implements PushPullOptionBook {
    /**
     * The history saving property name
     */
    public static final String HISTORY = "HISTORY";

    /**
     * The code for the history saving property value of "Do save history"
     */
    public static final int HISTORY_YES = 1;

    /**
     * The code for the history saving property value of "Don't save history"
     */
    public static final int HISTORY_NO = 0;

    /**
     * The XML tag name used in saving/loading
     */
    public static final String XML_TAG = "save";

    /**
     * The load properties names
     */
    public static final String CONTEXT_OPENED_NAME = "CONTEXT_OPENED";
    public static final String CONTEXT_SELECTED_NAME = "CONTEXT_SELECTED";
    public static final String SNAPSHOT_OPENED_NAME = "SNAPSHOT_OPENED";
    public static final String SNAPSHOT_SELECTED_NAME = "SNAPSHOT_SELECTED";

    /**
     * Code for history properties to be load at application launch
     */
    public static final int CONTEXT_OPENED = 0;
    public static final int CONTEXT_SELECTED = 1;
    public static final int SNAPSHOT_OPENED = 2;
    public static final int SNAPSHOT_SELECTED = 3;

    public static final int LOAD_ENTITY_YES = 1;
    public static final int LOAD_ENTITY_NO = 0;

    /**
     * Default constructor
     */
    public SaveOptions() {
	super(XML_TAG);
    }

    /*
     * (non-Javadoc)
     * 
     * @see bensikin.bensikin.options.PushPullOptionBook#fillFromOptionsDialog()
     */
    @Override
    public void fillFromOptionsDialog() {
	OptionsSaveOptionsTab saveTab = OptionsSaveOptionsTab.getInstance();
	ButtonModel selectedModel = saveTab.getButtonHistoryGroup().getSelection();
	String selectedActionCommand = selectedModel.getActionCommand();

	this.content.put(HISTORY, selectedActionCommand);

	boolean[] loadConfiguration = saveTab.getLoadPropertiesValues();

	this.content.put(CONTEXT_OPENED_NAME, (loadConfiguration[CONTEXT_OPENED]) ? Integer.toString(LOAD_ENTITY_YES)
		: Integer.toString(LOAD_ENTITY_NO));
	this.content.put(
		CONTEXT_SELECTED_NAME,
		(loadConfiguration[CONTEXT_SELECTED]) ? Integer.toString(LOAD_ENTITY_YES) : Integer
			.toString(LOAD_ENTITY_NO));
	this.content.put(SNAPSHOT_OPENED_NAME, (loadConfiguration[SNAPSHOT_OPENED]) ? Integer.toString(LOAD_ENTITY_YES)
		: Integer.toString(LOAD_ENTITY_NO));
	this.content.put(
		SNAPSHOT_SELECTED_NAME,
		(loadConfiguration[SNAPSHOT_SELECTED]) ? Integer.toString(LOAD_ENTITY_YES) : Integer
			.toString(LOAD_ENTITY_NO));
    }

    /*
     * (non-Javadoc)
     * 
     * @see bensikin.bensikin.options.PushPullOptionBook#push()
     */
    @Override
    public void push() {
	LifeCycleManager lifeCycleManager = LifeCycleManagerFactory.getCurrentImpl();

	// History save option
	String val_s = this.content.get(HISTORY);
	if (val_s != null) {
	    int val = Integer.parseInt(val_s);

	    switch (val) {
	    case HISTORY_YES:
		lifeCycleManager.setHasHistorySave(true);
		break;

	    case HISTORY_NO:
		lifeCycleManager.setHasHistorySave(false);
		break;
	    }

	    OptionsSaveOptionsTab saveTab = OptionsSaveOptionsTab.getInstance();
	    saveTab.selectHasSaveButton(val);
	}

	// load properties
	if (lifeCycleManager.hasHistorySave()) {
	    boolean[] loadProperties = new boolean[4];

	    loadProperties[CONTEXT_OPENED] = getLoadPropertyFromContent(CONTEXT_OPENED_NAME);
	    loadProperties[CONTEXT_SELECTED] = getLoadPropertyFromContent(CONTEXT_SELECTED_NAME);
	    loadProperties[SNAPSHOT_OPENED] = getLoadPropertyFromContent(SNAPSHOT_OPENED_NAME);
	    loadProperties[SNAPSHOT_SELECTED] = getLoadPropertyFromContent(SNAPSHOT_SELECTED_NAME);

	    lifeCycleManager.setLoadProperties(loadProperties);

	    OptionsSaveOptionsTab saveTab = OptionsSaveOptionsTab.getInstance();
	    saveTab.setLoadPropertiesCheckBox(loadProperties);
	}
    }

    /**
     * Search within the content to find the load property related to the donate
     * key
     * 
     * @param key
     *            the String to use for content search
     * @return the boolean property
     */
    private boolean getLoadPropertyFromContent(String key) {
	boolean result = false;
	String val_s = this.content.get(key);
	if (val_s != null) {
	    int val = Integer.parseInt(val_s);
	    result = (val == LOAD_ENTITY_YES);
	}
	return result;
    }
}
