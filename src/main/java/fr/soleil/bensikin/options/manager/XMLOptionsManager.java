// +======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/options/manager/XMLOptionsManager.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  XMLOptionsManager.
//						(Claisse Laurent) - 30 juin 2005
//
// $Author: pierrejoseph $
//
// $Revision: 1.7 $
//
// $Log: XMLOptionsManager.java,v $
// Revision 1.7  2007/08/30 14:01:51  pierrejoseph
// * java 1.5 programming
//
// Revision 1.6  2006/06/28 12:53:46  ounsy
// minor changes
//
// Revision 1.5  2005/12/14 16:46:46  ounsy
// added methods necessary for alternate attribute selection
//
// Revision 1.4  2005/11/29 18:25:08  chinkumo
// no message
//
// Revision 1.1.1.2  2005/08/22 11:58:41  chinkumo
// First commit
//
//
// copyleft :		Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.options.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.gui.manager.XMLDataManager;
import fr.soleil.bensikin.options.Options;
import fr.soleil.bensikin.options.sub.ContextOptions;
import fr.soleil.bensikin.options.sub.DisplayOptions;
import fr.soleil.bensikin.options.sub.PrintOptions;
import fr.soleil.bensikin.options.sub.SaveOptions;
import fr.soleil.bensikin.options.sub.SnapshotOptions;
import fr.soleil.bensikin.options.sub.WordlistOptions;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;

/**
 * An XML implementation.
 * 
 * @author CLAISSE
 */
public class XMLOptionsManager extends XMLDataManager<Options, Map<String, List<Map<String, String>>>> implements
        IOptionsManager {

    /*
     * (non-Javadoc)
     * 
     * @see
     * bensikin.bensikin.options.manager.IOptionsManager#saveOptions(bensikin
     * .bensikin.options.Options, java.lang.String)
     */
    @Override
    public void saveOptions(Options options, String optionsResourceLocation) throws ArchivingException {
        try {
            XMLUtils.save(options.toString(), optionsResourceLocation);
        } catch (Exception e) {
            throw ArchivingException.toArchivingException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * bensikin.bensikin.options.manager.IOptionsManager#loadOptions(java.lang
     * .String)
     */
    @Override
    public Options loadOptions(String optionsResourceLocation) throws ArchivingException {
        Options ret = Options.getInstance();

        Map<String, List<Map<String, String>>> optionsHt = loadDataIntoHash(optionsResourceLocation);

        // START BUILDING SUB OPTIONS
        List<Map<String, String>> options;

        options = optionsHt.get(DisplayOptions.XML_TAG);
        DisplayOptions displayOptions = new DisplayOptions();
        displayOptions.build(options);

        options = optionsHt.get(PrintOptions.XML_TAG);
        PrintOptions printOptions = new PrintOptions();
        printOptions.build(options);

        options = optionsHt.get(SaveOptions.XML_TAG);
        SaveOptions saveOptions = new SaveOptions();
        saveOptions.build(options);

        options = optionsHt.get(WordlistOptions.XML_TAG);
        WordlistOptions wordlistOptions = new WordlistOptions();
        wordlistOptions.build(options);

        options = optionsHt.get(SnapshotOptions.XML_TAG);
        SnapshotOptions snapshotOptions = new SnapshotOptions();
        snapshotOptions.build(options);

        options = optionsHt.get(ContextOptions.XML_TAG);
        ContextOptions contextOptions = new ContextOptions();
        contextOptions.build(options);

        // END BUILDING SUB OPTIONS

        // START BUILDING OPTIONS
        ret.setDisplayOptions(displayOptions);
        ret.setPrintOptions(printOptions);
        ret.setSaveOptions(saveOptions);
        ret.setWordlistOptions(wordlistOptions);
        ret.seSnapshotOptions(snapshotOptions);
        ret.setContextOptions(contextOptions);
        // END BUILDING OPTIONS
        return ret;
    }

    private List<Map<String, String>> loadOptionBook(Node currentBookNode) {
        List<Map<String, String>> book = new ArrayList<Map<String, String>>();

        if (currentBookNode.hasChildNodes()) {
            NodeList contextChapterNodes = currentBookNode.getChildNodes();
            // As many loops as there are options in the current options sub-block
            for (int i = 0; i < contextChapterNodes.getLength(); i++) {
                Node currentOptionChapterNode = contextChapterNodes.item(i);
                if (XMLUtils.isAFakeNode(currentOptionChapterNode)) {
                    continue;
                } else {
                    // String currentOptionChapterType =
                    // currentOptionChapterNode.getNodeName().trim();
                    Map<String, String> currentOptionChapter = null;
                    try {
                        currentOptionChapter = XMLUtils.loadAttributes(currentOptionChapterNode);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    book.add(currentOptionChapter);
                }
            }
        }
        return book;
    }

    @Override
    protected Options getDefaultData() {
        return Options.getInstance();
    }

    @Override
    protected Map<String, List<Map<String, String>>> loadDataIntoHashFromRoot(Node rootNode) throws ArchivingException {
        Map<String, List<Map<String, String>>> options = null;
        if (rootNode.hasChildNodes()) {
            NodeList bookNodes = rootNode.getChildNodes();
            options = new ConcurrentHashMap<String, List<Map<String, String>>>();
            // As many loops as there are options sub-blocks in the saved file
            // (which is normally five: display, print, logs, save, and wordlist)
            for (int i = 0; i < bookNodes.getLength(); i++) {
                Node currentBookNode = bookNodes.item(i);
                if (XMLUtils.isAFakeNode(currentBookNode)) {
                    continue;
                }
                String currentBookType = currentBookNode.getNodeName().trim();
                List<Map<String, String>> currentBook = loadOptionBook(currentBookNode);
                options.put(currentBookType, currentBook);
            }
        }
        return options;
    }
}
