// +======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/options/manager/DummyOptionsManager.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  DummyOptionsManager.
//						(Claisse Laurent) - 30 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.5 $
//
// $Log: DummyOptionsManager.java,v $
// Revision 1.5  2006/06/28 12:53:46  ounsy
// minor changes
//
// Revision 1.4  2005/11/29 18:25:08  chinkumo
// no message
//
// Revision 1.1.1.2  2005/08/22 11:58:41  chinkumo
// First commit
//
//
// copyleft :		Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.options.manager;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.bensikin.options.Options;

/**
 * Dummy implementation, does nothing
 * 
 * @author CLAISSE
 */
public class DummyOptionsManager implements IOptionsManager {

    /*
     * (non-Javadoc)
     * 
     * @see
     * bensikin.bensikin.options.manager.IOptionsManager#saveOptions(bensikin
     * .bensikin.options.Options, java.lang.String)
     */
    @Override
    public void saveOptions(Options options, String optionsResourceLocation) throws ArchivingException {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * bensikin.bensikin.options.manager.IOptionsManager#loadOptions(java.lang
     * .String)
     */
    @Override
    public Options loadOptions(String optionsResourceLocation) throws ArchivingException {
        // TODO Auto-generated method stub
        // Options ret = Options.getInstance();

        return null;
    }

}
