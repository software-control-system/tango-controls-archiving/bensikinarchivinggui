// +======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/history/context/ContextHistory.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  ContextHistory.
//						(Claisse Laurent) - 8 juil. 2005
//
// $Author: chinkumo $
//
// $Revision: 1.5 $
//
// $Log: ContextHistory.java,v $
// Revision 1.5  2005/11/29 18:25:08  chinkumo
// no message
//
// Revision 1.1.1.2  2005/08/22 11:58:39  chinkumo
// First commit
//
//
// copyleft :		Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.history.context;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.bensikin.data.context.Context;
import fr.soleil.bensikin.data.context.manager.ContextManagerFactory;
import fr.soleil.bensikin.data.context.manager.IContextManager;
import fr.soleil.bensikin.history.History;
import fr.soleil.bensikin.xml.BensikinXMLLine;

/**
 * A model for the historic of opened/selected contexts. Contains references to
 * both.
 * 
 * @author CLAISSE
 */
public class ContextHistory {
    private SelectedContextRef selectedContextRef;
    private OpenedContextRef[] openedContextRefs;

    /**
     * Build directly from a reference to the selected context, and references
     * to the opened contexts.
     * 
     * @param selectedContextRef A reference to the selected context
     * @param openedContextRefs Reference to the opened contexts
     */
    public ContextHistory(SelectedContextRef selectedContextRef, OpenedContextRef[] openedContextRefs) {
        this.selectedContextRef = selectedContextRef;
        this.openedContextRefs = openedContextRefs;
    }

    /**
     * Build indirectly from the selected context and the opened contexts. The
     * references of those are extracted and used.
     * 
     * @param selectedContext The selected context
     * @param openedContexts The opened contexts
     */
    public ContextHistory(Context selectedContext, Context[] openedContexts) {
        if (selectedContext != null) {
            if (selectedContext.getContextData() != null) {
                if (!selectedContext.isContextFile()) {
                    this.selectedContextRef = new SelectedContextRef(selectedContext);
                }
            }
        }

        if (openedContexts != null) {
            List<Context> filteredContextList = new ArrayList<Context>();
            for (Context context : openedContexts) {
                if ((context != null) && (context.getContextData() != null)) {
                    filteredContextList.add(context);
                }
            }
            Context[] filteredContexts = filteredContextList.toArray(new Context[filteredContextList.size()]);
            this.openedContextRefs = new OpenedContextRef[filteredContexts.length];
            for (int i = 0; i < filteredContexts.length; i++) {
                this.openedContextRefs[i] = new OpenedContextRef(filteredContexts[i]);
            }
        }

    }

    /**
     * Returns a XML representation of the context history
     * 
     * @return a XML representation of the context history
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        BensikinXMLLine startLine = new BensikinXMLLine(History.CONTEXTS_KEY, BensikinXMLLine.OPENING_TAG_CATEGORY);
        IContextManager manager = ContextManagerFactory.getCurrentImpl();
        if (manager != null) {
            String location = manager.getNonDefaultSaveLocation();
            if ((location != null) && (!location.trim().isEmpty())) {
                startLine.setAttribute(History.SAVE_LOCATION_KEY, location);
            }
        }
        builder.append(startLine);
        builder.append(GUIUtilities.CRLF);

        if (selectedContextRef != null) {
            builder.append(selectedContextRef);
            builder.append(GUIUtilities.CRLF);
        }

        if (openedContextRefs != null) {
            builder.append(new BensikinXMLLine(History.OPENED_CONTEXTS_KEY, BensikinXMLLine.OPENING_TAG_CATEGORY));
            builder.append(GUIUtilities.CRLF);

            for (int i = 0; i < openedContextRefs.length; i++) {
                builder.append(openedContextRefs[i]);
                builder.append(GUIUtilities.CRLF);
            }

            builder.append(new BensikinXMLLine(History.OPENED_CONTEXTS_KEY, BensikinXMLLine.CLOSING_TAG_CATEGORY));
            builder.append(GUIUtilities.CRLF);
        }

        builder.append(new BensikinXMLLine(History.CONTEXTS_KEY, BensikinXMLLine.CLOSING_TAG_CATEGORY));
        builder.append(GUIUtilities.CRLF);

        return builder.toString();
    }

    /**
     * @return Returns the openedContextRefs.
     */
    public OpenedContextRef[] getOpenedContextRefs() {
        return openedContextRefs;
    }

    /**
     * @param openedContextRefs The openedContextRefs to set.
     */
    public void setOpenedContextRefs(OpenedContextRef[] openedContextRefs) {
        this.openedContextRefs = openedContextRefs;
    }

    /**
     * @return Returns the selectedContextRef.
     */
    public SelectedContextRef getSelectedContextRef() {
        return selectedContextRef;
    }

    /**
     * @param selectedContextRef The selectedContextRef to set.
     */
    public void setSelectedContextRef(SelectedContextRef selectedContextRef) {
        this.selectedContextRef = selectedContextRef;
    }
}
