// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/bensikin/bensikin/history/manager/XMLHistoryManager.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class XMLHistoryManager.
// (Claisse Laurent) - 30 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.6 $
//
// $Log: XMLHistoryManager.java,v $
// Revision 1.6 2006/06/28 12:51:18 ounsy
// minor changes
//
// Revision 1.5 2005/11/29 18:25:08 chinkumo
// no message
//
// Revision 1.1.1.2 2005/08/22 11:58:39 chinkumo
// First commit
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.bensikin.history.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.gui.manager.XMLDataManager;
import fr.soleil.bensikin.data.context.manager.ContextManagerFactory;
import fr.soleil.bensikin.data.context.manager.IContextManager;
import fr.soleil.bensikin.data.snapshot.manager.ISnapshotManager;
import fr.soleil.bensikin.data.snapshot.manager.SnapshotManagerFactory;
import fr.soleil.bensikin.history.History;
import fr.soleil.bensikin.history.context.ContextHistory;
import fr.soleil.bensikin.history.context.OpenedContextRef;
import fr.soleil.bensikin.history.context.SelectedContextRef;
import fr.soleil.bensikin.history.snapshot.OpenedSnapshotRef;
import fr.soleil.bensikin.history.snapshot.SelectedSnapshotRef;
import fr.soleil.bensikin.history.snapshot.SnapshotHistory;
import fr.soleil.bensikin.tools.BensikinWarning;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;

/**
 * An XML implementation. Saves and loads history to/from XML files.
 * 
 * @author CLAISSE
 */
public class XMLHistoryManager extends XMLDataManager<History, Map<String, Map<String, ?>>> implements IHistoryManager {

    @Override
    public void saveHistory(History history, String historyResourceLocation) throws ArchivingException {
        saveData(history, historyResourceLocation);
    }

    @Override
    public History loadHistory(String historyResourceLocation) throws Exception {
        try {
            Map<String, Map<String, ?>> historyHt = loadDataIntoHash(historyResourceLocation);

            // BEGIN CONTEXTS
            // BEGIN OPEN BOOKS
            Map<String, ?> contextsBook = historyHt.get(History.CONTEXTS_KEY);
            // END OPEN BOOKS

            // BEGIN OPEN CHAPTERS
            Map<?, ?> selectedContextChapter;
            List<?> openedContextsChapter;
            if (contextsBook == null) {
                selectedContextChapter = null;
                openedContextsChapter = null;
            } else {
                selectedContextChapter = (Map<?, ?>) contextsBook.get(History.SELECTED_CONTEXT_KEY);
                openedContextsChapter = (List<?>) contextsBook.get(History.OPENED_CONTEXTS_KEY);
            }
            if (openedContextsChapter == null) {
                openedContextsChapter = new ArrayList<Object>();
            }
            // END OPEN CHAPTERS

            String id_s;
            SelectedContextRef selectedcontext = null;

            if (selectedContextChapter != null) {
                id_s = (String) selectedContextChapter.get(History.ID_KEY);
                if (id_s != null) {
                    int id = Integer.parseInt(id_s);
                    selectedcontext = new SelectedContextRef(id);
                }
            }
            // BEGIN OPEN PAGES
            List<OpenedContextRef> openedContextList = new ArrayList<OpenedContextRef>(openedContextsChapter.size());
            for (Object element : openedContextsChapter) {
                Map<?, ?> nextPage = (Map<?, ?>) element;
                id_s = (String) nextPage.get(History.ID_KEY);
                if (id_s != null) {
                    int id = Integer.parseInt(id_s);
                    OpenedContextRef openedcontext = new OpenedContextRef(id);
                    openedContextList.add(openedcontext);
                }
            }
            OpenedContextRef[] openedContexts = openedContextList
                    .toArray(new OpenedContextRef[openedContextList.size()]);
            openedContextList.clear();
            // END OPEN PAGES
            ContextHistory contextHistory = new ContextHistory(selectedcontext, openedContexts);
            // END CONTEXTS

            // BEGIN SNAPSHOTS
            // BEGIN OPEN BOOKS
            Map<String, ?> snapshotsBook = historyHt.get(History.SNAPSHOTS_KEY);
            // END OPEN BOOKS

            // BEGIN OPEN CHAPTERS
            // Map selectedSnapshotChapter = (Map) snapshotsBook.get
            // ( History.SELECTED_SNAPSHOT_KEY );
            List<?> selectedSnapshotsChapter;
            List<?> openedSnapshotsChapter;
            if (snapshotsBook == null) {
                selectedSnapshotsChapter = null;
                openedSnapshotsChapter = null;
            } else {
                selectedSnapshotsChapter = (List<?>) snapshotsBook.get(History.SELECTED_SNAPSHOTS_KEY);
                openedSnapshotsChapter = (List<?>) snapshotsBook.get(History.OPENED_SNAPSHOTS_KEY);
            }
            if (selectedSnapshotsChapter == null) {
                selectedSnapshotsChapter = new ArrayList<Object>();
            }
            if (openedSnapshotsChapter == null) {
                openedSnapshotsChapter = new ArrayList<Object>();
            }
            // END OPEN CHAPTERS

            // BEGIN OPEN PAGES
            List<OpenedSnapshotRef> openedSnapshotList = new ArrayList<OpenedSnapshotRef>(openedSnapshotsChapter.size());
            for (Object element : openedSnapshotsChapter) {
                Map<?, ?> nextPage = (Map<?, ?>) element;
                id_s = (String) nextPage.get(History.ID_KEY);
                if (id_s != null) {
                    int id = Integer.parseInt(id_s);
                    OpenedSnapshotRef openedsnapshot = new OpenedSnapshotRef(id);
                    openedSnapshotList.add(openedsnapshot);
                }
            }
            OpenedSnapshotRef[] openedSnapshots = openedSnapshotList.toArray(new OpenedSnapshotRef[openedSnapshotList
                    .size()]);
            openedSnapshotList.clear();

            List<SelectedSnapshotRef> selectedSnapshotList = new ArrayList<SelectedSnapshotRef>(
                    selectedSnapshotsChapter.size());
            for (Object element : selectedSnapshotsChapter) {
                Map<?, ?> nextPage = (Map<?, ?>) element;
                id_s = (String) nextPage.get(History.ID_KEY);
                if (id_s != null) {
                    int id = Integer.parseInt(id_s);
                    // We insert a special attribute for the tag referencing the focused tab
                    SelectedSnapshotRef selectedsnapshot;
                    String highlightedState = (String) nextPage.get(History.HIGHLIGHTED_KEY);
                    if (highlightedState != null) {
                        boolean isHighlight = Boolean.parseBoolean(highlightedState);
                        selectedsnapshot = new SelectedSnapshotRef(id, isHighlight);
                    } else {
                        selectedsnapshot = new SelectedSnapshotRef(id);
                    }
                    selectedSnapshotList.add(selectedsnapshot);
                }
            }
            SelectedSnapshotRef[] selectedSnapshots = selectedSnapshotList
                    .toArray(new SelectedSnapshotRef[selectedSnapshotList.size()]);
            selectedSnapshotList.clear();
            // END OPEN PAGES
            SnapshotHistory snapshotHistory = new SnapshotHistory(selectedSnapshots, openedSnapshots);
            // END SNAPSHOTS

            return new History(snapshotHistory, contextHistory);

        } catch (Exception e) {
            BensikinWarning bensikinWarning = new BensikinWarning(e.getMessage());
            bensikinWarning.setStackTrace(e.getStackTrace());
            throw bensikinWarning;
        }
    }

    /**
     * Loads the Contexts history into a Map, given the DOM node of the
     * XML file contexts part.
     * 
     * @param contextsNode
     *            The DOM node of the XML file contexts part
     * @return A Map which keys are History.SELECTED_CONTEXT_KEY and
     *         History.OPENED_CONTEXTS_KEY, and which values are respectively
     *         selected/opened Contexts Maps
     * @throws Exception
     */
    private static Map<String, Object> loadContextsBook(Node contextsNode) {
        Map<String, Object> book = new ConcurrentHashMap<String, Object>();
        Map<String, String> attributes = XMLUtils.loadAttributes(contextsNode);
        String location = attributes.get(History.SAVE_LOCATION_KEY);
        if (location != null) {
            IContextManager manager = ContextManagerFactory.getCurrentImpl();
            if (manager != null) {
                manager.setNonDefaultSaveLocation(location);
            }
        }
        if (contextsNode.hasChildNodes()) {
            NodeList contextChapterNodes = contextsNode.getChildNodes();
            // as many loops as there are contexts types, ie. selected or opened, in the current context
            // (which is normally three: contexts, snapshots, and options)
            for (int i = 0; i < contextChapterNodes.getLength(); i++) {
                Node currentContextChapterNode = contextChapterNodes.item(i);
                if (!XMLUtils.isAFakeNode(currentContextChapterNode)) {
                    String currentContextChapterType = currentContextChapterNode.getNodeName().trim();
                    if (History.SELECTED_CONTEXT_KEY.equals(currentContextChapterType)) {
                        Map<String, String> currentChapter = null;
                        try {
                            currentChapter = XMLUtils.loadAttributes(currentContextChapterNode);
                            if (currentChapter != null) {
                                book.put(History.SELECTED_CONTEXT_KEY, currentChapter);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (History.OPENED_CONTEXTS_KEY.equals(currentContextChapterType)) {
                        List<Map<String, String>> currentChapter = loadOpenedContextsPages(currentContextChapterNode);
                        book.put(History.OPENED_CONTEXTS_KEY, currentChapter);
                    }
                }
            }
        }
        return book;
    }

    /**
     * Loads the opened Contexts history into a List, given the DOM node of the
     * XML file opened contexts part.
     * 
     * @param openedContextsNode
     *            The DOM node of the XML file opened contexts part
     * @return A List which elements are opened Contexts Maps
     * @throws Exception
     */
    private static List<Map<String, String>> loadOpenedContextsPages(Node openedContextsNode) {
        List<Map<String, String>> pages = new ArrayList<Map<String, String>>();

        if (openedContextsNode.hasChildNodes()) {
            NodeList openedContextNodes = openedContextsNode.getChildNodes();

            for (int i = 0; i < openedContextNodes.getLength(); i++) {
                Node currentOpenedContextNode = openedContextNodes.item(i);

                if (XMLUtils.isAFakeNode(currentOpenedContextNode)) {
                    continue;
                }

                String currentOpenedContextType = currentOpenedContextNode.getNodeName().trim();
                Map<String, String> currentPage = null;
                if (History.OPENED_CONTEXT_KEY.equals(currentOpenedContextType)) {
                    try {
                        currentPage = XMLUtils.loadAttributes(currentOpenedContextNode);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    pages.add(currentPage);
                }
            }
        }

        return pages;
    }

    /**
     * Loads the Snapshots history into a Map, given the DOM node of the
     * XML file snapshots part.
     * 
     * @param snapshotsNode
     *            The DOM node of the XML file snapshots part
     * @return A Map which keys are History.SELECTED_SNAPSHOTS_KEY and
     *         History.OPENED_SNAPSHOTS_KEY, and which values are respectively
     *         selected/opened Snapshots Map
     * @throws Exception
     */
    private static Map<String, List<Map<String, String>>> loadSnapshotsBook(Node snapshotsNode) {
        Map<String, List<Map<String, String>>> book = new ConcurrentHashMap<String, List<Map<String, String>>>();
        Map<String, String> attributes = XMLUtils.loadAttributes(snapshotsNode);
        String location = attributes.get(History.SAVE_LOCATION_KEY);
        if (location != null) {
            final ISnapshotManager manager = SnapshotManagerFactory.getCurrentImpl();
            if (manager != null) {
                manager.setNonDefaultSaveLocation(location);
            }
        }
        if (snapshotsNode.hasChildNodes()) {
            NodeList snapshotChapterNodes = snapshotsNode.getChildNodes();
            // as many loops as there are snapshots types, ie. selected or
            // opened, in the current snapshot
            for (int i = 0; i < snapshotChapterNodes.getLength(); i++) {
                Node currentSnapshotChapterNode = snapshotChapterNodes.item(i);
                if (!XMLUtils.isAFakeNode(currentSnapshotChapterNode)) {
                    String currentSnapshotChapterType = currentSnapshotChapterNode.getNodeName().trim();
                    if (History.SELECTED_SNAPSHOTS_KEY.equals(currentSnapshotChapterType)) {
                        List<Map<String, String>> currentChapter = loadSelectedSnapshotsPages(currentSnapshotChapterNode);
                        book.put(History.SELECTED_SNAPSHOTS_KEY, currentChapter);
                    } else if (History.OPENED_SNAPSHOTS_KEY.equals(currentSnapshotChapterType)) {
                        List<Map<String, String>> currentChapter = loadOpenedSnapshotsPages(currentSnapshotChapterNode);
                        book.put(History.OPENED_SNAPSHOTS_KEY, currentChapter);
                    }
                }
            }
        }
        return book;
    }

    /**
     * Loads the opened Snapshots history into a List, given the DOM node of the
     * XML file opened snapshots part.
     * 
     * @param openedSnapshotsNode
     *            The DOM node of the XML file opened snapshots part
     * @return A List which elements are opened Snapshots Maps
     * @throws Exception
     */
    private static List<Map<String, String>> loadOpenedSnapshotsPages(Node openedSnapshotsNode) {
        List<Map<String, String>> pages = new ArrayList<Map<String, String>>();

        if (openedSnapshotsNode.hasChildNodes()) {
            NodeList openedSnapshotNodes = openedSnapshotsNode.getChildNodes();

            for (int i = 0; i < openedSnapshotNodes.getLength(); i++) {
                Node currentOpenedSnapshotNode = openedSnapshotNodes.item(i);

                if (!XMLUtils.isAFakeNode(currentOpenedSnapshotNode)) {
                    String currentOpenedSnapshotType = currentOpenedSnapshotNode.getNodeName().trim();
                    Map<String, String> currentPage = null;

                    if (History.OPENED_SNAPSHOT_KEY.equals(currentOpenedSnapshotType)) {
                        try {
                            currentPage = XMLUtils.loadAttributes(currentOpenedSnapshotNode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        pages.add(currentPage);
                    }
                }
            }
        }

        return pages;
    }

    /**
     * Loads the selected Snapshots history into a List, given the DOM node of
     * the XML file selected snapshots part.
     * 
     * @param selectedSnapshotsNode
     *            The DOM node of the XML file selected snapshots part
     * @return A List which elements are selected Snapshots Maps
     * @throws Exception
     */
    private static List<Map<String, String>> loadSelectedSnapshotsPages(Node selectedSnapshotsNode) {
        List<Map<String, String>> pages = new ArrayList<Map<String, String>>();
        if (selectedSnapshotsNode.hasChildNodes()) {
            NodeList selectedSnapshotNodes = selectedSnapshotsNode.getChildNodes();
            for (int i = 0; i < selectedSnapshotNodes.getLength(); i++) {
                Node currentSelectedSnapshotNode = selectedSnapshotNodes.item(i);
                if (!XMLUtils.isAFakeNode(currentSelectedSnapshotNode)) {
                    String currentSelectedSnapshotType = currentSelectedSnapshotNode.getNodeName().trim();
                    Map<String, String> currentPage = null;
                    if (History.SELECTED_SNAPSHOT_KEY.equals(currentSelectedSnapshotType)) {
                        try {
                            currentPage = XMLUtils.loadAttributes(currentSelectedSnapshotNode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        pages.add(currentPage);
                    }
                }
            }
        }
        return pages;
    }

    @Override
    protected History getDefaultData() {
        return History.getCurrentHistory();
    }

    /**
     * Loads the History into a Map, given the root node of the XML file.
     * 
     * @param rootNode
     *            The DOM root of the XML file
     * @return A Map which keys are History.CONTEXTS_KEY and
     *         History.SNAPSHOTS_KEY, and which values are respectively
     *         Context/Snapshot Maps
     */
    @Override
    protected Map<String, Map<String, ?>> loadDataIntoHashFromRoot(Node rootNode) {
        Map<String, Map<String, ?>> history = null;
        if (rootNode.hasChildNodes()) {
            NodeList bookNodes = rootNode.getChildNodes();
            history = new ConcurrentHashMap<String, Map<String, ?>>();
            // as many loops as there are history parts in the saved file
            // (which is normally three: contexts, snapshots, and options)
            for (int i = 0; i < bookNodes.getLength(); i++) {
                Node currentBookNode = bookNodes.item(i);
                if (!XMLUtils.isAFakeNode(currentBookNode)) {
                    String currentBookType = currentBookNode.getNodeName().trim();
                    Map<String, ?> currentBook;
                    if (History.CONTEXTS_KEY.equals(currentBookType)) {
                        currentBook = loadContextsBook(currentBookNode);
                        history.put(History.CONTEXTS_KEY, currentBook);
                    } else if (History.SNAPSHOTS_KEY.equals(currentBookType)) {
                        currentBook = loadSnapshotsBook(currentBookNode);
                        history.put(History.SNAPSHOTS_KEY, currentBook);
                    }
                    /*
                     * else if ( History.OPTIONS_KEY.equals ( currentBookType )
                     * ) { currentBook = loadOptionsBook ( currentBookNode );
                     * history.put ( History.OPTIONS_KEY , currentBook ); }
                     */
                }
            }
        }
        return history;
    }
}
