// +======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/impl/TangoManagerImpl.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  TangoManagerImpl.
//						(Claisse Laurent) - 30 juin 2005
//
// $Author: pierrejoseph $
//
// $Revision: 1.8 $
//
// $Log: TangoManagerImpl.java,v $
// Revision 1.8  2007/06/14 15:23:15  pierrejoseph
// Catch addition in the loadDomains (NullPointer raised by new Database())
//
// Revision 1.7  2006/11/29 10:02:17  ounsy
// package refactoring
//
// Revision 1.1  2005/12/14 16:56:05  ounsy
// has been renamed
//
// Revision 1.1.1.2  2005/08/22 11:58:40  chinkumo
// First commit
//
//
// copyleft :		Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.impl;

import java.util.ArrayList;
import java.util.List;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.tango.TangoLoaderUtil;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.bensikin.datasources.tango.ITangoManager;

/**
 * An implementation that uses the Database API.
 * 
 * @author CLAISSE
 */
public class TangoManagerImpl implements ITangoManager {

    /**
     * Loads Tango attributes into a List containing Domain objects:
     * <UL>
     * <LI>Gets the criterions on the respective domain, family, member, and attribute parts from
     * <code>searchCriterions</code>; if one of them is null, it is replaced with a joker
     * <LI>Uses the Database API to get the domains matching the domain criterion
     * <LI>For each such domain, uses the Database API to get the families rattached to this domain and matching the
     * family criterion
     * <LI>For each such family, uses the Database API to get the members rattached to this family and matching the
     * member criterion
     * <LI>The loading stops there, the attributes are not loaded yet lest Tango be overloaded
     * </UL>
     * 
     * @param searchCriterions
     *            The search criterions
     * @return A List containing Domain objects
     */
    @Override
    public List<Domain> loadDomains(final String searchCriterions) {
        return TangoLoaderUtil.searchAttributes(searchCriterions);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * bensikin.bensikin.impl.ITangoManager#dbGetAttributeList(java.lang.String)
     */
    @Override
    public List<String> dbGetAttributeList(final String device_name) {
        List<String> liste_att = new ArrayList<>(32);
        try {
            final DeviceProxy deviceProxy = DeviceProxyFactory.get(device_name);
            deviceProxy.ping();
            final DeviceData device_data_argin = new DeviceData();
            String[] device_data_argout;
            device_data_argin.insert(GUIUtilities.TANGO_JOKER + device_name + GUIUtilities.TANGO_JOKER);
            device_data_argout = deviceProxy.get_attribute_list();
            for (final String element : device_data_argout) {
                liste_att.add(element);
            }
        } catch (final DevFailed e) {
            liste_att.clear();
            liste_att = null;
        }
        return liste_att;
    }

}
