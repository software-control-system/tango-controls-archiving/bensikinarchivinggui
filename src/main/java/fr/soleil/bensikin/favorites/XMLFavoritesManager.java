// +======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/favorites/XMLFavoritesManager.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  XMLFavoritesManager.
//						(Claisse Laurent) - 8 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.6 $
//
// $Log: XMLFavoritesManager.java,v $
// Revision 1.6  2006/03/27 14:04:34  ounsy
// favorites contexts now have a label
//
// Revision 1.5  2005/11/29 18:25:08  chinkumo
// no message
//
// Revision 1.1.1.2  2005/08/22 11:58:38  chinkumo
// First commit
//
//
// copyleft :		Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.favorites;

import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.gui.manager.XMLDataManager;
import fr.soleil.bensikin.xml.BensikinXMLLine;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;

/**
 * An XML implementation.
 * 
 * @author CLAISSE
 */
public class XMLFavoritesManager extends XMLDataManager<Favorites, Map<String, Map<String, Object>>> implements
        IFavoritesManager {

    @Override
    public void saveFavorites(Favorites history, String favoritesResourceLocation) throws ArchivingException {
        saveData(history, favoritesResourceLocation);
    }

    /**
     * Loads a Favorites given its file location.
     * 
     * @param favoritesResourceLocation
     *            The complete path to the favorites file to load
     * @return The Favorites object built from its XML representation
     * @throws ArchivingException
     */
    @Override
    public Favorites loadFavorites(String favoritesResourceLocation) throws ArchivingException {
        Map<String, Map<String, Object>> favoritesHt = loadDataIntoHash(favoritesResourceLocation);
        // BEGIN OPEN BOOKS
        FavoritesContextSubMenu contextSubMenu = new FavoritesContextSubMenu(FavoritesContextSubMenu.XML_TAG);
        if (favoritesHt != null) {
            Map<String, Object> contextsBook = favoritesHt.get(FavoritesContextSubMenu.XML_TAG);
            contextSubMenu.build(contextsBook);
        } else {
            contextSubMenu.build(null);
        }
        // END OPEN BOOKS

        Favorites favorites = Favorites.getInstance();
        favorites.setContextSubMenu(contextSubMenu);
        return favorites;

    }

//    /**
//     * Loads a Favorites into a Map given its file location.
//     * 
//     * @param location
//     *            The complete path to the favorites file to load
//     * @return The Map built from its XML representation
//     * @throws Exception
//     */
//    private Map<String, Map<String, Object>> loadFavoritesIntoHash(String location) throws Exception {
//        Map<String, Map<String, Object>> ret;
//        File file = new File(location);
//        if ((file == null) || (!file.canRead())) {
//            ret = null;
//        } else {
//            Node rootNode = XMLUtils.getRootNode(file);
//            ret = loadDataIntoHashFromRoot(rootNode);
//        }
//        return ret;
//    }

//    /**
//     * Loads a Favorites given its XML root node.
//     * 
//     * @param rootNode
//     *            The file's root node
//     * @return The Context Map built from its XML representation
//     * @throws Exception
//     */
//    private Map<String, Map<String, Object>> loadFavoritesIntoHashFromRoot(Node rootNode) throws Exception {
//        try {
//            Map<String, Map<String, Object>> favorites = null;
//            if (rootNode.hasChildNodes()) {
//                NodeList bookNodes = rootNode.getChildNodes();
//                favorites = new ConcurrentHashMap<String, Map<String, Object>>();
//
//                // as many loops as there are favorites parts in the saved file
//                // (which is normally three: contexts, snapshots, and options)
//                for (int i = 0; i < bookNodes.getLength(); i++) {
//                    Node currentBookNode = bookNodes.item(i);
//                    if (!XMLUtils.isAFakeNode(currentBookNode)) {
//                        String currentBookType = currentBookNode.getNodeName().trim();
//                        Map<String, Object> currentBook;
//                        if (FavoritesContextSubMenu.XML_TAG.equals(currentBookType)) {
//                            currentBook = loadContextsBook(currentBookNode);
//                            favorites.put(FavoritesContextSubMenu.XML_TAG, currentBook);
//                        }
//                    }
//                }
//            }
//            return favorites;
//        } catch (Exception e) {
//            throw e;
//        }
//    }

    /**
     * Loads the context part of the favorites
     * 
     * @param currentBookNode
     *            The context node
     * @return A TreeMap containing the context part of the favorites
     * @throws Exception
     */
    private Map<String, Object> loadContextsBook(Node currentBookNode) throws ArchivingException {
        Map<String, Object> ret = new TreeMap<String, Object>();
        ret = getChapter(ret, currentBookNode);
        return ret;
    }

    /**
     * Loads recursively the favorite contexts nodes into a TreeMap.
     * 
     * @param mapIn
     *            The current step of TreeMap building
     * @param currentChapterNode
     *            The current node
     * @return A TreeMap containing the favorites structure for the current book
     */
    private Map<String, Object> getChapter(Map<String, Object> mapIn, Node currentChapterNode)
            throws ArchivingException {
        NodeList bookNodes = currentChapterNode.getChildNodes();
        for (int i = 0; i < bookNodes.getLength(); i++) {
            Node nextNode = bookNodes.item(i);
            if (!XMLUtils.isAFakeNode(nextNode)) {
                String nextNodeName = nextNode.getNodeName();
                if (XMLUtils.hasRealChildNodes(nextNode)) {
                    Map<String, Object> next = getChapter(new TreeMap<String, Object>(), nextNode);
                    mapIn.put(nextNodeName, next);
                } else {
                    Map<String, String> attributes = XMLUtils.loadAttributes(nextNode);
                    if (attributes == null) {
                        mapIn.put(nextNodeName, new TreeMap<String, Object>());
                    } else {
                        BensikinXMLLine line = new BensikinXMLLine(nextNodeName, BensikinXMLLine.EMPTY_TAG_CATEGORY);
                        line.setAttributes(attributes);
                        // WARNING!!
                        // mapIn.put(line.getId() , line);
                        mapIn.put(line.getItemName(), line);
                    }
                }
            }
        }
        return mapIn;
    }

    @Override
    protected Favorites getDefaultData() {
        return Favorites.getInstance();
    }

    @Override
    protected Map<String, Map<String, Object>> loadDataIntoHashFromRoot(Node rootNode) throws ArchivingException {
        try {
            Map<String, Map<String, Object>> favorites = null;
            if (rootNode.hasChildNodes()) {
                NodeList bookNodes = rootNode.getChildNodes();
                favorites = new ConcurrentHashMap<String, Map<String, Object>>();
                // as many loops as there are history parts in the saved file
                // (which is normally three: contexts, snapshots, and options)
                for (int i = 0; i < bookNodes.getLength(); i++) {
                    Node currentBookNode = bookNodes.item(i);
                    if (!XMLUtils.isAFakeNode(currentBookNode)) {
                        String currentBookType = currentBookNode.getNodeName().trim();
                        Map<String, Object> currentBook;
                        if (FavoritesContextSubMenu.XML_TAG.equals(currentBookType)) {
                            currentBook = loadContextsBook(currentBookNode);
                            favorites.put(FavoritesContextSubMenu.XML_TAG, currentBook);
                        }
                    }
                }
            }
            return favorites;
        } catch (Exception e) {
            e.printStackTrace();
            throw ArchivingException.toArchivingException(e);
        }
    }

}
