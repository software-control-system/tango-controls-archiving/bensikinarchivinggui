// +======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/favorites/DummyFavoritesManager.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  DummyFavoritesManager.
//						(Claisse Laurent) - 22 juin 2005
//
// $Author: chinkumo $
//
// $Revision: 1.5 $
//
// $Log: DummyFavoritesManager.java,v $
// Revision 1.5  2005/11/29 18:25:08  chinkumo
// no message
//
// Revision 1.1.1.2  2005/08/22 11:58:38  chinkumo
// First commit
//
//
// copyleft :		Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.favorites;

import java.util.Map;
import java.util.TreeMap;

import fr.soleil.bensikin.xml.BensikinXMLLine;

/**
 * Dummy implementation.
 * 
 * @author CLAISSE
 */
public class DummyFavoritesManager implements IFavoritesManager {

    @Override
    public void saveFavorites(Favorites history, String favoritesResourceLocation) {
        // nothing to do
    }

    @Override
    public Favorites loadFavorites(String favoritesResourceLocation) {
        Favorites favorites = Favorites.getInstance();

        FavoritesContextSubMenu ret = new FavoritesContextSubMenu(FavoritesContextSubMenu.XML_TAG);
        Map<String, Object> treeMap = new TreeMap<String, Object>();

        BensikinXMLLine line1 = new BensikinXMLLine("context");
        line1.setAttribute("id", "111111");

        BensikinXMLLine line2 = new BensikinXMLLine("context");
        line2.setAttribute("id", "222222");

        BensikinXMLLine line3 = new BensikinXMLLine("context");
        line3.setAttribute("id", "333333");

        BensikinXMLLine line4 = new BensikinXMLLine("context");
        line4.setAttribute("id", "444444");

        BensikinXMLLine line5 = new BensikinXMLLine("context");
        line5.setAttribute("id", "555555");

        BensikinXMLLine line6 = new BensikinXMLLine("context");
        line6.setAttribute("id", "666666");

        BensikinXMLLine line7 = new BensikinXMLLine("context");
        line7.setAttribute("id", "777777");

        BensikinXMLLine line8 = new BensikinXMLLine("context");
        line8.setAttribute("id", "888888");

        BensikinXMLLine line9 = new BensikinXMLLine("context");
        line9.setAttribute("id", "999999");

        Map<String, BensikinXMLLine> sous_cas_11 = new TreeMap<String, BensikinXMLLine>();
        sous_cas_11.put(line1.getId(), line1);
        sous_cas_11.put(line2.getId(), line2);
        sous_cas_11.put(line3.getId(), line3);

        Map<String, BensikinXMLLine> sous_cas_12 = new TreeMap<String, BensikinXMLLine>();
        sous_cas_12.put(line4.getId(), line4);

        Map<String, BensikinXMLLine> sous_cas_13 = new TreeMap<String, BensikinXMLLine>();
        sous_cas_13.put(line5.getId(), line5);

        Map<String, Map<String, BensikinXMLLine>> cas_1 = new TreeMap<String, Map<String, BensikinXMLLine>>();
        cas_1.put("sous_cas_11", sous_cas_11);
        cas_1.put("sous_cas_12", sous_cas_12);
        cas_1.put("sous_cas_13", sous_cas_13);

        Map<String, BensikinXMLLine> cas_2 = new TreeMap<String, BensikinXMLLine>();
        cas_2.put(line6.getId(), line6);
        cas_2.put(line7.getId(), line7);
        cas_2.put(line8.getId(), line8);

        treeMap.put("cas_1", cas_1);
        treeMap.put("cas_2", cas_2);
        treeMap.put(line9.getId(), line9);

        ret.buildMenu(treeMap);
        ret.buildTree(treeMap);

        favorites.setContextSubMenu(ret);

        return favorites;
    }

}
