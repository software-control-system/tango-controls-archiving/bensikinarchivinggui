/*	Synchrotron Soleil 
 *  
 *   File          :  SpectrumWriteValueDialog.java
 *  
 *   Project       :  Bensikin_CVS
 *  
 *   Description   :  
 *  
 *   Author        :  SOLEIL
 *  
 *   Original      :  14 f�vr. 2006 
 *  
 *   Revision:  					Author:  
 *   Date: 							State:  
 *  
 *   Log: SpectrumWriteValueDialog.java,v 
 *
 */
package fr.soleil.bensikin.containers.sub.dialogs;

import java.awt.Color;
import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.bensikin.actions.snapshot.SnapSpectrumCancelAction;
import fr.soleil.bensikin.actions.snapshot.SnapSpectrumSetAction;
import fr.soleil.bensikin.actions.snapshot.SnapSpectrumSetAllAction;
import fr.soleil.bensikin.components.editors.SnapSpectrumWriteTableEditor;
import fr.soleil.bensikin.components.editors.SnapshotWriteValueBooleanEditor;
import fr.soleil.bensikin.components.renderers.BensikinTableCellRenderer;
import fr.soleil.bensikin.components.renderers.SnapshotWriteValueBooleanRenderer;
import fr.soleil.bensikin.containers.BensikinFrame;
import fr.soleil.bensikin.data.snapshot.SnapshotAttributeValue;
import fr.soleil.bensikin.models.SpectrumWriteValueTableModel;
import fr.soleil.bensikin.tools.Messages;
import fr.soleil.bensikin.tools.SpringUtilities;

/**
 * 
 * @author SOLEIL
 */
public class SpectrumWriteValueDialog extends JDialog {

    private static final long serialVersionUID = -4599652886796809401L;

    private SpectrumWriteValueTableModel tableModel;
    private JButton set, setAll, cancel;
    private JTextField valueAll;
    private JTable table;

    // a variable to avoid undesired modification
    private Object duplicata;
    private boolean[] nullDuplicata;

    private static SpectrumWriteValueDialog instance;

    public static SpectrumWriteValueDialog getInstance() {
        return instance;
    }

    public static SpectrumWriteValueDialog getInstance(boolean forceReload, String name, SnapshotAttributeValue value) {
        if (instance == null || forceReload) {
            instance = new SpectrumWriteValueDialog(name, value);
        }
        return instance;
    }

    private SpectrumWriteValueDialog(String name, SnapshotAttributeValue value) {
        super(BensikinFrame.getInstance(), name, true);
        this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new DummyWindowListener(this));
        JPanel myPanel = new JPanel();
        myPanel.setLayout(new SpringLayout());
        cancel = new JButton(new SnapSpectrumCancelAction(Messages.getMessage("DIALOGS_SPECTRUM_ATTRIBUTE_CANCEL")));
        cancel.setMargin(new Insets(0, 0, 0, 0));
        cancel.setBackground(Color.RED);
        cancel.setForeground(Color.WHITE);
        JPanel cancelPanel = new JPanel();
        cancelPanel.add(cancel);
        cancelPanel.add(Box.createHorizontalGlue());
        cancelPanel.setLayout(new SpringLayout());
        GUIUtilities.setObjectBackground(cancelPanel, GUIUtilities.SNAPSHOT_COLOR);
        SpringUtilities.makeCompactGrid(cancelPanel, 1, cancelPanel.getComponentCount(), // rows,
                                                                                         // cols
                0, 0, // initX, initY
                0, 0, // xPad, yPad
                true);
        myPanel.add(cancelPanel);
        GUIUtilities.setObjectBackground(myPanel, GUIUtilities.SNAPSHOT_COLOR);
        duplicata = null;
        if (value == null)
            return;
        Object val = value.getSpectrumValue();
        if (val != null && !"NaN".equals(val) && !"[NaN]".equals(val)) {
            switch (value.getDataType()) {
            case TangoConst.Tango_DEV_BOOLEAN:
                if (val != null && !"NaN".equals(val)) {
                    duplicata = ((boolean[]) val).clone();
                }
                break;
            case TangoConst.Tango_DEV_STRING:
                if (val != null && !"NaN".equals(val)) {
                    duplicata = ((String[]) val).clone();
                }
                break;
            case TangoConst.Tango_DEV_UCHAR:
            case TangoConst.Tango_DEV_CHAR:
                if (val != null && !"NaN".equals(val)) {
                    duplicata = ((byte[]) val).clone();
                }
                break;
            case TangoConst.Tango_DEV_ULONG:
            case TangoConst.Tango_DEV_LONG:
                if (val != null && !"NaN".equals(val)) {
                    duplicata = ((int[]) val).clone();
                }
                break;
            case TangoConst.Tango_DEV_USHORT:
            case TangoConst.Tango_DEV_SHORT:
                if (val != null && !"NaN".equals(val)) {
                    duplicata = ((short[]) val).clone();
                }
                break;
            case TangoConst.Tango_DEV_FLOAT:
                if (val != null && !"NaN".equals(val)) {
                    duplicata = ((float[]) val).clone();
                }
                break;
            case TangoConst.Tango_DEV_DOUBLE:
                if (val != null && !"NaN".equals(val)) {
                    duplicata = ((double[]) val).clone();
                }
                break;
            } // end switch(value.getDataType())
            boolean[] temp = (boolean[]) value.getNullElements();
            nullDuplicata = (temp == null ? null : temp.clone());
            tableModel = new SpectrumWriteValueTableModel(value.getDataType(), duplicata, nullDuplicata);
            table = new JTable(tableModel);
            if (value.getDataType() == TangoConst.Tango_DEV_BOOLEAN) {
                table.setDefaultRenderer(Object.class, new SnapshotWriteValueBooleanRenderer());
                table.setDefaultEditor(Object.class, new SnapshotWriteValueBooleanEditor());
            } else {
                table.setDefaultRenderer(Object.class, new BensikinTableCellRenderer());
                table.setDefaultEditor(Object.class, new SnapSpectrumWriteTableEditor());
            }
            GUIUtilities.setObjectBackground(table, GUIUtilities.SNAPSHOT_COLOR);
            JScrollPane scrollPane = new JScrollPane(table);
            GUIUtilities.setObjectBackground(scrollPane, GUIUtilities.SNAPSHOT_COLOR);
            GUIUtilities.setObjectBackground(scrollPane.getViewport(), GUIUtilities.SNAPSHOT_COLOR);
            this.setSize(500, 400);
            JPanel setPanel = new JPanel();
            GUIUtilities.setObjectBackground(setPanel, GUIUtilities.SNAPSHOT_COLOR);
            setPanel.setLayout(new SpringLayout());
            set = new JButton(new SnapSpectrumSetAction(Messages.getMessage("DIALOGS_SPECTRUM_ATTRIBUTE_SET")));
            setAll = new JButton(new SnapSpectrumSetAllAction(Messages.getMessage("DIALOGS_SPECTRUM_ATTRIBUTE_SETALL")));
            set.setBackground(new Color(0, 150, 0));
            set.setForeground(Color.WHITE);
            setAll.setBackground(new Color(0, 150, 0));
            setAll.setForeground(Color.WHITE);
            set.setMargin(new Insets(0, 0, 0, 0));
            setAll.setMargin(new Insets(0, 0, 0, 0));
            valueAll = new JTextField("0");
            setPanel.add(set);
            setPanel.add(Box.createHorizontalGlue());
            setPanel.add(setAll);
            setPanel.add(valueAll);
            SpringUtilities.makeCompactGrid(setPanel, 1, setPanel.getComponentCount(), // rows,
                                                                                       // cols
                    0, 0, // initX, initY
                    0, 0, // xPad, yPad
                    true);
            myPanel.add(setPanel);
            myPanel.add(scrollPane);
        } else {
            JLabel label = new JLabel(Messages.getMessage("DIALOGS_SPECTRUM_ATTRIBUTE_NAN"), JLabel.CENTER);
            GUIUtilities.setObjectBackground(label, GUIUtilities.SNAPSHOT_COLOR);
            myPanel.add(label);
            this.setSize(300, 300);
        }
        SpringUtilities.makeCompactGrid(myPanel, myPanel.getComponentCount(), 1, // rows,
                                                                                 // cols
                0, 0, // initX, initY
                0, 0, // xPad, yPad
                true);
        this.setContentPane(myPanel);
        this.setLocation(
                (BensikinFrame.getInstance().getX() + BensikinFrame.getInstance().getWidth()) - (this.getWidth() + 50),
                (BensikinFrame.getInstance().getY() + BensikinFrame.getInstance().getHeight())
                        - (this.getHeight() + 50));
    }

    public Object getValues() {
        Object values;
        if (duplicata == null) {
            values = null;
        } else {
            values = tableModel.getValues();
        }
        return values;
    }

    public boolean[] getNullElements() {
        boolean[] nullElements;
        if (nullDuplicata == null) {
            nullElements = null;
        } else {
            nullElements = tableModel.getNullElements();
        }
        return nullElements;
    }

    public boolean isCanceled() {
        if (duplicata != null) {
            return (!tableModel.isCanSet());
        }
        return true;
    }

    public void cancel() {
        if (duplicata != null) {
            tableModel.setCanSet(false);
        }
        this.setVisible(false);
    }

    public void setAll() {
        if (duplicata != null) {
            tableModel.setAll(valueAll.getText());
        }
    }

    public void closeToSet() {
        if (duplicata != null) {
            if (table.isEditing()) {
                JTextField textField = (JTextField) table.getEditorComponent();
                tableModel.setValueAt(textField.getText(), table.getEditingRow(), table.getEditingColumn());
                table.getDefaultEditor(Object.class).stopCellEditing();
            }
            tableModel.setCanSet(true);
            setVisible(false);
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private class DummyWindowListener extends WindowAdapter {

        private final SpectrumWriteValueDialog dialog;

        public DummyWindowListener(SpectrumWriteValueDialog dial) {
            super();
            dialog = dial;
        }

        @Override
        public void windowClosing(WindowEvent e) {
            dialog.cancel();
        }

        @Override
        public void windowClosed(WindowEvent e) {
            dialog.cancel();
        }

    }

}
