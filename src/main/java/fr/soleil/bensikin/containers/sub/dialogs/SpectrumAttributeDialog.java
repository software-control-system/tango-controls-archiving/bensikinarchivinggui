// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/bensikin/bensikin/containers/sub/dialogs/SpectrumAttributeDialog.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class SpectrumAttributeDialog.
// (Claisse Laurent) - 16 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.11 $
//
// $Log: SpectrumAttributeDialog.java,v $
// Revision 1.11 2006/10/31 16:54:08 ounsy
// milliseconds and null values management
//
// Revision 1.10 2006/08/23 09:57:20 ounsy
// use of the bug correction in JLChart that allows to have a JDialog for
// "show table" instead of a JFrame
//
// Revision 1.9 2006/04/13 12:37:33 ounsy
// new spectrum types support
//
// Revision 1.8 2006/03/14 12:18:10 ounsy
// Freeze bug in case of spectrums of dim 1 corrected
//
// Revision 1.7 2006/02/23 13:33:14 ounsy
// no more freezing when the snapshot does not respond
//
// Revision 1.6 2006/02/15 09:19:10 ounsy
// spectrums rw management
//
// Revision 1.5 2005/11/29 18:25:08 chinkumo
// no message
//
// Revision 1.1.1.2 2005/08/22 11:58:36 chinkumo
// First commit
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.bensikin.containers.sub.dialogs;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.bensikin.components.renderers.BensikinTableCellRenderer;
import fr.soleil.bensikin.containers.BensikinFrame;
import fr.soleil.bensikin.data.snapshot.BensikinAdapterFactory;
import fr.soleil.bensikin.data.snapshot.DefaultDataArrayAdapter;
import fr.soleil.bensikin.data.snapshot.SnapshotAttributeValue;
import fr.soleil.bensikin.models.SnapshotDetailTableModel;
import fr.soleil.bensikin.models.SpectrumReadValueTableModel;
import fr.soleil.bensikin.tools.Messages;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swing.Chart;
import fr.soleil.data.service.AbstractKey;

/**
 * Not used yet.
 * 
 * @author CLAISSE
 */
public class SpectrumAttributeDialog extends JDialog {

    private static final long serialVersionUID = 3674130771331262970L;

    private static final Dimension DIM = new Dimension(500, 400);

    private JPanel myPanel;
    private Chart chart;
    private JTable spectrumTable;

    public SpectrumAttributeDialog(String name, int row, int column, SnapshotDetailTableModel tableModel) {
        super(BensikinFrame.getInstance(), name, true);
        SnapshotAttributeValue value = null;
        Object temp = null;
        if (tableModel != null) {
            temp = tableModel.getValueAt(row, column);
        }
        if (temp instanceof SnapshotAttributeValue) {
            value = (SnapshotAttributeValue) temp;
        }
        if (value == null)
            return;
        myPanel = new JPanel(new GridLayout(1, 1));
        GUIUtilities.setObjectBackground(myPanel, GUIUtilities.SNAPSHOT_COLOR);

        if ((value.getDataType() == TangoConst.Tango_DEV_CHAR) || (value.getDataType() == TangoConst.Tango_DEV_UCHAR)
                || (value.getDataType() == TangoConst.Tango_DEV_LONG)
                || (value.getDataType() == TangoConst.Tango_DEV_ULONG)
                || (value.getDataType() == TangoConst.Tango_DEV_SHORT)
                || (value.getDataType() == TangoConst.Tango_DEV_USHORT)
                || (value.getDataType() == TangoConst.Tango_DEV_FLOAT)
                || (value.getDataType() == TangoConst.Tango_DEV_DOUBLE)) {
            chart = new Chart();
            AbstractKey key = tableModel.getCometeKey(row, column);
            DefaultDataArrayAdapter numberDataArrayDAO = null;
            Map<String, Object> datas = null;
            if (key != null) {
                // TODO Avisto
                final BensikinAdapterFactory daoFactory = BensikinAdapterFactory.INSTANCE;
                numberDataArrayDAO = (DefaultDataArrayAdapter) daoFactory.createNumberDataArrayDAO(key);
                datas = numberDataArrayDAO.getDatas();
            }
            if (datas == null) {
                JLabel label = new JLabel(Messages.getMessage("DIALOGS_SPECTRUM_ATTRIBUTE_NAN"), SwingConstants.CENTER);
                GUIUtilities.setObjectBackground(label, GUIUtilities.SNAPSHOT_COLOR);
                myPanel.add(label);
            } else {

                String dataName = (String) tableModel.getValueAt(row, 0);

                chart.setPreferredSnapshotExtension("png");
                chart.setData(datas);
                chart.setDataViewCometeColor(dataName, CometeColor.RED);
                chart.setDataViewMarkerCometeColor(dataName, CometeColor.RED);
                chart.setDataViewLineStyle(dataName, 1);
                chart.setDataViewMarkerSize(dataName, 5);
                chart.setDataViewMarkerStyle(dataName, IChartViewer.MARKER_DIAMOND);
                chart.setDataViewLineStyle(dataName, IChartViewer.STYLE_SOLID);

                chart.setAutoScale(true, IChartViewer.Y1);
                chart.setAutoScale(true, IChartViewer.X);

                chart.setManagementPanelVisible(false);
                chart.setFreezePanelVisible(false);
                chart.setPreferDialogForTable(true, true);
                chart.setLegendVisible(false);
                chart.setCometeBackground(ColorTool.getCometeColor(GUIUtilities.getSnapshotColor()));
                myPanel.add(chart);
            }

        } else {
            boolean[] nullElements = (boolean[]) value.getNullElements();
            if (nullElements != null) {
                nullElements = nullElements.clone();
            }

            switch (value.getDataType()) {
            case TangoConst.Tango_DEV_STATE:
                Object valsta = value.getSpectrumValue();
                if (!"NaN".equals(valsta) && valsta != null && valsta instanceof int[]) {
                    int[] stateArray = ((int[]) valsta).clone();
                    if (stateArray.length > 0) {
                        spectrumTable = new JTable(new SpectrumReadValueTableModel(value.getDataType(), stateArray,
                                nullElements));
                        spectrumTable.setDefaultRenderer(Object.class, new BensikinTableCellRenderer());
                    } else {
                        spectrumTable = null;
                    }

                } else {
                    spectrumTable = null;
                }
                break;
            case TangoConst.Tango_DEV_BOOLEAN:
                Object valb = value.getSpectrumValue();
                if (!"NaN".equals(valb) && valb != null && valb instanceof boolean[]) {
                    boolean[] boolArray = ((boolean[]) valb).clone();
                    if (boolArray.length > 0) {
                        spectrumTable = new JTable(new SpectrumReadValueTableModel(value.getDataType(), boolArray,
                                nullElements));
                        spectrumTable.setDefaultRenderer(Object.class, new BensikinTableCellRenderer());
                    } else {
                        spectrumTable = null;
                    }
                } else {
                    spectrumTable = null;
                }
                break;
            case TangoConst.Tango_DEV_STRING:
                Object valstr = value.getSpectrumValue();
                if (!"NaN".equals(valstr) && valstr != null && valstr instanceof String[]) {
                    String[] stringArray = ((String[]) valstr).clone();
                    if (stringArray.length > 0) {
                        spectrumTable = new JTable(new SpectrumReadValueTableModel(value.getDataType(), stringArray,
                                nullElements));
                        spectrumTable.setDefaultRenderer(Object.class, new BensikinTableCellRenderer());
                    } else {
                        spectrumTable = null;
                    }
                } else {
                    spectrumTable = null;
                }
                break;
            default:
                spectrumTable = null;
            }
            if (spectrumTable != null) {
                GUIUtilities.setObjectBackground(spectrumTable, GUIUtilities.SNAPSHOT_COLOR);
                JScrollPane scrollpane = new JScrollPane(spectrumTable);
                GUIUtilities.setObjectBackground(scrollpane, GUIUtilities.SNAPSHOT_COLOR);
                GUIUtilities.setObjectBackground(scrollpane.getViewport(), GUIUtilities.SNAPSHOT_COLOR);
                myPanel.add(scrollpane);
            } else {
                JLabel label = new JLabel(Messages.getMessage("DIALOGS_SPECTRUM_ATTRIBUTE_NAN"), SwingConstants.CENTER);
                GUIUtilities.setObjectBackground(label, GUIUtilities.SNAPSHOT_COLOR);
                myPanel.add(label);
            }
        }

        this.setContentPane(myPanel);

        this.setSize(DIM);
        this.setLocation(
                (BensikinFrame.getInstance().getX() + BensikinFrame.getInstance().getWidth()) - (this.getWidth() + 50),
                (BensikinFrame.getInstance().getY() + BensikinFrame.getInstance().getHeight())
                        - (this.getHeight() + 50));
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

}
