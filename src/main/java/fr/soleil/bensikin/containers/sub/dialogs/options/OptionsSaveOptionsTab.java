// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/bensikin/bensikin/containers/sub/dialogs/options/OptionsSaveOptionsTab.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class OptionsSaveOptionsTab.
// (Claisse Laurent) - 16 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.6 $
//
// $Log: OptionsSaveOptionsTab.java,v $
// Revision 1.6 2006/06/28 12:49:44 ounsy
// minor changes
//
// Revision 1.5 2005/11/29 18:25:08 chinkumo
// no message
//
// Revision 1.1.1.2 2005/08/22 11:58:37 chinkumo
// First commit
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.bensikin.containers.sub.dialogs.options;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.bensikin.options.sub.SaveOptions;
import fr.soleil.bensikin.tools.Messages;

/**
 * The save options tab of OptionsDialog, used to set the save on shudown/load
 * on startup cycle of the application.
 * 
 * @author CLAISSE
 */
public class OptionsSaveOptionsTab extends JPanel {

    private static final long serialVersionUID = -9212162249698224136L;

    private static OptionsSaveOptionsTab instance = null;

    private JRadioButton saveHistoryYes;
    private JRadioButton saveHistoryNo;
    private JLabel label;
    private ButtonGroup buttonHistoryGroup;

    private Box loadPropertiesBox;
    // private JCheckBox contextOpenedCheckBox;
    private JCheckBox contextSelectedCheckBox;
    private JCheckBox snapshotsOpenedCheckBox;
    private JCheckBox snapshotsSelectedCheckBox;

    // private Dimension dim = new Dimension(300 , 300);
    private JPanel myPanel;

    /**
     * Instantiates itself if necessary, returns the instance.
     * 
     * @return The instance
     */
    public static OptionsSaveOptionsTab getInstance() {
	if (instance == null) {
	    instance = new OptionsSaveOptionsTab();
	}

	return instance;
    }

    /**
     * Builds the tab.
     */
    private OptionsSaveOptionsTab() {
	this.initComponents();
	this.initLayout();
	this.addComponents();
    }

    /**
     * Inits the tab's layout.
     */
    private void initLayout() {
	setLayout(new GridLayout());
    }

    /**
     * Adds the initialized components to the tab.
     */
    private void addComponents() {
	myPanel = new JPanel(new GridBagLayout());

	Insets gapInsets = new Insets(0, 0, 40, 0);

	JPanel buttonPanel = new JPanel(new GridBagLayout());

	GridBagConstraints yesConstraints = new GridBagConstraints();
	yesConstraints.fill = GridBagConstraints.HORIZONTAL;
	yesConstraints.gridx = 0;
	yesConstraints.gridy = 0;
	yesConstraints.weightx = 1;
	yesConstraints.weighty = 0;
	yesConstraints.insets = gapInsets;
	buttonPanel.add(saveHistoryYes, yesConstraints);
	GridBagConstraints noConstraints = new GridBagConstraints();
	noConstraints.fill = GridBagConstraints.HORIZONTAL;
	noConstraints.gridx = 0;
	noConstraints.gridy = 1;
	noConstraints.weightx = 1;
	noConstraints.weighty = 0;
	buttonPanel.add(saveHistoryNo, noConstraints);

	GridBagConstraints labelConstraints = new GridBagConstraints();
	labelConstraints.fill = GridBagConstraints.HORIZONTAL;
	labelConstraints.gridx = 0;
	labelConstraints.gridy = 0;
	labelConstraints.weightx = 0.3;
	labelConstraints.weighty = 0;
	labelConstraints.insets = new Insets(10, 0, 0, 40);
	labelConstraints.anchor = GridBagConstraints.EAST;
	myPanel.add(label, labelConstraints);
	GridBagConstraints buttonPanelConstraints = new GridBagConstraints();
	buttonPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
	buttonPanelConstraints.gridx = 1;
	buttonPanelConstraints.gridy = 0;
	buttonPanelConstraints.weightx = 0.7;
	buttonPanelConstraints.weighty = 0;
	buttonPanelConstraints.insets = new Insets(10, 0, 0, 0);
	buttonPanelConstraints.anchor = GridBagConstraints.WEST;
	myPanel.add(buttonPanel, buttonPanelConstraints);

	GridBagConstraints saveContextConstraints = new GridBagConstraints();
	saveContextConstraints.fill = GridBagConstraints.HORIZONTAL;
	saveContextConstraints.gridx = 0;
	saveContextConstraints.gridy = 1;
	saveContextConstraints.gridwidth = GridBagConstraints.REMAINDER;
	saveContextConstraints.weightx = 1;
	saveContextConstraints.weighty = 0;
	saveContextConstraints.insets = new Insets(10, 0, 0, 0);
	myPanel.add(loadPropertiesBox, saveContextConstraints);

	GridBagConstraints glueConstraints = new GridBagConstraints();
	glueConstraints.fill = GridBagConstraints.BOTH;
	glueConstraints.gridx = 0;
	glueConstraints.gridy = 2;
	glueConstraints.weightx = 1;
	glueConstraints.weighty = 1;
	glueConstraints.gridwidth = GridBagConstraints.REMAINDER;
	glueConstraints.gridheight = GridBagConstraints.REMAINDER;
	myPanel.add(Box.createGlue(), glueConstraints);

	this.add(myPanel);
    }

    /**
     * Inits the tab's components.
     */
    private void initComponents() {

	initHistoryComponents();

	initLoadEntitiesComponents();
    }

    private void initHistoryComponents() {
	buttonHistoryGroup = new ButtonGroup();

	String msgSaveHistoryOnShutdown = Messages.getMessage("DIALOGS_OPTIONS_SAVE_SAVEONSHUTDOWN");
	String msgYes = Messages.getMessage("DIALOGS_OPTIONS_SAVE_SAVEONSHUTDOWN_YES");
	String msgNo = Messages.getMessage("DIALOGS_OPTIONS_SAVE_SAVEONSHUTDOWN_NO");

	label = new JLabel(msgSaveHistoryOnShutdown, JLabel.RIGHT);

	saveHistoryYes = new JRadioButton(msgYes, true);
	saveHistoryYes.addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent e) {
		loadPropertiesBox.setVisible(true);
	    }
	});

	saveHistoryNo = new JRadioButton(msgNo, true);
	saveHistoryNo.addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent e) {
		loadPropertiesBox.setVisible(false);
	    }
	});

	saveHistoryYes.setActionCommand(String.valueOf(SaveOptions.HISTORY_YES));
	saveHistoryNo.setActionCommand(String.valueOf(SaveOptions.HISTORY_NO));

	buttonHistoryGroup.add(saveHistoryYes);
	buttonHistoryGroup.add(saveHistoryNo);
    }

    private void initLoadEntitiesComponents() {

	// String msg =
	// Messages.getMessage("DIALOGS_OPTIONS_SAVE_OPENED_CONTEXT");
	// contextOpenedCheckBox = new JCheckBox(msg, true);
	// contextOpenedCheckBox.setActionCommand(String.valueOf(SaveOptions.CONTEXT_OPENED));
	String msg = Messages.getMessage("DIALOGS_OPTIONS_SAVE_SELECTED_CONTEXT");
	contextSelectedCheckBox = new JCheckBox(msg, false);
	contextSelectedCheckBox.setActionCommand(String.valueOf(SaveOptions.CONTEXT_SELECTED));
	msg = Messages.getMessage("DIALOGS_OPTIONS_SAVE_OPENED_SNAPSHOT");
	snapshotsOpenedCheckBox = new JCheckBox(msg, false);
	snapshotsOpenedCheckBox.setActionCommand(String.valueOf(SaveOptions.SNAPSHOT_OPENED));
	msg = Messages.getMessage("DIALOGS_OPTIONS_SAVE_SELECTED_SNAPSHOT");
	snapshotsSelectedCheckBox = new JCheckBox(msg, false);
	snapshotsSelectedCheckBox.setActionCommand(String.valueOf(SaveOptions.SNAPSHOT_SELECTED));

	loadPropertiesBox = new Box(BoxLayout.Y_AXIS);
	// loadPropertiesBox.add(contextOpenedCheckBox);
	// loadPropertiesBox.add(Box.createVerticalStrut(5));
	loadPropertiesBox.add(contextSelectedCheckBox);
	loadPropertiesBox.add(Box.createVerticalStrut(5));
	loadPropertiesBox.add(snapshotsOpenedCheckBox);
	loadPropertiesBox.add(Box.createVerticalStrut(5));
	loadPropertiesBox.add(snapshotsSelectedCheckBox);

	msg = Messages.getMessage("DIALOGS_OPTIONS_SAVE_LOAD_ENTITIES");
	TitledBorder tb = new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED), msg,
		TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP, GUIUtilities.getOptionsTitleFont());
	loadPropertiesBox.setBorder(tb);
    }

    /**
     * @return The buttonGroup attribute, containing the Save Yes and Save No
     *         JRadioButtons
     */
    public ButtonGroup getButtonHistoryGroup() {
	return buttonHistoryGroup;
    }

    /**
     * @return a table of boolean with the 4 load properties boolean values
     */
    public boolean[] getLoadPropertiesValues() {
	boolean[] result = new boolean[4];
	// result[SaveOptions.CONTEXT_OPENED] =
	// contextOpenedCheckBox.isSelected();
	result[SaveOptions.CONTEXT_OPENED] = true;
	result[SaveOptions.CONTEXT_SELECTED] = contextSelectedCheckBox.isSelected();
	result[SaveOptions.SNAPSHOT_OPENED] = snapshotsOpenedCheckBox.isSelected();
	result[SaveOptions.SNAPSHOT_SELECTED] = snapshotsSelectedCheckBox.isSelected();
	return result;
    }

    /**
     * Select load properties check box
     * 
     * @param properties
     *            a set of boolean that indicate if the checkbox related is
     *            selected or not
     */
    public void setLoadPropertiesCheckBox(boolean[] properties) {
	if (properties != null && properties.length == 4) {
	    // contextOpenedCheckBox.setSelected(properties[SaveOptions.CONTEXT_OPENED]);
	    contextSelectedCheckBox.setSelected(properties[SaveOptions.CONTEXT_SELECTED]);
	    snapshotsOpenedCheckBox.setSelected(properties[SaveOptions.SNAPSHOT_OPENED]);
	    snapshotsSelectedCheckBox.setSelected(properties[SaveOptions.SNAPSHOT_SELECTED]);
	}
    }

    /**
     * Selects a save JRadioButton, depending on the hasSave parameter value
     * 
     * @param hasSave
     *            Has to be either HISTORY_YES or HISTORY_NO, otherwise a
     *            IllegalArgumentException is thrown
     * @throws IllegalArgumentException
     */
    public void selectHasSaveButton(int hasSave) throws IllegalArgumentException {
	switch (hasSave) {
	case SaveOptions.HISTORY_YES:
	    saveHistoryYes.setSelected(true);
	    if (loadPropertiesBox != null) {
		loadPropertiesBox.setVisible(true);
	    }
	    break;

	case SaveOptions.HISTORY_NO:
	    saveHistoryNo.setSelected(true);
	    if (loadPropertiesBox != null) {
		loadPropertiesBox.setVisible(false);
	    }
	    break;

	default:
	    throw new IllegalArgumentException();
	}
    }
}
