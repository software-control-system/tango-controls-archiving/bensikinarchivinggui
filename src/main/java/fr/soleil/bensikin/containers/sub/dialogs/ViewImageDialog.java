package fr.soleil.bensikin.containers.sub.dialogs;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.activation.UnsupportedDataTypeException;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import net.entropysoft.transmorph.ConverterException;
import net.entropysoft.transmorph.DefaultConverters;
import net.entropysoft.transmorph.Transmorph;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.bensikin.containers.BensikinFrame;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.data.container.matrix.DoubleMatrix;

public class ViewImageDialog extends JDialog {

    private static final long serialVersionUID = -993763702000345451L;
    private ImageViewer imageViewer;
    protected JPanel myPanel;
    private final Object value;

    public ViewImageDialog(String name, Object value) {
	super(BensikinFrame.getInstance(), name, true);
	this.value = value;
	initComponents();
	layoutComponents();
	this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	addWindowListener(new WindowAdapter() {
	    @Override
	    public void windowClosing(WindowEvent e) {
		if (imageViewer != null) {
		    clean();
		}
	    }
	});
    }

    private void clean() {
	myPanel.removeAll();
    }

    private void initBounds() {
	imageViewer.setMinimumSize(new Dimension(imageViewer.getWidth(), imageViewer.getHeight()));
	pack();
	setLocationRelativeTo(getParent());
    }

    private void initComponents() {
	imageViewer = new ImageViewer();
	imageViewer.setApplicationId("Bensikin");
	imageViewer.setUseMaskManagement(false);
	imageViewer.setEditable(false);
	imageViewer.setCometeBackground(ColorTool.getCometeColor(GUIUtilities.getSnapshotColor()));
    }

    private void layoutComponents() {
	myPanel = new JPanel();
	GUIUtilities.setObjectBackground(myPanel, GUIUtilities.SNAPSHOT_COLOR);

	myPanel.setLayout(new BorderLayout());
	myPanel.add(imageViewer, BorderLayout.CENTER);

	this.setContentPane(myPanel);
    }

    public void updateContent() {
	clean();
	imageViewer.setData(createMatrice());
	myPanel.add(imageViewer, BorderLayout.CENTER);
	initBounds();
    }

    private DoubleMatrix createMatrice() {
	DefaultConverters defaultConverters = new DefaultConverters();
	Transmorph transmorph = new Transmorph(defaultConverters);
	double[][] array = new double[0][0];
	try {
	    array = transmorph.convert(value, double[][].class);
	} catch (ConverterException e) {
	    e.printStackTrace();
	}
	DoubleMatrix matrice = new DoubleMatrix();
	try {
	    matrice.setValue(array);
	} catch (UnsupportedDataTypeException e) {
	    e.printStackTrace();
	}
	return matrice;
    }
}
