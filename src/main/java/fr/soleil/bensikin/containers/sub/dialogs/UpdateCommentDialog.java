// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/bensikin/bensikin/containers/sub/dialogs/UpdateCommentDialog.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class UpdateCommentDialog.
// (Claisse Laurent) - 16 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.6 $
//
// $Log: UpdateCommentDialog.java,v $
// Revision 1.6 2006/06/28 12:48:56 ounsy
// minor changes
//
// Revision 1.5 2005/11/29 18:25:08 chinkumo
// no message
//
// Revision 1.1.1.2 2005/08/22 11:58:36 chinkumo
// First commit
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.bensikin.containers.sub.dialogs;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import fr.soleil.bensikin.actions.CancelAction;
import fr.soleil.bensikin.actions.snapshot.EditCommentAction;
import fr.soleil.bensikin.containers.BensikinFrame;
import fr.soleil.bensikin.containers.snapshot.SnapshotDetailTabbedPane;
import fr.soleil.bensikin.containers.snapshot.SnapshotDetailTabbedPaneContent;
import fr.soleil.bensikin.data.snapshot.Snapshot;
import fr.soleil.bensikin.tools.Messages;

/**
 * A small JDialog to update the comment field of a snapshot. The current value
 * is displayed and can be changed by the user
 * 
 * @author CLAISSE
 */
public class UpdateCommentDialog extends CancelableDialog {

    private static final long serialVersionUID = -110030936831036271L;

    private static final Dimension DIM = new Dimension(300, 300);

    private JLabel commentLabel;
    private JTextField commentText;
    private JButton updateButton;
    private JButton cancelButton;

    /**
     * Builds the dialog.
     */
    public UpdateCommentDialog() {
        super(BensikinFrame.getInstance(), Messages.getMessage("DIALOGS_UPDATE_COMMENT"), true);

        this.initComponents();
        this.layoutComponents();

        this.setSize(DIM);
    }

    /**
     * Inits the dialog's components. The comment's value is initialized with
     * the snapshot's current comment.
     */
    private void initComponents() {
        SnapshotDetailTabbedPane tabbedPane = SnapshotDetailTabbedPane.getInstance();
        // SnapshotDetailTabbedPaneContent content = (
        // SnapshotDetailTabbedPaneContent ) tabbedPane.getSelectedComponent();
        Snapshot snapshotToUse = ((SnapshotDetailTabbedPaneContent) tabbedPane.getSelectedComponent()).getSnapshot();

        commentText = new JTextField();
        commentText.setPreferredSize(new Dimension(200, commentText.getPreferredSize().height));
        String previousComment = snapshotToUse.getSnapshotData().getComment();
        commentText.setText(previousComment);

        String msgComment = Messages.getMessage("DIALOGS_UPDATE_COMMENT_LABEL");
        commentLabel = new JLabel(msgComment, SwingConstants.TRAILING);
        commentLabel.setLabelFor(commentText);

        String msg = Messages.getMessage("DIALOGS_UPDATE_COMMENT_VALIDATE");
        updateButton = new JButton(new EditCommentAction(msg, this));

        msg = Messages.getMessage("DIALOGS_UPDATE_COMMENT_CANCEL");
        cancelButton = new JButton(new CancelAction(msg, this));
    }

    /**
     * Inits the dialog's layout.
     */
    private void layoutComponents() {

        Box fieldBox = Box.createHorizontalBox();
        fieldBox.add(commentLabel);
        fieldBox.add(Box.createHorizontalStrut(5));
        fieldBox.add(commentText);

        Box buttonBox = Box.createHorizontalBox();
        buttonBox.add(Box.createHorizontalGlue());
        buttonBox.add(cancelButton);
        buttonBox.add(Box.createHorizontalStrut(5));
        buttonBox.add(updateButton);

        JPanel myPanel = new JPanel();
        myPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        myPanel.setLayout(new BorderLayout(0, 5));

        myPanel.add(fieldBox, BorderLayout.NORTH);
        myPanel.add(buttonBox, BorderLayout.SOUTH);

        this.setContentPane(myPanel);
    }

    /**
     * Returns the new comment.
     * 
     * @return The new comment
     */
    public JTextField getCommentText() {
        return commentText;
    }

    @Override
    public void cancel() {
        // nothing particular to do
    }

}
