//+======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/containers/snapshot/SnapshotDetailPanel.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  SnapshotDetailPanel.
//						(Claisse Laurent) - 16 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.6 $
//
// $Log: SnapshotDetailPanel.java,v $
// Revision 1.6  2005/12/14 16:25:15  ounsy
// minor changes
//
// Revision 1.5  2005/11/29 18:25:08  chinkumo
// no message
//
// Revision 1.1.1.2  2005/08/22 11:58:36  chinkumo
// First commit
//
//
// copyleft :		Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.containers.snapshot;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.bensikin.tools.Messages;

/**
 * Contains the selected snapshots and the comparison launch panel.
 * 
 * @author CLAISSE
 */
public class SnapshotDetailPanel extends JPanel {

    private static final long serialVersionUID = -8134898179985453695L;

    private static SnapshotDetailPanel snapshotDetailPanelInstance = null;

    /**
     * Instantiates itself if necessary, returns the instance.
     * 
     * @return The instance
     */
    public static SnapshotDetailPanel getInstance() {
        if (snapshotDetailPanelInstance == null) {
            snapshotDetailPanelInstance = new SnapshotDetailPanel();
        }

        return snapshotDetailPanelInstance;
    }

    /**
     * Builds the panel
     */
    private SnapshotDetailPanel() {
        setLayout(new BorderLayout(0, 5));
        this.add(SnapshotDetailTablePanel.getInstance(), BorderLayout.CENTER);
        this.add(SnapshotDetailComparePanel.getInstance(), BorderLayout.SOUTH);

        String msg = Messages.getMessage("SNAPSHOT_DETAIL_BORDER");
        TitledBorder tb = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), msg,
                TitledBorder.CENTER, TitledBorder.TOP, GUIUtilities.getTitleFont());
        CompoundBorder cb = BorderFactory.createCompoundBorder(tb, BorderFactory.createEmptyBorder(0, 5, 5, 5));
        this.setBorder(cb);

        GUIUtilities.setObjectBackground(this, GUIUtilities.SNAPSHOT_COLOR);
    }
}
