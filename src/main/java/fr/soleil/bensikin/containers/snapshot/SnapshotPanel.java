// +======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/containers/snapshot/SnapshotPanel.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  SnapshotPanel.
//						(Claisse Laurent) - 16 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.7 $
//
// $Log: SnapshotPanel.java,v $
// Revision 1.7  2006/01/12 13:53:48  ounsy
// minor changes
//
// Revision 1.6  2006/01/10 13:29:11  ounsy
// minor changes
//
// Revision 1.5  2005/11/29 18:25:08  chinkumo
// no message
//
// Revision 1.1.1.2  2005/08/22 11:58:36  chinkumo
// First commit
//
//
// copyleft :		Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.containers.snapshot;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.WindowConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.snap.api.manager.SnapManagerApi;
import fr.soleil.archiving.snap.api.tools.SnapConst;
import fr.soleil.archiving.snap.api.tools.SnapshotingException;
import fr.soleil.bensikin.Bensikin;
import fr.soleil.bensikin.actions.listeners.ContextTableListener;
import fr.soleil.bensikin.actions.roles.RightsManagerFactory;
import fr.soleil.bensikin.actions.snapshot.FilterSnapshotsAction;
import fr.soleil.bensikin.components.snapshot.list.SnapshotListTable;
import fr.soleil.bensikin.data.context.Context;
import fr.soleil.bensikin.data.context.manager.ContextManagerFactory;
import fr.soleil.bensikin.data.snapshot.Snapshot;
import fr.soleil.bensikin.data.snapshot.manager.SnapshotManagerFactory;
import fr.soleil.bensikin.datasources.tango.TangoManagerFactory;
import fr.soleil.bensikin.favorites.FavoritesManagerFactory;
import fr.soleil.bensikin.history.manager.HistoryManagerFactory;
import fr.soleil.bensikin.models.SnapshotListTableModel;
import fr.soleil.bensikin.options.manager.OptionsManagerFactory;
import fr.soleil.bensikin.tools.Messages;

/**
 * Contains the snapshot half of the application, ie. a list of snapshots
 * (SnapshotListPanel) and details about the selected snapshots
 * (SnapshotDetailPanel).
 * 
 * @author CLAISSE
 */
public final class SnapshotPanel extends JPanel {

    private static final long serialVersionUID = 2273394175860758052L;

    private final static Logger LOGGER = LoggerFactory.getLogger(SnapshotPanel.class);

    private static final int INITIAL_DETAIL_SPLIT_POSITION = 340;
    private static final int INITIAL_DETAIL_SPLIT_SIZE = 8;
    private static SnapshotPanel snapshotPanelInstance = null;

    private static JComponent list;
    private static JComponent details;

    private final JSplitPane splitPane;

    private boolean snapshotListVisible;
    private boolean snapshotDetailsVisible;
    private int contextId;
    private Context context;

    private boolean equipmentVisible;

    /**
     * Instantiates itself if necessary, returns the instance.
     * 
     * @return The instance
     */
    public static synchronized SnapshotPanel getInstance() {
        if (snapshotPanelInstance == null) {

            // do a full initialization if panel is used from outside Bensikin
            if (RightsManagerFactory.getCurrentImpl() == null) {

                // start factories
                TangoManagerFactory.getImpl(TangoManagerFactory.REAL_IMPL_TYPE);

                HistoryManagerFactory.getImpl(HistoryManagerFactory.XML_IMPL_TYPE);
                OptionsManagerFactory.getImpl(OptionsManagerFactory.XML_IMPL_TYPE);
                FavoritesManagerFactory.getImpl(FavoritesManagerFactory.XML_IMPL_TYPE);

                ContextManagerFactory.getImpl(ContextManagerFactory.XML_IMPL_TYPE);
                SnapshotManagerFactory.getImpl(SnapshotManagerFactory.XML_IMPL_TYPE);

                RightsManagerFactory.getImpl(RightsManagerFactory.SNAPSHOTS_ONLY_OPERATOR);

                // load messages
                final Locale currentLocale = new Locale("en", "US");
                try {
                    Messages.initResourceBundle(currentLocale);
                } catch (final Exception e) {
                    e.printStackTrace();
                }

            }

            // now that everything has been initialized, we can create panels
            list = new JScrollPane(SnapshotListPanel.getInstance());
            details = new JScrollPane(SnapshotDetailPanel.getInstance());

            snapshotPanelInstance = new SnapshotPanel();
        }

        return snapshotPanelInstance;
    }

    public void loadOptions() {
        Bensikin.loadOptions();
    }

    /**
     * Builds the panel
     */
    private SnapshotPanel() {
        super(new BorderLayout());
        snapshotListVisible = true;
        snapshotDetailsVisible = true;
        contextId = -1;
        context = null;
        equipmentVisible = true;
        splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true);
        splitPane.setDividerLocation(INITIAL_DETAIL_SPLIT_POSITION);
        splitPane.setDividerSize(INITIAL_DETAIL_SPLIT_SIZE);
        splitPane.setOneTouchExpandable(true);
        GUIUtilities.setObjectBackground(splitPane, GUIUtilities.SNAPSHOT_COLOR);

        final String msg = Messages.getMessage("SNAPSHOT_BORDER");
        final TitledBorder border = BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), msg, TitledBorder.DEFAULT_JUSTIFICATION,
                TitledBorder.TOP, GUIUtilities.getTitleFont());
        this.setBorder(border);

        GUIUtilities.setObjectBackground(this, GUIUtilities.SNAPSHOT_COLOR);

        updateLayout();
    }

    /**
     * Update the layout and repaint the panel after a component has been
     * hidden/displayed.
     */
    private void updateLayout() {
        removeAll();

        if (snapshotListVisible && snapshotDetailsVisible) {
            splitPane.setTopComponent(list);
            splitPane.setBottomComponent(details);
            add(splitPane, BorderLayout.CENTER);
        } else {
            if (snapshotListVisible) {
                splitPane.remove(list);
                add(list, BorderLayout.CENTER);
            } else if (snapshotDetailsVisible) {
                splitPane.remove(details);
                add(details, BorderLayout.CENTER);
            }
        }

        revalidate();
        repaint();
    }

    /**
     * Returns visibility state for the snapshot list.
     * 
     * @return true if the snapshot list is displayed, false otherwise
     */
    public boolean isSnapshotListVisible() {
        return snapshotListVisible;
    }

    /**
     * Changes visibility state for the snapshot list.
     * 
     * @param snapshotListVisible
     *            true to display the snapshot list, false to hide it
     */
    public void setSnapshotListVisible(final boolean snapshotListVisible) {
        this.snapshotListVisible = snapshotListVisible;

        updateLayout();
    }

    /**
     * Returns visibility state for the snapshot details panel.
     * 
     * @return true if the snapshot details panel is displayed, false otherwise
     */
    public boolean isSnapshotDetailsVisible() {
        return snapshotDetailsVisible;
    }

    /**
     * Changes visibility state for the snapshot details panel.
     * 
     * @param snapshotDetailsVisible
     *            true to display the snapshot details panel, false to hide it
     */
    public void setSnapshotDetailsVisible(final boolean snapshotDetailsVisible) {
        this.snapshotDetailsVisible = snapshotDetailsVisible;

        updateLayout();
    }

    /**
     * Returns the id of the current context.
     * 
     * @return the id of the current context if any, -1 if there is no context
     *         loaded
     */
    public int getContextID() {
        return contextId;
    }

    /**
     * Opens the context with the specified id, if it exists.
     * 
     * @param contextId
     *            the id of the context to open
     */
    public void setContextID(final int contextId) {
        this.contextId = contextId;

        context = new Context(contextId);
        Context.setSelectedContext(context);

        try {
            context.loadAttributes(null);

            Snapshot.reset(true, false);

            // preparing snapshot filter
            ContextTableListener.transferFilter();

            final FilterSnapshotsAction filterSnapshotsAction = FilterSnapshotsAction.getInstance();
            filterSnapshotsAction.setEnabled(true);

            final String msg = Messages.getLogMessage("LOAD_CONTEXT_ATTRIBUTES_OK");
            LOGGER.debug(msg);
        } catch (final SnapshotingException e) {
            clearContext();

            String msg;
            if (e.computeIsDueToATimeOut()) {
                msg = Messages.getLogMessage("LOAD_CONTEXT_ATTRIBUTES_TIMEOUT");
            } else {
                msg = Messages.getLogMessage("LOAD_CONTEXT_ATTRIBUTES_KO");
            }
            LOGGER.error(msg, e);
            JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(this),
                    Messages.getMessage("SNAPSHOT_ERROR_INVALID_CONTEXT"), Messages.getMessage("CONTEXT_ERROR_TITLE"),
                    JOptionPane.ERROR_MESSAGE);
            // } catch (final Exception e) {
            // clearContext();
            //
            // final String msg =
            // Messages.getLogMessage("LOAD_CONTEXT_ATTRIBUTES_KO");
            // LOGGER.error(msg, e);
        }
    }

    /**
     * Closes the context and all opened snapshots.
     */
    public void clearContext() {
        Context.removeOpenedContext(contextId);

        Snapshot.reset(true, false);

        final FilterSnapshotsAction filterSnapshotsAction = FilterSnapshotsAction.getInstance();
        filterSnapshotsAction.setEnabled(false);

        contextId = -1;
        context = null;
    }

    /**
     * Returns the ids of the opened snapshots.
     * 
     * @return an array of the opened snapshots' ids
     */
    public int[] getOpenedSnapshotsIDs() {
        // getOpenedSnapshots returns all the snapshots of the current context.
        // getSelectedSnapshots returns the opened snapshots in terms of
        // displayed tabs
        final Map<String, Snapshot> openedSnapshots = Snapshot.getSelectedSnapshots();
        final int[] snapshotIds = new int[openedSnapshots.size()];

        int index = 0;
        for (final Snapshot openedSnapshot : openedSnapshots.values()) {
            snapshotIds[index] = openedSnapshot.getSnapshotData().getId();
            index++;
        }

        return snapshotIds;
    }

    /**
     * Returns the id of the selected snapshot in tabbed pane, ie the currently
     * visible one.
     * 
     * @return the id of the currently visible snapshot
     */
    public int getSnapshotID() {
        int result = -1;

        final SnapshotDetailTabbedPane tabbedPane = SnapshotDetailTabbedPane.getInstance();
        final SnapshotDetailTabbedPaneContent selectedSnapshotDetailTabbedPaneContent = tabbedPane
                .getSelectedSnapshotDetailTabbedPaneContent();

        if (selectedSnapshotDetailTabbedPaneContent != null) {
            result = selectedSnapshotDetailTabbedPaneContent.getSnapshot().getSnapshotData().getId();
        }
        return result;
    }

    /**
     * Opens the snapshot with the specified id. A context must be opened, and
     * the snapshot must belong to the currently opened context.
     * 
     * @param snapshotId
     *            the id of the snapshot to open
     * 
     * 
     * @throws SnapshotingException
     */
    public void setSnapshotID(final int snapshotId) throws SnapshotingException {
        // if there is a currently opened context
        if (context != null) {
            // reset filter
            FilterSnapshotsAction.getInstance().actionPerformed(
                    new ActionEvent(SnapshotFilterPanel.getInstance().getResetButton(), ActionEvent.ACTION_PERFORMED,
                            ""));
            FilterSnapshotsAction.getInstance().actionPerformed(
                    new ActionEvent(new JButton(), ActionEvent.ACTION_PERFORMED, ""));
            // update panel with filter on ID
            final SnapshotFilterPanel source = SnapshotFilterPanel.getInstance();
            source.getTextId().setText(Integer.toString(snapshotId));
            source.getSelectId().setSelectedItem(SnapConst.OP_EQUALS);

            final SnapshotListTableModel modelToUpdate = (SnapshotListTableModel) SnapshotListTable.getInstance()
                    .getModel();
            final Snapshot[] snapshots = context.getSnapshots();
            Snapshot selectedSnapshot = null;
            if (snapshots != null) {
                for (final Snapshot snapshot : snapshots) {
                    if (snapshot.getSnapshotData().getId() == snapshotId) {
                        selectedSnapshot = snapshot;
                        modelToUpdate.updateList(selectedSnapshot);
                        // look for the specified snapshot id in the snapshots
                        // of the
                        // context
                        // if the snapshot belongs to the current context, open
                        // it
                        Snapshot.addSelectedSnapshot(selectedSnapshot);
                        selectedSnapshot.setLoadable(true);
                        final SnapshotDetailTabbedPane tabbedPane = SnapshotDetailTabbedPane.getInstance();
                        tabbedPane.addSnapshotDetail(selectedSnapshot);
                        FilterSnapshotsAction.getInstance().actionPerformed(
                                new ActionEvent(new JButton(), ActionEvent.ACTION_PERFORMED, ""));
                        LOGGER.debug(Messages.getLogMessage("OPEN_SNAPSHOT_OK"), snapshotId);
                        break;
                    }
                }

            }
            if (selectedSnapshot == null) {
                LOGGER.error(Messages.getLogMessage("OPEN_SNAPSHOT_INVALID"), snapshotId, contextId);
                JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(this),
                        Messages.getMessage("SNAPSHOT_ERROR_INVALID_SNAPSHOT"),
                        Messages.getMessage("SNAPSHOT_ERROR_TITLE"), JOptionPane.ERROR_MESSAGE);

            }

        } else {
            LOGGER.error(Messages.getLogMessage("OPEN_SNAPSHOT_NO_CONTEXT"), snapshotId);
            JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(this),
                    Messages.getMessage("SNAPSHOT_ERROR_NO_CONTEXT_SELECTED"),
                    Messages.getMessage("SNAPSHOT_ERROR_TITLE"), JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * @return true if equipment controls are visible, false otherwise
     */
    public boolean isEquipmentVisible() {
        return equipmentVisible;
    }

    /**
     * Allow to show or hide controls for equipment, including buttons and table
     * column.
     * 
     * @param equipmentVisible
     *            true to show equipment controls, false to hide it
     */
    public void setEquipmentVisible(final boolean equipmentVisible) {
        this.equipmentVisible = equipmentVisible;

        SnapshotDetailTabbedPane.getInstance().setEquipmentVisible(equipmentVisible);
    }

    public static void main(final String... args) {

        if (args == null || args.length < 1 || args.length > 3) {
            System.out.println("Incorrect arguments. Correct syntax: java SnapshotPanel userdb passdb userRights");
            System.exit(1);
        }

        final String pathToResources = System.getProperty(GUIUtilities.WORKING_DIR);
        Bensikin.setPathToResources(pathToResources);
        try {
            final File path = new File(pathToResources);
            if (!path.isDirectory()) {
                final boolean created = path.mkdirs();
                if (!created) {
                    throw new ArchivingException("Could not create the path");
                }
            }
            if (!path.canRead()) {
                throw new ArchivingException("Invalid path");
            }
            if (!path.canWrite()) {
                throw new ArchivingException("The path is read only");
            }
        } catch (final Exception e) {
            System.out.println("Incorrect arguments. The parameter " + pathToResources + " is not a valid directory");
            e.printStackTrace();
            System.exit(1);
        }

        final String user = args[0];
        final String password = args[1];

        final String userRights_s = args.length == 3 ? args[2] : null;
        Bensikin.setUserRights(userRights_s);

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    SnapManagerApi.initFullSnapConnection("", "", "", user, password, "");
                } catch (final SnapshotingException e) {
                    LOGGER.error("", e);
                }

                // create the test frame

                final SnapshotPanel snapshotPanel = SnapshotPanel.getInstance();

                final JToggleButton toggleList = new JToggleButton("Hide List");
                toggleList.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(final ActionEvent event) {
                        snapshotPanel.setSnapshotListVisible(!snapshotPanel.isSnapshotListVisible());
                    }
                });
                final JToggleButton toggleDetails = new JToggleButton("Hide Details");
                toggleDetails.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(final ActionEvent event) {
                        snapshotPanel.setSnapshotDetailsVisible(!snapshotPanel.isSnapshotDetailsVisible());
                    }
                });
                final JTextField contextField = new JTextField(3);
                contextField.addFocusListener(new FocusAdapter() {
                    @Override
                    public void focusGained(final FocusEvent event) {
                        contextField.selectAll();
                    }
                });
                final JButton contextButton = new JButton("Open Context");
                contextButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(final ActionEvent event) {
                        try {
                            final Integer contextId = Integer.valueOf(contextField.getText());
                            snapshotPanel.setContextID(contextId);

                        } catch (final NumberFormatException e1) {
                            // nop
                        }
                    }
                });
                contextField.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyPressed(final KeyEvent event) {
                        if (event.getKeyCode() == KeyEvent.VK_ENTER) {
                            contextButton.doClick();
                        }
                    }
                });
                final JButton clearContextButton = new JButton("Clear Context");
                clearContextButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(final ActionEvent event) {
                        snapshotPanel.clearContext();
                    }
                });
                final JTextField snapshotField = new JTextField(3);
                snapshotField.addFocusListener(new FocusAdapter() {
                    @Override
                    public void focusGained(final FocusEvent event) {
                        snapshotField.selectAll();
                    }
                });
                final JButton snapshotButton = new JButton("Open Snapshot");
                snapshotButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(final ActionEvent event) {
                        try {
                            final Integer snapshotId = Integer.valueOf(snapshotField.getText());
                            snapshotPanel.setSnapshotID(snapshotId);

                        } catch (final NumberFormatException e1) {
                            // nop
                        } catch (final SnapshotingException e) {
                            LOGGER.error("", e);
                        }
                    }
                });
                snapshotField.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyPressed(final KeyEvent event) {
                        if (event.getKeyCode() == KeyEvent.VK_ENTER) {
                            snapshotButton.doClick();
                        }
                    }
                });
                final JButton openedSnapshotButton = new JButton("Opened Snapshots");
                openedSnapshotButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(final ActionEvent event) {

                        final int[] openedSnapshotsIDs = snapshotPanel.getOpenedSnapshotsIDs();
                        final ArrayList<Integer> listIds = new ArrayList<Integer>(openedSnapshotsIDs.length);
                        for (final int id : openedSnapshotsIDs) {
                            listIds.add(id);
                        }
                        System.out.println("Opened snapshots= " + listIds);
                    }
                });
                final JToggleButton showHideEquipment = new JToggleButton("Equipment", true);
                showHideEquipment.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(final ActionEvent arg0) {
                        snapshotPanel.setEquipmentVisible(showHideEquipment.isSelected());
                    }
                });

                final Box buttonBox = new Box(BoxLayout.LINE_AXIS);
                buttonBox.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                buttonBox.add(Box.createHorizontalGlue());
                buttonBox.add(toggleList);
                buttonBox.add(Box.createHorizontalStrut(5));
                buttonBox.add(toggleDetails);
                buttonBox.add(Box.createHorizontalStrut(20));
                buttonBox.add(contextField);
                buttonBox.add(Box.createHorizontalStrut(5));
                buttonBox.add(contextButton);
                buttonBox.add(Box.createHorizontalStrut(10));
                buttonBox.add(clearContextButton);
                buttonBox.add(Box.createHorizontalStrut(20));
                buttonBox.add(snapshotField);
                buttonBox.add(Box.createHorizontalStrut(5));
                buttonBox.add(snapshotButton);
                buttonBox.add(Box.createHorizontalStrut(10));
                buttonBox.add(openedSnapshotButton);
                buttonBox.add(Box.createHorizontalStrut(10));
                buttonBox.add(showHideEquipment);
                buttonBox.add(Box.createHorizontalGlue());

                final JPanel content = new JPanel(new BorderLayout());
                content.add(snapshotPanel, BorderLayout.CENTER);
                content.add(buttonBox, BorderLayout.SOUTH);

                final JFrame frame = new JFrame("Test SnapshotPanel");
                frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                frame.setContentPane(content);
                frame.pack();
                frame.setVisible(true);
            }
        });

    }

}
