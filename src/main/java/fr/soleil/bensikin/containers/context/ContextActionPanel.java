// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/bensikin/bensikin/containers/context/ContextActionPanel.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class ContextActionPanel.
// (Claisse Laurent) - 16 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.9 $
//
// $Log: ContextActionPanel.java,v $
// Revision 1.9 2007/08/24 14:06:14 ounsy
// bug correction with context printing as text
//
// Revision 1.8 2007/08/23 15:28:48 ounsy
// Print Context as tree, table or text (Mantis bug 3913)
//
// Revision 1.7 2006/01/12 10:27:39 ounsy
// minor changes
//
// Revision 1.6 2005/12/14 16:19:08 ounsy
// removed the quick save button
//
// Revision 1.5 2005/11/29 18:25:08 chinkumo
// no message
//
// Revision 1.1.1.2 2005/08/22 11:58:35 chinkumo
// First commit
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.bensikin.containers.context;

import java.awt.Font;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.bensikin.Bensikin;
import fr.soleil.bensikin.actions.context.ContextDetailPrintChooseAction;
import fr.soleil.bensikin.actions.context.LaunchSnapshotAction;
import fr.soleil.bensikin.actions.context.RegisterContextAction;
import fr.soleil.bensikin.actions.context.SaveSelectedContextAction;
import fr.soleil.bensikin.components.context.detail.ContextAttributesTree;
import fr.soleil.bensikin.models.ContextAttributesTreeModel;
import fr.soleil.bensikin.tools.Messages;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Contains the buttons used to launch actions on the current context.
 * 
 * @author CLAISSE
 */
public class ContextActionPanel extends JPanel {

    private static final long serialVersionUID = -6324717904753618814L;

    private static final ImageIcon REGISTER_ICON = new ImageIcon(Bensikin.class.getResource("icons/register.gif"));
    private static final ImageIcon LAUNCH_ICON = new ImageIcon(Bensikin.class.getResource("icons/launch.gif"));
    private static final ImageIcon QUICK_SAVE_ICON = new ImageIcon(Bensikin.class.getResource("icons/quick_save.gif"));
    private static final ImageIcon PRINT_ICON = new ImageIcon(Bensikin.class.getResource("icons/print.gif"));
    private static final Font BUTTON_FONT = new Font("Arial", Font.PLAIN, 11);
    private static ContextActionPanel contextActionPanelInstance = null;

    private final LaunchSnapshotAction launchSnapshotAction;
    private final JButton registerContextButton;
    private final JButton printContextButton;
    private ContextDetailPrintChooseAction printAction;

    /**
     * Instantiates itself if necessary, returns the instance.
     * 
     * @return The instance
     */
    public static ContextActionPanel getInstance() {
        if (contextActionPanelInstance == null) {
            contextActionPanelInstance = new ContextActionPanel();
        }
        return contextActionPanelInstance;
    }

    /**
     * Builds the panel
     */
    private ContextActionPanel() {
        super();
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        String msg;

        msg = Messages.getMessage("CONTEXT_DETAIL_PRINT_TITLE");
        printAction = new ContextDetailPrintChooseAction(msg);

        printContextButton = new JButton(printAction);
        printContextButton.setIcon(PRINT_ICON);
        printContextButton.setMargin(CometeUtils.getzInset());
        printContextButton.setText(ObjectUtils.EMPTY_STRING);
        printContextButton.setToolTipText(msg);
        GUIUtilities.setObjectBackground(printContextButton, GUIUtilities.CONTEXT_CLIPBOARD_COLOR);

        msg = Messages.getMessage("CONTEXT_DETAIL_LAUNCH_SNAPSHOT");
        launchSnapshotAction = LaunchSnapshotAction.getInstance(msg);
        JButton launchSnapshotButton = new JButton(launchSnapshotAction);
        // Be sure to have the expected text, and not the one from the BensikinMenuBar
        launchSnapshotButton.setText(msg);
        launchSnapshotButton.setIcon(LAUNCH_ICON);
        launchSnapshotButton.setMargin(CometeUtils.getzInset());
        launchSnapshotButton.setFocusable(false);
        launchSnapshotButton.setFocusPainted(false);
        launchSnapshotButton.setFont(BUTTON_FONT);
        GUIUtilities.setObjectBackground(launchSnapshotButton, GUIUtilities.CONTEXT_COLOR);

        msg = Messages.getMessage("CONTEXT_DETAIL_REGISTER_CONTEXT");
        RegisterContextAction registerContextAction = RegisterContextAction.getInstance(msg);
        registerContextButton = new JButton(registerContextAction);
        registerContextButton.setIcon(REGISTER_ICON);
        registerContextButton.setMargin(CometeUtils.getzInset());
        registerContextButton.setFocusable(false);
        registerContextButton.setFocusPainted(false);
        registerContextButton.setFont(BUTTON_FONT);
        GUIUtilities.setObjectBackground(registerContextButton, GUIUtilities.CONTEXT_COLOR);

        msg = Messages.getMessage("CONTEXT_DETAIL_SAVE_CONTEXT");
        SaveSelectedContextAction saveSnapshotAction = new SaveSelectedContextAction(msg, true);
        JButton saveSnapshotButton = new JButton(saveSnapshotAction);
        saveSnapshotButton.setIcon(QUICK_SAVE_ICON);
        saveSnapshotButton.setMargin(CometeUtils.getzInset());
        saveSnapshotButton.setFocusable(false);
        saveSnapshotButton.setFocusPainted(false);
        saveSnapshotButton.setFont(BUTTON_FONT);
        GUIUtilities.setObjectBackground(saveSnapshotButton, GUIUtilities.CONTEXT_COLOR);

        add(Box.createHorizontalGlue());
        add(printContextButton);
        add(Box.createHorizontalGlue());
        add(registerContextButton);
        add(Box.createHorizontalGlue());
        add(launchSnapshotButton);
        add(Box.createHorizontalGlue());

        GUIUtilities.setObjectBackground(this, GUIUtilities.CONTEXT_COLOR);
    }

    /**
     * Updates the text of the "register" button
     */
    public void updateRegisterButton() {
        boolean changed = false;
        ContextAttributesTree tree = ContextAttributesTree.getInstance();
        if (tree != null) {
            ContextAttributesTreeModel model = (ContextAttributesTreeModel) tree.getModel();
            changed = (model != null) && model.isAttributeListChanged();
        }
        String msg = Messages
                .getMessage(changed ? "CONTEXT_DETAIL_REGISTER_CONTEXT" : "CONTEXT_DETAIL_REGISTER_CONTEXT_AS");
        registerContextButton.setText(msg);
    }

    /**
     * Resets the text of the "register" button to the default text
     */
    public void resetRegisterButton() {
        String msg = Messages.getMessage("CONTEXT_DETAIL_REGISTER_CONTEXT");
        registerContextButton.setText(msg);
    }

    /**
     * Calls the printAction
     */
    public void openPrintDialog() {
        if (printAction != null) {
            printAction.actionPerformed(null);
        }
    }

    public void allowPrint(boolean allowed) {
        if (printAction != null) {
            printAction.setEnabled(allowed);
        }
    }

}
