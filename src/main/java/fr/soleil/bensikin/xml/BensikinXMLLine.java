// +======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/xml/BensikinXMLLine.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  BensikinXMLLine.
//						(Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.4 $
//
// $Log: BensikinXMLLine.java,v $
// Revision 1.4  2006/03/21 11:25:50  ounsy
// added a getItemName method
//
// Revision 1.3  2005/11/29 18:25:13  chinkumo
// no message
//
// Revision 1.1.1.2  2005/08/22 11:58:42  chinkumo
// First commit
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.xml;

import java.util.Map;

import fr.soleil.bensikin.history.History;
import fr.soleil.lib.project.xmlhelpers.XMLLine;

/**
 * An object representation of an XML line. The line can either be an opening
 * line <tag>, a closing line </tag>, or an empty line <tag />. The XML
 * attributes can be set one by one, then a call to toString gives the String
 * representation.
 * 
 * @author CLAISSE
 */
public class BensikinXMLLine extends XMLLine {

    /**
     * Builds a XMLLine with this name. By default the line is an empty line.
     * 
     * @param name
     *            The tag name
     */
    public BensikinXMLLine(String name) {
        super(name);
    }

    /**
     * Builds a XMLLine with this name.
     * 
     * @param name The tag name
     * @param tagCategory The line category
     * @throws IllegalArgumentException If _tagCategory is not in (EMPTY_TAG_CATEGORY, OPENING_TAG_CATEGORY,
     *             CLOSING_TAG_CATEGORY)
     */
    public BensikinXMLLine(String name, int tagCategory) throws IllegalArgumentException {
        super(name, tagCategory);
    }

    /**
     * Builds a XMLLine with this name, and pre-filled attributes. By default
     * the line is an empty line.
     * 
     * @param name
     * @param attributes
     */
    public BensikinXMLLine(String name, Map<String, String> attributes) {
        super(name, attributes);
    }

    /**
     * Calls setAttribute ( History.ID_KEY , id )
     * 
     * @param id The id value
     */
    public void setId(String id) {
        setAttribute(History.ID_KEY, id);
    }

    /**
     * Calls getAttribute ( History.ID_KEY )
     * 
     * @return The id value
     */
    public String getId() {
        return getAttribute(History.ID_KEY);
    }

    /**
     * Calls setAttribute ( History.LABEL_KEY , id )
     * 
     * @param id
     *            The id value
     */
    public void setLabel(String id) {
        setAttribute(History.LABEL_KEY, id);
    }

    /**
     * Calls getAttribute ( History.LABEL_KEY )
     * 
     * @return The id value
     */
    public String getLabel() {
        return getAttribute(History.LABEL_KEY);
    }

    public String getItemName() {
        String id = this.getId();
        String label = this.getLabel();

        String itemName;
        if (label != null && !label.trim().isEmpty()) {
            itemName = label + " (" + id + ")";
        } else {
            itemName = id;
        }

        return itemName;
    }

}
