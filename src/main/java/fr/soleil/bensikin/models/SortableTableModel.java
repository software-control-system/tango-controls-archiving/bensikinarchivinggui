package fr.soleil.bensikin.models;

import javax.swing.table.TableModel;

import fr.soleil.bensikin.components.BensikinComparator;

public interface SortableTableModel extends TableModel {

    /**
     * Returns the index of the currently sorted column
     * 
     * @return An <code>in</code>
     */
    public int getSortedColumn();

    /**
     * Returns the type of sort used on sorted column
     * 
     * @return An <code>int</code>
     * @see BensikinComparator#SORT_UP
     * @see BensikinComparator#SORT_DOWN
     * @see BensikinComparator#NO_SORT
     */
    public int getIdSort();
}
