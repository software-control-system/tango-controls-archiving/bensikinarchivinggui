package fr.soleil.bensikin.models;

import javax.swing.table.DefaultTableModel;

public class ColumnHeaderModel extends DefaultTableModel {

    private static final long serialVersionUID = -8781773342590377939L;

    private int rows;

    public ColumnHeaderModel(int rowCount) {
        super();
        rows = rowCount;
    }

    @Override
    public int getRowCount() {
        return rows;
    }

    @Override
    public int getColumnCount() {
        return 1;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return Integer.toString(rowIndex + 1);
    }

    @Override
    public String getColumnName(int columnIndex) {
        return "";
    }

}
