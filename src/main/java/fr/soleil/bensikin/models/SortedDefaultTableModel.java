package fr.soleil.bensikin.models;

import javax.swing.table.DefaultTableModel;

import fr.soleil.bensikin.components.BensikinComparator;

public class SortedDefaultTableModel extends DefaultTableModel implements SortableTableModel {

    private static final long serialVersionUID = 5421409965486402606L;

    protected volatile int idSort;
    protected volatile int sortedColumn;

    public SortedDefaultTableModel() {
        idSort = BensikinComparator.NO_SORT;
        sortedColumn = -1;
    }

    @Override
    public int getSortedColumn() {
        return sortedColumn;
    }

    @Override
    public int getIdSort() {
        return idSort;
    }

}
