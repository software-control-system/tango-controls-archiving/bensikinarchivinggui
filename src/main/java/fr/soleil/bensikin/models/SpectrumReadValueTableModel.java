package fr.soleil.bensikin.models;

import fr.esrf.TangoDs.TangoConst;
import fr.soleil.bensikin.data.snapshot.SnapshotAttributeValue;

public class SpectrumReadValueTableModel extends SpectrumWriteValueTableModel {

    private static final long serialVersionUID = 3445266777825723265L;

    public SpectrumReadValueTableModel(int dataType, Object spectrumValues, boolean[] nullElements) {
        super(dataType, spectrumValues, nullElements);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value;
        if (columnIndex == 0) {
            value = "" + (rowIndex + 1);
        } else {
            if ((values != null) && (!"NaN".equals(values))) {
                switch (type) {
                case TangoConst.Tango_DEV_STATE:
                    value = new SnapshotAttributeValue(SnapshotAttributeValue.SCALAR_DATA_FORMAT, type,
                            ((int[]) values)[rowIndex], (nullElements == null ? null : nullElements.clone()));
                    break;
                case TangoConst.Tango_DEV_STRING:
                    value = new SnapshotAttributeValue(SnapshotAttributeValue.SCALAR_DATA_FORMAT, type, new String(
                            ((String[]) values)[rowIndex]), (nullElements == null ? null : nullElements.clone()));
                    break;
                case TangoConst.Tango_DEV_BOOLEAN:
                    value = new SnapshotAttributeValue(SnapshotAttributeValue.SCALAR_DATA_FORMAT, type,
                            ((boolean[]) values)[rowIndex], (nullElements == null ? null : nullElements.clone()));
                    break;
                case TangoConst.Tango_DEV_UCHAR:
                case TangoConst.Tango_DEV_CHAR:
                    value = "" + ((byte[]) values)[rowIndex];
                    break;
                case TangoConst.Tango_DEV_ULONG:
                case TangoConst.Tango_DEV_LONG:
                    value = "" + ((int[]) values)[rowIndex];
                    break;
                case TangoConst.Tango_DEV_USHORT:
                case TangoConst.Tango_DEV_SHORT:
                    value = "" + ((short[]) values)[rowIndex];
                    break;
                case TangoConst.Tango_DEV_FLOAT:
                    value = "" + ((float[]) values)[rowIndex];
                    break;
                case TangoConst.Tango_DEV_DOUBLE:
                    value = "" + ((double[]) values)[rowIndex];
                    break;
                default:
                    value = "";
                    break;
                }
            } else {
                value = "";
            }
        }
        return value;
    }
}
