package fr.soleil.bensikin.models;

import fr.soleil.bensikin.tools.Messages;

public class SpectrumDeltaValueTableModel extends SpectrumWriteValueTableModel {

    private static final long serialVersionUID = -7176446835446182566L;

    public SpectrumDeltaValueTableModel(int dataType, Object spectrumValues, boolean[] nullElements) {
        super(dataType, spectrumValues, nullElements);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String name;
        if (columnIndex == 0) {
            name = Messages.getMessage("DIALOGS_SPECTRUM_ATTRIBUTE_INDEX");
        } else if (columnIndex == 1) {
            name = Messages.getMessage("DIALOGS_SPECTRUM_ATTRIBUTE_SUBDELTA");
        } else {
            name = "";
        }
        return name;
    }
}
