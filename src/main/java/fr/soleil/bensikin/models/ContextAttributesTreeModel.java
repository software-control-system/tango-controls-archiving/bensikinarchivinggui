// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/bensikin/bensikin/models/ContextAttributesTreeModel.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class ContextAttributesTreeModel.
// (Claisse Laurent) - 30 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.9 $
//
// $Log: ContextAttributesTreeModel.java,v $
// Revision 1.9 2007/08/24 14:09:12 ounsy
// Context attributes ordering (Mantis bug 3912)
//
// Revision 1.8 2007/08/23 13:00:21 ounsy
// minor changes
//
// Revision 1.7 2006/06/28 12:53:20 ounsy
// minor changes
//
// Revision 1.6 2006/01/12 12:59:11 ounsy
// minor changes
//
// Revision 1.5 2005/12/14 16:42:30 ounsy
// added methods necessary for alternate attribute selection
//
// Revision 1.4 2005/11/29 18:25:13 chinkumo
// no message
//
// Revision 1.1.1.2 2005/08/22 11:58:40 chinkumo
// First commit
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.bensikin.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.model.AttributesSelectTableModel;
import fr.soleil.bensikin.comparators.BensikinToStringObjectComparator;
import fr.soleil.bensikin.components.context.detail.ContextAttributesTree;
import fr.soleil.bensikin.components.context.detail.PossibleAttributesTree;
import fr.soleil.bensikin.containers.context.ContextActionPanel;
import fr.soleil.bensikin.containers.context.ContextDetailPanel;
import fr.soleil.bensikin.data.context.ContextAttribute;
import fr.soleil.lib.project.ObjectUtils;

/**
 * A daughter of AttributesTreeModel representing the attributes of the current
 * context.
 * <UL>
 * <LI>Attributes can be chosen from the Tango attributes list and added to this model
 * <LI>Attributes can be removed from this model
 * </UL>
 * 
 * @author CLAISSE
 */
public class ContextAttributesTreeModel extends AttributesTreeModel {

    private static final long serialVersionUID = 7973888977887098597L;

    private static ContextAttributesTreeModel instance = null;
    private ContextAttributesTree contextAttributesTreeInstance;

    // boolean useful to know whether attribute list did change, which means a new Context should be created in database
    // (TANGOARCH-629)
    private boolean attributeListChanged;

    /**
     * Calls mother constructor
     */
    private ContextAttributesTreeModel() {
        super();
        contextAttributesTreeInstance = null;
        attributeListChanged = true;
    }

    /**
     * Instantiates itself if necessary, returns the instance.
     */
    public static ContextAttributesTreeModel getInstance(boolean force) {
        if (instance == null || force) {
            instance = new ContextAttributesTreeModel();
        }

        return instance;
    }

    /**
     * Sets the tree reference of this model. Has to be used if one wishes to
     * automatically make each new added attribute visible.
     * 
     * @param tree
     *            The tree this is a model of.
     */
    public void setTree(ContextAttributesTree tree) {
        this.contextAttributesTreeInstance = tree;
    }

    /**
     * Adds the following list of attributes.
     * 
     * @param validSelected
     *            A list of TreePath references to attributes. All TreePath that
     *            point to a non-attribute node will be ignored.
     */
    public List<TreePath> addSelectedAttributes(Collection<TreePath> validSelected, boolean updateTableToo) {
        AttributesSelectTableModel attributesSelectTableModel = ContextDetailPanel.getInstance()
                .getAttributeTableSelectionBean().getSelectionPanel().getAttributesSelectTable().getModel();

        List<TreePath> paths = new ArrayList<>();

        TreeMap<String, ContextAttribute> possibleAttr = null;
        try {
            possibleAttr = ((AttributesTreeModel) PossibleAttributesTree.getInstance().getModel()).getAttributes();
        } catch (Exception e) {
            // nothing to do
            possibleAttr = null;
        }

        TreeMap<String, ContextAttribute> htAttr = getAttributes();

        for (TreePath aPath : validSelected) {
            // START CURRENT SELECTED PATH
            DefaultMutableTreeNode currentTreeObject = null;
            for (int depth = 0; depth < CONTEXT_TREE_DEPTH; depth++) {
                Object currentPathObject = aPath.getPathComponent(depth);

                if (depth == 0) {
                    currentTreeObject = (DefaultMutableTreeNode) getRoot();
                    continue;
                }

                int numOfChildren = getChildCount(currentTreeObject);
                List<String> vect = new ArrayList<>();
                for (int childIndex = 0; childIndex < numOfChildren; childIndex++) {
                    Object childAt = getChild(currentTreeObject, childIndex);
                    vect.add(childAt.toString());
                }

                if (vect.contains(currentPathObject.toString())) {
                    int pos = vect.indexOf(currentPathObject.toString());
                    currentTreeObject = (DefaultMutableTreeNode) getChild(currentTreeObject, pos);
                } else {
                    vect.add(currentPathObject.toString());
                    Collections.sort(vect, new BensikinToStringObjectComparator());
                    DefaultMutableTreeNode newChild = new DefaultMutableTreeNode(currentPathObject.toString());
                    int index = vect.indexOf(currentPathObject.toString());
                    insertNodeInto(newChild, currentTreeObject, index);

                    paths.add(new TreePath(newChild.getPath()));

                    if (depth == CONTEXT_TREE_DEPTH - 1) {
                        TreeNode[] path = newChild.getPath();

                        ContextAttribute attribute = (possibleAttr == null) ? null
                                : (ContextAttribute) possibleAttr.get(translatePathIntoKey(path));
                        if (attribute == null) {
                            attribute = new ContextAttribute(path);
                        }

                        ContextAttribute old = htAttr.put(translatePathIntoKey(path), attribute);
                        if (!ObjectUtils.sameObject(old, attribute)) {
                            attributeListChanged = true;
                        }

                        // ----------------
                        if (contextAttributesTreeInstance != null) {
                            TreePath treePath = new TreePath(path);
                            contextAttributesTreeInstance.expandPath(treePath);
                            contextAttributesTreeInstance.makeVisible(treePath);
                        }
                        // ----------------
                    }

                    currentTreeObject = newChild;
                }
            }
        }
        attributesHT = htAttr;
        if (updateTableToo) {
            Collection<ContextAttribute> values = getAttributes().values();
            ContextAttribute[] attrArray = values.toArray(new ContextAttribute[values.size()]);
            attributesSelectTableModel.setRows(attrArray);
        }
        return paths;
    }

    public void addSelectedAttributes(ContextAttribute[] attributesToAdd, boolean updateTableToo) {
        if (attributesToAdd != null) {
            AttributesSelectTableModel attributesSelectTableModel = ContextDetailPanel.getInstance()
                    .getAttributeTableSelectionBean().getSelectionPanel().getAttributesSelectTable().getModel();

            TreeMap<String, ContextAttribute> htAttr = getAttributes();

            for (ContextAttribute attribute : attributesToAdd) {
                if (attribute != null) {
                    TreePath aPath = attribute.getTreePath();

                    // START CURRENT SELECTED PATH
                    DefaultMutableTreeNode currentTreeObject = null;
                    for (int depth = 0; depth < CONTEXT_TREE_DEPTH; depth++) {
                        Object currentPathObject = aPath.getPathComponent(depth);

                        if (depth == 0) {
                            currentTreeObject = (DefaultMutableTreeNode) getRoot();
                            continue;
                        }

                        int numOfChildren = getChildCount(currentTreeObject);
                        List<String> vect = new ArrayList<>();
                        for (int childIndex = 0; childIndex < numOfChildren; childIndex++) {
                            Object childAt = getChild(currentTreeObject, childIndex);
                            vect.add(childAt.toString());
                        }

                        if (vect.contains(currentPathObject.toString())) {
                            int pos = vect.indexOf(currentPathObject.toString());
                            currentTreeObject = (DefaultMutableTreeNode) getChild(currentTreeObject, pos);
                        } else {
                            vect.add(currentPathObject.toString());
                            Collections.sort(vect, new BensikinToStringObjectComparator());
                            DefaultMutableTreeNode newChild = new DefaultMutableTreeNode(currentPathObject.toString());
                            int index = vect.indexOf(currentPathObject.toString());
                            insertNodeInto(newChild, currentTreeObject, index);

                            if (depth == CONTEXT_TREE_DEPTH - 1) {
                                TreeNode[] path = newChild.getPath();

                                ContextAttribute old = htAttr.put(translatePathIntoKey(path), attribute);
                                if (!ObjectUtils.sameObject(old, attribute)) {
                                    attributeListChanged = true;
                                }

                                // ----------------
                                if (contextAttributesTreeInstance != null) {
                                    TreePath treePath = new TreePath(path);
                                    contextAttributesTreeInstance.makeVisible(treePath);
                                }
                                // ----------------
                            }

                            currentTreeObject = newChild;
                        }
                    }
                }
            }

            super.attributesHT = htAttr;
            if (updateTableToo) {
                Collection<ContextAttribute> values = getAttributes().values();
                ContextAttribute[] attrArray = values.toArray(new ContextAttribute[values.size()]);
                attributesSelectTableModel.setRows(attrArray);
            }
        }
    }

    /**
     * Removes a list of attributes from the model.
     * 
     * @param validSelected A list of TreePath references to the attributes that should be removed
     */
    public void removeSelectedAttributes(Collection<TreePath> validSelected) {
        AttributesSelectTableModel attributesSelectTableModel = ContextDetailPanel.getInstance()
                .getAttributeTableSelectionBean().getSelectionPanel().getAttributesSelectTable().getModel();

        for (TreePath aPath : validSelected) {
            MutableTreeNode node = (MutableTreeNode) aPath.getLastPathComponent();

            String key = aPath.getPathComponent(1) + GUIUtilities.TANGO_DELIM + aPath.getPathComponent(2)
                    + GUIUtilities.TANGO_DELIM + aPath.getPathComponent(3) + GUIUtilities.TANGO_DELIM
                    + aPath.getPathComponent(4);

            // to do global remove
            if (getAttributes().remove(key) != null) {
                attributeListChanged = true;
            }
            try {
                removeNodeFromParent(node);
            } catch (Exception e) {
                // do nothing
            }
        }

        Collection<ContextAttribute> values = getAttributes().values();
        ContextAttribute[] attrArray = values.toArray(new ContextAttribute[values.size()]);

        attributesSelectTableModel.setRows(attrArray);
    }

    @Override
    public void build(List<Domain> domains) {
        super.build(domains);
        attributeListChanged = false;
        ContextActionPanel.getInstance().updateRegisterButton();
    }

    @Override
    public void removeAll() {
        super.removeAll();
        attributeListChanged = true;
    }

    /**
     * Returns whether attribute list did change.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isAttributeListChanged() {
        return attributeListChanged;
    }

    /**
     * @param validSelected
     *            29 juin 2005
     */
    public void removeSelectedAttributes2(Collection<TreePath> validSelected) {
        for (TreePath aPath : validSelected) {
            DefaultMutableTreeNode currentTreeObject = null;
            for (int depth = 0; depth < CONTEXT_TREE_DEPTH; depth++) {
                String userObjectRef = (String) aPath.getPathComponent(depth);

                if (depth == 0) {
                    currentTreeObject = (DefaultMutableTreeNode) getRoot();
                    continue;
                }

                int numOfChildren = getChildCount(currentTreeObject);
                for (int childIndex = 0; childIndex < numOfChildren; childIndex++) {
                    DefaultMutableTreeNode childAt = (DefaultMutableTreeNode) getChild(currentTreeObject, childIndex);
                    String userObjectAt = (String) childAt.getUserObject();
                    if (userObjectRef.equals(userObjectAt)) {
                        currentTreeObject = childAt;
                        break;
                    }
                }
            }

            TreeNode[] path = currentTreeObject.getPath();
            removeNodeFromParent(currentTreeObject);

            if (getAttributes().remove(translatePathIntoKey(path)) != null) {
                attributeListChanged = true;
            }
        }
    }
}
