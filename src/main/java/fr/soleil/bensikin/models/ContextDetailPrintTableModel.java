package fr.soleil.bensikin.models;

import javax.swing.table.DefaultTableModel;

import fr.soleil.archiving.tango.entity.model.AttributesSelectTableModel;
import fr.soleil.bensikin.containers.context.ContextDetailPanel;

public class ContextDetailPrintTableModel extends DefaultTableModel {

    private static final long serialVersionUID = 3638923027466144999L;

    public ContextDetailPrintTableModel() {
        super();
    }

    @Override
    public int getColumnCount() {
        int count;
        AttributesSelectTableModel model = ContextDetailPanel.getInstance().getAttributeTableSelectionBean()
                .getSelectionPanel().getAttributesSelectTable().getModel();
        if ((model != null) && (model.getColumnCount() > 0)) {
            count = model.getColumnCount() - 1;
        } else {
            count = 0;
        }
        return count;
    }

    @Override
    public int getRowCount() {
        AttributesSelectTableModel model = ContextDetailPanel.getInstance().getAttributeTableSelectionBean()
                .getSelectionPanel().getAttributesSelectTable().getModel();
        if (model != null) {
            return model.getRowCount();
        }
        return 0;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value = null;
        AttributesSelectTableModel model = ContextDetailPanel.getInstance().getAttributeTableSelectionBean()
                .getSelectionPanel().getAttributesSelectTable().getModel();
        if (model != null) {
            value = model.getValueAt(rowIndex, columnIndex);
        }
        return value;
    }

    @Override
    public String getColumnName(int column) {
        String name;
        AttributesSelectTableModel model = ContextDetailPanel.getInstance().getAttributeTableSelectionBean()
                .getSelectionPanel().getAttributesSelectTable().getModel();
        if (model != null) {
            name = model.getColumnName(column);
        } else {
            name = "";
        }
        return name;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
