// +======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/models/ContextListTableModel.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  ContextListTableModel.
//						(Claisse Laurent) - 30 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.5 $
//
// $Log: ContextListTableModel.java,v $
// Revision 1.5  2006/06/28 12:53:20  ounsy
// minor changes
//
// Revision 1.4  2005/11/29 18:25:13  chinkumo
// no message
//
// Revision 1.1.1.2  2005/08/22 11:58:40  chinkumo
// First commit
//
//
// copyleft :		Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.soleil.bensikin.components.context.ContextDataComparator;
import fr.soleil.bensikin.data.context.Context;
import fr.soleil.bensikin.data.context.ContextData;
import fr.soleil.bensikin.models.listeners.ContextTableModelListener;
import fr.soleil.bensikin.tools.Messages;

/**
 * The table model used by ContextListTable, this model lists the current list
 * of contexts. Its rows are ContextData objects. A singleton class.
 * 
 * @author CLAISSE
 */
public class ContextListTableModel extends SortedDefaultTableModel {

    private static final long serialVersionUID = -5430440838487668790L;

    private ContextData[] rows;
    private final String[] columnsNames;

    private static ContextListTableModel instance = new ContextListTableModel();

    /**
     * Instantiates itself if necessary, returns the instance.
     * 
     * @return The instance
     */
    public static ContextListTableModel getInstance() {
        return instance;
    }

    /**
     * Forces a new instantiation, used to reset the model.
     * 
     * @return The new instance
     */
    public static ContextListTableModel forceReset() {
        instance = new ContextListTableModel();
        return instance;
    }

    /**
     * Returns the ContextData located at row <code>rowIndex</code>.
     * 
     * @param rowIndex
     *            The specified row
     * @return The ContextData located at row <code>rowIndex</code>
     */
    public ContextData getContextAtRow(int rowIndex) {
        ContextData result;
        if (rows != null) {
            result = rows[rowIndex];
        } else {
            result = null;
        }
        return result;
    }

    /**
     * Initializes the columns titles, and adds a ContextTableModelListener to
     * itself.
     */
    private ContextListTableModel() {
        String msgId = Messages.getMessage("CONTEXT_LIST_COLUMNS_ID");
        String msgTime = Messages.getMessage("CONTEXT_LIST_COLUMNS_TIME");
        String msgName = Messages.getMessage("CONTEXT_LIST_COLUMNS_NAME");
        String msgAuthor = Messages.getMessage("CONTEXT_LIST_COLUMNS_AUTHOR");
        String msgReason = Messages.getMessage("CONTEXT_LIST_COLUMNS_REASON");
        String msgDescription = Messages.getMessage("CONTEXT_LIST_COLUMNS_DESCRIPTION");

        columnsNames = new String[this.getColumnCount()];
        columnsNames[0] = msgId;
        columnsNames[1] = msgTime;
        columnsNames[2] = msgName;
        columnsNames[3] = msgAuthor;
        columnsNames[4] = msgReason;
        columnsNames[5] = msgDescription;

        this.addTableModelListener(new ContextTableModelListener());
    }

    /**
     * Removes all rows and refreshes the model.
     */
    public void reset() {
        if (this.getRowCount() != 0) {
            int firstRemoved = 0;
            int lastRemoved = this.getRowCount() - 1;

            this.rows = null;

            this.fireTableRowsDeleted(firstRemoved, lastRemoved);
        }
    }

    /**
     * Removes all rows which indexes are found in <code>indexesToRemove</code>.
     * 
     * @param indexesToRemove
     *            The list of rows to remove
     */
    public void removeRows(final int... indexesToRemove) {
        if (indexesToRemove != null) {
            ContextData[] formerRows = rows;
            if (formerRows != null) {
                List<ContextData> newRowList = new ArrayList<ContextData>(formerRows.length - indexesToRemove.length);
                for (ContextData row : formerRows) {
                    newRowList.add(row);
                }
                for (int index : indexesToRemove) {
                    if ((index > -1) && (index < formerRows.length)) {
                        newRowList.remove(formerRows[index]);
                    }
                }
                rows = newRowList.toArray(new ContextData[newRowList.size()]);
                newRowList.clear();
                fireTableDataChanged();
            }
        }
    }

    /**
     * Completely sets the model by specifying its rows.
     * 
     * @param rows The new rows
     */
    public void setRows(ContextData[] rows) {
        int count = getRowCount();
        this.rows = rows;
//        this.fireTableRowsInserted(0, rows.length - 1);
        if (count == 0) {
            fireTableRowsInserted(0, rows.length - 1);
        } else if (count == getRowCount()) {
            fireTableDataChanged();
        } else {
            fireTableStructureChanged();
        }
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public int getRowCount() {
        int rowCount;
        ContextData[] rows = this.rows;
        if (rows == null) {
            rowCount = 0;
        } else {
            rowCount = rows.length;
        }
        return rowCount;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value = null;
        ContextData[] rows = this.rows;
        ContextData row;
        if ((rows == null) || (rowIndex < 0) || (rowIndex > rows.length)) {
            row = null;
        } else {
            row = rows[rowIndex];
        }
        if (row != null) {
            switch (columnIndex) {
                case 0:
                    value = Integer.valueOf(row.getId());
                    break;
                case 1:
                    value = row.getCreationDate();
                    break;
                case 2:
                    value = row.getName();
                    break;
                case 3:
                    value = row.getAuthorName();
                    break;
                case 4:
                    value = row.getReason();
                    break;
                case 5:
                    value = row.getDescription();
                    break;
                default:
                    value = null;
            }
        }
        return value;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnsNames[columnIndex];
    }

    /**
     * Sorts the table's lines relative to the specified column. If the the
     * table is already sorted relative to this column, reverses the sort.
     * 
     * @param clickedColumnIndex
     *            The index of the column to sort the lines by
     */
    public void sort(int clickedColumnIndex) {
        sortedColumn = clickedColumnIndex;
        switch (clickedColumnIndex) {
            case 0:
                sortByColumn(ContextDataComparator.COMPARE_ID);
                break;
            case 1:
                sortByColumn(ContextDataComparator.COMPARE_TIME);
                break;
            case 2:
                sortByColumn(ContextDataComparator.COMPARE_NAME);
                break;
            case 3:
                sortByColumn(ContextDataComparator.COMPARE_AUTHOR);
                break;
            case 4:
                sortByColumn(ContextDataComparator.COMPARE_REASON);
                break;
            case 5:
                sortByColumn(ContextDataComparator.COMPARE_DESCRIPTION);
                break;
        }
    }

    /**
     * Sorts the table's lines relative to the specified field. If the the table
     * is already sorted relative to this column, reverses the sort.
     * 
     * @param compareCase
     *            The type of field to sort the lines by
     */
    private void sortByColumn(int compareCase) {
        ContextData[] formerRows = rows;
        if (formerRows != null) {
            int newSortType = ContextDataComparator.getNewSortType(this.idSort);
            List<ContextData> v = new ArrayList<ContextData>();
            for (ContextData data : formerRows) {
                v.add(data);
            }
            Collections.sort(v, new ContextDataComparator(compareCase));
            if (newSortType == ContextDataComparator.SORT_DOWN) {
                Collections.reverse(v);
            }
            ContextData[] newRows = v.toArray(new ContextData[v.size()]);
            v.clear();
            this.rows = newRows;
            this.idSort = newSortType;
            fireTableDataChanged();
        }
    }

    /**
     * Adds a line at the end of the current model for the specified context,
     * and refreshes the list.
     * 
     * @param savedContext
     *            The context to add to the current list
     */
    public void addToList(Context savedContext) {
        ContextData[] before = this.rows;
        if (before == null) {
            before = new ContextData[0];
        }
        ContextData[] rows = new ContextData[before.length + 1];
        for (int i = 0; i < before.length; i++) {
            rows[i] = before[i];
        }
        rows[before.length] = savedContext.getContextData();
        this.rows = rows;
        fireTableRowsInserted(before.length, before.length);
    }

    public void updateList(Context savedContext) {
        for (int i = 0; i < rows.length; i++) {
            if (rows[i].getId() == savedContext.getContextId()) {
                rows[i] = savedContext.getContextData();
                break;
            }
        }
        this.fireTableRowsUpdated(0, rows.length);
    }

}
