// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/bensikin/bensikin/models/AttributesTreeModel.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class AttributesTreeModel.
// (Claisse Laurent) - 30 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.6 $
//
// $Log: AttributesTreeModel.java,v $
// Revision 1.6 2007/08/24 14:09:12 ounsy
// Context attributes ordering (Mantis bug 3912)
//
// Revision 1.5 2005/12/14 16:41:59 ounsy
// added a removeAll method to clean the tree
//
// Revision 1.4 2005/11/29 18:25:13 chinkumo
// no message
//
// Revision 1.1.1.2 2005/08/22 11:58:40 chinkumo
// First commit
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.bensikin.models;

import java.text.Collator;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.Family;
import fr.soleil.archiving.tango.entity.Member;
import fr.soleil.archiving.tango.entity.ui.comparator.EntitiesComparator;
import fr.soleil.bensikin.data.context.ContextAttribute;

/**
 * The mother class of all tree models of the application. It implements methods to add/get attributes, and keeps track
 * of the current attributes in a Map, which keys are the attributes paths. A Map
 * 
 * @author CLAISSE
 */
public class AttributesTreeModel extends DefaultTreeModel {

    private static final long serialVersionUID = -3716882502771514877L;

    /**
     * The list of Domains for this model
     */
    protected List<Domain> domains;
    /**
     * The attributes TreeMap. Each key is the String representations of an
     * attribute's path, and each value is the corresponding ContextAttribute.
     */
    protected TreeMap<String, ContextAttribute> attributesHT;

    private static String DEFAULT_ROOT_NAME = "ROOT";
    public static final int CONTEXT_TREE_DEPTH = 5;

    /**
     * Returns a node representing the TANGO_HOST property in the local system.
     * 
     * @return A node representing the TANGO_HOST property in the local system
     */
    private static DefaultMutableTreeNode findRootInSystemProperties() {
        String rootName = System.getProperty("TANGO_HOST");

        if (rootName == null) {
            rootName = DEFAULT_ROOT_NAME;
        }

        return new DefaultMutableTreeNode(rootName);
    }

    /**
     * Initializes the root and the attributes Map.
     */
    public AttributesTreeModel() {
        super(findRootInSystemProperties());
        attributesHT = new TreeMap<>(Collator.getInstance());
    }

    /**
     * Builds the model for the given Domains:
     * <UL>
     * <LI>Adds a node for each Domain to the root
     * <LI>For each Domain, adds all of its Family nodes
     * <LI>For each Family, adds all of its Members nodes
     * <LI>For each Member, adds all of its Attributes nodes (provided the attributes have been loaded yet)
     * </UL>
     * 
     * @param domains The Domains to display
     */
    public void build(final List<Domain> domains) {
        this.attributesHT = new TreeMap<>(Collator.getInstance());
        this.domains = domains;
        if (domains != null) {
            Collections.sort(domains, new EntitiesComparator());

            for (Domain domain : domains) {
                // START CURRENT DOMAIN
                final DefaultMutableTreeNode domainNode = new DefaultMutableTreeNode(domain.getName());

                ((DefaultMutableTreeNode) getRoot()).add(domainNode);

                final List<Family> familiesToSort = domain.getFamilies();
                Collections.sort(familiesToSort, new EntitiesComparator());

                for (Family family : familiesToSort) {
                    // START CURRENT FAMILY
                    final DefaultMutableTreeNode familyNode = new DefaultMutableTreeNode(family.getName());
                    domainNode.add(familyNode);

                    final List<Member> membersToSort = family.getMembers();
                    Collections.sort(membersToSort, new EntitiesComparator());

                    for (Member member : membersToSort) {
                        // START CURRENT MEMBER
                        final DefaultMutableTreeNode memberNode = new DefaultMutableTreeNode(member.getName());
                        familyNode.add(memberNode);

                        final List<Attribute> attributesToSort = member.getAttributes();
                        Collections.sort(attributesToSort, new EntitiesComparator());

                        for (Attribute attribute : attributesToSort) {
                            // START CURRENT ATTRIBUTE
                            attribute.setCompleteName(domain.getName() + "/" + family.getName() + "/" + member.getName()
                                    + "/" + attribute.getName());
                            attribute.setDomain(domain.getName());
                            attribute.setFamily(family.getName());
                            attribute.setMember(member.getName());
                            attribute.setDevice(member.getName());

                            final DefaultMutableTreeNode attributeNode = new DefaultMutableTreeNode(
                                    attribute.getName());
                            memberNode.add(attributeNode);
                            if (attribute instanceof ContextAttribute) {
                                addAttribute(attributeNode.getPath(), (ContextAttribute) attribute);
                            } else {
                                addAttribute(attributeNode.getPath(), new ContextAttribute(attribute));
                            }
                            // END CURRENT ATTRIBUTE
                        }
                        attributesToSort.clear();
                        // END CURRENT MEMBER
                    }
                    membersToSort.clear();
                    // END CURRENT FAMILY
                }
                familiesToSort.clear();
                // END CURRENT DOMAIN
            }
        }
    }

    /**
     * Adds a (String representations of path in tree, attribute) key/value
     * couple in the attributes Map.
     * 
     * @param path
     *            The attribute's path in the tree
     * @param attribute
     *            The attribute to add
     */
    public void addAttribute(final TreeNode[] path, final ContextAttribute attribute) {
        attributesHT.put(translatePathIntoKey(path), attribute);
    }

    /**
     * Returns the attribute located at a given path in the tree.
     * 
     * @param key The String representations of the path of the desired attribute
     * @return The attribute at this path in the tree
     */
    public ContextAttribute getAttribute(final String key) {
        return attributesHT.get(key);
    }

    /**
     * Returns the attributes Map.
     * 
     * @return The attributes Map
     */
    public TreeMap<String, ContextAttribute> getAttributes() {
        return attributesHT;
    }

    /**
     * Returns an enumeration on all of the current tree attributes.
     * 
     * @return An enumeration on all of the current tree attributes
     */
    public Collection<String> getTreeAttributes() {
        return attributesHT.keySet();
    }

    /**
     * Returns the String representation of the given path, usable as key in the attributes Map.
     * 
     * @param path The path to convert
     * @return The String representation of the given path
     */
    public static String translatePathIntoKey(final TreeNode[] path) {
        String ret = "";

        for (int i = 1; i < path.length; i++) {
            ret += path[i].toString();
            if (i < path.length - 1) {
                ret += GUIUtilities.TANGO_DELIM;
            }
        }

        return ret;
    }

    /**
     * Returns the current number of attributes.
     * 
     * @return The current number of attributes
     */
    public int size() {
        if (attributesHT == null) {
            return 0;
        } else {
            return attributesHT.size();
        }
    }

    /*
     * public void traceAttributesHT () { if ( this.size () == 0 ) { return; }
     * Enumeration enum = attributesHT.keys (); System.out.println (
     * "traceAttributesHT------------------" ); while ( enum.hasMoreElements ()
     * ) { String nextKey = (String) enum.nextElement (); System.out.println (
     * "nextKey|"+nextKey+"|" ); } System.out.println (
     * "traceAttributesHT------------------" ); }
     */

    public void removeAll() {
        final DefaultMutableTreeNode root = (DefaultMutableTreeNode) getRoot();
        final String name = (String) root.getUserObject();
        attributesHT = new TreeMap<>(Collator.getInstance());
        setRoot(new DefaultMutableTreeNode(name));
    }
}
