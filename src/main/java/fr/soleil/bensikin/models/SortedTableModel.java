package fr.soleil.bensikin.models;

import javax.swing.table.AbstractTableModel;

import fr.soleil.bensikin.components.BensikinComparator;

public abstract class SortedTableModel extends AbstractTableModel implements SortableTableModel {

    private static final long serialVersionUID = 1026621109979859128L;

    protected volatile int idSort;
    protected volatile int sortedColumn;

    public SortedTableModel() {
        super();
        idSort = BensikinComparator.NO_SORT;
        sortedColumn = -1;
    }

    // public abstract void sort(int clickedColumnIndex);

    @Override
    public int getSortedColumn() {
        return sortedColumn;
    }

    @Override
    public int getIdSort() {
        return idSort;
    }
}
