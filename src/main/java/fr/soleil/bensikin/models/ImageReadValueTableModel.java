package fr.soleil.bensikin.models;

import javax.swing.table.DefaultTableModel;

import fr.esrf.TangoDs.TangoConst;

public class ImageReadValueTableModel extends DefaultTableModel {

    private static final long serialVersionUID = -3378243105041173270L;

    protected Object value;
    protected int data_type;

    public ImageReadValueTableModel(Object value, int data_type) {
        super();
        this.value = value;
        this.data_type = data_type;
    }

    @Override
    public int getColumnCount() {
        int count = 0;
        if (value != null) {
            switch (data_type) {
            case TangoConst.Tango_DEV_CHAR:
            case TangoConst.Tango_DEV_UCHAR:
                if (((byte[][]) value).length != 0 && ((byte[][]) value)[0].length != 0) {
                    count = ((byte[][]) value).length;
                }
                break;
            case TangoConst.Tango_DEV_SHORT:
            case TangoConst.Tango_DEV_USHORT:
                if (((short[][]) value).length != 0 && ((short[][]) value)[0].length != 0) {
                    count = ((short[][]) value).length;
                }
                break;
            case TangoConst.Tango_DEV_LONG:
            case TangoConst.Tango_DEV_ULONG:
                if (((int[][]) value).length != 0 && ((int[][]) value)[0].length != 0) {
                    count = ((int[][]) value).length;
                }
                break;
            case TangoConst.Tango_DEV_FLOAT:
                if (((float[][]) value).length != 0 && ((float[][]) value)[0].length != 0) {
                    count = ((float[][]) value).length;
                }
                break;
            case TangoConst.Tango_DEV_DOUBLE:
                if (((double[][]) value).length != 0 && ((double[][]) value)[0].length != 0) {
                    count = ((double[][]) value).length;
                }
                break;
            default: // nothing to do
            }
        }
        return count;
    }

    @Override
    public int getRowCount() {
        int count = 0;
        if (value != null) {
            switch (data_type) {
            case TangoConst.Tango_DEV_CHAR:
            case TangoConst.Tango_DEV_UCHAR:
                if (((byte[][]) value).length != 0 && ((byte[][]) value)[0].length != 0) {
                    count = ((byte[][]) value)[0].length;
                }
                break;
            case TangoConst.Tango_DEV_SHORT:
            case TangoConst.Tango_DEV_USHORT:
                if (((short[][]) value).length != 0 && ((short[][]) value)[0].length != 0) {
                    count = ((short[][]) value)[0].length;
                }
                break;
            case TangoConst.Tango_DEV_LONG:
            case TangoConst.Tango_DEV_ULONG:
                if (((int[][]) value).length != 0 && ((int[][]) value)[0].length != 0) {
                    count = ((int[][]) value)[0].length;
                }
                break;
            case TangoConst.Tango_DEV_FLOAT:
                if (((float[][]) value).length != 0 && ((float[][]) value)[0].length != 0) {
                    count = ((float[][]) value)[0].length;
                }
                break;
            case TangoConst.Tango_DEV_DOUBLE:
                if (((double[][]) value).length != 0 && ((double[][]) value)[0].length != 0) {
                    count = ((double[][]) value)[0].length;
                }
                break;
            default: // nothing to do
            }
        }
        return count;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return Integer.toString(columnIndex + 1);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object res = null;
        if (value != null) {
            switch (data_type) {
            case TangoConst.Tango_DEV_CHAR:
            case TangoConst.Tango_DEV_UCHAR:
                res = ((byte[][]) value)[columnIndex][rowIndex];
                break;
            case TangoConst.Tango_DEV_SHORT:
            case TangoConst.Tango_DEV_USHORT:
                res = ((short[][]) value)[columnIndex][rowIndex];
                break;
            case TangoConst.Tango_DEV_LONG:
            case TangoConst.Tango_DEV_ULONG:
                res = ((int[][]) value)[columnIndex][rowIndex];
                break;
            case TangoConst.Tango_DEV_FLOAT:
                res = ((float[][]) value)[columnIndex][rowIndex];
                break;
            case TangoConst.Tango_DEV_DOUBLE:
                res = ((double[][]) value)[columnIndex][rowIndex];
                break;
            default: // nothing to do
            }
        }
        return res;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

}
