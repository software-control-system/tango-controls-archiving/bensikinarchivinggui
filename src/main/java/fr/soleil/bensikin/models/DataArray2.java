package fr.soleil.bensikin.models;

import fr.soleil.archiving.common.api.utils.DoubleLinkedList;

public class DataArray2 {

    private String id = null;
    private DoubleLinkedList pointList = new DoubleLinkedList();
    private double[] data;

    public DataArray2(String id) {
        this.id = id;
        pointList = new DoubleLinkedList();
        data = null;
    }

    public String getId() {
        return id;
    }

    /**
     * Adds x and y values to this {@link DataArray2}. Warning, expert usage and
     * for construction only. Won't work if any other data interaction method is
     * called.
     * 
     * @param x
     *            x value to add
     * @param y
     *            y value to add
     */
    public void add(double x, double y) {
        pointList.add(x);
        pointList.add(y);
    }

    /**
     * Subtract y values from other data array. This data array is assumed to be
     * the longer one.
     * 
     * @param other
     */
    public void subtract(DataArray2 other) {
        if (other != null) {
            double[] array = getData();
            double[] otherArray = other.getData();
            if ((array != null) && (otherArray != null)) {
                int length = Math.min(array.length / 2, otherArray.length / 2);
                for (int i = 0; i < length; i++) {
                    int index = 2 * i + 1;
                    array[index] -= otherArray[index];
                }
            }
        }
    }

    /**
     * Substracts a value to all y values. Once called,
     * {@link #add(double, double)} won't work any more
     * 
     * @param value
     *            The value to substract
     */
    public void subtract(double value) {
        double[] array = getData();
        if (array != null) {
            int length = array.length / 2;
            for (int i = 0; i < length; i++) {
                array[2 * i + 1] -= value;
            }
        }
    }

    /**
     * Returns this {@link DataArray2}'s data as a <code>double</code> array.
     * Once called, {@link #add(double, double)} won't work any more
     * 
     * @return A <code>double[]</code>.
     */
    public double[] getData() {
        if (data == null) {
            if (pointList != null) {
                data = pointList.toArray();
                pointList.clear();
                pointList = null;
            }
        }
        return data;
    }

    /**
     * Returns this {@link DataArray2} size. Once called,
     * {@link #add(double, double)} won't work any more
     * 
     * @return An <code>int</code>
     */
    public int size() {
        double[] array = getData();
        return (array == null ? 0 : array.length / 2);
    }

    /**
     * Clears this {@link DataArray2}. Once called, {@link #add(double, double)}
     * won't work any more
     */
    public void clear() {
        data = null;
        if (pointList != null) {
            pointList.clear();
        }
        pointList = null;
    }

    @Override
    protected void finalize() throws Throwable {
        clear();
        super.finalize();
    }
}
