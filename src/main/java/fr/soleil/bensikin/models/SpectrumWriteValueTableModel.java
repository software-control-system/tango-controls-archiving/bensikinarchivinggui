/*
 * Synchrotron Soleil File : SpectrumWriteValueTableModel.java Project :
 * Bensikin_CVS Description : Author : SOLEIL Original : 14 f�vr. 2006 Revision:
 * Author: Date: State: Log: SpectrumWriteValueTableModel.java,v
 */
package fr.soleil.bensikin.models;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import fr.esrf.TangoDs.TangoConst;
import fr.soleil.bensikin.containers.BensikinFrame;
import fr.soleil.bensikin.tools.Messages;

/**
 * @author SOLEIL
 */
public class SpectrumWriteValueTableModel extends AbstractTableModel {

    private static final long serialVersionUID = -5473698182375910722L;

    protected Object values;
    protected boolean[] nullElements;
    protected int type;
    protected boolean canSet;

    public SpectrumWriteValueTableModel(int dataType, Object spectrumValues, boolean[] nullElements) {
        super();
        this.type = dataType;
        this.values = spectrumValues;
        this.nullElements = nullElements;
        canSet = false;
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public int getRowCount() {
        int count;
        if (values != null && !"NaN".equals(values)) {
            switch (type) {
            case TangoConst.Tango_DEV_STRING:
                count = ((String[]) values).length;
                break;
            case TangoConst.Tango_DEV_BOOLEAN:
                count = ((boolean[]) values).length;
                break;
            case TangoConst.Tango_DEV_UCHAR:
            case TangoConst.Tango_DEV_CHAR:
                count = ((byte[]) values).length;
                break;
            case TangoConst.Tango_DEV_STATE:
            case TangoConst.Tango_DEV_ULONG:
            case TangoConst.Tango_DEV_LONG:
                count = ((int[]) values).length;
                break;
            case TangoConst.Tango_DEV_USHORT:
            case TangoConst.Tango_DEV_SHORT:
                count = ((short[]) values).length;
                break;
            case TangoConst.Tango_DEV_FLOAT:
                count = ((float[]) values).length;
                break;
            case TangoConst.Tango_DEV_DOUBLE:
                count = ((double[]) values).length;
                break;
            default:
                count = 0;
                break;
            }
        } else {
            count = 0;
        }
        return count;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value;
        if (columnIndex == 0) {
            value = "" + (rowIndex + 1);
        } else {
            if (values != null && !"NaN".equals(values)) {
                switch (type) {
                case TangoConst.Tango_DEV_STRING:
                    value = ((String[]) values)[rowIndex];
                    break;
                case TangoConst.Tango_DEV_BOOLEAN:
                    value = "" + ((boolean[]) values)[rowIndex];
                    break;
                case TangoConst.Tango_DEV_UCHAR:
                case TangoConst.Tango_DEV_CHAR:
                    value = "" + ((byte[]) values)[rowIndex];
                    break;
                case TangoConst.Tango_DEV_STATE:
                case TangoConst.Tango_DEV_ULONG:
                case TangoConst.Tango_DEV_LONG:
                    value = "" + ((int[]) values)[rowIndex];
                    break;
                case TangoConst.Tango_DEV_USHORT:
                case TangoConst.Tango_DEV_SHORT:
                    value = "" + ((short[]) values)[rowIndex];
                    break;
                case TangoConst.Tango_DEV_FLOAT:
                    value = "" + ((float[]) values)[rowIndex];
                    break;
                case TangoConst.Tango_DEV_DOUBLE:
                    value = "" + ((double[]) values)[rowIndex];
                    break;
                default:
                    value = "";
                    break;
                }
            } else {
                value = "";
            }
        }
        return value;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (columnIndex == 1) {
            if (isValidValue(aValue.toString())) {
                if (values != null && !"NaN".equals(values)) {
                    switch (type) {
                    case TangoConst.Tango_DEV_STRING:
                        ((String[]) values)[rowIndex] = String.valueOf(aValue);
                        if (nullElements != null) {
                            nullElements[rowIndex] = false;
                        }
                        break;
                    case TangoConst.Tango_DEV_BOOLEAN:
                        ((boolean[]) values)[rowIndex] = "true".equalsIgnoreCase(String.valueOf(aValue));
                        if (nullElements != null) {
                            nullElements[rowIndex] = false;
                        }
                        break;
                    case TangoConst.Tango_DEV_UCHAR:
                    case TangoConst.Tango_DEV_CHAR:
                        ((byte[]) values)[rowIndex] = (byte) Double.parseDouble(String.valueOf(aValue));
                        if (nullElements != null) {
                            nullElements[rowIndex] = false;
                        }
                        break;
                    case TangoConst.Tango_DEV_STATE:
                    case TangoConst.Tango_DEV_ULONG:
                    case TangoConst.Tango_DEV_LONG:
                        ((int[]) values)[rowIndex] = (int) Double.parseDouble(String.valueOf(aValue));
                        if (nullElements != null) {
                            nullElements[rowIndex] = false;
                        }
                        break;
                    case TangoConst.Tango_DEV_USHORT:
                    case TangoConst.Tango_DEV_SHORT:
                        ((short[]) values)[rowIndex] = (short) Double.parseDouble(String.valueOf(aValue));
                        if (nullElements != null) {
                            nullElements[rowIndex] = false;
                        }
                        break;
                    case TangoConst.Tango_DEV_FLOAT:
                        ((float[]) values)[rowIndex] = Float.parseFloat(String.valueOf(aValue));
                        if (nullElements != null) {
                            nullElements[rowIndex] = false;
                        }
                        break;
                    case TangoConst.Tango_DEV_DOUBLE:
                        ((double[]) values)[rowIndex] = Double.parseDouble(String.valueOf(aValue));
                        if (nullElements != null) {
                            nullElements[rowIndex] = false;
                        }
                        break;
                    }
                    fireTableDataChanged();
                }
            } else {
                String title = Messages.getMessage("MODIFY_SNAPSHOT_INVALID_VALUE_TITLE");
                String msg = Messages.getMessage("MODIFY_SNAPSHOT_INVALID_VALUE_MESSAGE");
                JOptionPane.showMessageDialog(BensikinFrame.getInstance(), msg, title, JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex == 1);
    }

    @Override
    public String getColumnName(int columnIndex) {
        String name;
        if (columnIndex == 0) {
            name = Messages.getMessage("DIALOGS_SPECTRUM_ATTRIBUTE_INDEX");
        } else if (columnIndex == 1) {
            return Messages.getMessage("DIALOGS_SPECTRUM_ATTRIBUTE_VALUE");
        } else {
            name = "";
        }
        return name;
    }

    /**
     * Returns the values saved in the table. Use them to set the spectrum value
     * of your AttibuteWriteValue.
     * 
     * @return the values saved in the table
     */
    public Object getValues() {
        return values;
    }

    public boolean[] getNullElements() {
        return nullElements;
    }

    private boolean isValidValue(String val) {
        boolean valid;
        switch (type) {
        case TangoConst.Tango_DEV_STRING:
            valid = true;
            break;
        case TangoConst.Tango_DEV_BOOLEAN:
            if ("true".equalsIgnoreCase(val.trim()) || "false".equalsIgnoreCase(val.trim())) {
                valid = true;
            } else {
                valid = false;
            }
            break;
        case TangoConst.Tango_DEV_UCHAR:
        case TangoConst.Tango_DEV_CHAR:
            try {
                Byte.parseByte(val);
                valid = true;
            } catch (Exception e) {
                valid = false;
            }
            break;
        case TangoConst.Tango_DEV_STATE:
        case TangoConst.Tango_DEV_ULONG:
        case TangoConst.Tango_DEV_LONG:
            try {
                Integer.parseInt(val);
                valid = true;
            } catch (Exception e) {
                valid = false;
            }
            break;
        case TangoConst.Tango_DEV_USHORT:
        case TangoConst.Tango_DEV_SHORT:
            try {
                Short.parseShort(val);
                valid = true;
            } catch (Exception e) {
                valid = false;
            }
            break;
        case TangoConst.Tango_DEV_FLOAT:
            try {
                Float.parseFloat(val);
                valid = true;
            } catch (Exception e) {
                valid = false;
            }
            break;
        case TangoConst.Tango_DEV_DOUBLE:
            try {
                Double.parseDouble(val);
                return true;
            } catch (Exception e) {
                return false;
            }
        default:
            valid = false;
            break;
        }
        return valid;
    }

    /**
     * Sets all the elements of the table to a specific value
     * 
     * @param value
     *            the value
     * @return <code>true</code> if the value is valid and every element is set
     *         to this value, <code>false></code> otherwise
     */
    public boolean setAll(String value) {
        if (isValidValue(value)) {
            for (int i = 0; i < this.getRowCount(); i++) {
                this.setValueAt(value, i, 1);
            }
            fireTableDataChanged();
            canSet = true;
        } else {
            canSet = false;
        }
        if (!canSet) {
            String title = Messages.getMessage("MODIFY_SNAPSHOT_INVALID_VALUE_TITLE");
            String msg = Messages.getMessage("MODIFY_SNAPSHOT_INVALID_VALUE_MESSAGE");
            JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);
        }
        return canSet;
    }

    /**
     * Tells wheather this table values can be used or not
     * 
     * @return <code>true</code> if this table values can be used,
     *         <code>false</code> otherwise
     */
    public boolean isCanSet() {
        return canSet;
    }

    /**
     * Forces the table to set its "canSet" variable to a specific boolean value
     * 
     * @param b
     *            the value of the "canSet" variable
     * @see #isCanSet()
     */
    public void setCanSet(boolean b) {
        canSet = b;
    }

}
