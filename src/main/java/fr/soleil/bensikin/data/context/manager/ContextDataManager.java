package fr.soleil.bensikin.data.context.manager;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import fr.soleil.archiving.common.api.tools.Condition;
import fr.soleil.archiving.common.api.tools.Criterions;
import fr.soleil.archiving.snap.api.manager.ISnapManager;
import fr.soleil.archiving.snap.api.tools.SnapConst;
import fr.soleil.archiving.snap.api.tools.SnapContext;
import fr.soleil.archiving.snap.api.tools.SnapshotingException;
import fr.soleil.bensikin.data.context.ContextData;
import fr.soleil.bensikin.datasources.snapdb.SnapManagerFactory;

public class ContextDataManager {
    private static ContextDataManager instance = new ContextDataManager();
    private Map<Integer, ContextData> contextsMap = new ConcurrentHashMap<Integer, ContextData>();

    public static ContextDataManager getInstance() {
        return instance;
    }

    public void resetContextData(int contextId) {
        contextsMap.remove(contextId);
    }

    public ContextData getContextData(int contextId) {
        if (!contextsMap.containsKey(contextId)) {
            try {
                loadContextByDb(contextId);
            } catch (SnapshotingException e) {
                e.printStackTrace();
            }
        }
        return contextsMap.get(contextId);
    }

    private void addContextData(int contextId, ContextData contextData) {
        contextsMap.put(contextId, contextData);
    }

    //
    // private void removeContextData(int contextId) {
    // contextsMap.remove(contextId);
    // }
    //
    // private int getSize() {
    // return contextsMap.size();
    // }

    private void loadContextByDb(int contextId) throws SnapshotingException {
        Condition condition = new Condition(SnapConst.ID_CONTEXT, SnapConst.OP_EQUALS, String.valueOf(contextId));
        Criterions searchCriterions = new Criterions();
        searchCriterions.addCondition(condition);
        ISnapManager source = SnapManagerFactory.getCurrentImpl();
        SnapContext[] contexts = source.findContexts(searchCriterions);
        if ((contexts != null) && (contexts.length > 0)) {
            ContextData context = new ContextData(contexts[0]);
            addContextData(contextId, context);
        }
    }

    public int getContextIdBySnapshotId(int snapshotId) throws SnapshotingException {
        ISnapManager source = SnapManagerFactory.getCurrentImpl();
        int contextId = source.findContextId(snapshotId);

        ContextData res = this.getContextData(contextId);
        if (res == null) {
            // ContextData unknown into database
            return -1;
        }
        return contextId;
    }
}
