// +======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/data/context/manager/XMLContextManagerImpl.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  XMLContextManagerImpl.
//						(Claisse Laurent) - sept. 2005
//
// $Author: ounsy $
//
// $Revision: 1.4 $
//
// $Log: XMLContextManagerImpl.java,v $
// Revision 1.4  2007/04/19 13:59:02  ounsy
// added /ctx to the default save location
//
// Revision 1.3  2006/11/29 10:00:45  ounsy
// minor changes
//
// Revision 1.2  2005/12/14 16:35:03  ounsy
// added methods necessary for the new Word-like file management
//
// Revision 1.1  2005/12/14 14:07:18  ounsy
// first commit  including the new  "tools,xml,lifecycle,profile" sub-directories
// under "bensikin.bensikin" and removing the same from their former locations
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.data.context.manager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.gui.manager.XMLDataManager;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.bensikin.Bensikin;
import fr.soleil.bensikin.data.context.Context;
import fr.soleil.bensikin.data.context.ContextAttribute;
import fr.soleil.bensikin.data.context.ContextAttributes;
import fr.soleil.bensikin.data.context.ContextData;
import fr.soleil.bensikin.tools.Messages;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;

/**
 * An XML implementation. Saves and loads contexts to/from XML files.
 * 
 * @author CLAISSE
 */
public class XMLContextManagerImpl extends XMLDataManager<Context, Context> implements IContextManager {

    /*
     * private String resourceLocation; private static final String DEFAULT_FILE
     * = "default.ctx";
     */

    private String defaultSaveLocation;
    private String saveLocation;
    private BufferedWriter writer;

    /**
     * Default constructor. Calls startUp ().
     */
    protected XMLContextManagerImpl() {
        startUp();
    }

    @Override
    public void startUp() throws IllegalStateException {
        String resourceLocation = null;
        boolean illegal = false;

        String acPath = Bensikin.getPathToResources() + "/ctx";

        File f = null;
        try {
            f = new File(acPath);
            if (!f.canWrite()) {
                illegal = true;
            }
        } catch (Exception e) {
            illegal = true;
        }

        if (illegal) {
            // boolean b =
            f.mkdir();
        }
        resourceLocation = acPath/* + "/" + DEFAULT_FILE */;
        defaultSaveLocation = resourceLocation + "/ctx";
    }

    @Override
    public void saveContext(Context context) throws IOException {
        String location = this.getSaveLocation();
        if ((location != null) && (!location.trim().isEmpty()) && (!new File(location).isDirectory())) {
            // System.out.println (
            // "XMLContextManagerImpl/saveContext/_location/"+_location+"/" );
            writer = new BufferedWriter(new FileWriter(location, false));
            // GUIUtilities.write2(writer, XMLUtils.XML_HEADER, true);
            // GUIUtilities.write2(writer, context.toString(), true);
            try {
                writer.write(XMLUtils.XML_HEADER);
                writer.newLine();
                writer.write(context.toString());
                writer.newLine();
            } finally {
                writer.close();
            }
        } else {
            throw new IOException(String.format(Messages.getMessage("FILE_CHOOSER_SAVE_FILE_ERROR"),
                    String.valueOf(location)));
        }
    }

    @Override
    public Context loadContext() throws ArchivingException {
        return loadDataIntoHash(getSaveLocation());
    }

    @Override
    public String getDefaultSaveLocation() {
        return defaultSaveLocation;
    }

    @Override
    public String getNonDefaultSaveLocation() {
        return this.saveLocation;
    }

    @Override
    public void setNonDefaultSaveLocation(String saveLocation) {
        this.saveLocation = saveLocation;
    }

    @Override
    public String getSaveLocation() {
        return ((saveLocation == null) || saveLocation.trim().isEmpty()) ? defaultSaveLocation : saveLocation;
    }

    /**
     * Loads the ContextData part of a Context given the XML Context node.
     * 
     * @param node
     *            The XML Context node
     * @return A Context with only the ContextData part loaded
     */
    private static Context loadContextData(Node node) {
        Context viewConf = new Context();
        ContextData viewConfData = new ContextData();

        Map<String, String> VCProperties = XMLUtils.loadAttributes(node);

        String creationDate_s = VCProperties.get(ContextData.CREATION_DATE_PROPERTY_XML_TAG);
        String author = VCProperties.get(ContextData.AUTHOR_PROPERTY_XML_TAG);
        String description = VCProperties.get(ContextData.DESCRIPTION_DATE_PROPERTY_XML_TAG);
        String id_s = VCProperties.get(ContextData.ID_PROPERTY_XML_TAG);
        String name = VCProperties.get(ContextData.NAME_PROPERTY_XML_TAG);
        String reason = VCProperties.get(ContextData.REASON_PROPERTY_XML_TAG);

        String path = VCProperties.get(ContextData.PATH_PROPERTY_XML_TAG);
        String isModified_s = VCProperties.get(ContextData.IS_MODIFIED_PROPERTY_XML_TAG);
        boolean isModified = GUIUtilities.stringToBoolean(isModified_s);

        if (creationDate_s != null) {
            Date creationDate = Date.valueOf(creationDate_s);
            viewConfData.setCreationDate(creationDate);
        }
        if (id_s != null) {
            int id = Integer.parseInt(id_s);
            viewConfData.setId(id);
        }
        viewConfData.setAuthorName(author);
        viewConfData.setDescription(description);
        viewConfData.setName(name);
        viewConfData.setReason(reason);
        viewConfData.setPath(path);
        viewConf.setContextId(viewConfData.getId());
        viewConf.setContextData(viewConfData);
        viewConf.setModified(isModified);

        return viewConf;
    }

    @Override
    protected boolean canSaveDefaultDataInLoadData() {
        return false;
    }

    @Override
    protected Context getDefaultData() {
        return new Context();
    }

    /**
     * Loads a Context given its XML root node.
     * 
     * @param rootNode
     *            The file's root node
     * @return The Context object built from its XML representation
     * @throws ArchivingException If a problem occurred
     */
    @Override
    protected Context loadDataIntoHashFromRoot(Node rootNode) throws ArchivingException {
        Context viewConf = loadContextData(rootNode);
        if (rootNode.hasChildNodes()) {
            NodeList attributesNodes = rootNode.getChildNodes();
            ContextAttributes viewConfAttr = new ContextAttributes(viewConf);
            List<ContextAttribute> listOfAttributesToAdd = new ArrayList<ContextAttribute>();
            // as many loops as there are attributes in the VC
            for (int i = 0; i < attributesNodes.getLength(); i++) {
                Node currentAttributeNode = attributesNodes.item(i);
                if (!XMLUtils.isAFakeNode(currentAttributeNode)) {
                    // has to be "attribute" or "genericPlotParameters"
                    String currentAttributeType = currentAttributeNode.getNodeName().trim();
                    if (currentAttributeType.equals(ContextAttribute.XML_TAG)) {
                        ContextAttribute currentAttribute = new ContextAttribute(viewConfAttr);
                        Map<String, String> attributeProperties = XMLUtils.loadAttributes(currentAttributeNode);
                        String attributeCompleteName = attributeProperties
                                .get(ContextAttribute.COMPLETE_NAME_PROPERTY_XML_TAG);
                        String device = attributeProperties.get(ContextAttribute.DEVICE_PROPERTY_XML_TAG);
                        String domain = attributeProperties.get(ContextAttribute.DOMAIN_PROPERTY_XML_TAG);
                        String family = attributeProperties.get(ContextAttribute.FAMILY_PROPERTY_XML_TAG);
                        String member = attributeProperties.get(ContextAttribute.MEMBER_PROPERTY_XML_TAG);
                        String name = attributeProperties.get(ContextAttribute.NAME_PROPERTY_XML_TAG);

                        currentAttribute.setCompleteName(attributeCompleteName);
                        currentAttribute.setDevice(device);
                        currentAttribute.setDomain(domain);
                        currentAttribute.setFamily(family);
                        currentAttribute.setMember(member);
                        currentAttribute.setName(name);

                        currentAttribute.setContextAttributes(viewConfAttr);
                        listOfAttributesToAdd.add(currentAttribute);
                    } else {
                        throw new ArchivingException("Invalid XML file");
                    }
                }
            }
            ContextAttribute[] contextAttributesTab = new ContextAttribute[listOfAttributesToAdd.size()];
            int i = 0;
            for (ContextAttribute next : listOfAttributesToAdd) {
                contextAttributesTab[i++] = next;
            }
            viewConfAttr.setContextAttributes(contextAttributesTab);
            viewConf.setContextAttributes(viewConfAttr);
        }
        return viewConf;
    }

}
