// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/bensikin/bensikin/data/context/ContextAttributes.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class ContextAttributes.
// (Claisse Laurent) - 16 juin 2005
//
// $Author: pierrejoseph $
//
// $Revision: 1.12 $
//
// $Log: ContextAttributes.java,v $
// Revision 1.12 2008/01/08 16:10:24 pierrejoseph
// Pb with the validate cmd in table mode : correction in the remove method
// - see with RGIR
//
// Revision 1.11 2007/08/24 14:09:13 ounsy
// Context attributes ordering (Mantis bug 3912)
//
// Revision 1.10 2007/08/23 15:28:48 ounsy
// Print Context as tree, table or text (Mantis bug 3913)
//
// Revision 1.9 2007/02/08 14:45:09 ounsy
// corrected a bug in toString() that left out some attributes
//
// Revision 1.8 2006/11/29 10:00:26 ounsy
// minor changes
//
// Revision 1.7 2006/01/12 12:57:39 ounsy
// traces removed
//
// Revision 1.6 2005/12/14 16:33:27 ounsy
// added methods necessary for alternate attribute selection
//
// Revision 1.5 2005/11/29 18:25:13 chinkumo
// no message
//
// Revision 1.1.1.2 2005/08/22 11:58:37 chinkumo
// First commit
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.bensikin.data.context;

import java.text.Collator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import javax.swing.tree.DefaultMutableTreeNode;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.archiving.tango.entity.Attributes;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.Family;
import fr.soleil.archiving.tango.entity.Member;
import fr.soleil.archiving.tango.entity.model.AttributesSelectTableModel;
import fr.soleil.bensikin.containers.context.ContextDataPanel;
import fr.soleil.bensikin.containers.context.ContextDetailPanel;
import fr.soleil.bensikin.models.ContextAttributesTreeModel;

/**
 * Represents the list of attributes attached to a context
 * 
 * @author CLAISSE
 */
public class ContextAttributes extends Attributes {

    private static final long serialVersionUID = -7653095499046579148L;

    private ContextAttribute[] contextAttributes;
    private Context context;

    /**
     * Builds a ContextAttribute with a reference to its Context.
     * 
     * @param _context
     *            The Context this attributes belong to
     */
    public ContextAttributes(Context _context) {
        super();
        this.context = _context;
    }

    /**
     * @param _context
     *            The Context this attributes belong to
     * @param _contextAttributes
     *            The list of attributes
     */
    public ContextAttributes(Context _context, ContextAttribute[] _contextAttributes) {
        super();
        this.context = _context;
        this.contextAttributes = _contextAttributes;
    }

    public ContextAttributes(Context _context, Attributes attributes) {
        super();
        if (attributes != null) {
            Attribute[] attrArray = attributes.getAttributes();
            if (attrArray != null) {
                contextAttributes = new ContextAttribute[attrArray.length];
                for (int i = 0; i < attrArray.length; i++) {
                    contextAttributes[i] = new ContextAttribute(attrArray[i]);
                    contextAttributes[i].setContextAttributes(this);
                }
            }
        }
    }

    /**
     * Returns a XML representation of the attributes.
     * 
     * @return a XML representation of the attributes
     */
    @Override
    public String toString() {
        String ret = "";

        if (this.contextAttributes != null) {
            for (int i = 0; i < this.contextAttributes.length; i++) {
                ContextAttribute nextValue = this.contextAttributes[i];
                ret += nextValue.toString();
                if (i < contextAttributes.length - 1) {
                    ret += GUIUtilities.CRLF;
                }
            }
        }

        return ret;
    }

    public void appendUserFriendlyString(StringBuilder buffer) {
        if (buffer != null) {
            if (this.contextAttributes != null) {
                for (int i = 0; i < this.contextAttributes.length; i++) {
                    ContextAttribute nextValue = this.contextAttributes[i];
                    nextValue.appendUserFriendlyString(buffer);
                    if (i < contextAttributes.length - 1) {
                        buffer.append(GUIUtilities.CRLF);
                    }
                }
            }
        }
    }

    /**
     * Visibly make these attributes the current context's attributes
     */
    public void push() {
        int count = 0;
        if (contextAttributes != null) {
            count = contextAttributes.length;
            for (int i = 0; i < count; i++) {
                if (contextAttributes[i] != null) {
                    contextAttributes[i].setNew(false);
                }
            }
        }
        ContextDetailPanel contextDetailPanel = ContextDetailPanel.getInstance();

        // update table
        AttributesSelectTableModel tableModel = ContextDetailPanel.getInstance().getAttributeTableSelectionBean()
                .getSelectionPanel().getAttributesSelectTable().getModel();
        tableModel.reset();
        tableModel.setRows(contextAttributes);
        // update tree
        List<Domain> newAttributes = this.getHierarchy();
        ContextAttributesTreeModel model = ContextAttributesTreeModel.getInstance(false);
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
        model.setRoot(new DefaultMutableTreeNode(root.getUserObject()));
        model.build(newAttributes);
        model.reload();

        contextDetailPanel.revalidate();
        contextDetailPanel.repaint();

        ContextDataPanel.getInstance().getAttributeCountField().setText(Integer.toString(count));
    }

    /**
     * @return Returns the context.
     */
    public Context getContext() {
        return context;
    }

    /**
     * @param context
     *            The context to set.
     */
    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * @return Returns the contextAttributes.
     */
    public ContextAttribute[] getContextAttributes() {
        return contextAttributes;
    }

    /**
     * @param contextAttributes
     *            The contextAttributes to set.
     */
    public void setContextAttributes(ContextAttribute[] contextAttributes) {
        this.contextAttributes = contextAttributes;
    }

    public void removeAttributesNotInList(Map<String, ContextAttribute> attrs) {
        if ((this.contextAttributes != null) && (this.contextAttributes.length > 0)) {
            Map<String, ContextAttribute> ht = new TreeMap<String, ContextAttribute>(Collator.getInstance());
            for (int i = 0; i < this.contextAttributes.length; i++) {
                ht.put(this.contextAttributes[i].getCompleteName(), this.contextAttributes[i]);
            }

            // ---
            List<String> toRemove = new ArrayList<String>();
            for (String next : ht.keySet()) {
                if (!attrs.containsKey(next)) {
                    toRemove.add(next);
                }
            }
            for (String key : toRemove) {
                ht.remove(key);
            }
            toRemove.clear();
            // ---

            ContextAttribute[] result = new ContextAttribute[ht.size()];
            int i = 0;
            for (ContextAttribute val : ht.values()) {
                result[i++] = val;
            }

            this.contextAttributes = result;
        }
    }

    @Override
    public Vector<Domain> getHierarchy() {
        Vector<Domain> result;
        if (this.contextAttributes == null) {
            result = null;
        } else {
            result = new Vector<Domain>();
            int nbAttributes = this.contextAttributes.length;
            // Map domains
            Map<String, Map<String, Map<String, Vector<String>>>> htDomains = new HashMap<String, Map<String, Map<String, Vector<String>>>>();
            for (int i = 0; i < nbAttributes; i++) {
                Attribute currentAttribute = this.contextAttributes[i];
                String currentDomain = currentAttribute.getDomainName();
                htDomains.put(currentDomain, new HashMap<String, Map<String, Vector<String>>>());
            }

            // Map families
            for (int i = 0; i < nbAttributes; i++) {
                Attribute currentAttribute = this.contextAttributes[i];
                String currentDomain = currentAttribute.getDomainName();
                String currentFamily = currentAttribute.getFamilyName();

                Map<String, Map<String, Vector<String>>> htCurrentDomain = htDomains.get(currentDomain);
                htCurrentDomain.put(currentFamily, new HashMap<String, Vector<String>>());
            }
            // Map members
            for (int i = 0; i < nbAttributes; i++) {
                Attribute currentAttribute = this.contextAttributes[i];
                String currentDomain = currentAttribute.getDomainName();
                String currentFamily = currentAttribute.getFamilyName();
                String currentMember = currentAttribute.getMemberName();

                Map<String, Map<String, Vector<String>>> htCurrentDomain = htDomains.get(currentDomain);
                Map<String, Vector<String>> htCurrentFamily = htCurrentDomain.get(currentFamily);

                htCurrentFamily.put(currentMember, new Vector<String>());
            }
            // Vector attributes
            for (int i = 0; i < nbAttributes; i++) {
                Attribute currentAttribute = this.contextAttributes[i];
                String currentDomain = currentAttribute.getDomainName();
                String currentFamily = currentAttribute.getFamilyName();
                String currentMember = currentAttribute.getMemberName();
                String currentAttributeName = currentAttribute.getName();
                Map<String, Map<String, Vector<String>>> htCurrentDomain = htDomains.get(currentDomain);
                Map<String, Vector<String>> htCurrentFamily = htCurrentDomain.get(currentFamily);
                Vector<String> veCurrentMember = htCurrentFamily.get(currentMember);
                veCurrentMember.add(currentAttributeName);
            }
            for (String nextDomainName : htDomains.keySet()) {
                Domain nextDomain = new Domain(nextDomainName);
                result.add(nextDomain);
                Map<String, Map<String, Vector<String>>> htFamilies = htDomains.get(nextDomainName);
                for (String nextFamilyName : htFamilies.keySet()) {
                    Family nextFamily = new Family(nextFamilyName);
                    nextDomain.addFamily(nextFamily);
                    Map<String, Vector<String>> htMembers = htFamilies.get(nextFamilyName);
                    for (String nextMemberName : htMembers.keySet()) {
                        Member nextMember = new Member(nextMemberName);
                        nextFamily.addMember(nextMember);

                        Vector<String> vectAttr = htMembers.get(nextMemberName);
                        for (int i = 0; i < vectAttr.size(); i++) {
                            nextMember.addAttribute(new Attribute(vectAttr.get(i)));
                        }
                    }
                }
            }
            result.trimToSize();
        }
        return result;
    }

}
