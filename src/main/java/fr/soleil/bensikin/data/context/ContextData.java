// +======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/data/context/ContextData.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  ContextData.
//						(Claisse Laurent) - 22 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.6 $
//
// $Log: ContextData.java,v $
// Revision 1.6  2005/12/14 16:33:57  ounsy
// added methods necessary for the new Word-like file management
//
// Revision 1.5  2005/11/29 18:25:13  chinkumo
// no message
//
// Revision 1.1.1.2  2005/08/22 11:58:37  chinkumo
// First commit
//
//
// copyleft :		Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.data.context;

import java.util.Date;

import javax.swing.JTextField;

import fr.soleil.archiving.snap.api.tools.SnapContext;
import fr.soleil.bensikin.components.BensikinMenuBar;
import fr.soleil.bensikin.components.context.detail.IDTextField;
import fr.soleil.bensikin.containers.context.ContextActionPanel;
import fr.soleil.bensikin.containers.context.ContextDataPanel;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Represents the non-attribute dependant informations attached to a context
 * 
 * @author CLAISSE
 */
public class ContextData implements Comparable<ContextData> {

    public static final int COLUMNS_COUNT = 6;

    private int id;
    private Date creationDate = null;
    private String name = ObjectUtils.EMPTY_STRING;
    private String authorName = ObjectUtils.EMPTY_STRING;
    private String reason = ObjectUtils.EMPTY_STRING;
    private String description = ObjectUtils.EMPTY_STRING;

    private String path = null;

    /**
     * The XML tag name used for the "id" property
     */
    public final static String ID_PROPERTY_XML_TAG = "ID";

    /**
     * The XML tag name used for the "creation date" property
     */
    public final static String CREATION_DATE_PROPERTY_XML_TAG = "CreationDate";

    /**
     * The XML tag name used for the "name" property
     */
    public final static String NAME_PROPERTY_XML_TAG = "Name";

    /**
     * The XML tag name used for the "author" property
     */
    public final static String AUTHOR_PROPERTY_XML_TAG = "Author";

    /**
     * The XML tag name used for the "reason" property
     */
    public final static String REASON_PROPERTY_XML_TAG = "Reason";

    /**
     * The XML tag name used for the "description" property
     */
    public final static String DESCRIPTION_DATE_PROPERTY_XML_TAG = "Description";

    public static final String IS_MODIFIED_PROPERTY_XML_TAG = "isModified";
    public static final String PATH_PROPERTY_XML_TAG = "path";

    private Context context;

    /**
     * Converts a SnapContext object to a ContextData
     * 
     * @param source
     */
    public ContextData(final SnapContext source) {
        setAuthorName(source.getAuthor_name());
        setCreationDate(source.getCreation_date());
        setDescription(source.getDescription());
        setId(source.getId());
        setName(source.getName());
        setReason(source.getReason());
    }

    /**
     * Builds a ContextData from parameters
     * 
     * @param id
     * @param creation_date
     * @param name
     * @param authorName
     * @param reason
     * @param description
     */
    public ContextData(final int id, final Date creation_date, final String name, final String authorName,
            final String reason, final String description) {
        this.id = id;
        this.creationDate = creation_date;
        this.name = name;
        this.authorName = authorName;
        this.reason = reason;
        this.description = description;
    }

    /**
     * Default constructor, does nothing
     */
    public ContextData() {
    }

    /**
     * Displays this data in the current context's data area
     */
    public void push() {
        ContextDataPanel contextDataPanel = ContextDataPanel.getInstance();

        JTextField timeField = contextDataPanel.getCreationDateField();
        String creation_date_s = getCreationDate() == null ? ObjectUtils.EMPTY_STRING : getCreationDate().toString();
        timeField.setText(creation_date_s);

        JTextField nameField = contextDataPanel.getNameField();
        if (!nameField.getText().equals(getName())) {
            nameField.setText(getName());
        }

        JTextField authorField = contextDataPanel.getAuthorNameField();
        if (!authorField.getText().equals(getAuthorName())) {
            authorField.setText(getAuthorName());
        }

        JTextField reasonField = contextDataPanel.getReasonField();
        if (!reasonField.getText().equals(getReason())) {
            reasonField.setText(getReason());
        }

        JTextField descriptionField = contextDataPanel.getDescriptionField();
        if (!descriptionField.getText().equals(getName())) {
            descriptionField.setText(getDescription());
        }

        // Update ID at end to ensure SelectedContextListener disables "register context" action (TANGOARCH-629)
        IDTextField idField = contextDataPanel.getIDField();
        String id_s = getId() == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(getId());
        idField.setText(id_s);

        BensikinMenuBar.getInstance().updateRegisterItem();
        ContextActionPanel.getInstance().updateRegisterButton();
    }

    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            The description to set.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return Returns the id.
     */
    public int getId() {
        return id;
    }

    /**
     * @param id
     *            The id to set.
     */
    public void setId(final int id) {
        this.id = id;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return Returns the reason.
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason
     *            The reason to set.
     */
    public void setReason(final String reason) {
        this.reason = reason;
    }

    /**
     * @return Returns the context.
     */
    public Context getContext() {
        return context;
    }

    /**
     * @param context
     *            The context to set.
     */
    public void setContext(final Context context) {
        this.context = context;
    }

    /**
     * @return Returns the author_name.
     */
    public String getAuthorName() {
        return authorName;
    }

    /**
     * @param author_name
     *            The author_name to set.
     */
    public void setAuthorName(final String author_name) {
        this.authorName = author_name;
    }

    /**
     * @return Returns the creation_date.
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * @param creation_date
     *            The creation_date to set.
     */
    public void setCreationDate(final Date creation_date) {
        this.creationDate = creation_date;
    }

    /**
     * @return Returns the path.
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path
     *            The path to set.
     */
    public void setPath(final String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(getClass().getSimpleName());
        builder.append("{id: ").append(id);
        builder.append(", creationDate:").append(creationDate);
        builder.append(", name:").append(name);
        builder.append(", authorName:").append(authorName);
        builder.append(", reason:").append(reason);
        builder.append(", description:").append(description);
        builder.append('}');
        return builder.toString();
    }

    @Override
    public int compareTo(ContextData o) {
        int compare;
        if (o == null) {
            compare = 1;
        } else {
            compare = Double.compare(id, o.id);
        }
        return compare;
    }

}
