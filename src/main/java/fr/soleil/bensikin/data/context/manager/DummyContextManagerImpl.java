//+======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/data/context/manager/DummyContextManagerImpl.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  DummyContextManagerImpl.
//						(Claisse Laurent) - sept. 2005
//
// $Author: ounsy $
//
// $Revision: 1.2 $
//
// $Log: DummyContextManagerImpl.java,v $
// Revision 1.2  2005/12/14 16:34:41  ounsy
// added methods necessary for the new Word-like file management
//
// Revision 1.1  2005/12/14 14:07:18  ounsy
// first commit  including the new  "tools,xml,lifecycle,profile" sub-directories
// under "bensikin.bensikin" and removing the same from their former locations
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.data.context.manager;

import fr.soleil.bensikin.data.context.Context;

/**
 * A dummy implementation (does nothing)
 * 
 * @author CLAISSE
 */
public class DummyContextManagerImpl implements IContextManager {

    public DummyContextManagerImpl() {
        super();
    }

    @Override
    public void startUp() throws Exception {
        // nothing to do
    }

    @Override
    public void saveContext(Context ac) throws Exception {
        // nothing to do
    }

    @Override
    public Context loadContext() throws Exception {
        // nothing to do
        return null;
    }

    @Override
    public String getDefaultSaveLocation() {
        // nothing to do
        return null;
    }

    @Override
    public String getSaveLocation() {
        // nothing to do
        return null;
    }

    @Override
    public String getNonDefaultSaveLocation() {
        // nothing to do
        return null;
    }

    @Override
    public void setNonDefaultSaveLocation(String location) {
        // nothing to do
    }

}
