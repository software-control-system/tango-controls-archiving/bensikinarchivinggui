package fr.soleil.bensikin.data.snapshot;

import fr.soleil.data.service.AbstractKey;



public enum BensikinAdapterFactory {
    INSTANCE;

    public IViewAdapter createNumberDataArrayDAO(AbstractKey key) {
	Object value = key.getPropertyValue("Attribute");
	if (value != null) {
	    Object name = key.getPropertyValue("Name");
	    if (name != null) {
		if ((value instanceof DefaultDataArrayAdapter) && (name instanceof String)) {
		    return (DefaultDataArrayAdapter) value;
		}
	    }
	}
	return null;
    }

}
