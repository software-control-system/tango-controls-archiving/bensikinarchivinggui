package fr.soleil.bensikin.data.snapshot;

//+======================================================================
//$Source:
///cvsroot/tango-cs/tango/tools/bensikin/bensikin/data/snapshot/SnapshotAttributeDeltaAbsValue.java,v
//$
//
//Project: Tango Archiving Service
//
//Description: Java source code for the class SnapshotAttributeDeltaAbsValue.
//(Claisse Laurent) - 22 juin 2005
//
//$Author: ounsy $
//
//$Revision: 1.14 $
//
//$Log: SnapshotAttributeDeltaAbsValue.java,v $
//Revision 1.14 2007/08/23 12:59:23 ounsy
//toUserFriendlyString() available in SnapshotAttributeValue
//
//Revision 1.13 2007/06/29 09:18:58 ounsy
//devLong represented as Integer + SnapshotCompareTable sorting
//
//Revision 1.12 2007/06/28 12:51:00 ounsy
//spectrum and image values represented as arrays
//
//Revision 1.11 2006/10/31 16:54:08 ounsy
//milliseconds and null values management
//
//Revision 1.10 2006/04/13 15:20:22 ounsy
//setting value to null when necessary
//
//Revision 1.9 2006/04/13 12:37:33 ounsy
//new spectrum types support
//
//Revision 1.8 2006/03/29 10:23:00 ounsy
//corrected a bug in the delta calculation for shorts
//
//Revision 1.7 2006/03/20 15:51:04 ounsy
//added the case of Snapshot delta value for spectrums which
//read and write parts are the same length.
//
//Revision 1.6 2005/12/14 16:36:13 ounsy
//added methods necessary for direct clipboard edition
//
//Revision 1.5 2005/11/29 18:25:08 chinkumo
//no message
//
//Revision 1.1.1.2 2005/08/22 11:58:38 chinkumo
//First commit
//
//
//copyleft : Synchrotron SOLEIL
//L'Orme des Merisiers
//Saint-Aubin - BP 48
//91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================

import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.snap.api.DataBaseAPI.NullableData;
import fr.soleil.bensikin.tools.Messages;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.NumberArrayUtils;

/**
 * Represents the difference between an attribute effective read value and its
 * write value.
 * 
 * @author CLAISSE
 */
public class SnapshotAttributeDeltaAbsValue extends SnapshotAttributeValue {

    /**
     * Builds a SnapshotAttributeDeltaAbsValue from the difference between a
     * read value and a write value.
     * 
     * @param writeAbsValue The write abs value
     * @param readAbsValue The read abs value
     */
    public SnapshotAttributeDeltaAbsValue(SnapshotAttributeWriteAbsValue writeAbsValue,
            SnapshotAttributeReadAbsValue readAbsValue) {
        this(writeAbsValue, readAbsValue, false);
    }

    public SnapshotAttributeDeltaAbsValue(SnapshotAttributeWriteAbsValue writeAbsValue,
            SnapshotAttributeReadAbsValue readAbsValue, boolean manageAllTypes) {
        super(writeAbsValue.getDataFormat(), writeAbsValue.getDataType(), null, null);

        Object deltaValue = getDeltaAbsValue(writeAbsValue, readAbsValue, manageAllTypes);
        if (readAbsValue == null || writeAbsValue == null) {
            setNotApplicable(true);
        } else if (readAbsValue.isNotApplicable() || writeAbsValue.isNotApplicable()) {
            setNotApplicable(true);
        }
        Object value = deltaValue;
        Object nullElements = null;
        if (deltaValue instanceof NullableData<?>) {
            NullableData<?> data = (NullableData<?>) deltaValue;
            value = data.getValue();
            nullElements = data.getNullElements();
        }
        if (value == null) {
            setNotApplicable(true);
        }

        if ((value instanceof String) || (value instanceof String[])) {
            setDataType(TangoConst.Tango_DEV_STRING);
        }

        this.setValue(value, nullElements);
    }

    /**
     * Builds a SnapshotAttributeDeltaAbsValue directly, given its Object value
     * and the format of this Object.
     * 
     * @param _dataFormat
     *            The Tango type of _value
     * @param _value
     *            The Object value
     * @param _nullElements
     *            The null elements identifier
     */
    public SnapshotAttributeDeltaAbsValue(int _dataFormat, Object _value, Object _nullElements) {
        super(_dataFormat, 0, _value, _nullElements);
    }

    private Object getDeltaAbsValue(SnapshotAttributeWriteAbsValue writeAbsValue,
            SnapshotAttributeReadAbsValue readAbsValue, boolean manageAllTypes) {
        switch (this.dataFormat) {
            case SCALAR_DATA_FORMAT:
                return getScalarDeltaAbsValue(writeAbsValue, readAbsValue, manageAllTypes);

            case SPECTRUM_DATA_FORMAT:
                return getSpectrumDeltaAbsValue(writeAbsValue, readAbsValue, manageAllTypes);

            case IMAGE_DATA_FORMAT:
                return getImageDeltaAbsValue(writeAbsValue, readAbsValue, manageAllTypes);

            default:
                return null;
        }
    }

    /**
     * Returns the difference between the values of a SnapshotAttributewriteAbsValue and a
     * SnapshotAttributeReadAbsValue, provided they are scalar.
     * 
     * @param writeAbsValue The write value
     * @param readAbsValue The read value
     * @param manageAllTypes A boolean to manage all types or not. If <code>false</code>, the delta value will be build
     *            only if type corresponds to a Number. otherwise, all types are managed, and the returned value for
     *            other types is a string that says whether there is a difference.
     * @return 29 juin 2005
     */
    private NullableData<Boolean> getScalarDeltaAbsValue(SnapshotAttributeWriteAbsValue writeAbsValue,
            SnapshotAttributeReadAbsValue readAbsValue, boolean manageAllTypes) {
        Object ret;
        Boolean nullElements = Boolean.FALSE;
        if (writeAbsValue == null || readAbsValue == null) {
            if (manageAllTypes) {
                if (writeAbsValue == null && readAbsValue == null) {
                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                } else {
                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_DIFFERENT");
                }
            } else {
                ret = null;
            }
        } else {
            Boolean nullRead = (Boolean) readAbsValue.getNullElements();
            Boolean nullWrite = (Boolean) writeAbsValue.getNullElements();
            nullElements = Boolean.valueOf(((nullRead != null) && nullRead.booleanValue())
                    || ((nullWrite != null) && nullWrite.booleanValue()));
            Object write = writeAbsValue.getScalarValue();
            Object read = readAbsValue.getScalarValue();
            switch (this.getDataType()) {
                case TangoConst.Tango_DEV_USHORT:
                case TangoConst.Tango_DEV_SHORT:
                    try {
                        Number writeShort = (Number) write;
                        Number readShort = (Number) read;
                        if (writeShort == null || readShort == null) {
                            nullElements = Boolean.FALSE;
                            if (manageAllTypes) {
                                if (writeShort == null && readShort == null) {
                                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                                } else {
                                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_DIFFERENT");
                                }
                            } else {
                                ret = null;
                            }
                        } else {
                            ret = Short.valueOf((short) Math.abs(readShort.shortValue() - writeShort.shortValue()));
                        }
                    } catch (ClassCastException e) {
                        // happens when the read/write values are String loaded from
                        // a file
                        String writeDouble_s = String.valueOf(write);
                        String readDouble_s = String.valueOf(read);
                        if (NULL.equals(writeDouble_s) || writeDouble_s.isEmpty() || NULL.equals(readDouble_s)
                                || readDouble_s.isEmpty()) {
                            nullElements = Boolean.FALSE;
                            if (manageAllTypes) {
                                if (NULL.equals(writeDouble_s) && NULL.equals(readDouble_s)) {
                                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                                } else if (writeDouble_s.isEmpty() && readDouble_s.isEmpty()) {
                                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                                } else {
                                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_DIFFERENT");
                                }
                            } else {
                                ret = null;
                            }
                        } else {
                            /*
                             * short readDouble = Short.parseShort( readDouble_s );
                             * short writeDouble = Short.parseShort( writeDouble_s
                             * );
                             */
                            double readDouble = Double.parseDouble(readDouble_s);
                            double writeDouble = Double.parseDouble(writeDouble_s);

                            ret = Short.valueOf((short) Math.abs(readDouble - writeDouble));
                        }
                    }
                    break;
                // --------------------
                case TangoConst.Tango_DEV_DOUBLE:
                    try {
                        Number writeDouble = (Number) write;
                        Number readDouble = (Number) read;
                        if (writeDouble == null || readDouble == null) {
                            nullElements = Boolean.FALSE;
                            if (manageAllTypes) {
                                if (writeDouble == null && readDouble == null) {
                                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                                } else {
                                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_DIFFERENT");
                                }
                            } else {
                                ret = null;
                            }
                        } else {
                            ret = Double.valueOf(Math.abs(readDouble.doubleValue() - writeDouble.doubleValue()));
                        }
                    } catch (ClassCastException e) {
                        // happens when the read/write values are String loaded from
                        // a file
                        String writeDouble_s = String.valueOf(write);
                        String readDouble_s = String.valueOf(read);
                        if (NULL.equals(writeDouble_s) || writeDouble_s.isEmpty() || NULL.equals(readDouble_s)
                                || readDouble_s.isEmpty()) {
                            nullElements = Boolean.FALSE;
                            if (manageAllTypes) {
                                if (NULL.equals(writeDouble_s) && NULL.equals(readDouble_s)) {
                                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                                } else if (writeDouble_s.isEmpty() && readDouble_s.isEmpty()) {
                                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                                } else {
                                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_DIFFERENT");
                                }
                            } else {
                                ret = null;
                            }
                        } else {
                            double readDouble = Double.parseDouble(readDouble_s);
                            double writeDouble = Double.parseDouble(writeDouble_s);

                            ret = Double.valueOf(Math.abs(readDouble - writeDouble));
                        }
                    }
                    break;
                case TangoConst.Tango_DEV_ULONG:
                case TangoConst.Tango_DEV_LONG:
                    try {
                        Number writeLong = (Number) write;
                        Number readLong = (Number) read;
                        if (writeLong == null || readLong == null) {
                            nullElements = Boolean.FALSE;
                            if (manageAllTypes) {
                                if (writeLong == null && readLong == null) {
                                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                                } else {
                                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_DIFFERENT");
                                }
                            } else {
                                ret = null;
                            }
                        } else {
                            ret = Integer.valueOf(Math.abs(readLong.intValue() - writeLong.intValue()));
                        }
                    } catch (ClassCastException e) {
                        // happens when the read/write values are String loaded from
                        // a file
                        String writeLong_s = String.valueOf(write);
                        String readLong_s = String.valueOf(read);
                        if (NULL.equals(writeLong_s) || writeLong_s.isEmpty() || NULL.equals(readLong_s)
                                || readLong_s.isEmpty()) {
                            nullElements = Boolean.FALSE;
                            if (manageAllTypes) {
                                if (NULL.equals(writeLong_s) && NULL.equals(readLong_s)) {
                                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                                } else if (writeLong_s.isEmpty() && readLong_s.isEmpty()) {
                                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                                } else {
                                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_DIFFERENT");
                                }
                            } else {
                                ret = null;
                            }
                        } else {
                            double readDouble = Double.parseDouble(readLong_s);
                            double writeDouble = Double.parseDouble(writeLong_s);

                            ret = Integer.valueOf((int) Math.abs(readDouble - writeDouble));
                        }
                    }
                    break;
                case TangoConst.Tango_DEV_FLOAT:
                    try {
                        Number writeFloat = (Number) write;
                        Number readFloat = (Number) read;
                        if (writeFloat == null || readFloat == null) {
                            nullElements = Boolean.FALSE;
                            if (manageAllTypes) {
                                if (writeFloat == null && readFloat == null) {
                                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                                } else {
                                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_DIFFERENT");
                                }
                            } else {
                                ret = null;
                            }
                        } else {
                            ret = Float.valueOf(Math.abs(readFloat.longValue() - writeFloat.longValue()));
                        }
                    } catch (ClassCastException e) {
                        // happens when the read/write values are String loaded from
                        // a file
                        String writeFloat_s = String.valueOf(write);
                        String readFloat_s = String.valueOf(read);
                        if (NULL.equals(writeFloat_s) || writeFloat_s.isEmpty() || NULL.equals(readFloat_s)
                                || readFloat_s.isEmpty()) {
                            nullElements = Boolean.FALSE;
                            if (manageAllTypes) {
                                if (NULL.equals(writeFloat_s) && NULL.equals(readFloat_s)) {
                                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                                } else if (writeFloat_s.isEmpty() && readFloat_s.isEmpty()) {
                                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                                } else {
                                    ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_DIFFERENT");
                                }
                            } else {
                                ret = null;
                            }
                        } else {
                            float readFloat = Float.parseFloat(readFloat_s);
                            float writeFloat = Float.parseFloat(writeFloat_s);

                            ret = Float.valueOf(Math.abs(readFloat - writeFloat));
                        }
                    }
                    break;
                default:
                    nullElements = Boolean.FALSE;
                    if (manageAllTypes) {
                        if (write == null && read == null) {
                            if (write == null && read == null) {
                                ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                            } else {
                                ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_DIFFERENT");
                            }
                        } else {
                            if (write != null && write.equals(read)) {
                                ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                            } else {
                                ret = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_DIFFERENT");
                            }
                        }
                    } else {
                        ret = null;
                    }
                    break;
            }
        }
        return new NullableData<Boolean>(ret, nullElements);
    }

    /**
     * @param writeAbsValue The write value
     * @param readAbsValue The read value
     * @return 29 juin 2005
     */
    private Object getImageDeltaAbsValue(SnapshotAttributeWriteAbsValue writeAbsValue,
            SnapshotAttributeReadAbsValue readAbsValue, boolean manageAllTypes) {
        return null;
    }

    /**
     * @param writeAbsValue The write value
     * @param readAbsValue The read value
     * @return 29 juin 2005
     */
    private NullableData<boolean[]> getSpectrumDeltaAbsValue(SnapshotAttributeWriteAbsValue writeAbsValue,
            SnapshotAttributeReadAbsValue readAbsValue, boolean manageAllTypes) {
        if (writeAbsValue == null || readAbsValue == null) {
            return null;
        }

        Object readAbsValueTab = readAbsValue.getSpectrumValue();
        Object writeAbsValueTab = writeAbsValue.getSpectrumValue();
        int readLength = 0;
        int writeLength = 0;
        byte[] readChar = null, writeChar = null, diffChar = null;
        int[] readLong = null, writeLong = null, diffLong = null;
        short[] readShort = null, writeShort = null, diffShort = null;
        float[] readFloat = null, writeFloat = null, diffFloat = null;
        double[] readDouble = null, writeDouble = null, diffDouble = null;
        boolean[] readBoolean = null, writeBoolean = null;
        String[] readString = null, writeString = null, diffString = null;
        boolean[] nullRead = (boolean[]) readAbsValue.getNullElements();
        boolean[] nullWrite = (boolean[]) writeAbsValue.getNullElements();
        boolean[] nullElements = null;
        if (nullRead == null) {
            if (nullWrite == null) {
                nullElements = null;
            } else {
                nullElements = nullWrite.clone();
            }
        } else if (nullWrite == null) {
            nullElements = nullRead.clone();
        } else {
            int maxLength = Math.max(nullRead.length, nullWrite.length);
            int minLength = Math.min(nullRead.length, nullWrite.length);
            nullElements = new boolean[maxLength];
            for (int i = 0; i < minLength; i++) {
                nullElements[i] = (nullRead[i] || nullWrite[i]);
            }
            for (int i = minLength; i < maxLength; i++) {
                nullElements[i] = true;
            }
        }
        switch (dataType) {

            case TangoConst.Tango_DEV_DOUBLE:
                if (readAbsValueTab != null && !"Nan".equals(readAbsValueTab)) {
                    readDouble = NumberArrayUtils.extractDoubleArray(readAbsValueTab);
                    readLength = readDouble.length;
                }
                if (writeAbsValueTab != null && !"Nan".equals(writeAbsValueTab)) {
                    writeDouble = NumberArrayUtils.extractDoubleArray(writeAbsValueTab);
                    writeLength = writeDouble.length;
                }
                break;

            case TangoConst.Tango_DEV_FLOAT:
                if (readAbsValueTab != null && !"Nan".equals(readAbsValueTab)) {
                    readFloat = NumberArrayUtils.extractFloatArray(readAbsValueTab);
                    readLength = readFloat.length;
                }
                if (writeAbsValueTab != null && !"Nan".equals(writeAbsValueTab)) {
                    writeFloat = NumberArrayUtils.extractFloatArray(writeAbsValueTab);
                    writeLength = writeFloat.length;
                }
                break;

            case TangoConst.Tango_DEV_USHORT:
            case TangoConst.Tango_DEV_SHORT:
                if (readAbsValueTab != null && !"Nan".equals(readAbsValueTab)) {
                    readShort = NumberArrayUtils.extractShortArray(readAbsValueTab);
                    readLength = readShort.length;
                }
                if (writeAbsValueTab != null && !"Nan".equals(writeAbsValueTab)) {
                    writeShort = NumberArrayUtils.extractShortArray(writeAbsValueTab);
                    writeLength = writeShort.length;
                }
                break;

            case TangoConst.Tango_DEV_UCHAR:
            case TangoConst.Tango_DEV_CHAR:
                if (readAbsValueTab != null && !"Nan".equals(readAbsValueTab)) {
                    readChar = NumberArrayUtils.extractByteArray(readAbsValueTab);
                    readLength = readChar.length;
                }
                if (writeAbsValueTab != null && !"Nan".equals(writeAbsValueTab)) {
                    writeChar = NumberArrayUtils.extractByteArray(writeAbsValueTab);
                    writeLength = writeChar.length;
                }
                break;

            case TangoConst.Tango_DEV_STATE:
                if (!manageAllTypes) {
                    break;
                }
            case TangoConst.Tango_DEV_ULONG:
            case TangoConst.Tango_DEV_LONG:
                if (readAbsValueTab != null && !"Nan".equals(readAbsValueTab)) {
                    readLong = NumberArrayUtils.extractIntArray(readAbsValueTab);
                    readLength = readLong.length;
                }
                if (writeAbsValueTab != null && !"Nan".equals(writeAbsValueTab)) {
                    writeLong = NumberArrayUtils.extractIntArray(writeAbsValueTab);
                    writeLength = writeLong.length;
                }
                break;

            case TangoConst.Tango_DEV_BOOLEAN:
                if (manageAllTypes) {
                    if (readAbsValueTab != null && !"Nan".equals(readAbsValueTab)) {
                        readBoolean = (boolean[]) readAbsValueTab;
                        readLength = readBoolean.length;
                    }
                    if (writeAbsValueTab != null && !"Nan".equals(writeAbsValueTab)) {
                        writeBoolean = (boolean[]) writeAbsValueTab;
                        writeLength = writeBoolean.length;
                    }
                }
                break;
            case TangoConst.Tango_DEV_STRING:
                if (manageAllTypes) {
                    if (readAbsValueTab != null && !"Nan".equals(readAbsValueTab)) {
                        readString = (String[]) readAbsValueTab;
                        readLength = readString.length;
                    }
                    if (writeAbsValueTab != null && !"Nan".equals(writeAbsValueTab)) {
                        writeString = (String[]) writeAbsValueTab;
                        writeLength = writeString.length;
                    }
                }
                break;
            default:
                // nothing to do

        } // end switch (dataType)
        if (readAbsValueTab == null || readLength == 0) {
            return null;
        }
        if (writeAbsValueTab == null || writeLength == 0) {
            return null;
        }

        if (readLength != writeLength) {
            return null;
        }

        Object ret = null;
        switch (dataType) {

            case TangoConst.Tango_DEV_DOUBLE:
                diffDouble = new double[readLength];
                for (int i = 0; i < diffDouble.length; i++) {
                    diffDouble[i] = Math.abs(readDouble[i] - writeDouble[i]);
                }
                ret = diffDouble;
                break;

            case TangoConst.Tango_DEV_FLOAT:
                diffFloat = new float[readLength];
                for (int i = 0; i < diffFloat.length; i++) {
                    diffFloat[i] = Math.abs(readFloat[i] - writeFloat[i]);
                }
                ret = diffFloat;
                break;

            case TangoConst.Tango_DEV_USHORT:
            case TangoConst.Tango_DEV_SHORT:
                diffShort = new short[readLength];
                for (int i = 0; i < diffShort.length; i++) {
                    diffShort[i] = (short) Math.abs(readShort[i] - writeShort[i]);
                }
                ret = diffShort;
                break;

            case TangoConst.Tango_DEV_UCHAR:
            case TangoConst.Tango_DEV_CHAR:
                diffChar = new byte[readLength];
                for (int i = 0; i < diffChar.length; i++) {
                    diffChar[i] = (byte) Math.abs(readChar[i] - writeChar[i]);
                }
                ret = diffChar;
                break;

            case TangoConst.Tango_DEV_ULONG:
            case TangoConst.Tango_DEV_LONG:
                diffLong = new int[readLength];
                for (int i = 0; i < diffLong.length; i++) {
                    diffLong[i] = Math.abs(readLong[i] - writeLong[i]);
                }
                ret = diffLong;
                break;
            case TangoConst.Tango_DEV_STATE:
                if (manageAllTypes) {
                    diffString = new String[readLength];
                    for (int i = 0; i < diffString.length; i++) {
                        boolean readKo = getNullValue(nullRead, i);
                        boolean writeKo = getNullValue(nullWrite, i);
                        if (readKo == writeKo) {
                            if (readKo) {
                                diffString[i] = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                            } else if (readLong[i] == writeLong[i]) {
                                diffString[i] = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                            } else {
                                diffString[i] = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_DIFFERENT");
                            }
                        } else {
                            diffString[i] = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_DIFFERENT");
                        }
                    }
                    ret = diffString;
                    nullElements = null;
                }
                break;
            case TangoConst.Tango_DEV_BOOLEAN:
                if (manageAllTypes) {
                    diffString = new String[readLength];
                    for (int i = 0; i < diffString.length; i++) {
                        boolean readKo = getNullValue(nullRead, i);
                        boolean writeKo = getNullValue(nullWrite, i);
                        if (readKo == writeKo) {
                            if (readKo) {
                                diffString[i] = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                            } else if (readBoolean[i] == writeBoolean[i]) {
                                diffString[i] = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                            } else {
                                diffString[i] = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_DIFFERENT");
                            }
                        } else {
                            diffString[i] = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_DIFFERENT");
                        }
                    }
                    ret = diffString;
                    nullElements = null;
                }
                break;
            case TangoConst.Tango_DEV_STRING:
                if (manageAllTypes) {
                    diffString = new String[readLength];
                    for (int i = 0; i < diffString.length; i++) {
                        boolean readKo = getNullValue(nullRead, i);
                        boolean writeKo = getNullValue(nullWrite, i);
                        if (readKo == writeKo) {
                            if (readKo) {
                                diffString[i] = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                            } else if (ObjectUtils.sameObject(readString[i], writeString[i])) {
                                diffString[i] = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                            } else {
                                diffString[i] = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_DIFFERENT");
                            }
                        } else if ((writeKo && (readString[i] == null)) || (readKo && (writeString[i] == null))) {
                            diffString[i] = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_SAME");
                        } else {
                            diffString[i] = Messages.getMessage("SNAPSHOT_COMPARE_VALUE_DIFFERENT");
                        }
                    }
                    ret = diffString;
                    nullElements = null;
                }
                break;

            default:
                // nothing to do

        } // end switch (dataType)

        return new NullableData<boolean[]>(ret, nullElements);
    }

    /**
     * Returns a SnapshotAttributeDeltaAbsValue calculated from the values of
     * writeAbsValue and readAbsValue when possible. When impossible, returns a
     * "Not applicable" SnapshotAttributeDeltaAbsValue.
     * 
     * @param writeAbsValue The write value
     * @param readAbsValue The read value
     * @return A SnapshotAttributeDeltaAbsValue calculated from the values of writeAbsValue and readAbsValue when
     *         possible
     */
    public static SnapshotAttributeDeltaAbsValue getInstance(SnapshotAttributeWriteAbsValue writeAbsValue,
            SnapshotAttributeReadAbsValue readAbsValue) {
        switch (writeAbsValue.getDataType()) {
            case TangoConst.Tango_DEV_FLOAT:
            case TangoConst.Tango_DEV_ULONG:
            case TangoConst.Tango_DEV_LONG:
            case TangoConst.Tango_DEV_DOUBLE:
            case TangoConst.Tango_DEV_USHORT:
            case TangoConst.Tango_DEV_SHORT:
            case TangoConst.Tango_DEV_UCHAR:
            case TangoConst.Tango_DEV_CHAR:
                return new SnapshotAttributeDeltaAbsValue(writeAbsValue, readAbsValue);

            default:
                return new SnapshotAttributeDeltaAbsValue(SnapshotAttributeValue.NOT_APPLICABLE_DATA_FORMAT, null,
                        null);
        }
    }

}
