// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/bensikin/bensikin/data/snapshot/SnapshotAttributeValue.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class SnapshotAttributeValue.
// (Claisse Laurent) - 22 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.19 $
//
// $Log: SnapshotAttributeValue.java,v $
// Revision 1.19 2007/08/30 14:33:28 ounsy
// small bug correction
//
// Revision 1.18 2007/08/23 12:59:23 ounsy
// toUserFriendlyString() available in SnapshotAttributeValue
//
// Revision 1.17 2007/07/03 08:39:30 ounsy
// * managing null value
//
// Revision 1.16 2007/06/29 09:18:58 ounsy
// devLong represented as Integer + SnapshotCompareTable sorting
//
// Revision 1.15 2007/06/28 12:51:00 ounsy
// spectrum and image values represented as arrays
//
// Revision 1.14 2007/02/28 11:18:18 ounsy
// better snapshot file management
//
// Revision 1.13 2006/10/31 16:54:08 ounsy
// milliseconds and null values management
//
// Revision 1.12 2006/06/28 12:51:06 ounsy
// image support
//
// Revision 1.11 2006/06/16 08:52:57 ounsy
// ready for images
//
// Revision 1.10 2006/04/13 12:37:33 ounsy
// new spectrum types support
//
// Revision 1.9 2006/03/20 15:51:04 ounsy
// added the case of Snapshot delta value for spectrums which
// read and write parts are the same length.
//
// Revision 1.8 2006/03/16 16:40:52 ounsy
// taking care of String formating
//
// Revision 1.7 2006/03/15 15:08:36 ounsy
// boolean scalar management
//
// Revision 1.6 2006/02/15 09:21:27 ounsy
// minor changes
//
// Revision 1.5 2005/11/29 18:25:08 chinkumo
// no message
//
// Revision 1.1.1.2 2005/08/22 11:58:38 chinkumo
// First commit
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.bensikin.data.snapshot;

import java.lang.reflect.Array;
import java.util.Comparator;
import java.util.Locale;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.tools.StringFormater;
import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.math.MathConst;

/**
 * All snapshot attributes values, be it read write or delta, extend this class
 * that implements basic cast operations, value tests, and String
 * representation.
 * 
 * @author CLAISSE
 */
public class SnapshotAttributeValue {

    private static final String NAN_ARRAY = "[NaN]";
    private static final String NAN = "NaN";
    protected static final String NULL = "null";
    private static final String TRUE = "true";

    protected int dataFormat;
    protected int dataType;

    private static final Logger LOGGER = LoggerFactory.getLogger(SnapshotAttributeValue.class);

    /**
     * The XML tag name used in saving/loading
     */
    public static final String NOT_APPLICABLE_XML_TAG = "notApplicable";

    /**
     * The data format used for "not applicable" values, ie. values that are
     * meaningless (such as the delta value of a read-only attribute)
     */
    public static final int NOT_APPLICABLE_DATA_FORMAT = -1;

    /**
     * The data format used for scalar values, ie. 0-dimensionnal values
     */
    public static final int SCALAR_DATA_FORMAT = 0;

    /**
     * The data format used for spectrum values, ie. 1-dimensionnal values
     */
    public static final int SPECTRUM_DATA_FORMAT = 1;

    /**
     * The data format used for image values, ie. 2-dimensionnal values
     */
    public static final int IMAGE_DATA_FORMAT = 2;

    /**
     * The attribute value
     */
    protected Object value;

    /**
     * The display format in case of scalar value
     */
    protected String display_format;

    /**
     * The attribute value, if it's a scalar
     */
    protected Object scalarValue;

    /**
     * The attribute value, if it's a spectrum
     */
    protected Object spectrumValue;

    /**
     * The attribute value, if it's an image
     */
    protected Object[] imageValue;

    /**
     * The attribute null elements. May be boolean[] or boolean[][]
     */
    protected Object nullElements;

    /**
     * True if the value is a "not applicable" value, ie. a value that is
     * meaningless (such as the delta value of a read-only attribute)
     */
    protected boolean isNotApplicable = false;

    /**
     * True if the value has been user-modified
     */
    protected boolean isModified = false;

    /**
     * Separator used for spectrum and images values
     */
    public static final String X_SEPARATOR = ",";

    /**
     * Separator used for images values (not used yet)
     */
    public static final String Y_SEPARATOR = "|";

    /**
     * Builds a SnapshotAttributeValue given its format, type, and value.
     * 
     * @param dataFormat The data format ( scalar, spectrum, or image )
     * @param dataType The Tango data type
     * @param value The value content
     * @param nullElements The null elements identifier
     * @throws IllegalArgumentException if dataFormat is not in (SCALAR_DATA_FORMAT,SPECTRUM_DATA_FORMAT
     *             ,IMAGE_DATA_FORMAT,NOT_APPLICABLE_DATA_FORMAT)
     */
    public SnapshotAttributeValue(final int dataFormat, final int dataType, final Object value,
            final Object nullElements) throws IllegalArgumentException {
        this.dataFormat = dataFormat;
        this.dataType = dataType;

        switch (dataFormat) {

            case NOT_APPLICABLE_DATA_FORMAT:
                setNotApplicable(true);
            case SCALAR_DATA_FORMAT:
            case SPECTRUM_DATA_FORMAT:
            case IMAGE_DATA_FORMAT:
                setValue(value, nullElements);
                break;

            default:
                throw new IllegalArgumentException();
        }
    }

    protected boolean getNullValue(boolean[] nullElements, int index) {
        boolean result = false;
        if ((nullElements != null) && (index < nullElements.length)) {
            result = nullElements[index];
        }
        return result;
    }

    /**
     * Returns a formated representation of the value
     */
    public String toFormatedString() {
        StringBuilder ret = new StringBuilder();
        if (dataFormat == SCALAR_DATA_FORMAT) {
            if (scalarValue != null) {
                String formatedValue = "";
                if (scalarValue instanceof String) {
                    ret.append(StringFormater.formatStringToRead(new String((String) scalarValue)));
                } else if ((display_format == null) || display_format.trim().isEmpty()
                        || (dataType == TangoConst.Tango_DEV_BOOLEAN) || !(scalarValue instanceof Number)) {
                    ret.append(scalarValue);
                } else {
                    try {
                        formatedValue = Format.format(Locale.UK, display_format, (Number) scalarValue);
                        // Check that formatedValue still represents a number
                        if (formatedValue == null) {
                            ret.append(scalarValue);
                        } else {
                            Double val = null;
                            try {
                                val = Double.valueOf(formatedValue);
                            } catch (final Exception e) {
                                val = null;
                            }
                            if (val == null) {
                                ret.append(scalarValue);
                            } else {
                                ret.append(formatedValue);
                            }
                            val = null;
                        }
                    } catch (final Exception e) {
                        e.printStackTrace();
                        ret.append(scalarValue);
                    }
                }
            }
        } else {
            appendToString(ret);
        }
        return ret.toString().trim();
    }

    /**
     * Returns a XML representation of the value.
     * 
     * @return a XML representation of the value
     */
    @Override
    public String toString() {
        final StringBuilder buffer = new StringBuilder();
        appendToString(buffer);
        return buffer.toString().trim();
    }

    protected boolean isSpectrumValueOk() {
        boolean isOk;
        if ((spectrumValue == null) || (!spectrumValue.getClass().isArray()) || (Array.getLength(spectrumValue) == 0)) {
            isOk = false;
        } else {
            Object tmp = Array.get(spectrumValue, 0);
            isOk = (!NAN.equals(tmp)) && (!NAN_ARRAY.equals(tmp));
        }
        return isOk;
    }

    public void appendToString(final StringBuilder buffer) {
        if (buffer != null) {
            switch (dataFormat) {
                case SCALAR_DATA_FORMAT:
                    if (scalarValue != null) {
                        buffer.append(scalarValue);
                    }
                    break;

                case SPECTRUM_DATA_FORMAT:
                    if (isSpectrumValueOk()) {
                        // Don't use dataType as attribute may have change since first registering (TANGOARCH-210)
                        int dimX;
                        if (spectrumValue instanceof String[]) {
                            final String[] stval = (String[]) spectrumValue;
                            dimX = stval.length;
                            for (int i = 0; i < dimX; i++) {
                                buffer.append(StringFormater.formatStringToRead(new String(stval[i])));
                                if (i < dimX - 1) {
                                    buffer.append(X_SEPARATOR);
                                }
                            }
                        } else if (spectrumValue instanceof double[]) {
                            final double[] dval = (double[]) spectrumValue;
                            dimX = dval.length;
                            for (int i = 0; i < dimX; i++) {
                                buffer.append(dval[i]);
                                if (i < dimX - 1) {
                                    buffer.append(X_SEPARATOR);
                                }
                            }
                        } else if (spectrumValue instanceof float[]) {
                            final float[] fval = (float[]) spectrumValue;
                            dimX = fval.length;
                            for (int i = 0; i < dimX; i++) {
                                buffer.append(fval[i]);
                                if (i < dimX - 1) {
                                    buffer.append(X_SEPARATOR);
                                }
                            }
                        } else if (spectrumValue instanceof long[]) {
                            final long[] sval = (long[]) spectrumValue;
                            dimX = sval.length;
                            for (int i = 0; i < dimX; i++) {
                                buffer.append(sval[i]);
                                if (i < dimX - 1) {
                                    buffer.append(X_SEPARATOR);
                                }
                            }
                        } else if (spectrumValue instanceof int[]) {
                            final int[] lval = (int[]) spectrumValue;
                            dimX = lval.length;
                            for (int i = 0; i < dimX; i++) {
                                buffer.append(lval[i]);
                                if (i < dimX - 1) {
                                    buffer.append(X_SEPARATOR);
                                }
                            }
                        } else if (spectrumValue instanceof short[]) {
                            final short[] sval = (short[]) spectrumValue;
                            dimX = sval.length;
                            for (int i = 0; i < dimX; i++) {
                                buffer.append(sval[i]);
                                if (i < dimX - 1) {
                                    buffer.append(X_SEPARATOR);
                                }
                            }
                        } else if (spectrumValue instanceof byte[]) {
                            final byte[] cval = (byte[]) spectrumValue;
                            dimX = cval.length;
                            for (int i = 0; i < dimX; i++) {
                                buffer.append(cval[i]);
                                if (i < dimX - 1) {
                                    buffer.append(X_SEPARATOR);
                                }
                            }
                        } else if (spectrumValue instanceof boolean[]) {
                            final boolean[] bval = (boolean[]) spectrumValue;
                            dimX = bval.length;
                            for (int i = 0; i < dimX; i++) {
                                buffer.append(bval[i]);
                                if (i < dimX - 1) {
                                    buffer.append(X_SEPARATOR);
                                }
                            }
                        } else {
                            dimX = Array.getLength(spectrumValue);
                            for (int i = 0; i < dimX; i++) {
                                buffer.append(Array.get(spectrumValue, i));
                                if (i < dimX - 1) {
                                    buffer.append(X_SEPARATOR);
                                }
                            }
                        }
                    } else {
                        buffer.append(NAN);
                    }
                    break;

                case IMAGE_DATA_FORMAT:
                    if (imageValue != null) {
                        for (int i = 0; i < imageValue.length; i++) {
                            int length = Array.getLength(imageValue[i]);
                            for (int j = 0; j < length; j++) {
                                buffer.append(Array.get(imageValue[i], j));
                                if (j < length - 1) {
                                    buffer.append(Y_SEPARATOR);
                                }

                            }
                            if (i < imageValue.length - 1) {
                                buffer.append(X_SEPARATOR);
                            }
                        }
                    }
                    break;
            }

        }
    }

    /**
     * Sets the value attribute, and depending on the data format, the scalarValue, imageValue, or spectrumValue.
     * 
     * @param value The raw value
     * @param nullElements The data that indicates null elements in value (can be Boolean, boolean[], boolean[][],
     *            etc...)
     */
    public void setValue(final Object value, final Object nullElements) {
        this.nullElements = nullElements;
        this.value = value;
        switch (dataFormat) {
            case SCALAR_DATA_FORMAT:
                if (value instanceof String) {
                    try {
                        final String stringValue = ((String) value).trim();
                        Double doubleValue = null;

                        try {
                            doubleValue = Double.valueOf(stringValue);
                        } catch (final Exception e) {
                            doubleValue = null;
                        }

                        switch (dataType) {

                            case TangoConst.Tango_DEV_STRING:
                                this.scalarValue = stringValue;
                                break;

                            case TangoConst.Tango_DEV_DOUBLE:
                                this.scalarValue = doubleValue;
                                this.nullElements = Boolean.valueOf(doubleValue == null);
                                break;

                            case TangoConst.Tango_DEV_FLOAT:
                                this.scalarValue = doubleValue == null ? null : Float.valueOf(doubleValue.floatValue());
                                this.nullElements = Boolean.valueOf(doubleValue == null);
                                break;

                            case TangoConst.Tango_DEV_USHORT:
                            case TangoConst.Tango_DEV_SHORT:
                                this.scalarValue = doubleValue == null ? null : Short.valueOf(doubleValue.shortValue());
                                this.nullElements = Boolean.valueOf(doubleValue == null);
                                break;

                            case TangoConst.Tango_DEV_STATE:
                            case TangoConst.Tango_DEV_ULONG:
                            case TangoConst.Tango_DEV_LONG:
                                this.scalarValue = doubleValue == null ? null : Integer.valueOf(doubleValue.intValue());
                                this.nullElements = Boolean.valueOf(doubleValue == null);
                                break;

                            case TangoConst.Tango_DEV_BOOLEAN:
                                this.scalarValue = Boolean.valueOf(stringValue);
                                this.nullElements = Boolean.FALSE;
                                break;

                            default:
                                this.scalarValue = value;

                        } // end switch (dataType)
                    } catch (final Exception e) {
                        this.scalarValue = value;
                    }
                } // end if (value instanceof String)
                else {
                    this.scalarValue = value;
                }
                break;

            case SPECTRUM_DATA_FORMAT:
                if (value instanceof String) {
                    try {
                        final String stringValue = ((String) value).trim();
                        if (stringValue == null || stringValue.isEmpty()) {
                            this.spectrumValue = null;
                            break;
                        }
                        final StringTokenizer tokenizer = new StringTokenizer(stringValue, ",");
                        final int length = tokenizer.countTokens();

                        final boolean[] nullElementsArray = new boolean[length];
                        switch (dataType) {

                            case TangoConst.Tango_DEV_STRING:
                                final String[] strArray = new String[length];
                                for (int i = 0; i < length; i++) {
                                    String token = tokenizer.nextToken();
                                    if (token.isEmpty() || NULL.equals(token)) {
                                        strArray[i] = null;
                                    } else {
                                        strArray[i] = token;
                                    }
                                }
                                if (!(nullElements instanceof boolean[])) {
                                    this.nullElements = nullElementsArray;
                                }
                                break;

                            case TangoConst.Tango_DEV_DOUBLE:
                                final double[] dbArray = new double[length];
                                for (int i = 0; i < length; i++) {
                                    String token = tokenizer.nextToken();
                                    if (token.isEmpty() || NULL.equals(token)) {
                                        dbArray[i] = MathConst.NAN_FOR_NULL;
                                        nullElementsArray[i] = true;
                                    } else {
                                        dbArray[i] = Double.parseDouble(token);
                                    }
                                }
                                this.spectrumValue = dbArray;
                                if (!(nullElements instanceof boolean[])) {
                                    this.nullElements = nullElementsArray;
                                }
                                break;

                            case TangoConst.Tango_DEV_FLOAT:
                                final float[] flArray = new float[length];
                                for (int i = 0; i < length; i++) {
                                    String token = tokenizer.nextToken();
                                    if (token.isEmpty() || NULL.equals(token)) {
                                        flArray[i] = Float.NaN;
                                        nullElementsArray[i] = true;
                                    } else {
                                        flArray[i] = Float.parseFloat(token);
                                    }
                                }
                                this.spectrumValue = flArray;
                                if (!(nullElements instanceof boolean[])) {
                                    this.nullElements = nullElementsArray;
                                }
                                break;

                            case TangoConst.Tango_DEV_USHORT:
                            case TangoConst.Tango_DEV_SHORT:
                                final short[] shArray = new short[length];
                                for (int i = 0; i < length; i++) {
                                    String token = tokenizer.nextToken();
                                    if (token.isEmpty() || NULL.equals(token)) {
                                        shArray[i] = 0;
                                        nullElementsArray[i] = true;
                                    } else {
                                        shArray[i] = (short) Double.parseDouble(token);
                                    }
                                    token = null;
                                }
                                this.spectrumValue = shArray;
                                if (!(nullElements instanceof boolean[])) {
                                    this.nullElements = nullElementsArray;
                                }
                                break;

                            case TangoConst.Tango_DEV_STATE:
                            case TangoConst.Tango_DEV_ULONG:
                            case TangoConst.Tango_DEV_LONG:
                                final int[] intArray = new int[length];
                                for (int i = 0; i < length; i++) {
                                    String token = tokenizer.nextToken();
                                    if (token.isEmpty() || NULL.equals(token)) {
                                        intArray[i] = 0;
                                        nullElementsArray[i] = true;
                                    } else {
                                        intArray[i] = (int) Double.parseDouble(token);
                                    }
                                }
                                this.spectrumValue = intArray;
                                if (!(nullElements instanceof boolean[])) {
                                    this.nullElements = nullElementsArray;
                                }
                                break;

                            case TangoConst.Tango_DEV_BOOLEAN:
                                final boolean[] boolArray = new boolean[length];
                                for (int i = 0; i < length; i++) {
                                    String token = tokenizer.nextToken();
                                    if (token.isEmpty() || NULL.equals(token)) {
                                        boolArray[i] = false;
                                        nullElementsArray[i] = true;
                                    } else {
                                        boolArray[i] = TRUE.equalsIgnoreCase(token);
                                    }
                                }
                                this.spectrumValue = boolArray;
                                if (!(nullElements instanceof boolean[])) {
                                    this.nullElements = nullElementsArray;
                                }
                                break;

                            default:
                                this.spectrumValue = new Object[] { value };
                                if (!(nullElements instanceof boolean[])) {
                                    this.nullElements = nullElementsArray;
                                }

                        } // end switch (dataType)
                    } catch (final Exception e) {
                        this.spectrumValue = new Object[] { value };
                        this.nullElements = null;
                    }
                } // end if (value instanceof String)
                else {
                    if (value == null) {
                        this.spectrumValue = null;
                    } else {
                        int rank = ArrayUtils.recoverArrayRank(value.getClass());
                        if (rank == 0) {
                            if (nullElements instanceof Boolean) {
                                this.nullElements = new boolean[] { ((Boolean) nullElements).booleanValue() };
                            }
                            if (value instanceof Boolean) {
                                this.spectrumValue = new boolean[] { ((Boolean) value).booleanValue() };
                            } else if (value instanceof Byte) {
                                this.spectrumValue = new byte[] { ((Byte) value).byteValue() };
                            } else if (value instanceof Short) {
                                this.spectrumValue = new short[] { ((Short) value).shortValue() };
                            } else if (value instanceof Integer) {
                                this.spectrumValue = new int[] { ((Integer) value).intValue() };
                            } else if (value instanceof Long) {
                                this.spectrumValue = new long[] { ((Long) value).longValue() };
                            } else if (value instanceof Float) {
                                this.spectrumValue = new float[] { ((Float) value).floatValue() };
                            } else if (value instanceof Double) {
                                this.spectrumValue = new double[] { ((Double) value).doubleValue() };
                            } else {
                                this.spectrumValue = new Object[] { value };
                            }
                        } else {
                            this.spectrumValue = value;
                        }
                    }
                }
                break;

            case IMAGE_DATA_FORMAT:
                if (value == null) {
                    setImageValue(null, null);
                } else {
                    int rank = ArrayUtils.recoverArrayRank(value.getClass());
                    boolean[][] nullElementsMatrix;
                    if (rank == 0) {
                        if (nullElements instanceof boolean[]) {
                            nullElementsMatrix = (boolean[][]) nullElements;
                        } else if (nullElements instanceof Boolean) {
                            nullElementsMatrix = new boolean[][] { { ((Boolean) nullElements).booleanValue() } };
                        } else {
                            nullElementsMatrix = new boolean[][] { { value == null } };
                        }
                        if (value instanceof Boolean) {
                            setImageValue(new boolean[][] { { ((Boolean) value).booleanValue() } }, nullElementsMatrix);
                        } else if (value instanceof Byte) {
                            setImageValue(new byte[][] { { ((Byte) value).byteValue() } }, nullElementsMatrix);
                        } else if (value instanceof Short) {
                            setImageValue(new short[][] { { ((Short) value).shortValue() } }, nullElementsMatrix);
                        } else if (value instanceof Integer) {
                            setImageValue(new int[][] { { ((Integer) value).intValue() } }, nullElementsMatrix);
                        } else if (value instanceof Long) {
                            setImageValue(new long[][] { { ((Long) value).longValue() } }, nullElementsMatrix);
                        } else if (value instanceof Float) {
                            setImageValue(new float[][] { { ((Float) value).floatValue() } }, nullElementsMatrix);
                        } else if (value instanceof Double) {
                            setImageValue(new double[][] { { ((Double) value).doubleValue() } }, nullElementsMatrix);
                        } else {
                            setImageValue(new Object[][] { { value } }, nullElementsMatrix);
                        }
                    } else if (rank == 1) {
                        if (nullElements instanceof boolean[]) {
                            nullElementsMatrix = (boolean[][]) nullElements;
                        } else if (nullElements instanceof boolean[]) {
                            nullElementsMatrix = new boolean[][] { (boolean[]) nullElements };
                        } else {
                            nullElementsMatrix = null;
                        }
                        setImageValue(new Object[] { value }, nullElementsMatrix);
                    } else {
                        if (nullElements instanceof boolean[]) {
                            nullElementsMatrix = (boolean[][]) nullElements;
                        } else {
                            nullElementsMatrix = null;
                        }
                        setImageValue((Object[]) value, nullElementsMatrix);
                    }
                }
                break;

            case NOT_APPLICABLE_DATA_FORMAT:
                setNotApplicable(true);
                break;

            default:
                throw new IllegalStateException();
        }
    }

    /**
     * Tests whether a value is coherent with the data type
     * 
     * @param value
     *            The value to test
     * @return True if it is, false otherwise
     */
    public boolean validateValue(final Object value) {
        if (value == null) {
            return false;
        }
        try {
            SnapshotAttributeValue.castValue(value, getDataType());
        } catch (final ClassCastException e) {
            return false;
        } catch (final NumberFormatException e) {
            return false;
        } catch (final Exception e) {
            LOGGER.error("Abnormal case in validateValue", e);
            return false;
        }
        return true;
    }

    /**
     * @return Returns the isModified.
     */
    public boolean isModified() {
        return isModified;
    }

    /**
     * @param isModified
     *            The isModified to set.
     */
    public void setModified(final boolean isModified) {
        this.isModified = isModified;
    }

    /**
     * Casts a value content to its Tango type.
     * 
     * @param value
     *            The value content
     * @param _data_type
     *            The Tango type
     */
    // XXX RG: This code seems stupid, and it should not even work. Remove it?
    public static Object castValue(final Object value, final int _data_type) {
        if (value == null) {
            return null;
        }
        if (!(value instanceof String)) {
            return value;
        }
        String s;
        switch (_data_type) {
            case TangoConst.Tango_DEV_STRING:
                return value;
            case TangoConst.Tango_DEV_STATE:
                try {
                    return value;
                } catch (final ClassCastException e) {
                    s = (String) value;
                    return Integer.valueOf(s);
                }
            case TangoConst.Tango_DEV_UCHAR:
                try {
                    return value;
                } catch (final ClassCastException e) {
                    s = (String) value;
                    return Byte.valueOf(s);
                }
            case TangoConst.Tango_DEV_LONG:
                try {
                    return value;
                } catch (final ClassCastException e) {
                    s = (String) value;
                    return Integer.valueOf(s);
                }
            case TangoConst.Tango_DEV_ULONG:
                try {
                    return value;
                } catch (final ClassCastException e) {
                    s = (String) value;
                    return Integer.valueOf(s);
                }
            case TangoConst.Tango_DEV_BOOLEAN:
                try {
                    return value;
                } catch (final ClassCastException e) {
                    s = (String) value;
                    return Boolean.valueOf(s);
                }
            case TangoConst.Tango_DEV_SHORT:
                try {
                    return value;
                } catch (final ClassCastException e) {
                    s = (String) value;
                    return Short.valueOf(s);
                }
            case TangoConst.Tango_DEV_FLOAT:
                try {
                    return value;
                } catch (final ClassCastException e) {
                    s = (String) value;
                    return Float.valueOf(s);
                }
            case TangoConst.Tango_DEV_DOUBLE:
                try {
                    return value;
                } catch (final ClassCastException e) {
                    s = (String) value;
                    return Double.valueOf(s);
                }
            default:
                try {
                    return value;
                } catch (final ClassCastException e) {
                    s = (String) value;
                    return Double.valueOf(s);
                }
        }
    }

    /**
     * @return Returns the imageValue.
     */
    public Object[] getImageValue() {
        return imageValue;
    }

    /**
     * @param imageValue
     *            The imageValue to set.
     */
    public void setImageValue(final Object[] imageValue, final boolean[][] nullElements) {
        if (imageValue == null) {
            this.imageValue = null;
        } else {
            Object val = Array.get(imageValue[0], 0);
            if (NAN.equals(val) || NAN_ARRAY.equals(val)) {
                this.imageValue = null;
            } else {
                this.imageValue = imageValue;
            }
        }
        this.nullElements = nullElements;
    }

    /**
     * @return Returns the scalarValue.
     */
    public Object getScalarValue() {
        return scalarValue;
    }

    /**
     * @param scalarValue
     *            The scalarValue to set.
     */
    public void setScalarValue(final Object scalarValue, final Object nullElements) {
        this.scalarValue = scalarValue;
        this.nullElements = nullElements;
    }

    /**
     * @return Returns the spectrumValue.
     */
    public Object getSpectrumValue() {
        return spectrumValue;
    }

    /**
     * @param spectrumValue
     *            The spectrumValue to set.
     */
    public void setSpectrumValue(final Object spectrumValue, final Object nullElements) {
        if (spectrumValue == null) {
            this.spectrumValue = null;
        } else {
            Object val = Array.get(spectrumValue, 0);
            if (NAN.equals(val) || NAN_ARRAY.equals(val)) {
                this.spectrumValue = null;
            } else {
                this.spectrumValue = spectrumValue;
            }
        }
        this.nullElements = nullElements;
    }

    public Object getNullElements() {
        return nullElements;
    }

    /**
     * @return Returns the value.
     */
    public Object getValue() {
        return value;
    }

    /**
     * @return Returns the dataFormat.
     */
    public int getDataFormat() {
        return dataFormat;
    }

    /**
     * @param dataFormat
     *            The dataFormat to set.
     */
    public void setDataFormat(final int dataFormat) {
        this.dataFormat = dataFormat;
    }

    /**
     * @return Returns the dataType.
     */
    public int getDataType() {
        return dataType;
    }

    /**
     * @param dataType
     *            The dataType to set.
     */
    public void setDataType(final int dataType) {
        this.dataType = dataType;
    }

    /**
     * @return Returns the isNotApplicable.
     */
    public boolean isNotApplicable() {
        return isNotApplicable;
    }

    /**
     * @param isNotApplicable
     *            The isNotApplicable to set.
     */
    public void setNotApplicable(final boolean isNotApplicable) {
        this.isNotApplicable = isNotApplicable;
    }

    /**
     * @return the display format in case of scalar value
     */
    public String getDisplayFormat() {
        return display_format;
    }

    /**
     * Sets the display format in case of scalar value
     * 
     * @param format
     *            the display format
     */
    public void setDisplayFormat(final String format) {
        display_format = format;
        // updates value formating
        if (value != null) {
            setValue(value, nullElements);
        }
    }

    /**
     * @return
     */
    public String toUserFriendlyString() {
        String ret = "";

        ret += toString();

        return ret;
    }

    public static class SnapshotValueComparator implements Comparator<SnapshotAttributeValue> {

        @Override
        public int compare(SnapshotAttributeValue obj1, SnapshotAttributeValue obj2) {
            Object obj1_value = obj1.getValue();
            Object obj2_value = obj2.getValue();

            // String valueType1 = (obj1_value != null) ?
            // obj1_value.getClass().getSimpleName() : "NULL";
            // String valueType2 = (obj2_value != null) ?
            // obj2_value.getClass().getSimpleName() : "NULL";
            //
            // System.out.println("Obj1 value : " + valueType1 +
            // ", Obj2 value : " + valueType2);

            // Order : Number > String > Boolean > Array > Others

            int comp;

            if (obj1_value != null && obj1_value.getClass().isArray()) {
                if (obj2_value == null) {
                    comp = 1;
                } else if (obj2_value != null && obj2_value.getClass().isArray()) {
                    comp = 0;
                } else {
                    comp = -1;
                }
            } else if (obj1_value instanceof Boolean) {
                if (obj2_value instanceof Boolean) {
                    comp = ((Boolean) obj1_value).compareTo((Boolean) obj2_value);
                } else if (obj2_value instanceof Number || obj2_value instanceof String) {
                    comp = -1;
                } else {
                    comp = 1;
                }
            } else if (obj1_value instanceof String) {
                if (obj2_value instanceof Number) {
                    comp = -1;
                } else if (obj2_value instanceof String) {
                    comp = ((String) obj1_value).compareTo(((String) obj2_value));
                } else {
                    comp = 1;
                }
            } else if (obj1_value instanceof Number) {
                if (obj2_value instanceof Number) {
                    double valueDouble = ((Number) obj1_value).doubleValue();
                    double o_valueDouble = ((Number) obj2_value).doubleValue();
                    comp = Double.compare(valueDouble, o_valueDouble);
                } else {
                    comp = 1;
                }
            } else {
                comp = -1;
            }
            return comp;
        }
    }

}
