package fr.soleil.bensikin.data.snapshot;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.bensikin.xml.BensikinXMLLine;

public class SnapshotAttributeWriteAbsValue extends SnapshotAttributeValue {

    /**
     * The XML tag name used in saving/loading
     */
    public static final String XML_TAG = "WriteAbsValue";

    private boolean settable;

    /**
     * Calls its mother's constructor.
     * 
     * @param dataFormat The data format (scalar/spectrum/image)
     * @param dataType The data type (double/string/...)
     * @param value The write value, not cast yet
     * @param nullElements The null elements identifier
     */
    public SnapshotAttributeWriteAbsValue(int dataFormat, int dataType, Object value, Object nullElements) {
        super(dataFormat, dataType, value, nullElements);
        this.settable = false;
    }

    /**
     * Returns a XML representation of the write value.
     * 
     * @return a XML representation of the write value
     */
    public String toXMLString() {
        String ret = "";

        BensikinXMLLine openingLine = new BensikinXMLLine(SnapshotAttributeWriteAbsValue.XML_TAG,
                BensikinXMLLine.OPENING_TAG_CATEGORY);
        BensikinXMLLine closingLine = new BensikinXMLLine(SnapshotAttributeWriteAbsValue.XML_TAG,
                BensikinXMLLine.CLOSING_TAG_CATEGORY);

        openingLine.setAttribute(SnapshotAttributeValue.NOT_APPLICABLE_XML_TAG, String.valueOf(this.isNotApplicable));

        ret += openingLine.toString();
        ret += GUIUtilities.CRLF;

        ret += super.toString();
        ret += GUIUtilities.CRLF;

        ret += closingLine.toString();

        return ret;
    }

    /**
     * Sets the boolean value to know wheather this WriteValue can be used to
     * set equipment or not
     * 
     * @param set The boolean value
     */
    public void setSettable(boolean set) {
        this.settable = set;
    }

    /**
     * @return a boolean value that tells wheather this WriteValue can be used to set equipment or not
     */
    public boolean isSettable() {
        return this.settable;
    }
}
