//+======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/data/snapshot/manager/DummySnapshotManagerImpl.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  DummySnapshotManagerImpl.
//						(Claisse Laurent) - sept. 2005
//
// $Author: ounsy $
//
// $Revision: 1.2 $
//
// $Log: DummySnapshotManagerImpl.java,v $
// Revision 1.2  2005/12/14 16:38:10  ounsy
// added methods necessary for the new Word-like file management
//
// Revision 1.1  2005/12/14 14:07:18  ounsy
// first commit  including the new  "tools,xml,lifecycle,profile" sub-directories
// under "bensikin.bensikin" and removing the same from their former locations
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.data.snapshot.manager;

import fr.soleil.bensikin.data.snapshot.Snapshot;

/**
 * Dummy implementation. Does nothing.
 * 
 * @author CLAISSE
 */
public class DummySnapshotManagerImpl implements ISnapshotManager {

    public DummySnapshotManagerImpl() {
        super();
    }

    @Override
    public void startUp() throws Exception {
        // nothing to do

    }

    @Override
    public void saveSnapshot(Snapshot ac) throws Exception {
        // nothing to do
    }

    @Override
    public String getDefaultSaveLocation() {
        // nothing to do
        return null;
    }

    @Override
    public String getSaveLocation() {
        // nothing to do
        return null;
    }

    @Override
    public Snapshot loadSnapshot() throws Exception {
        // nothing to do
        return null;
    }

    @Override
    public String getFileName() {
        // nothing to do
        return null;
    }

    @Override
    public String getNonDefaultSaveLocation() {
        // nothing to do
        return null;
    }

    @Override
    public void setNonDefaultSaveLocation(String location) {
        // nothing to do
    }

}
