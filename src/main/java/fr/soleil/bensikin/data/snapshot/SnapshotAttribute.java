//+======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/data/snapshot/SnapshotAttribute.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  SnapshotAttribute.
//						(Claisse Laurent) - 22 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.16 $
//
// $Log: SnapshotAttribute.java,v $
// Revision 1.16  2007/07/03 08:39:54  ounsy
// trace removed
//
// Revision 1.15  2007/06/29 09:18:58  ounsy
// devLong represented as Integer + SnapshotCompareTable sorting
//
// Revision 1.14  2006/10/31 16:54:08  ounsy
// milliseconds and null values management
//
// Revision 1.13  2006/06/28 12:50:52  ounsy
// image support
//
// Revision 1.12  2006/04/13 12:37:33  ounsy
// new spectrum types support
//
// Revision 1.11  2006/03/29 10:22:13  ounsy
// added a protection against null values
//
// Revision 1.10  2006/03/27 15:26:05  ounsy
// avoiding java.lang.NegativeArraySizeException
//
// Revision 1.9  2006/03/20 15:51:04  ounsy
// added the case of Snapshot delta value for spectrums which
// read and write parts are the same length.
//
// Revision 1.8  2006/03/14 13:06:25  ounsy
// corrected the SNAP/spectrums/RW problem
// about the read and write values having the same length
//
// Revision 1.7  2006/02/15 09:20:12  ounsy
// spectrums rw management
//
// Revision 1.6  2005/12/14 16:35:56  ounsy
// added methods necessary for direct clipboard edition
//
// Revision 1.5  2005/11/29 18:25:08  chinkumo
// no message
//
// Revision 1.1.1.2  2005/08/22 11:58:38  chinkumo
// First commit
//
//
// copyleft :		Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.data.snapshot;

import java.lang.reflect.Array;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.TangoApi.AttributeProxy;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.tools.AttributeLight;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.snap.api.tools.SnapAttributeExtract;
import fr.soleil.bensikin.options.Options;
import fr.soleil.bensikin.options.sub.SnapshotOptions;
import fr.soleil.bensikin.xml.BensikinXMLLine;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Represents an attribute attached to a snapshot. A SnapshotAttribute belongs
 * to a SnapshotAttributes group, and has 3 kinds of value:
 * <UL>
 * <LI>A SnapshotAttributeReadValue
 * <LI>A SnapshotAttributeWriteValue
 * <LI>A SnapshotAttributeDeltaValue
 * </UL>
 * 
 * @author CLAISSE
 */
public class SnapshotAttribute {
    private SnapshotAttributeReadValue readValue;
    private SnapshotAttributeWriteValue writeValue;
    private SnapshotAttributeDeltaValue deltaValue;

    private String attributeCompleteName;
    private int attributeId = 0;
    private int dataType;
    private int dataFormat;// [0 - > SCALAR] (1 - > SPECTRUM] [2 - > IMAGE]
    private int permit;
    private boolean canSet;
    private String displayFormat;

    /**
     * The XML tag name used in saving/loading
     */
    public static final String XML_TAG = "Attribute";

    /**
     * The XML tag name used for the "id" property
     */
    public static final String ID_PROPERTY_XML_TAG = "Id";

    /**
     * The XML tag name used for the "data type" property
     */
    public static final String DATA_TYPE_PROPERTY_XML_TAG = "DataType";

    /**
     * The XML tag name used for the "data format" property
     */
    public static final String DATA_FORMAT_PROPERTY_XML_TAG = "DataFormat";

    /**
     * The XML tag name used for the "writable" property
     */
    public static final String WRITABLE_PROPERTY_XML_TAG = "Writable";

    /**
     * The XML tag name used for the "complete name" property
     */
    public static final String COMPLETE_NAME_PROPERTY_XML_TAG = "CompleteName";

    private SnapshotAttributes snapshotAttributes;

    /**
     * Default constructor, does nothing
     */
    public SnapshotAttribute() {

    }

    /**
     * Builds a SnapshotAttribute with the data from the SnapAttributeExtract,
     * keeping a reference to the SnapshotAttributes group this attribute is
     * rattached to.
     * 
     * @param extract
     *            The object containing the attribute fields.
     * @param attrs
     *            The SnapshotAttributes group this attribute belongs to.
     */
    public SnapshotAttribute(final SnapAttributeExtract extract, final SnapshotAttributes attrs) {
        snapshotAttributes = attrs;

        final int _idAttr = extract.getAttId();
        final int _format = extract.getDataFormat();
        final int _dataType = extract.getDataType();
        final int _writable = extract.getWritable();
        final String _completeName = extract.getAttributeCompleteName();

        attributeCompleteName = _completeName;
        attributeId = _idAttr;
        dataType = _dataType;
        dataFormat = _format;
        permit = _writable;
        displayFormat = ObjectUtils.EMPTY_STRING;

        SnapshotAttributeReadValue _readValue = null;
        SnapshotAttributeWriteValue _writeValue = null;
        // SnapshotAttributeDeltaValue _deltaValue = null;

        final Object val = extract.getValue();
        final Object nullElements = extract.getNullElements();

        switch (_format) {
            case AttrDataFormat._SCALAR:
                switch (_writable) {
                    case AttrWriteType._READ:
                        _readValue = new SnapshotAttributeReadValue(_format, _dataType, val, nullElements);
                        break;

                    case AttrWriteType._READ_WRITE:
                    case AttrWriteType._READ_WITH_WRITE:
                        final Object nullRead;
                        final Object nullWrite;
                        if (nullElements == null) {
                            nullRead = null;
                            nullWrite = null;
                        } else if (nullElements.getClass().isArray()) {
                            nullRead = Array.get(nullElements, 0);
                            nullWrite = Array.get(nullElements, 1);
                        } else {
                            // Mantis 25290
                            nullRead = nullElements;
                            nullWrite = null;
                        }
                        _readValue = new SnapshotAttributeReadValue(_format, _dataType, getValue(val, 0), nullRead);
                        _writeValue = new SnapshotAttributeWriteValue(_format, _dataType, getValue(val, 1), nullWrite);
                        break;

                    case AttrWriteType._WRITE:
                        _writeValue = new SnapshotAttributeWriteValue(_format, _dataType, val, nullElements);
                        break;

                }
                break;
            case AttrDataFormat._SPECTRUM:
                switch (_writable) {
                    case AttrWriteType._READ:
                        switch (_dataType) {
                            case TangoConst.Tango_DEV_STATE:
                            case TangoConst.Tango_DEV_STRING:
                            case TangoConst.Tango_DEV_BOOLEAN:
                            case TangoConst.Tango_DEV_CHAR:
                            case TangoConst.Tango_DEV_UCHAR:
                            case TangoConst.Tango_DEV_LONG:
                            case TangoConst.Tango_DEV_ULONG:
                            case TangoConst.Tango_DEV_SHORT:
                            case TangoConst.Tango_DEV_USHORT:
                            case TangoConst.Tango_DEV_FLOAT:
                            case TangoConst.Tango_DEV_DOUBLE:
                                _readValue = new SnapshotAttributeReadValue(_format, _dataType, val, nullElements);
                                break;
                            default:
                                // nothing to do
                        }
                        break;

                    case AttrWriteType._WRITE:
                        switch (_dataType) {
                            case TangoConst.Tango_DEV_STRING:
                            case TangoConst.Tango_DEV_BOOLEAN:
                            case TangoConst.Tango_DEV_CHAR:
                            case TangoConst.Tango_DEV_UCHAR:
                            case TangoConst.Tango_DEV_LONG:
                            case TangoConst.Tango_DEV_ULONG:
                            case TangoConst.Tango_DEV_SHORT:
                            case TangoConst.Tango_DEV_USHORT:
                            case TangoConst.Tango_DEV_FLOAT:
                            case TangoConst.Tango_DEV_DOUBLE:
                                _writeValue = new SnapshotAttributeWriteValue(_format, _dataType, val, nullElements);
                                break;
                            default:
                                // nothing to do
                        }
                        break;

                    case AttrWriteType._READ_WITH_WRITE:
                    case AttrWriteType._READ_WRITE:
                        final Object nullRead;
                        final Object nullWrite;
                        if (nullElements == null) {
                            nullRead = null;
                            nullWrite = null;
                        } else {
                            nullRead = Array.get(nullElements, 0);
                            nullWrite = Array.get(nullElements, 1);
                        }
                        switch (_dataType) {
                            case TangoConst.Tango_DEV_STRING:
                                try {
                                    final String[] readst = (String[]) getValue(val, 0);
                                    _readValue = new SnapshotAttributeReadValue(_format, _dataType, readst, nullRead);
                                } catch (final ClassCastException cce) {
                                    _readValue = new SnapshotAttributeReadValue(_format, _dataType, new String[0],
                                            null);
                                }
                                try {
                                    final String[] writest = (String[]) getValue(val, 1);
                                    _writeValue = new SnapshotAttributeWriteValue(_format, _dataType, writest,
                                            nullWrite);
                                } catch (final ClassCastException cce) {
                                    _writeValue = new SnapshotAttributeWriteValue(_format, _dataType, new String[0],
                                            null);
                                }
                                break;
                            case TangoConst.Tango_DEV_BOOLEAN:
                                try {
                                    final boolean[] readb = (boolean[]) getValue(val, 0);
                                    _readValue = new SnapshotAttributeReadValue(_format, _dataType, readb, nullRead);
                                } catch (final ClassCastException cce) {
                                    _readValue = new SnapshotAttributeReadValue(_format, _dataType, new boolean[0],
                                            null);
                                }
                                try {
                                    final boolean[] writeb = (boolean[]) getValue(val, 1);
                                    _writeValue = new SnapshotAttributeWriteValue(_format, _dataType, writeb,
                                            nullWrite);
                                } catch (final ClassCastException cce) {
                                    _writeValue = new SnapshotAttributeWriteValue(_format, _dataType, new boolean[0],
                                            null);
                                }
                                break;
                            case TangoConst.Tango_DEV_CHAR:
                            case TangoConst.Tango_DEV_UCHAR:
                                try {
                                    final byte[] readc = (byte[]) getValue(val, 0);
                                    _readValue = new SnapshotAttributeReadValue(_format, _dataType, readc, nullRead);
                                } catch (final ClassCastException cce) {
                                    _readValue = new SnapshotAttributeReadValue(_format, _dataType, new byte[0], null);
                                }
                                try {
                                    final byte[] writec = (byte[]) getValue(val, 1);
                                    _writeValue = new SnapshotAttributeWriteValue(_format, _dataType, writec,
                                            nullWrite);
                                } catch (final ClassCastException cce) {
                                    _writeValue = new SnapshotAttributeWriteValue(_format, _dataType, new byte[0],
                                            null);
                                }
                                break;
                            case TangoConst.Tango_DEV_LONG:
                            case TangoConst.Tango_DEV_ULONG:
                                try {
                                    final int[] readl = (int[]) getValue(val, 0);
                                    _readValue = new SnapshotAttributeReadValue(_format, _dataType, readl, nullRead);
                                } catch (final ClassCastException cce) {
                                    _readValue = new SnapshotAttributeReadValue(_format, _dataType, new int[0], null);
                                }
                                try {
                                    final int[] writel = (int[]) getValue(val, 1);
                                    _writeValue = new SnapshotAttributeWriteValue(_format, _dataType, writel,
                                            nullWrite);
                                } catch (final ClassCastException cce) {
                                    _writeValue = new SnapshotAttributeWriteValue(_format, _dataType, new int[0], null);
                                }
                                break;
                            case TangoConst.Tango_DEV_SHORT:
                            case TangoConst.Tango_DEV_USHORT:
                                try {
                                    final short[] reads = (short[]) getValue(val, 0);
                                    _readValue = new SnapshotAttributeReadValue(_format, _dataType, reads, nullRead);
                                } catch (final ClassCastException cce) {
                                    _readValue = new SnapshotAttributeReadValue(_format, _dataType, new short[0], null);
                                }
                                try {
                                    final short[] writes = (short[]) getValue(val, 1);
                                    _writeValue = new SnapshotAttributeWriteValue(_format, _dataType, writes,
                                            nullWrite);
                                } catch (final ClassCastException cce) {
                                    _writeValue = new SnapshotAttributeWriteValue(_format, _dataType, new short[0],
                                            null);
                                }
                                break;
                            case TangoConst.Tango_DEV_FLOAT:
                                try {
                                    final float[] readf = (float[]) getValue(val, 0);
                                    _readValue = new SnapshotAttributeReadValue(_format, _dataType, readf, nullRead);
                                } catch (final ClassCastException cce) {
                                    _readValue = new SnapshotAttributeReadValue(_format, _dataType, new float[0], null);
                                }
                                try {
                                    final float[] writef = (float[]) getValue(val, 1);
                                    _writeValue = new SnapshotAttributeWriteValue(_format, _dataType, writef,
                                            nullWrite);
                                } catch (final ClassCastException cce) {
                                    _writeValue = new SnapshotAttributeWriteValue(_format, _dataType, new float[0],
                                            null);
                                }
                                break;
                            case TangoConst.Tango_DEV_DOUBLE:
                                try {
                                    final double[] readd = (double[]) getValue(val, 0);
                                    _readValue = new SnapshotAttributeReadValue(_format, _dataType, readd, nullRead);
                                } catch (final ClassCastException cce) {
                                    _readValue = new SnapshotAttributeReadValue(_format, _dataType, new double[0],
                                            null);
                                }
                                try {
                                    final double[] writed = (double[]) getValue(val, 1);
                                    _writeValue = new SnapshotAttributeWriteValue(_format, _dataType, writed,
                                            nullWrite);
                                } catch (final ClassCastException cce) {
                                    _writeValue = new SnapshotAttributeWriteValue(_format, _dataType, new double[0],
                                            null);
                                }
                                break;
                            default: // nothing to do
                        }
                        break;

                    default: // nothing to do
                }
                break;

            case AttrDataFormat._IMAGE:// to do
                switch (_writable) {
                    case AttrWriteType._READ:
                        switch (_dataType) {
                            case TangoConst.Tango_DEV_CHAR:
                            case TangoConst.Tango_DEV_UCHAR:
                            case TangoConst.Tango_DEV_LONG:
                            case TangoConst.Tango_DEV_ULONG:
                            case TangoConst.Tango_DEV_SHORT:
                            case TangoConst.Tango_DEV_USHORT:
                            case TangoConst.Tango_DEV_FLOAT:
                            case TangoConst.Tango_DEV_DOUBLE:
                                _readValue = new SnapshotAttributeReadValue(_format, _dataType, val, nullElements);
                                break;
                            case TangoConst.Tango_DEV_STATE:
                            case TangoConst.Tango_DEV_STRING:
                            case TangoConst.Tango_DEV_BOOLEAN:
                            default:
                                // nothing to do
                        }
                        break;
                    default: // nothing to do
                }
                break;

            default:
        }

        if (_readValue != null && _writeValue != null) {
            if (_format == AttrDataFormat._SCALAR || _format == AttrDataFormat._SPECTRUM) {
                deltaValue = SnapshotAttributeDeltaValue.getInstance(_writeValue, _readValue);
            } else {
                deltaValue = new SnapshotAttributeDeltaValue(SnapshotAttributeValue.NOT_APPLICABLE_DATA_FORMAT, null,
                        null);
            }
        } else {
            deltaValue = new SnapshotAttributeDeltaValue(SnapshotAttributeValue.NOT_APPLICABLE_DATA_FORMAT, null, null);
        }

        if (_readValue != null) {
            readValue = _readValue;
        } else {
            readValue = new SnapshotAttributeReadValue(SnapshotAttributeValue.NOT_APPLICABLE_DATA_FORMAT, 0, null,
                    null);
        }

        if (_writeValue != null) {
            writeValue = _writeValue;
        } else {
            writeValue = new SnapshotAttributeWriteValue(SnapshotAttributeValue.NOT_APPLICABLE_DATA_FORMAT, 0, null,
                    null);
        }

        updateDisplayFormat();
    }

    /**
     * Only initializes the reference to the SnapshotAttributes group this
     * attribute belongs to.
     * 
     * @param attrs
     */
    public SnapshotAttribute(final SnapshotAttributes attrs) {
        snapshotAttributes = attrs;
    }

    /**
     * Initializes the reference to the SnapshotAttributes group this attribute
     * belongs to, sets the name of the attribute and calculates its display
     * format.
     * 
     * @param name
     *            the name of the attribute
     * @param attrs
     *            the reference to the SnapshotAttributes group
     */
    public SnapshotAttribute(final String name, final SnapshotAttributes attrs) {
        setAttributeCompleteName(name);
        snapshotAttributes = attrs;
        displayFormat = ObjectUtils.EMPTY_STRING;
        updateDisplayFormat();
    }

    /**
     * Sets the display format, reading it from Tango. Uses a Thread to avoid
     * freezing
     */
    public void updateDisplayFormat() {
        new Thread("SnapshotAttribute.updateDisplayFormat()") {
            @Override
            public void run() {
                try {
                    AttributeProxy proxy;
                    proxy = new AttributeProxy(attributeCompleteName);
                    displayFormat = proxy.get_info().format;
                } catch (final Exception e) {
                    displayFormat = ObjectUtils.EMPTY_STRING;
                } finally {
                    if (readValue != null) {
                        readValue.setDisplayFormat(displayFormat);
                    }
                    if (writeValue != null) {
                        writeValue.setDisplayFormat(displayFormat);
                    }
                    if (deltaValue != null) {
                        deltaValue.setDisplayFormat(displayFormat);
                    }
                }
            }
        }.start();

    }

    /**
     * Converts to a SnapAttributeExtract object
     * 
     * @return The result conversion
     */
    public SnapAttributeExtract toSnapAttributeExtrac() {
        final AttributeLight snapAttributeLight = new AttributeLight();

        snapAttributeLight.setAttributeCompleteName(attributeCompleteName);
        snapAttributeLight.setAttributeId(attributeId);
        snapAttributeLight.setDataType(dataType);
        snapAttributeLight.setDataFormat(dataFormat);
        snapAttributeLight.setWritable(permit);

        final SnapAttributeExtract ret = new SnapAttributeExtract(snapAttributeLight);
        final Snapshot snapshot = getSnapshotAttributes().getSnapshot();
        final SnapshotData snapshotData = snapshot.getSnapshotData();

        ret.setSnapId(snapshotData.getId());
        ret.setSnapDate(snapshotData.getTime());

        Object nullElements = null;
        Object nullElementsAfter = null;
        Object[] value = null;
        Object valueAfter = null;
        switch (permit) {
            case AttrWriteType._READ:
                Object tempRead = readValue.getNullElements();
                if (tempRead == null) {
                    nullElements = null;
                } else if (tempRead instanceof Boolean) {
                    nullElements = new boolean[] { ((Boolean) tempRead).booleanValue() };
                } else {
                    nullElements = Array.newInstance(tempRead.getClass(), 1);
                    Array.set(nullElements, 0, tempRead);
                }
                break;
            case AttrWriteType._READ_WITH_WRITE:
            case AttrWriteType._READ_WRITE:
                tempRead = readValue.getNullElements();
                Object tempWrite = writeValue.getNullElements();
                if ((tempRead == null) && (tempWrite == null)) {
                    nullElements = null;
                } else if ((tempRead instanceof Boolean) || (tempWrite instanceof Boolean)) {
                    boolean[] bool = new boolean[2];
                    if (tempRead != null) {
                        bool[0] = ((Boolean) tempRead).booleanValue();
                    }
                    if (tempWrite != null) {
                        bool[1] = ((Boolean) tempWrite).booleanValue();
                    }
                    nullElements = bool;
                } else {
                    Class<?> nullClass;
                    if (tempRead == null) {
                        nullClass = tempWrite.getClass();
                    } else {
                        nullClass = tempRead.getClass();
                    }
                    nullElements = Array.newInstance(nullClass, 2);
                    Array.set(nullElements, 0, tempRead);
                    Array.set(nullElements, 1, tempWrite);
                }
                break;
            case AttrWriteType._WRITE:
                tempWrite = writeValue.getNullElements();
                if (tempWrite == null) {
                    nullElements = null;
                } else if (tempWrite instanceof Boolean) {
                    nullElements = new boolean[] { ((Boolean) tempWrite).booleanValue() };
                } else {
                    nullElements = Array.newInstance(tempWrite.getClass(), 1);
                    Array.set(nullElements, 0, tempWrite);
                }
                break;
        }
        switch (dataFormat) {
            case AttrDataFormat._SCALAR:
                switch (permit) {
                    case AttrWriteType._READ:
                        value = new Object[1];
                        value[0] = readValue.getScalarValue();
                        break;
                    case AttrWriteType._READ_WITH_WRITE:
                    case AttrWriteType._READ_WRITE:
                        value = new Object[2];
                        value[0] = readValue.getScalarValue();
                        value[1] = writeValue.getScalarValue();
                        break;
                    case AttrWriteType._WRITE:
                        value = new Object[1];
                        value[0] = writeValue.getScalarValue();
                        break;
                }
                break;

            case AttrDataFormat._SPECTRUM:
                switch (permit) {
                    case AttrWriteType._READ:
                        value = new Object[1];
                        value[0] = readValue.getSpectrumValue();
                        break;
                    case AttrWriteType._READ_WITH_WRITE:
                    case AttrWriteType._READ_WRITE:
                        value = new Object[2];
                        value[0] = readValue.getSpectrumValue();
                        value[1] = writeValue.getSpectrumValue();
                        break;
                    case AttrWriteType._WRITE:
                        value = new Object[1];
                        value[0] = writeValue.getSpectrumValue();
                        break;
                }
                break;

            default: // nothing to do
        }

        value = getValueTable(value);

        if (value != null) {
            if (value.length == 1) {
                valueAfter = value[0];
                if (nullElements != null) {
                    nullElementsAfter = Array.get(nullElements, 0);
                }
            } else {
                valueAfter = value;
                nullElementsAfter = nullElements;
            }
        }

        ret.setValue(valueAfter, nullElementsAfter);
        return ret;
    }

    /**
     * Returns a XML representation of the attribute.
     * 
     * @return a XML representation of the attribute
     */
    @Override
    public String toString() {
        String ret = ObjectUtils.EMPTY_STRING;

        final BensikinXMLLine openingLine = new BensikinXMLLine(SnapshotAttribute.XML_TAG,
                BensikinXMLLine.OPENING_TAG_CATEGORY);
        final BensikinXMLLine closingLine = new BensikinXMLLine(SnapshotAttribute.XML_TAG,
                BensikinXMLLine.CLOSING_TAG_CATEGORY);

        openingLine.setAttribute(SnapshotAttribute.ID_PROPERTY_XML_TAG, String.valueOf(attributeId));
        openingLine.setAttribute(SnapshotAttribute.DATA_TYPE_PROPERTY_XML_TAG, String.valueOf(dataType));
        openingLine.setAttribute(SnapshotAttribute.DATA_FORMAT_PROPERTY_XML_TAG, String.valueOf(dataFormat));
        openingLine.setAttribute(SnapshotAttribute.WRITABLE_PROPERTY_XML_TAG, String.valueOf(permit));
        openingLine.setAttribute(SnapshotAttribute.COMPLETE_NAME_PROPERTY_XML_TAG,
                String.valueOf(attributeCompleteName));

        ret += openingLine.toString();
        ret += GUIUtilities.CRLF;

        if (readValue != null) {
            ret += readValue.toXMLString();
            ret += GUIUtilities.CRLF;
        }
        if (writeValue != null) {
            ret += writeValue.toXMLString();
            ret += GUIUtilities.CRLF;
        }

        ret += closingLine.toString();

        return ret;
    }

    /**
     * Extracts the read or write value from the raw <code>value</code> Object.
     * First, value is cast to an Object []; the index is used to tell the
     * method whether one wants the read value, or the write value.
     * 
     * @param value
     *            The Object containing the read/write values
     * @param pos
     *            The index to take the value at, once <code>value</code> has
     *            been cast to an Object []
     * @return The read/write value
     */
    private Object getValue(final Object value, final int pos) {
        Object result;
        if (value == null) {
            result = null;
        } else {
            try {
                result = Array.get(value, pos);
            } catch (final Exception e) {
                result = null;
            }
        }
        return result;
    }

    /**
     * Casts all elements of <code>valueIn</code> to a type depending of the
     * attribute <code>data_type</code>, and returns the result.
     * 
     * @param valueIn
     *            The raw Object[] value
     * @return The cast value
     */
    private Object[] getValueTable(final Object[] valueIn) {
        if (valueIn == null) {
            return null;
        }

        final int l = valueIn.length;
        final Object[] valueOut = new Object[l];
        for (int i = 0; i < l; i++) {
            valueOut[i] = SnapshotAttributeValue.castValue(valueIn[i], dataType);
        }

        return valueOut;
    }

    /**
     * Sets the modified state of an attribute.
     * 
     * @param b
     *            The new modified state
     */
    public void setModified(final boolean b) {
        if (writeValue != null) {
            writeValue.setModified(b);
        }
    }

    /**
     * @return Returns the attribute_complete_name.
     */
    public String getAttributeCompleteName() {
        return attributeCompleteName;

    }

    /**
     * @param attribute_complete_name
     *            The attribute_complete_name to set.
     */
    public void setAttributeCompleteName(final String attribute_complete_name) {
        this.attributeCompleteName = attribute_complete_name;
    }

    /**
     * @return Returns the attribute_id.
     */
    public int getAttributeId() {
        return attributeId;
    }

    /**
     * @param attribute_id
     *            The attribute_id to set.
     */
    public void setAttributeId(final int attribute_id) {
        this.attributeId = attribute_id;
    }

    /**
     * @return Returns the data_format.
     */
    public int getDataFormat() {
        return dataFormat;
    }

    /**
     * @param data_format
     *            The data_format to set.
     */
    public void setDataFormat(final int data_format) {
        this.dataFormat = data_format;
    }

    /**
     * @return Returns the data_type.
     */
    public int getDataType() {
        return dataType;
    }

    /**
     * @param data_type
     *            The data_type to set.
     */
    public void setDataType(final int data_type) {
        this.dataType = data_type;
    }

    /**
     * @return Returns the deltaValue.
     */
    public SnapshotAttributeDeltaValue getDeltaValue() {
        return deltaValue;
    }

    /**
     * @param deltaValue
     *            The deltaValue to set.
     */
    public void setDeltaValue(final SnapshotAttributeDeltaValue deltaValue) {
        this.deltaValue = deltaValue;
        if (this.deltaValue != null) {
            this.deltaValue.setDisplayFormat(displayFormat);
        }
    }

    /**
     * @return Returns the permit.
     */
    public int getPermit() {
        return permit;
    }

    /**
     * @param permit
     *            The permit to set.
     */
    public void setPermit(final int permit) {
        this.permit = permit;
    }

    public void setCanSet(boolean value) {
        canSet = value;
    }

    public boolean isCanSet() {
        return canSet;
    }

    /**
     * @return Returns the readValue.
     */
    public SnapshotAttributeReadValue getReadValue() {
        return readValue;
    }

    /**
     * @param readValue
     *            The readValue to set.
     */
    public void setReadValue(final SnapshotAttributeReadValue readValue) {
        this.readValue = readValue;
        if (this.readValue != null) {
            this.readValue.setDisplayFormat(displayFormat);
        }
    }

    /**
     * @return Returns the snapshotAttributes.
     */
    public SnapshotAttributes getSnapshotAttributes() {
        return snapshotAttributes;
    }

    /**
     * @param snapshotAttributes
     *            The snapshotAttributes to set.
     */
    public void setSnapshotAttributes(final SnapshotAttributes snapshotAttributes) {
        this.snapshotAttributes = snapshotAttributes;
    }

    /**
     * @return Returns the writeValue.
     */
    public SnapshotAttributeWriteValue getWriteValue() {
        return writeValue;
    }

    /**
     * @param writeValue
     *            The writeValue to set.
     */
    public void setWriteValue(final SnapshotAttributeWriteValue writeValue) {
        this.writeValue = writeValue;
        if (this.writeValue != null) {
            this.writeValue.setDisplayFormat(displayFormat);
        }
    }

    /**
     * Return the read absolute value
     * 
     * @return
     */
    public SnapshotAttributeReadAbsValue getReadAbsValue() {

        final Object readScalarValue = readValue.getScalarValue();
        Object readAbsScalarValue = null;

        if (readScalarValue != null) {
            if (readValue.getDataType() == TangoConst.Tango_DEV_DOUBLE) {
                readAbsScalarValue = Math.abs(((Number) readScalarValue).doubleValue());
            }
            if (readValue.getDataType() == TangoConst.Tango_DEV_SHORT) {
                readAbsScalarValue = Math.abs(((Number) readScalarValue).shortValue());
            }
            if (readValue.getDataType() == TangoConst.Tango_DEV_FLOAT) {
                readAbsScalarValue = Math.abs(((Number) readScalarValue).floatValue());
            }
            if (readValue.getDataType() == TangoConst.Tango_DEV_LONG) {
                readAbsScalarValue = Math.abs(((Number) readScalarValue).intValue());
            }
            if (readValue.getDataType() == TangoConst.Tango_DEV_INT) {
                readAbsScalarValue = Math.abs(((Number) readScalarValue).intValue());
            }
        }

        final SnapshotAttributeReadAbsValue snapshotReadAbsValue = new SnapshotAttributeReadAbsValue(
                readValue.getDataFormat(), readValue.getDataType(), readAbsScalarValue, readValue.getNullElements());
        return snapshotReadAbsValue;
    }

    /**
     * Return the write absolute value
     * 
     * @return
     */
    public SnapshotAttributeWriteAbsValue getWriteAbsValue() {
        final Object writeScalarValue = writeValue.getScalarValue();
        Object writeAbsScalarValue = null;

        if (writeScalarValue != null) {
            if (writeValue.getDataType() == TangoConst.Tango_DEV_DOUBLE) {
                writeAbsScalarValue = Math.abs(((Number) writeScalarValue).doubleValue());
            }
            if (writeValue.getDataType() == TangoConst.Tango_DEV_SHORT) {
                writeAbsScalarValue = Math.abs(((Number) writeScalarValue).shortValue());
            }
            if (writeValue.getDataType() == TangoConst.Tango_DEV_FLOAT) {
                writeAbsScalarValue = Math.abs(((Number) writeScalarValue).floatValue());
            }
            if (writeValue.getDataType() == TangoConst.Tango_DEV_LONG) {
                writeAbsScalarValue = Math.abs(((Number) writeScalarValue).intValue());
            }
            if (writeValue.getDataType() == TangoConst.Tango_DEV_INT) {
                writeAbsScalarValue = Math.abs(((Number) writeScalarValue).intValue());
            }
        }
        final SnapshotAttributeWriteAbsValue snapshotWriteAbsValue = new SnapshotAttributeWriteAbsValue(
                writeValue.getDataFormat(), writeValue.getDataType(), writeAbsScalarValue,
                writeValue.getNullElements());
        return snapshotWriteAbsValue;
    }

    /**
     * Return the delta absolute value
     * 
     * @return
     */
    public SnapshotAttributeDeltaAbsValue getDeltaAbsValue() {

        final SnapshotAttributeDeltaAbsValue snapshotdeltaAbsValue = new SnapshotAttributeDeltaAbsValue(
                getWriteAbsValue(), getReadAbsValue(), false);
        return snapshotdeltaAbsValue;
    }

    /**
     * @return the display format of this attribute. Exemple : "%6.3f"
     */
    public String getDisplayFormat() {
        return displayFormat;
    }

    /**
     * sets the display format
     */
    public void setDisplayFormat(final String format) {
        displayFormat = format;
        if (readValue != null) {
            readValue.setDisplayFormat(displayFormat);
        }
        if (writeValue != null) {
            writeValue.setDisplayFormat(displayFormat);
        }
        if (deltaValue != null) {
            deltaValue.setDisplayFormat(displayFormat);
        }
    }

    /**
     * @return
     */
    public String toUserFriendlyString() {
        String ret = ObjectUtils.EMPTY_STRING;
        final Options options = Options.getInstance();
        final SnapshotOptions snapshotOptions = options.getSnapshotOptions();
        final String separator = snapshotOptions.getCSVSeparator();

        ret += String.valueOf(attributeCompleteName);
        ret += separator;

        if (readValue != null) {
            ret += readValue.toUserFriendlyString();
        }
        ret += separator;

        if (writeValue != null) {
            ret += writeValue.toUserFriendlyString();
        }
        ret += separator;

        if (deltaValue != null) {
            ret += deltaValue.toUserFriendlyString();
        }

        return ret;
    }

    /**
     * @param attribute
     * @return
     */
    public static SnapshotAttribute createDummyAttribute(final SnapshotAttribute attribute) {
        final SnapshotAttribute ret = new SnapshotAttribute();

        ret.setAttributeCompleteName(attribute.getAttributeCompleteName());

        final SnapshotAttributeReadValue readValue = new SnapshotAttributeReadValue(0, 0, null, null);
        readValue.setNotApplicable(true);
        ret.setReadValue(readValue);

        final SnapshotAttributeWriteValue writeValue = new SnapshotAttributeWriteValue(0, 0, null, null);
        writeValue.setNotApplicable(true);
        ret.setWriteValue(writeValue);

        final SnapshotAttributeDeltaValue deltaValue = new SnapshotAttributeDeltaValue(writeValue, readValue);
        deltaValue.setNotApplicable(true);
        ret.setDeltaValue(deltaValue);

        ret.setDataType(attribute.getDataType());
        ret.setDataFormat(attribute.getDataFormat());

        return ret;
    }
}
