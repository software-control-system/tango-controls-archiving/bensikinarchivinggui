// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/bensikin/bensikin/data/snapshot/Snapshot.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class Snapshot.
// (Garda Laure) - 22 juin 2005
//
// $Author: soleilarc $
//
// $Revision: 1.17 $
//
// $Log: Snapshot.java,v $
// Revision 1.17 2007/10/15 13:17:42 soleilarc
// Author: XP
// Mantis bug ID: 6695
// Comment: For the setEquipments method, change the Exception exception into a
// SnapshotingException exception, after the throws key-word.
//
// Revision 1.16 2007/08/24 12:52:56 ounsy
// minor changes
//
// Revision 1.15 2006/11/29 10:01:10 ounsy
// minor changes
//
// Revision 1.14 2006/04/13 12:37:15 ounsy
// minor changes
//
// Revision 1.13 2006/04/13 12:27:20 ounsy
// cleaned the code
//
// Revision 1.12 2006/03/29 10:23:13 ounsy
// small modification
//
// Revision 1.11 2006/03/20 15:49:40 ounsy
// removed useless logs
//
// Revision 1.10 2006/02/23 10:06:16 ounsy
// Bug corrected : when 1 attribute is modified, the other ones are not set
// modified
//
// Revision 1.9 2006/02/15 09:19:49 ounsy
// minor changes : uncomment to debug
//
// Revision 1.8 2006/01/13 13:25:45 ounsy
// File replacement warning
//
// Revision 1.7 2005/12/14 16:35:21 ounsy
// added methods necessary for the new Word-like file management
//
// Revision 1.6 2005/11/29 18:25:08 chinkumo
// no message
//
// Revision 1.1.1.2 2005/08/22 11:58:37 chinkumo
// First commit
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.bensikin.data.snapshot;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.AttributeInfoEx;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.soleil.archiving.common.api.tools.AttributeLight;
import fr.soleil.archiving.common.api.tools.Condition;
import fr.soleil.archiving.common.api.tools.Criterions;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.snap.api.manager.ISnapManager;
import fr.soleil.archiving.snap.api.tools.SnapAttributeExtract;
import fr.soleil.archiving.snap.api.tools.SnapConst;
import fr.soleil.archiving.snap.api.tools.SnapContext;
import fr.soleil.archiving.snap.api.tools.SnapshotLight;
import fr.soleil.archiving.snap.api.tools.SnapshotingException;
import fr.soleil.bensikin.components.DataFileFilter;
import fr.soleil.bensikin.components.renderers.SnapshotDetailRenderer;
import fr.soleil.bensikin.components.snapshot.SnapshotFileFilter;
import fr.soleil.bensikin.components.snapshot.list.SnapshotListTable;
import fr.soleil.bensikin.containers.BensikinFrame;
import fr.soleil.bensikin.containers.context.ContextDataPanel;
import fr.soleil.bensikin.containers.snapshot.SnapshotDetailComparePanel;
import fr.soleil.bensikin.containers.snapshot.SnapshotDetailTabbedPane;
import fr.soleil.bensikin.containers.snapshot.SnapshotDetailTabbedPaneContent;
import fr.soleil.bensikin.data.context.Context;
import fr.soleil.bensikin.data.context.ContextData;
import fr.soleil.bensikin.data.context.manager.ContextDataManager;
import fr.soleil.bensikin.data.snapshot.manager.ISnapshotManager;
import fr.soleil.bensikin.datasources.snapdb.SnapManagerFactory;
import fr.soleil.bensikin.models.SnapshotListTableModel;
import fr.soleil.bensikin.options.Options;
import fr.soleil.bensikin.options.sub.SnapshotOptions;
import fr.soleil.bensikin.tools.Messages;
import fr.soleil.bensikin.xml.BensikinXMLLine;
import fr.soleil.tango.clientapi.InsertExtractUtils;
import fr.soleil.tango.clientapi.TangoGroupAttribute;

/**
 * A pivotal class of Bensikin, this class is the upper level modelisation of a
 * snapshot.
 * <p/>
 * A Snapshot object has two main attributes: - a SnapshotData attribute representing all non-attribute-dependant
 * information - a SnapshotAttributes attribute representing all attribute-dependant information and a reference to the
 * context of this snapshot.
 * <p/>
 * The Context class also saves static references to the currently opened contexts, and the selected context.
 * 
 * @author CLAISSE
 */
public class Snapshot implements Comparable<Snapshot> {

    private static final Logger LOGGER = LoggerFactory.getLogger(Snapshot.class);

    private int contextId = -1;
    private SnapshotData snapshotData;
    private SnapshotAttributes snapshotAttributes = null;

    private static Map<String, Snapshot> selectedSnapshots = new ConcurrentHashMap<String, Snapshot>();
    private static Map<String, Snapshot> openedSnapshots = new ConcurrentHashMap<String, Snapshot>();

    private static String snapshotDefaultComment;

    private static Snapshot firstSnapshotOfComparison;
    private static Snapshot secondSnapshotOfComparison;
    private static String firstSnapshotOfComparisonTitle;
    private static String secondSnapshotOfComparisonTitle;

    /**
     * The XML tag name used in saving/loading
     */
    public static final String XML_TAG = "Snapshot";
    private boolean isModified;

    public static final String BENSIKIN_AUTOMATIC_INSERT_COMMENT = "BENSIKIN_AUTOMATIC_SNAPSHOT";

    private boolean loadable = false;

    /**
     * Sets the snapshotData and context attributes
     * 
     * @param _context
     * @param _snapshotData
     */
    public Snapshot(final SnapshotData _snapshotData) {
        snapshotData = _snapshotData;
    }

    /**
     * Default constructor. The snapshotData and snapshotAttributes attributes
     * are null.
     */
    public Snapshot() {

    }

    /**
     * @param saveLocation
     */
    public void setPath(final String path) {
        if (snapshotData != null) {
            snapshotData.setPath(path);
        }

    }

    public String getPath() {
        String path;
        if (snapshotData == null) {
            path = null;
        } else {
            path = snapshotData.getPath();
        }
        return path;
    }

    /**
     * Looks up in database the referenced attributes for this snapshot. Once
     * the attributes are found ,they are converted to the SnasphotAttributes
     * attribute.
     * 
     * @throws SnapshotingException
     */
    public void loadAttributes() throws SnapshotingException {
        final ISnapManager source = SnapManagerFactory.getCurrentImpl();
        if (contextId < 0) {
            contextId = ContextDataManager.getInstance().getContextIdBySnapshotId(snapshotData.getId());
        }
        if ((getSnapshotAttributes() == null) || (getSnapshotAttributes().getSnapshotAttributes() == null)
                || (getSnapshotAttributes().getSnapshotAttributes().length == 0)) {
            final SnapshotLight light = getSnapshotData().getAsSnapshotLight();

            final SnapAttributeExtract[] sae = source.findSnapshotAttributes(light, contextId);
            final SnapshotAttributes attrs = new SnapshotAttributes(this);
            final int numberOfAttributes = sae.length;
            final SnapshotAttribute[] rows = new SnapshotAttribute[numberOfAttributes];

            int i = 0;

            for (final SnapAttributeExtract currentSnapAttributeExtract : sae) {
                rows[i++] = new SnapshotAttribute(currentSnapAttributeExtract, attrs);
            }
            attrs.setSnapshotAttributes(rows);

            setSnapshotAttributes(attrs);
        }
    }

    /**
     * Updates the comment field of a Snapshot.
     * 
     * @param comment
     *            the new value of comment.
     * @throws Exception
     */
    public void updateComment(final String comment) throws SnapshotingException {
        getSnapshotData().setComment(comment);

        final ISnapManager source = SnapManagerFactory.getCurrentImpl();
        final SnapshotLight light = getSnapshotData().getAsSnapshotLight();

        try {
            source.updateCommentOfSnapshot(light, comment);
        } catch (final SnapshotingException e) {
            e.printStackTrace();
            throw e;
        }

        // --auto refresh the comment in the snapshots list
        final SnapshotListTableModel modelToRefresh = SnapshotListTableModel.getInstance();
        modelToRefresh.refreshComment(getSnapshotData());
    }

    /**
     * Sets write values of equipments, using the values of this snapshot for
     * each attribute.
     * 
     * @throws Exception
     */
    public void setEquipments() throws SnapshotingException {

        // System.out.println("******bensikin Snapshot.setEquipments ()");
        // System.out.println("******Snapshot Set Equipments test0");
        final fr.soleil.archiving.snap.api.tools.Snapshot transformedSnap = toSnapshot(false);
        // System.out.println("******Snapshot Set Equipments test1");
        final ISnapManager source = SnapManagerFactory.getCurrentImpl();
        // System.out.println("******Snapshot Set Equipments test2");

        try {
            source.setEquipmentsWithSnapshot(transformedSnap);
        } catch (final SnapshotingException e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * Sets equipment command input, using the values of this snapshot for each
     * attribute.
     * 
     * @param option
     * @param cmd_name
     * @throws Exception
     */
    public String setEquipmentWithCommand(final String cmd_name, final String option) throws SnapshotingException {
        if (cmd_name == null || option == null) {
            throw new SnapshotingException();
        }
        final fr.soleil.archiving.snap.api.tools.Snapshot transformedSnap = toSnapshot(false);
        final ISnapManager source = SnapManagerFactory.getCurrentImpl();

        return source.setEquipmentsWithCommand(cmd_name, option, transformedSnap);
    }

    /**
     * Sets equipment command input, using only the values of selected
     * snapshots.
     * 
     * @param option
     * @param cmd_name
     * @throws Exception
     */
    public String setEquipmentWithCommandUsingSelected(final String cmd_name, final String option)
            throws SnapshotingException {
        if (cmd_name == null || option == null) {
            throw new SnapshotingException();
        }
        final fr.soleil.archiving.snap.api.tools.Snapshot transformedSnap = toSnapshot(true);
        final ISnapManager source = SnapManagerFactory.getCurrentImpl();

        return source.setEquipmentsWithCommand(cmd_name, option, transformedSnap);
    }

    /**
     * Adds this snapshot to the current list of opened snapshots.
     * 
     * @param snapshot
     *            The Snapshot to add
     */
    public static void addOpenedSnapshot(final Snapshot snapshot) {
        final int id = snapshot.getSnapshotData().getId();
        openedSnapshots.put(String.valueOf(id), snapshot);
    }

    /**
     * Remove the snapshot with this id from the current list of opened
     * snapshots.
     * 
     * @param id
     *            The id of the Snapshot to remove
     */
    public static void removeOpenedSnapshot(final int id) {
        openedSnapshots.remove(String.valueOf(id));
    }

    /**
     * Adds this snapshot to the current list of opened snapshots.
     * 
     * @param snapshot
     *            The Snapshot to add
     */
    public static void addSelectedSnapshot(final Snapshot snapshot) {
        final int id = snapshot.getSnapshotData().getId();
        selectedSnapshots.put(String.valueOf(id), snapshot);
    }

    /**
     * Remove the snapshot with this id from the current list of selected
     * snapshots.
     * 
     * @param id
     *            The id of the Snapshot to remove
     */
    public static void removeSelectedSnapshot(final int id) {
        selectedSnapshots.remove(String.valueOf(id));
    }

    /**
     * Resets the list of selected snapshots.
     */
    public static void removeAllSelectedSnapshots() {
        selectedSnapshots = new ConcurrentHashMap<String, Snapshot>();
    }

    /**
     * Empties all references to opened and selected snapshots, and resets the
     * snapshot displays, list and detail, accordingly.
     */
    public static void reset(final boolean hasToRemoveSelectedSnapshots, final boolean resetContextFields) {
        final SnapshotListTable table = SnapshotListTable.getInstance();
        table.setModel(SnapshotListTableModel.forceReset());

        openedSnapshots = new ConcurrentHashMap<String, Snapshot>();

        if (resetContextFields) {
            final ContextDataPanel dataPanel = ContextDataPanel.getInstance();
            dataPanel.resetFields();
        }

        if (hasToRemoveSelectedSnapshots) {
            final SnapshotDetailTabbedPane tabbedPane = SnapshotDetailTabbedPane.getInstance();
            tabbedPane.closeAllTabs();

            selectedSnapshots = new ConcurrentHashMap<String, Snapshot>();
        }

        final SnapshotDetailComparePanel comparePanel = SnapshotDetailComparePanel.getInstance();
        comparePanel.reset();
        Snapshot.setFirstSnapshotOfComparison(null);
        Snapshot.setSecondSnapshotOfComparison(null);
        Snapshot.setFirstSnapshotOfComparisonTitle(null);
        Snapshot.setSecondSnapshotOfComparisonTitle(null);
    }

    /**
     * Looks up in database a particular Snapshot, defined by its id. Loads only
     * its ContextData.
     * 
     * @param id
     *            The key of the snapshot
     * @return The snapshot with this id
     * @throws SnapshotingException
     */
    public static Snapshot findSnapshotById(final int id) throws SnapshotingException {
        final ISnapManager snapManager = SnapManagerFactory.getCurrentImpl();

        final Condition condition = new Condition(SnapConst.ID_SNAP, "=", String.valueOf(id));
        final Criterions searchCriterions = new Criterions();
        searchCriterions.addCondition(condition);

        final SnapshotLight[] snapshots = snapManager.findSnapshots(searchCriterions);
        final SnapshotData snapshotData = new SnapshotData(snapshots[0]);

        final Snapshot ret = new Snapshot(snapshotData);
        return ret;
    }

    /**
     * Converts to a fr.soleil.archiving.snap.api.tools.SnapShot object
     * 
     * @return The conversion result
     */
    private fr.soleil.archiving.snap.api.tools.Snapshot toSnapshot(boolean withCanSetFilter) {

        final SnapshotData snapData = getSnapshotData();
        final SnapshotAttributes snapAttrs = getSnapshotAttributes();

        final SnapshotAttributes filteredSnapAttrs = new SnapshotAttributes(this);
        SnapshotAttribute[] attributeList = snapAttrs.getSnapshotAttributes();

        boolean canSetValid = false;
        if (withCanSetFilter) {
            List<SnapshotAttribute> canSetAttributes = new ArrayList<SnapshotAttribute>();
            for (SnapshotAttribute attribute : attributeList) {
                if (attribute.isCanSet()) {
                    canSetAttributes.add(attribute);
                }
            }
            if (canSetAttributes.size() > 0) {
                attributeList = new SnapshotAttribute[canSetAttributes.size()];
                attributeList = canSetAttributes.toArray(attributeList);
                canSetValid = true;
            }
        }

        /*
         * **** filtering attributes depending which ones user wants **** * ****
         * to use to set equipment ****
         */
        int nbAttrs = 0;
        for (final SnapshotAttribute element : attributeList) {
            final SnapshotAttributeWriteValue value = element.getWriteValue();
            if (value != null) {
                if (value.isSettable()) {
                    nbAttrs++;
                }
            } else {
                nbAttrs++;
            }
        }
        final SnapshotAttribute[] filteredAttributeList = new SnapshotAttribute[nbAttrs];
        int nbAttrs2 = 0;
        for (final SnapshotAttribute element : attributeList) {
            final SnapshotAttributeWriteValue value = element.getWriteValue();
            // System.out.println("*****Name : " +
            // attributeList[i].getAttribute_complete_name());
            if (value != null) {
                /*
                 * if (value.getDataFormat() == AttrDataFormat._SPECTRUM){
                 * System.out.println(attributeList[ i
                 * ].getAttribute_complete_name());
                 * System.out.println("write value not null : " +
                 * value.getSpectrumValue().getClass()); }
                 */
                if (value.isSettable()) {
                    // System.out.println("write value settable");
                    filteredAttributeList[nbAttrs2] = element;
                    nbAttrs2++;
                }
            } else {
                // System.out.println("write value null");
                filteredAttributeList[nbAttrs2] = element;
                nbAttrs2++;
            }
        }
        filteredSnapAttrs.setSnapshotAttributes(filteredAttributeList);
        /* **** end filtering **** */

        // ArrayList transformedAttrs = snapAttrs.toArrayList();
        final ArrayList<SnapAttributeExtract> transformedAttrs = filteredSnapAttrs.toArrayList();

        final String[] argin = new String[3];
        argin[0] = String.valueOf(contextId);
        argin[1] = String.valueOf(snapData.getId());
        argin[2] = String.valueOf(snapData.getTime());

        final fr.soleil.archiving.snap.api.tools.Snapshot ret = new fr.soleil.archiving.snap.api.tools.Snapshot(argin);
        ret.setFiltered(canSetValid);

        ret.setAttribute_List(transformedAttrs);

        return ret;
    }

    /**
     * Resets the list of opened snapshots
     */
    public static void removeAllOpenedSnapshots() {
        openedSnapshots = new ConcurrentHashMap<String, Snapshot>();
    }

    /**
     * Returns a XML representation of the snapshot.
     * 
     * @return a XML representation of the snapshot
     */
    @Override
    public String toString() {
        String ret = "";

        final BensikinXMLLine openingLine = new BensikinXMLLine(Snapshot.XML_TAG, BensikinXMLLine.OPENING_TAG_CATEGORY);
        final BensikinXMLLine closingLine = new BensikinXMLLine(Snapshot.XML_TAG, BensikinXMLLine.CLOSING_TAG_CATEGORY);

        final int id = snapshotData.getId();
        final Timestamp time = snapshotData.getTime();
        final String comment = snapshotData.getComment();
        final String path = snapshotData.getPath();
        final String isModified = "" + isModified();

        openingLine.setAttribute(SnapshotData.ID_PROPERTY_XML_TAG, String.valueOf(id));
        openingLine.setAttribute(SnapshotData.IS_MODIFIED_PROPERTY_XML_TAG, isModified);
        if (time != null) {
            openingLine.setAttribute(SnapshotData.TIME_PROPERTY_XML_TAG, time.toString());
        }
        if (comment != null) {
            openingLine.setAttribute(SnapshotData.COMMENT_PROPERTY_XML_TAG, comment.toString());
        }
        if (path != null) {
            openingLine.setAttribute(SnapshotData.PATH_PROPERTY_XML_TAG, path);
        }

        ret += openingLine.toString();
        ret += GUIUtilities.CRLF;

        if (snapshotAttributes != null) {
            ret += snapshotAttributes.toString();
        }

        ret += closingLine.toString();

        return ret;
    }

    /**
     * Returns a CSV representation of the snapshot.
     * 
     * @return a CSV representation of the snapshot
     */
    public String toUserFriendlyString() {
        String ret = "";

        if (snapshotAttributes != null) {
            ret += snapshotAttributes.toUserFriendlyString();
        }

        return ret;
    }

    /**
     * @return Returns the firstSnapshotOfComparison.
     */
    public static Snapshot getFirstSnapshotOfComparison() {
        return firstSnapshotOfComparison;
    }

    /**
     * @param _firstSnapshotOfComparison
     *            The firstSnapshotOfComparison to set.
     */
    public static void setFirstSnapshotOfComparison(final Snapshot _firstSnapshotOfComparison) {
        firstSnapshotOfComparison = _firstSnapshotOfComparison;
    }

    /**
     * @return Returns the secondSnapshotOfComparison.
     */
    public static Snapshot getSecondSnapshotOfComparison() {
        return secondSnapshotOfComparison;
    }

    /**
     * @param _secondSnapshotOfComparison
     *            The secondSnapshotOfComparison to set.
     */
    public static void setSecondSnapshotOfComparison(final Snapshot _secondSnapshotOfComparison) {
        secondSnapshotOfComparison = _secondSnapshotOfComparison;
    }

    /**
     * Tests whether both snapshots have the same attributes
     * 
     * @param snapshot
     *            The Snapshot to compare to
     * @return True if both snapshots have the same attributes (including a null
     *         list)
     */
    public boolean hasSameAttributesAs(final Snapshot snapshot) {
        final SnapshotAttributes snapshotAttributes1 = getSnapshotAttributes();
        final SnapshotAttributes snapshotAttributes2 = snapshot.getSnapshotAttributes();

        if (snapshotAttributes1 == null) {
            return snapshotAttributes2 == null;
        }

        return snapshotAttributes1.hasSameAttributesAs(snapshotAttributes2);
    }

    /**
     * Sets the modified state of a Snapshot
     * 
     * @param b
     *            True if the Snapshot has been modified
     */
    public void setModified(final boolean b) {
        isModified = b;

        // Bug : forced all attributes to be "modified", what we do not want
        /*
         * SnapshotAttributes snapshotAttributes1 =
         * this.getSnapshotAttributes(); if ( snapshotAttributes1 == null ) {
         * return; } snapshotAttributes1.setModified( b );
         */
    }

    /**
     * @return Returns the firstSnapshotOfComparisonTitle.
     */
    public static String getFirstSnapshotOfComparisonTitle() {
        return firstSnapshotOfComparisonTitle;
    }

    /**
     * @param firstSnapshotOfComparisonTitle
     *            The firstSnapshotOfComparisonTitle to set.
     */
    public static void setFirstSnapshotOfComparisonTitle(final String firstSnapshotOfComparisonTitle) {
        Snapshot.firstSnapshotOfComparisonTitle = firstSnapshotOfComparisonTitle;
    }

    /**
     * @return Returns the secondSnapshotOfComparisonTitle.
     */
    public static String getSecondSnapshotOfComparisonTitle() {
        return secondSnapshotOfComparisonTitle;
    }

    /**
     * @param secondSnapshotOfComparisonTitle
     *            The secondSnapshotOfComparisonTitle to set.
     */
    public static void setSecondSnapshotOfComparisonTitle(final String secondSnapshotOfComparisonTitle) {
        Snapshot.secondSnapshotOfComparisonTitle = secondSnapshotOfComparisonTitle;
    }

    /**
     * @return Returns the openedSnapshots.
     */
    public static Map<String, Snapshot> getOpenedSnapshots() {
        return openedSnapshots;
    }

    /**
     * @param openedSnapshots
     *            The openedSnapshots to set.
     */
    public static void setOpenedSnapshots(final Map<String, Snapshot> openedSnapshots) {
        Snapshot.openedSnapshots = openedSnapshots;
    }

    /**
     * @return Returns the selectedSnapshots.
     */
    public static Map<String, Snapshot> getSelectedSnapshots() {
        return selectedSnapshots;
    }

    /**
     * @param selectedSnapshots
     *            The selectedSnapshots to set.
     */
    public static void setSelectedSnapshots(final Map<String, Snapshot> selectedSnapshots) {
        Snapshot.selectedSnapshots = selectedSnapshots;
    }

    /**
     * @return Returns the snapshotAttributes.
     */
    public SnapshotAttributes getSnapshotAttributes() {
        return snapshotAttributes;
    }

    /**
     * @param snapshotAttributes
     *            The snapshotAttributes to set.
     */
    public void setSnapshotAttributes(final SnapshotAttributes snapshotAttributes) {
        this.snapshotAttributes = snapshotAttributes;
    }

    /**
     * @return Returns the snapshotData.
     */
    public SnapshotData getSnapshotData() {
        return snapshotData;
    }

    /**
     * @param snapshotData
     *            The snapshotData to set.
     */
    public void setSnapshotData(final SnapshotData snapshotData) {
        this.snapshotData = snapshotData;
    }

    /**
     * @return Returns the snapshotDefaultComment.
     */
    public static String getSnapshotDefaultComment() {
        return snapshotDefaultComment;
    }

    /**
     * @param snapshotDefaultComment
     *            The snapshotDefaultComment to set.
     */
    public static void setSnapshotDefaultComment(final String snapshotDefaultComment) {
        Snapshot.snapshotDefaultComment = snapshotDefaultComment;
    }

    /**
     * @param manager
     * @param b
     */
    public void save(final ISnapshotManager manager, final boolean saveAs) {
        final String pathToUse = getPathToUse(manager, saveAs);
        if (pathToUse != null) {
            // System.out.println (
            // "Context/save/pathToUse/"+pathToUse+"/saveAs/"+saveAs+"/" );
            manager.setNonDefaultSaveLocation(pathToUse);
            try {
                setPath(pathToUse);
                setModified(false);
                // manager.saveArchivingConfiguration( this , null );
                manager.saveSnapshot(this);
                Snapshot.removeModifiedMarkers();

                final String msg = Messages.getLogMessage("SAVE_SNAPSHOT_ACTION_OK");
                LOGGER.debug(msg);
            } catch (final Exception e) {
                final String msg = Messages.getLogMessage("SAVE_SNAPSHOT_ACTION_KO");
                LOGGER.error(msg, e);

                setPath(null);
                setModified(true);
            }
        }
    }

    private String getPathToUse(final ISnapshotManager manager, final boolean saveAs) {
        String path = null;
        final String pathToUse = getPath();
        // System.out.println (
        // "Snapshot/getPathToUse/pathToUse/"+pathToUse+"/saveAs/"+saveAs+"/" );
        final JFileChooser chooser = new JFileChooser();
        final SnapshotFileFilter ACfilter = new SnapshotFileFilter();
        chooser.addChoosableFileFilter(ACfilter);
        if (pathToUse == null) {
            final String location = manager.getSaveLocation();
            if ((location == null) || location.trim().isEmpty()) {
                chooser.setCurrentDirectory(new File(manager.getDefaultSaveLocation()));
            } else {
                File file = new File(location);
                if (file.isFile()) {
                    file = file.getParentFile();
                }
                chooser.setCurrentDirectory(file);
            }
        } else {
            if (saveAs) {
                chooser.setCurrentDirectory(new File(pathToUse));
            } else {
                path = pathToUse;
            }
        }
        if (path == null) {
            final int returnVal = chooser.showSaveDialog(BensikinFrame.getInstance());
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File f = chooser.getSelectedFile();
                if (f != null) {
                    path = f.getAbsolutePath();
                    final String extension = DataFileFilter.getExtension(f);
                    final String expectedExtension = ACfilter.getExtension();

                    if ((extension == null) || (!extension.equalsIgnoreCase(expectedExtension))) {
                        path += "." + expectedExtension;
                        f = new File(path);
                        chooser.setSelectedFile(f);
                    }
                    if (f.exists()) {
                        final int choice = JOptionPane.showConfirmDialog(BensikinFrame.getInstance(),
                                Messages.getMessage("DIALOGS_FILE_CHOOSER_FILE_EXISTS"),
                                Messages.getMessage("DIALOGS_FILE_CHOOSER_FILE_EXISTS_TITLE"),
                                JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                        if (choice != JOptionPane.OK_OPTION) {
                            return null;
                        }
                    }
                    manager.setNonDefaultSaveLocation(path);
                }
            } else {
                path = null;
            }
        }
        return path;
    }

    /**
     * @return Returns the isModified.
     */
    public boolean isModified() {
        return isModified;
    }

    /**
     * If the snapshot is a file snapshot, removes the "modified" mark (if there
     * is one)
     */
    private static void removeModifiedMarkers() {
        final SnapshotDetailTabbedPane snapshotDetailTabbedPane = SnapshotDetailTabbedPane.getInstance();
        if (snapshotDetailTabbedPane == null) {
            return;
        }

        final SnapshotDetailTabbedPaneContent selectedContent = snapshotDetailTabbedPane
                .getSelectedSnapshotDetailTabbedPaneContent();
        if (selectedContent == null) {
            return;
        }

        if (selectedContent.isFileContent()) {
            selectedContent.setModified(false);
        }

    }

    // ------ former way to recover current state start ------ //

    public static Snapshot getCurrentStateSnapshot(final Snapshot sn1) throws Exception {
        final ISnapManager source = SnapManagerFactory.getCurrentImpl();
        final SnapContext snap = sn1.getAsSnapContext();

        final fr.soleil.archiving.snap.api.tools.Snapshot savedSnapshot = source.launchSnapshot(snap);
        final int id = savedSnapshot.getId_snap();

        final Snapshot toUpdate = Snapshot.findSnapshotById(id);
        toUpdate.loadAttributes();

        toUpdate.updateComment(Snapshot.BENSIKIN_AUTOMATIC_INSERT_COMMENT);

        return toUpdate;
    }

    // ------ former way to recover current state end ------ //

    // ------ new way to recover current state (24516) start ------ //

    /**
     * Creates a virtual snapshot that represents current state (i.e. no
     * snapshot is registered in database), based on a real snapshot (i.e. one
     * coming from database).
     * 
     * @param sn1
     *            The real {@link Snapshot}
     * @return A {@link Snapshot}. <code>null</code> if <code>sn1</code> is <code>null</code>, or if <code>sn1</code>
     *         attributes are <code>null</code>
     */
    public static Snapshot getCurrentStateVirtualSnapshot(final Snapshot sn1) {
        final Snapshot currentState;
        if (sn1 == null) {
            currentState = null;
        } else {
            final SnapshotAttributes oldAttributes = sn1.getSnapshotAttributes();
            if (oldAttributes == null) {
                currentState = null;
            } else {
                final SnapshotAttribute[] oldRows = (oldAttributes == null ? null
                        : oldAttributes.getSnapshotAttributes());
                if (oldRows == null) {
                    currentState = null;
                } else {
                    currentState = new Snapshot(new SnapshotData(-1, new Timestamp(System.currentTimeMillis()),
                            Snapshot.BENSIKIN_AUTOMATIC_INSERT_COMMENT));
                    final SnapshotAttributes currentStateAttributes = new SnapshotAttributes(currentState);
                    final SnapshotAttribute[] rows = new SnapshotAttribute[oldRows.length];
                    // DeviceAttributes recovery START
                    List<String> effectiveNames = new ArrayList<String>(rows.length);
                    String[] names = new String[rows.length];
                    int i = 0;
                    for (final SnapshotAttribute row : oldRows) {
                        if (row != null) {
                            String name = row.getAttributeCompleteName();
                            if (name != null) {
                                names[i] = name;
                                effectiveNames.add(name);
                            }
                        }
                        i++;
                    }
                    AttributeInfoEx[] attrInfo;
                    DeviceAttribute[] attributes;
                    try {
                        TangoGroupAttribute group = new TangoGroupAttribute(false,
                                effectiveNames.toArray(new String[effectiveNames.size()]));
                        DeviceAttribute[] tmp = group.getReadAsyncReplies();
                        AttributeInfoEx[] config = group.getConfig();
                        if (tmp == null) {
                            attributes = new DeviceAttribute[names.length];
                            attrInfo = new AttributeInfoEx[names.length];
                        } else {
                            if (tmp.length == rows.length) {
                                attributes = tmp;
                                attrInfo = config;
                            } else {
                                attributes = new DeviceAttribute[names.length];
                                attrInfo = new AttributeInfoEx[names.length];
                                i = 0;
                                for (int j = 0; j < names.length; j++) {
                                    if (names[j] != null) {
                                        attributes[j] = tmp[i];
                                        attrInfo[j] = config[i];
                                        i++;
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        attributes = new DeviceAttribute[names.length];
                        attrInfo = new AttributeInfoEx[names.length];
                        LOGGER.error("Failed to read current state", e);
                    }
                    effectiveNames.clear();
                    // DeviceAttributes recovery END
                    i = 0;
                    int deviceAttributeIndex = 0;
                    for (final SnapshotAttribute row : oldRows) {
                        DeviceAttribute da = attributes[deviceAttributeIndex];
                        AttributeInfoEx info = attrInfo[deviceAttributeIndex];
                        deviceAttributeIndex++;
                        if (row != null) {
                            String attributeCompleteName = row.getAttributeCompleteName();
                            SnapshotAttribute attribute = new SnapshotAttribute(currentStateAttributes);
                            attribute.setAttributeCompleteName(attributeCompleteName);
                            attribute.setAttributeId(row.getAttributeId());
                            AttrDataFormat dataFormat;
                            AttrWriteType writable;
                            int dataType;
                            if (info == null) {
                                writable = extractWriteType(row);
                                dataType = row.getDataType();
                            } else {
                                writable = info.writable;
                                dataType = info.data_type;
                            }
                            if (da == null) {
                                dataFormat = extractDataFormat(row);
                            } else {
                                try {
                                    dataFormat = da.getDataFormat();
                                } catch (DevFailed e1) {
                                    dataFormat = extractDataFormat(row);
                                }
                            }
                            attribute.setDataFormat(dataFormat.value());
                            attribute.setDataType(dataType);
                            if ((writable != null) && (dataFormat != AttrDataFormat.FMT_UNKNOWN)) {
                                try {
                                    attribute.setReadValue(recoverReadValue(row.getReadValue(), da, dataType,
                                            dataFormat, writable, attributeCompleteName));
                                    attribute.setWriteValue(recoverWriteValue(row.getWriteValue(), da, dataType,
                                            dataFormat, writable, attributeCompleteName));
                                    attribute.setDeltaValue(new SnapshotAttributeDeltaValue(attribute.getWriteValue(),
                                            attribute.getReadValue()));
                                } catch (Exception e) {
                                    LOGGER.error("Failed to read current state for " + attributeCompleteName, e);
                                }
                            }
                            rows[i++] = attribute;
                        }
                    }
                    currentStateAttributes.setSnapshotAttributes(rows);
                    currentState.setSnapshotAttributes(currentStateAttributes);
                }
            }
        }
        return currentState;
    }

    protected static AttrDataFormat extractDataFormat(SnapshotAttribute attr) {
        AttrDataFormat dataFormat;
        if (attr == null) {
            dataFormat = AttrDataFormat.FMT_UNKNOWN;
        } else {
            switch (attr.getDataFormat()) {
                case AttrDataFormat._SCALAR:
                    dataFormat = AttrDataFormat.SCALAR;
                    break;
                case AttrDataFormat._SPECTRUM:
                    dataFormat = AttrDataFormat.SPECTRUM;
                    break;
                case AttrDataFormat._IMAGE:
                    dataFormat = AttrDataFormat.IMAGE;
                    break;
                default:
                    dataFormat = AttrDataFormat.FMT_UNKNOWN;
                    break;
            }
        }
        return dataFormat;
    }

    protected static AttrWriteType extractWriteType(SnapshotAttribute attr) {
        AttrWriteType writable;
        if (attr == null) {
            writable = AttrWriteType.READ_WRITE;
        } else if (attr.getReadValue() == null) {
            if (attr.getWriteValue() == null) {
                writable = null;
            } else {
                writable = AttrWriteType.WRITE;
            }
        } else if (attr.getWriteValue() == null) {
            writable = AttrWriteType.READ;
        } else {
            writable = AttrWriteType.READ_WRITE;
        }
        return writable;
    }

    protected static Object recoverNewValue(SnapshotAttributeValue formerSnapshotValue, DeviceAttribute attr,
            AttrDataFormat dataFormat, AttrWriteType writable, String attributeName, boolean read) {
        Object result;
        if ((formerSnapshotValue == null) || (attr == null) || (dataFormat == null) || (writable == null)) {
            result = null;
        } else {
            try {
                Object formerValue = formerSnapshotValue.getValue();
                switch (dataFormat.value()) {
                    case AttrDataFormat._SCALAR:
                        if (read) {
                            if (writable == AttrWriteType.WRITE) {
                                result = null;
                            } else {
                                if (formerValue == null) {
                                    result = InsertExtractUtils.extractRead(attr, dataFormat);
                                } else {
                                    result = InsertExtractUtils.extractRead(attr, dataFormat, formerValue.getClass());
                                }
                            }
                        } else {
                            if (writable == AttrWriteType.READ) {
                                result = null;
                            } else {
                                if (formerValue == null) {
                                    result = InsertExtractUtils.extractWrite(attr, writable, dataFormat);
                                } else {
                                    result = InsertExtractUtils.extractWrite(attr, dataFormat, writable,
                                            formerValue.getClass());
                                }
                            }
                        }
                        break;
                    case AttrDataFormat._SPECTRUM:
                    case AttrDataFormat._IMAGE:
                        if (read) {
                            if (writable == AttrWriteType.WRITE) {
                                result = null;
                            } else {
                                if (formerValue == null) {
                                    result = InsertExtractUtils.extractReadArray(attr, dataFormat);
                                } else {
                                    result = InsertExtractUtils.extractReadArray(attr, dataFormat,
                                            formerValue.getClass());
                                }
                            }
                        } else {
                            if (writable == AttrWriteType.READ) {
                                result = null;
                            } else {
                                if (formerValue == null) {
                                    result = InsertExtractUtils.extractWriteArray(attr, writable, dataFormat);
                                } else {
                                    result = InsertExtractUtils.extractWriteArray(attr, writable, dataFormat,
                                            formerValue.getClass());
                                }
                            }
                        }
                        break;
                    default:
                        result = null;
                        break;
                }
            } catch (DevFailed df) {
                result = null;
                LOGGER.error("Failed to read current " + (read ? "read" : "write") + " for " + attributeName, df);
            }
        }
        return result;
    }

    protected static SnapshotAttributeReadValue recoverReadValue(SnapshotAttributeReadValue formerReadValue,
            DeviceAttribute attr, int dataType, AttrDataFormat dataFormat, AttrWriteType writable,
            String attributeName) {
        int format;
        if (dataFormat == null) {
            if (formerReadValue == null) {
                format = AttrDataFormat._FMT_UNKNOWN;
            } else {
                format = formerReadValue.getDataFormat();
            }
        } else {
            format = dataFormat.value();
        }
        return new SnapshotAttributeReadValue(format, dataType,
                recoverNewValue(formerReadValue, attr, dataFormat, writable, attributeName, true), null);
    }

    protected static SnapshotAttributeWriteValue recoverWriteValue(SnapshotAttributeWriteValue formerWriteValue,
            DeviceAttribute attr, int dataType, AttrDataFormat dataFormat, AttrWriteType writable,
            String attributeName) {
        int format;
        if (dataFormat == null) {
            if (formerWriteValue == null) {
                format = AttrDataFormat._FMT_UNKNOWN;
            } else {
                format = formerWriteValue.getDataFormat();
            }
        } else {
            format = dataFormat.value();
        }
        return new SnapshotAttributeWriteValue(format, dataType,
                recoverNewValue(formerWriteValue, attr, dataFormat, writable, attributeName, false), null);
    }

    // ------ new way to recover current state (24516) end ------ //

    private SnapContext getAsSnapContext() throws Exception {

        SnapContext snapContext;

        final ContextDataManager contextDataManager = ContextDataManager.getInstance();
        final ContextData contextData = contextDataManager.getContextData(contextId);

        if (contextData == null) {
            final String author = Context.BENSIKIN_AUTOMATIC_INSERT_COMMENT;
            final String name = Context.BENSIKIN_AUTOMATIC_INSERT_COMMENT;
            final String reason = Context.BENSIKIN_AUTOMATIC_INSERT_COMMENT;
            final String description = Context.BENSIKIN_AUTOMATIC_INSERT_COMMENT;
            snapContext = new SnapContext(author, name, -1, null, reason, description);
            snapContext.setCreation_date(new java.sql.Date(System.currentTimeMillis()));
            snapContext.setAttributeList(getAsAttributeList());

            final Context ctx = Context.save(snapContext);
            snapContext = ctx.getAsSnapContext();
        } else {
            snapContext = new SnapContext();
            snapContext.setAuthor_name(contextData.getAuthorName());
            snapContext.setCreation_date(contextData.getCreationDate());
            snapContext.setDescription(contextData.getDescription());
            snapContext.setId(contextData.getId());
            snapContext.setName(contextData.getName());
            snapContext.setReason(contextData.getReason());
            snapContext.setAttributeList(getAsAttributeList());
        }
        return snapContext;
    }

    private List<AttributeLight> getAsAttributeList() {
        final List<AttributeLight> attributeList = new ArrayList<AttributeLight>();

        final SnapshotAttribute[] attrs = getSnapshotAttributes().getSnapshotAttributes();
        if (attrs == null) {
            return attributeList;
        }

        for (final SnapshotAttribute next : attrs) {
            final String attribute_complete_name = next.getAttributeCompleteName();

            final AttributeLight currentAttr = new AttributeLight(attribute_complete_name);
            attributeList.add(currentAttr);
        }

        return attributeList;
    }

    public StringBuilder snapshotInfoToUserFriendlyStringBuilder(StringBuilder infoBuffer) {
        if (infoBuffer == null) {
            infoBuffer = new StringBuilder();
        }
        int columnCount = 0;
        SnapshotOptions snapshotOptions = Options.getInstance().getSnapshotOptions();
        String comment = snapshotData.getComment();
        if (snapshotOptions.isCSVID()) {
            columnCount++;
        }
        if (snapshotOptions.isCSVTime()) {
            columnCount++;
        }
        if (snapshotOptions.isCSVContextID()) {
            columnCount++;
        }
        if (comment != null && !comment.isEmpty()) {
            columnCount++;
        }
        if (columnCount > 0) {
            int i = 0;
            if (snapshotOptions.isCSVID()) {
                infoBuffer.append(Messages.getMessage("SNAPSHOT_DETAIL_ID_TITLE"));
                i++;
                if (i < columnCount) {
                    infoBuffer.append(snapshotOptions.getCSVSeparator());
                }
            }
            if (snapshotOptions.isCSVTime()) {
                infoBuffer.append(Messages.getMessage("SNAPSHOT_DETAIL_TIME_TITLE"));
                i++;
                if (i < columnCount) {
                    infoBuffer.append(snapshotOptions.getCSVSeparator());
                }
            }
            if (snapshotOptions.isCSVContextID()) {
                infoBuffer.append(Messages.getMessage("SNAPSHOT_DETAIL_CONTEXT_TITLE"));
                i++;
                if (i < columnCount) {
                    infoBuffer.append(snapshotOptions.getCSVSeparator());
                }
            }
            if (comment != null && !comment.isEmpty()) {
                infoBuffer.append(Messages.getMessage("SNAPSHOT_DETAIL_COMMENT_TITLE"));
                i++;
                if (i < columnCount) {
                    infoBuffer.append(snapshotOptions.getCSVSeparator());
                }
            }
            infoBuffer.append(GUIUtilities.CRLF);
            i = 0;
            if (snapshotOptions.isCSVID()) {
                if (snapshotData != null) {
                    infoBuffer.append(snapshotData.getId());
                }
                i++;
                if (i < columnCount) {
                    infoBuffer.append(snapshotOptions.getCSVSeparator());
                }
            }
            if (snapshotOptions.isCSVTime()) {
                String timeText = "";
                if (snapshotData != null && snapshotData.getTime() != null) {
                    timeText = snapshotData.getTime().toString();
                    if (timeText == null) {
                        timeText = "";
                    } else {
                        timeText = timeText.trim();
                    }
                }
                infoBuffer.append(timeText);
                timeText = null;
                i++;
                if (i < columnCount) {
                    infoBuffer.append(snapshotOptions.getCSVSeparator());
                }
            }
            if (snapshotOptions.isCSVContextID()) {
                infoBuffer.append(contextId);
                i++;
                if (i < columnCount) {
                    infoBuffer.append(snapshotOptions.getCSVSeparator());
                }
            }
            if (comment != null && !comment.isEmpty()) {
                infoBuffer.append(comment);
                i++;
                if (i < columnCount) {
                    infoBuffer.append(snapshotOptions.getCSVSeparator());
                }
            }
        }

        return infoBuffer;
    }

    private StringBuilder snapshotAttributesToUserFriendlyStringBuilder(StringBuilder attrBuffer) {
        if (attrBuffer == null) {
            attrBuffer = new StringBuilder();
        }
        int columnCount = 1;
        SnapshotOptions snapshotOptions = Options.getInstance().getSnapshotOptions();
        if (snapshotOptions.isCSVRead()) {
            columnCount++;
        }
        if (snapshotOptions.isCSVWrite()) {
            columnCount++;
        }
        if (snapshotOptions.isCSVDelta()) {
            columnCount++;
        }
        int i = 0;
        attrBuffer.append(Messages.getMessage("SNAPSHOT_DETAIL_COLUMNS_NAME"));
        i++;
        if (i < columnCount) {
            attrBuffer.append(snapshotOptions.getCSVSeparator());
        }
        if (snapshotOptions.isCSVWrite()) {
            attrBuffer.append(Messages.getMessage("SNAPSHOT_DETAIL_COLUMNS_W"));
            i++;
            if (i < columnCount) {
                attrBuffer.append(snapshotOptions.getCSVSeparator());
            }
        }
        if (snapshotOptions.isCSVRead()) {
            attrBuffer.append(Messages.getMessage("SNAPSHOT_DETAIL_COLUMNS_R"));
            i++;
            if (i < columnCount) {
                attrBuffer.append(snapshotOptions.getCSVSeparator());
            }
        }
        if (snapshotOptions.isCSVDelta()) {
            attrBuffer.append(Messages.getMessage("SNAPSHOT_DETAIL_COLUMNS_DELTA"));
            i++;
            if (i < columnCount) {
                attrBuffer.append(snapshotOptions.getCSVSeparator());
            }
        }
        if (snapshotAttributes != null && snapshotAttributes.getSnapshotAttributes() != null) {
            for (int attrIndex = 0; attrIndex < snapshotAttributes.getSnapshotAttributes().length; attrIndex++) {
                attrBuffer.append(GUIUtilities.CRLF);
                i = 0;
                final SnapshotAttribute attr = snapshotAttributes.getSnapshotAttributes()[attrIndex];
                attrBuffer.append(attr.getAttributeCompleteName());
                i++;
                if (i < columnCount) {
                    attrBuffer.append(snapshotOptions.getCSVSeparator());
                }
                if (snapshotOptions.isCSVWrite()) {
                    attrBuffer.append(SnapshotDetailRenderer.getTextFor(attr.getWriteValue()));
                    i++;
                    if (i < columnCount) {
                        attrBuffer.append(snapshotOptions.getCSVSeparator());
                    }
                }
                if (snapshotOptions.isCSVRead()) {
                    attrBuffer.append(SnapshotDetailRenderer.getTextFor(attr.getReadValue()));
                    i++;
                    if (i < columnCount) {
                        attrBuffer.append(snapshotOptions.getCSVSeparator());
                    }
                }
                if (snapshotOptions.isCSVDelta()) {
                    attrBuffer.append(SnapshotDetailRenderer.getTextFor(attr.getDeltaValue()));
                    i++;
                    if (i < columnCount) {
                        attrBuffer.append(snapshotOptions.getCSVSeparator());
                    }
                }
            }
        }
        snapshotOptions = null;
        return attrBuffer;
    }

    public String snapshotToUserFriendlyString() {
        StringBuilder outputBuffer = new StringBuilder();

        outputBuffer = snapshotInfoToUserFriendlyStringBuilder(outputBuffer);
        if (outputBuffer.length() > 0) {
            outputBuffer.append(GUIUtilities.CRLF).append(GUIUtilities.CRLF);
        }
        outputBuffer = snapshotAttributesToUserFriendlyStringBuilder(outputBuffer);

        return outputBuffer.toString();
    }

    public boolean isLoadable() {
        return loadable;
    }

    public void setLoadable(final boolean loadable) {
        this.loadable = loadable;
    }

    public int getContextId() {
        return contextId;
    }

    public void updateSnapshotCanSetProperty(String cellValue, boolean value) {
        if (cellValue != null && snapshotAttributes != null) {
            for (SnapshotAttribute attribute : snapshotAttributes.getSnapshotAttributes()) {
                if (cellValue.equalsIgnoreCase(attribute.getAttributeCompleteName())) {
                    attribute.setCanSet(value);
                    break;
                }
            }
        }
    }

    @Override
    public int compareTo(Snapshot o) {
        int compare;
        if (o == null) {
            compare = 1;
        } else if (o.contextId == contextId) {
            SnapshotData data = getSnapshotData();
            SnapshotData oData = o.getSnapshotData();
            if (data == null) {
                if (oData == null) {
                    compare = 0;
                } else {
                    compare = -1;
                }
            } else if (oData == null) {
                compare = 1;
            } else {
                compare = data.compareTo(oData);
            }
        } else {
            compare = 0;
        }
        return compare;
    }
}
