package fr.soleil.bensikin.data.snapshot;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fr.soleil.bensikin.models.DataArray2;

public class DefaultDataArrayAdapter implements IViewAdapter {

    Map<String, Object> datas = null;

    public DefaultDataArrayAdapter(List<DataArray2> data) {
	synchronized (data) {
	    datas = new LinkedHashMap<String, Object>(data.size());
	    for (DataArray2 item : data) {
		datas.put(item.getId(), item.getData());
	    }
	}
    }

    public Map<String, Object> getDatas() {
	return datas;
    }

    @Override
    public void clean() {
	datas.clear();
    }

}
