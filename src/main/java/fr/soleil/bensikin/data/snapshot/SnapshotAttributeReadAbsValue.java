package fr.soleil.bensikin.data.snapshot;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.bensikin.xml.BensikinXMLLine;

public class SnapshotAttributeReadAbsValue extends SnapshotAttributeValue {

    /**
     * The XML tag name used in saving/loading
     */
    public static final String XML_TAG = "ReadAbsValue";

    /**
     * Calls its mother's constructor.
     * 
     * @param dataFormat The data format (scalar/spectrum/image)
     * @param dataType The data type (double/string/...)
     * @param value The read value, not cast yet
     * @param nullElements The null elements identifier
     */
    public SnapshotAttributeReadAbsValue(int dataFormat, int dataType, Object value, Object nullElements) {
        super(dataFormat, dataType, value, nullElements);
    }

    /**
     * Returns a XML representation of the read value.
     * 
     * @return a XML representation of the read value
     */
    public String toXMLString() {
        String ret = "";

        BensikinXMLLine openingLine = new BensikinXMLLine(SnapshotAttributeReadAbsValue.XML_TAG,
                BensikinXMLLine.OPENING_TAG_CATEGORY);
        BensikinXMLLine closingLine = new BensikinXMLLine(SnapshotAttributeReadAbsValue.XML_TAG,
                BensikinXMLLine.CLOSING_TAG_CATEGORY);

        openingLine.setAttribute(SnapshotAttributeValue.NOT_APPLICABLE_XML_TAG, String.valueOf(this.isNotApplicable));

        ret += openingLine.toString();
        ret += GUIUtilities.CRLF;

        ret += super.toString();
        ret += GUIUtilities.CRLF;

        ret += closingLine.toString();

        return ret;
    }
}
