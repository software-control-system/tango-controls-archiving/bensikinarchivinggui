package fr.soleil.bensikin.data.snapshot;

import org.w3c.dom.Node;

import fr.soleil.bensikin.xml.BensikinXMLLine;
import fr.soleil.data.exception.KeyCompatibilityException;
import fr.soleil.data.service.AbstractKey;

public class BensikinAdapterKey extends AbstractKey {

    public BensikinAdapterKey(String name, Object value) {
        super(BensikinAdapterFactory.class.getName());
        registerProperty("Attribute", value);
        registerProperty("Name", name);
    }

    @Override
    public String getInformationKey() {
        return null;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public BensikinXMLLine[] toXMLLines(String tag) {
        return null;
    }

    @Override
    public void parseXML(Node xmlNode) throws KeyCompatibilityException {
    }
}
