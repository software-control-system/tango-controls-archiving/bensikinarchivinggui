// +======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/datasources/tango/ITangoManager.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  ITangoManager.
//						(Claisse Laurent) - 30 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.1 $
//
// $Log: ITangoManager.java,v $
// Revision 1.1  2005/12/14 16:56:05  ounsy
// has been renamed
//
// Revision 1.1.1.2  2005/08/22 11:58:39  chinkumo
// First commit
//
//
// copyleft :		Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.datasources.tango;

import java.util.List;

import fr.soleil.archiving.tango.entity.Domain;

/**
 * Defines operations on Tango attributes. Currently used only for searching
 * those attributes.
 * 
 * @author CLAISSE
 */
public interface ITangoManager {
    /**
     * Loads Tango attributes into a List containing Domain objects.
     * 
     * @param searchCriterions
     *            The search criterions
     * @return A List containing Domain objects
     */
    public List<Domain> loadDomains(String searchCriterions);

    /**
     * Loads the Tango attributes of a given device.
     * 
     * @param device_name
     *            The complete name of the device to load attributes for
     * @return A List containing the names of the device's attributes
     */
    public List<String> dbGetAttributeList(String device_name);
}
