//+======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/datasources/snapdb/SnapManagerFactory.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  SnapManagerFactory.
//						(Claisse Laurent) - 30 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.3 $
//
// $Log: SnapManagerFactory.java,v $
// Revision 1.3  2007/03/14 15:49:11  ounsy
// the user/password is no longer hard-coded in the APIs, Bensikin takes two more parameters
//
// Revision 1.2  2006/11/29 10:02:17  ounsy
// package refactoring
//
// Revision 1.1  2005/12/14 16:55:28  ounsy
// has been renamed
//
// Revision 1.1.1.2  2005/08/22 11:58:39  chinkumo
// First commit
//
//
// copyleft :		Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.datasources.snapdb;

import fr.soleil.archiving.snap.api.manager.ISnapManager;
import fr.soleil.archiving.snap.api.manager.SnapManagerImpl;

/**
 * A factory used to instantiate ISnapManager of 2 types: DUMMY_IMPL_TYPE or
 * REAL_IMPL_TYPE (DB access)
 * 
 * @author CLAISSE
 */
public class SnapManagerFactory {

    private static ISnapManager currentImpl = new SnapManagerImpl();

    /**
     * Returns the current implementation as precedently instantiated.
     * 
     * @return The current implementation as precedently instantiated
     */
    public static ISnapManager getCurrentImpl() {
	return currentImpl;
    }
}
