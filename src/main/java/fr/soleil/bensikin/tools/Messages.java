// +======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/bensikin/bensikin/tools/Messages.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  Messages.
//						(Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.2 $
//
// $Log: Messages.java,v $
// Revision 1.2  2006/11/16 11:53:15  ounsy
// correction mantis 2580
//
// Revision 1.1  2005/12/14 14:07:18  ounsy
// first commit  including the new  "tools,xml,lifecycle,profile" sub-directories
// under "bensikin.bensikin" and removing the same from their former locations
//
// Revision 1.1.1.2  2005/08/22 11:58:32  chinkumo
// First commit
//
//
// copyleft :		Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.bensikin.tools;

import java.util.Locale;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;

/**
 * The class in charge of internationalization. Uses 2 resource bundles, one for
 * the application's labels, and a smaller one for all log messages and error
 * messages.
 * 
 * @author CLAISSE
 */
public class Messages {

    private static final Logger LOGGER = LoggerFactory.getLogger(Messages.class);

    private static ResourceBundle myResources;
    private static ResourceBundle myLogResources;
    private static ResourceBundle myAppProperties;

    /**
     * Initializes the resource bundles used for labels and log messages
     * 
     * @param currentLocale
     *            The locale used by the application
     * @throws Exception
     */
    public static void initResourceBundle(final Locale currentLocale) throws ArchivingException {
        try {
            myResources = ResourceBundle.getBundle("fr.soleil.bensikin.resources.messages.resources", currentLocale);
            myLogResources = ResourceBundle.getBundle("fr.soleil.bensikin.resources.messages.logs", currentLocale);
            myAppProperties = ResourceBundle.getBundle("fr.soleil.bensikin.resources.application", currentLocale);
        } catch (final Exception e) {
            // of course if the resources loading failed we can't look up the
            // failure message in the ressources..
            final String msg = "Failed to load resources";
            LOGGER.error("CRITIC " + msg, e);
            throw ArchivingException.toArchivingException(e);
        }
    }

    /**
     * @param key
     *            The message key
     * @return The label for this key
     */
    public static String getMessage(final String key) {
        try {
            final String ret = myResources.getString(key);
            return ret;
        } catch (final Exception e) {
            return key;
        }
    }

    /**
     * @param key
     *            The log message key
     * @return The log message for this key
     */
    public static String getLogMessage(final String key) {
        try {
            final String ret = myLogResources.getString(key);
            return ret;
        } catch (final Exception e) {
            return key;
        }
    }

    public static String getAppMessage(final String key) {
        try {
            final String ret = myAppProperties.getString(key);
            return ret;
        } catch (final Exception e) {
            return key;
        }
    }
}
