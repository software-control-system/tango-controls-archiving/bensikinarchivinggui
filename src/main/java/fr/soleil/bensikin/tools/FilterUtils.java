package fr.soleil.bensikin.tools;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FilterUtils {

    private FilterUtils() {

    }

    /**
     * Get a subset of String that correspond to the specified pattern. Case
     * insensitive.
     * 
     * @param stringArray
     *            The array of String to filter
     * @param filter
     *            The pattern containing * or ? as wildchar
     * @return
     */
    public static String[] filterStrings(String[] stringArray, String filter) {
        List<String> results = new ArrayList<String>();
        for (String stringPattern : stringArray) {
            Pattern pattern = Pattern.compile(wildcardToRegex(filter), Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(stringPattern);
            if (matcher.matches()) {
                results.add(stringPattern);
            }
        }
        // System.out.println("results " + results);
        return results.toArray(new String[results.size()]);
    }

    /**
     * convert a string with wildchar ? or * to a regex
     * 
     * @param wildcard
     * @return
     */
    public static String wildcardToRegex(String wildcard) {
        StringBuilder s = new StringBuilder(wildcard.length());
        s.append('^');
        for (int i = 0, is = wildcard.length(); i < is; i++) {
            char c = wildcard.charAt(i);
            switch (c) {
            case '*':
                s.append(".*");
                break;
            case '?':
                s.append(".");
                break;
            // escape special regexp-characters
            case '(':
            case ')':
            case '[':
            case ']':
            case '$':
            case '^':
            case '.':
            case '{':
            case '}':
            case '|':
            case '\\':
                s.append("\\");
                s.append(c);
                break;
            default:
                s.append(c);
                break;
            }
        }
        s.append('$');
        // System.out.println("regex: "+s.toString());
        return s.toString();
    }

    // public static void main(String[] args) {
    // filterStrings(new String[] { "dsdqAbcid", "qAbcid", "ferfe" }, "?a*");
    // }

}
