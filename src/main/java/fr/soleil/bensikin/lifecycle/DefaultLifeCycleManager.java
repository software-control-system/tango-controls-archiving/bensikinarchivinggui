// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/bensikin/bensikin/lifecycle/DefaultLifeCycleManager.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class DefaultLifeCycleManager.
// (Claisse Laurent) - 5 juil. 2005
//
// $Author: pierrejoseph $
//
// $Revision: 1.11 $
//
// $Log: DefaultLifeCycleManager.java,v $
// Revision 1.11 2007/06/14 15:22:25 pierrejoseph
// comments addition
//
// Revision 1.10 2007/03/15 14:26:35 ounsy
// corrected the table mode add bug and added domains buffer
//
// Revision 1.9 2007/03/14 15:49:11 ounsy
// the user/password is no longer hard-coded in the APIs, Bensikin takes two
// more parameters
//
// Revision 1.8 2006/06/28 12:51:51 ounsy
// minor changes
//
// Revision 1.7 2006/05/03 13:06:56 ounsy
// modified the limited operator rights
//
// Revision 1.6 2006/04/26 12:38:01 ounsy
// splash added
//
// Revision 1.5 2006/04/10 08:48:05 ounsy
// added RightsManagerFactory in startFactories
//
// Revision 1.4 2005/12/14 16:41:03 ounsy
// added methods necessary for alternate attribute selection
//
// Revision 1.3 2005/11/29 18:25:13 chinkumo
// no message
//
// Revision 1.1.1.2 2005/08/22 11:58:41 chinkumo
// First commit
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.bensikin.lifecycle;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.bensikin.Bensikin;
import fr.soleil.bensikin.actions.roles.IRightsManager;
import fr.soleil.bensikin.actions.roles.RightsManagerFactory;
import fr.soleil.bensikin.containers.context.ContextDetailPanel;
import fr.soleil.bensikin.data.context.manager.ContextManagerFactory;
import fr.soleil.bensikin.data.snapshot.manager.SnapshotManagerFactory;
import fr.soleil.bensikin.datasources.tango.TangoManagerFactory;
import fr.soleil.bensikin.favorites.Favorites;
import fr.soleil.bensikin.favorites.FavoritesManagerFactory;
import fr.soleil.bensikin.favorites.IFavoritesManager;
import fr.soleil.bensikin.history.History;
import fr.soleil.bensikin.history.manager.HistoryManagerFactory;
import fr.soleil.bensikin.history.manager.IHistoryManager;
import fr.soleil.bensikin.options.Options;
import fr.soleil.bensikin.options.manager.IOptionsManager;
import fr.soleil.bensikin.options.manager.OptionsManagerFactory;
import fr.soleil.bensikin.options.sub.SaveOptions;
import fr.soleil.bensikin.tools.Messages;
import fr.soleil.lib.project.swing.Splash;

/**
 * The default implementation.
 * 
 * @author CLAISSE
 */
public class DefaultLifeCycleManager implements LifeCycleManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultLifeCycleManager.class);

    private String historyPath;
    private String favoritesPath;

    private boolean hasHistorySave = true;
    private boolean[] loadProperties = null;

    public DefaultLifeCycleManager() {
    }

    /**
     * Called before the GUI graphics containers are instantiated. Loads from
     * files :
     * <UL>
     * <LI>the application's state from the history file
     * <LI>the application's options
     * </UL>
     * And :
     * <UL>
     * <LI>initializes the application's ressources
     * <LI>instantiates static implementations of various managers
     * </UL>
     * 
     * @param startParameters
     *            Not used
     */
    @Override
    public void applicationWillStart(final Splash splash) throws ArchivingException {
        startFactories();

        final Locale currentLocale = new Locale("en", "US");
        try {
            Messages.initResourceBundle(currentLocale);
        } catch (final Exception e) {
            throw ArchivingException.toArchivingException(e);
        }
        // treatError is already done in this method
        loadOptions(splash);

        // treatError is already done in this method
        loadFavorites(splash);

        if (hasHistorySave) {
            // No exception can be raised
            loadHistory(splash);
        }
    }

    /**
     * Loads the application's options.
     */
    private void loadOptions(final Splash splash) {
        splash.progress(5);
        splash.setMessage("loading options...");
        Bensikin.loadOptions();
        splash.progress(6);
        // we have to call it now before the normal options.push () that
        // occurs in applicationStarted
        // because otherwise the loaded desired log level won't be in effect
        // before applicationStarted
        // is called, and we always get applicationWillStart DEBUG level
        // logs.
        splash.progress(8);
        splash.setMessage("Loading history options");
        // same for history loading
        setHasToLoadHistory();
        splash.progress(9);
        splash.setMessage("Loading history options done");

        final String msg = Messages.getLogMessage("APPLICATION_WILL_START_LOAD_OPTIONS_OK");
        LOGGER.debug(msg);
    }

    /**
     * Sets the load history boolean early so that the history isn't loaded if
     * it doesn't have to.
     */
    private void setHasToLoadHistory() {
        final Options options = Options.getOptionsInstance();
        final SaveOptions saveOptions = options.getSaveOptions();
        saveOptions.push();
    }

    /**
     * Loads the application's favorites.
     */
    private void loadFavorites(Splash splash) {
        try {
            favoritesPath = Bensikin.getPathToResources() + "/favorites";
            final File f = new File(favoritesPath);
            if (!f.exists()) {
                // boolean b =
                f.mkdirs();
            }
            favoritesPath += "/favorites.xml";

            final IFavoritesManager favoritesManager = FavoritesManagerFactory.getCurrentImpl();

            favoritesManager.loadFavorites(favoritesPath);

            final String msg = Messages.getLogMessage("APPLICATION_WILL_START_LOAD_FAVORITES_OK");
            LOGGER.debug(msg);
//        } catch (final FileNotFoundException fnfe) {
//            final String msg = Messages.getLogMessage("APPLICATION_WILL_START_LOAD_FAVORITES_WARNING");
//            LOGGER.warn(msg, fnfe);
        } catch (final Exception e) {
            final String msg = Messages.getLogMessage("APPLICATION_WILL_START_LOAD_FAVORITES_KO");
            Bensikin.treatError(e, msg, splash);
        }
    }

    /**
     * Instantiates the following managers with the following types:
     * <UL>
     * <LI>ILogger (DEFAULT_TYPE)
     * <LI>ITangoManager (REAL_IMPL_TYPE)
     * <LI>ISnapManager (REAL_IMPL_TYPE)
     * <LI>IHistoryManager (XML_IMPL_TYPE)
     * <LI>IOptionsManager (XML_IMPL_TYPE)
     * <LI>IFavoritesManager (XML_IMPL_TYPE)
     * <LI>IContextManager (XML_IMPL_TYPE)
     * <LI>ISnapshotManager (XML_IMPL_TYPE)
     * </UL>
     */
    private void startFactories() {
        // BensikinLoggerFactory.getImpl(LoggerFactory.BensikinLoggerFactory);

        TangoManagerFactory.getImpl(TangoManagerFactory.REAL_IMPL_TYPE);
        // TangoAlternateSelectionManagerFactory
        // .getImpl(TangoAlternateSelectionManagerFactory.BUFFERED_AND_ORDERED);

        HistoryManagerFactory.getImpl(HistoryManagerFactory.XML_IMPL_TYPE);
        OptionsManagerFactory.getImpl(OptionsManagerFactory.XML_IMPL_TYPE);
        FavoritesManagerFactory.getImpl(FavoritesManagerFactory.XML_IMPL_TYPE);

        ContextManagerFactory.getImpl(ContextManagerFactory.XML_IMPL_TYPE);
        SnapshotManagerFactory.getImpl(SnapshotManagerFactory.XML_IMPL_TYPE);

        RightsManagerFactory.getImpl(RightsManagerFactory.SNAPSHOTS_ONLY_OPERATOR);
    }

    /**
     * Loads the application's history.
     */
    private void loadHistory(final Splash splash) {
        try {
            splash.progress(10);
            splash.setMessage("preparing history");
            historyPath = Bensikin.getPathToResources() + "/history";
            final File f = new File(historyPath);
            if (!f.exists()) {
                // boolean b =
                f.mkdirs();

            }
            historyPath += "/history.xml";

            splash.progress(11);
            splash.setMessage("initializing history manager");
            final IHistoryManager historyManager = HistoryManagerFactory.getCurrentImpl();

            splash.progress(12);
            splash.setMessage("loading history...");
            final History history = historyManager.loadHistory(historyPath);
            if (history != null) {
                splash.progress(13);
                splash.setMessage("applying history");
                History.setHistory(history);
                splash.progress(14);
                history.setLoadProperties(loadProperties);

            }
            splash.progress(15);
            splash.setMessage("history fully loaded");

            final String msg = Messages.getLogMessage("APPLICATION_WILL_START_LOAD_HISTORY_OK");
            LOGGER.debug(msg);
        } catch (final FileNotFoundException fnfe) {
            final String msg = Messages.getLogMessage("APPLICATION_WILL_START_LOAD_HISTORY_WARNING");
            splash.progress(15);
            splash.setMessage(msg);
            LOGGER.warn(msg, fnfe);
            Bensikin.treatError(fnfe, msg, splash);
        } catch (final Exception e) {
            final String msg = Messages.getLogMessage("APPLICATION_WILL_START_LOAD_HISTORY_KO");
            splash.progress(15);
            splash.setMessage(msg);
            Bensikin.treatError(e, msg, splash);
        }
    }

    /**
     * Called just after the GUI graphics containers are instantiated. Is used
     * for operations that need the containers to already exist: -pushing the
     * pre loaded history and options to the display components -setting the
     * window size
     * 
     * @param startParameters
     *            Not used
     */
    @Override
    public void applicationStarted() {
        final Options options = Options.getInstance();
        try {
            options.push();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        // We have to push the selected context data only after the data panel
        // is created. Otherwise the fields don't exist yet, hence we can't set
        // their values
        final History history = History.getHistory();
        try {
            history.push();
        } catch (final Exception e) {
            // No selected context to pre-load
        }

        final IRightsManager rightsManager = RightsManagerFactory.getCurrentImpl();
        if (Bensikin.isRestricted()) {
            rightsManager.disableUselessFields();
        } else {
            ContextDetailPanel.getInstance().getAttributeTableSelectionBean().start();
        }
    }

    /**
     * Called when the application detects a shutdown request, be it through the
     * close icon or through the menu's Exit option. Is used to: -save
     * everything that has to be saved -close resources And finally, shutdowns
     * the application.
     * 
     * @param endParameters
     *            Not used
     */
    @Override
    public void applicationClosed() {
        try {
            // begin do stuff
            System.out.println("Bensikin will close !");

            saveOptions();
            saveFavorites();
            if (hasHistorySave) {
                saveHistory();
            }

            System.out.println("Bensikin closed");
            // end do stuff
            System.exit(0);
        } catch (final Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Saves the application's favorites.
     * 
     * @throws ArchivingException
     */
    private void saveFavorites() throws ArchivingException {
        try {
            final Favorites favoritesToSave = Favorites.getInstance();
            final IFavoritesManager favoritesManager = FavoritesManagerFactory.getCurrentImpl();
            favoritesManager.saveFavorites(favoritesToSave, favoritesPath);

            final String msg = Messages.getLogMessage("APPLICATION_WILL_STOP_SAVE_FAVORITES_OK");
            LOGGER.debug(msg);
        } catch (final Exception e) {
            e.printStackTrace();
            final String msg = Messages.getLogMessage("APPLICATION_WILL_STOP_SAVE_FAVORITES_KO");
            LOGGER.error(msg, e);

        }
    }

    /**
     * Saves the application's options.
     * 
     * @throws ArchivingException
     */
    private void saveOptions() throws ArchivingException {
        try {
            final Options optionsToSave = Options.getInstance();
            final IOptionsManager optionsManager = OptionsManagerFactory.getCurrentImpl();
            optionsManager.saveOptions(optionsToSave, Bensikin.getOptionPath());

            final String msg = Messages.getLogMessage("APPLICATION_WILL_STOP_SAVE_OPTIONS_OK");
            LOGGER.debug(msg);
        } catch (final Exception e) {
            final String msg = Messages.getLogMessage("APPLICATION_WILL_STOP_SAVE_OPTIONS_KO");
            LOGGER.error(msg, e);
            return;
        }
    }

    /**
     * Saves the application's history.
     * 
     * @throws ArchivingException
     */
    private void saveHistory() throws ArchivingException {
        try {
            final History historyToSave = History.getCurrentHistory();
            final IHistoryManager historyManager = HistoryManagerFactory.getCurrentImpl();
            // XXX
            LOGGER.debug("save history to " + historyPath);
            historyManager.saveHistory(historyToSave, historyPath);

            final String msg = Messages.getLogMessage("APPLICATION_WILL_STOP_SAVE_HISTORY_OK");
            LOGGER.debug(msg);
        } catch (final Exception e) {
            final String msg = Messages.getLogMessage("APPLICATION_WILL_STOP_SAVE_HISTORY_KO");
            LOGGER.error(msg, e);
            return;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see bensikin.lifecycle.LifeCycleManager#setHasHistorySave(boolean)
     */
    @Override
    public void setHasHistorySave(final boolean b) {
        hasHistorySave = b;
    }

    /*
     * (non-Javadoc)
     * 
     * @see bensikin.lifecycle.LifeCycleManager#hasHistorySave()
     */
    @Override
    public boolean hasHistorySave() {
        return hasHistorySave;
    }

    @Override
    public boolean[] getLoadProperties() {
        return loadProperties;
    }

    @Override
    public void setLoadProperties(boolean[] loadProperties) {
        this.loadProperties = loadProperties;
    }

}
