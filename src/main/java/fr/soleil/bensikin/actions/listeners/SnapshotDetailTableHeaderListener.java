package fr.soleil.bensikin.actions.listeners;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import fr.soleil.bensikin.models.SnapshotDetailTableModel;

/**
 * Listens to double clicks on the snapshots list table header. Responds by
 * sorting the clicked column.
 * <UL>
 * <LI>Checks the click is not a single click, if it is does nothing
 * <LI>Gets the index of the clicked column
 * <LI>Sorts the SnapshotListTableModel instance for this column index
 * </UL>
 * 
 * @author CLAISSE
 */
public class SnapshotDetailTableHeaderListener extends MouseAdapter {
    // private final SnapshotDetailTableModel model;

    /**
     * Does nothing
     */
    public SnapshotDetailTableHeaderListener(SnapshotDetailTableModel _model) {
        // this.model = _model;
    }

    @Override
    public void mousePressed(MouseEvent event) {
        // int clickCount = event.getClickCount();
        // if (clickCount == 1) {
        // return;
        // // so that the use can resize columns
        // }

        // Point point = event.getPoint();
        // JTableHeader header = (JTableHeader) event.getSource();
        // int clickedColunIndex = header.columnAtPoint(point);

        // this.model.sort(clickedColunIndex);

    }
}
