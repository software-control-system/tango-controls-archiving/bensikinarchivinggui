package fr.soleil.bensikin.actions.roles;

import javax.swing.Action;

import fr.soleil.bensikin.actions.context.AddSelectedContextAttributesAction;
import fr.soleil.bensikin.actions.context.MatchContextAttributesAction;
import fr.soleil.bensikin.actions.context.MatchPossibleContextAttributesAction;
import fr.soleil.bensikin.actions.context.NewContextAction;
import fr.soleil.bensikin.actions.context.RegisterContextAction;
import fr.soleil.bensikin.actions.context.RemoveSelectedContextAttributesAction;
import fr.soleil.bensikin.actions.context.ValidateAlternateSelectionAction;
import fr.soleil.bensikin.components.BensikinToolbar;
import fr.soleil.bensikin.containers.context.ContextDataPanel;
import fr.soleil.bensikin.containers.context.ContextDetailPanel;

/**
 * @author CLAISSE
 */
public class SnapshotsOnlyRightsManager implements IRightsManager {

    public SnapshotsOnlyRightsManager() {
        super();
    }

    @Override
    public boolean isGrantedToOperator(Action action) {
        boolean result;
        if (action instanceof AddSelectedContextAttributesAction) {
            return false;
        } else if (action instanceof MatchContextAttributesAction) {
            result = false;
        } else if (action instanceof MatchPossibleContextAttributesAction) {
            result = false;
        } else if (action instanceof NewContextAction) {
            result = false;
        } else if (action instanceof RegisterContextAction) {
            result = false;
        } else if (action instanceof RemoveSelectedContextAttributesAction) {
            result = false;
        } else if (action instanceof ValidateAlternateSelectionAction) {
            result = false;
        }
        /*
         * else if ( action instanceof EditCommentAction ) { result= false; }
         * else if ( action instanceof OpenEditCommentAction ) { result = false;
         * } else if ( action instanceof SelectAllOrNoneAction ) { result =
         * false; } else if ( action instanceof SetEquipmentsAction ) { result =
         * false; } else if ( action instanceof SnapSpectrumSetAction ) { result
         * = false; } else if ( action instanceof SnapSpectrumSetAllAction ) {
         * result = false; }
         */
        else {
            result = true;
        }
        return result;
    }

    @Override
    public void disableUselessFields() {
        BensikinToolbar bensikinToolbar = BensikinToolbar.getInstance();
        if (bensikinToolbar != null) {
            bensikinToolbar.removeNewContextButton();
        }

        ContextDataPanel.disableInput();
        ContextDetailPanel.getInstance().getAttributeTableSelectionBean().getSelectionPanel().setEditionEnabled(false);
        ContextDetailPanel.getInstance().getAttributeTableSelectionBean().stop();
        ContextDetailPanel.getInstance().getAttributeTableSelectionBean().getSelectionPanel().stepDone(0, 0);
    }

}
