/*
 * Synchrotron Soleil File : ReadOnlyRightsManager.java Project :
 * bensikinOperator Description : Author : CLAISSE Original : 28 mars 2006
 * Revision: Author: Date: State: Log: ReadOnlyRightsManager.java,v
 */
/*
 * Created on 28 mars 2006 To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.soleil.bensikin.actions.roles;

import javax.swing.Action;

import fr.soleil.bensikin.actions.context.AddSelectedContextAttributesAction;
import fr.soleil.bensikin.actions.context.LaunchSnapshotAction;
import fr.soleil.bensikin.actions.context.MatchContextAttributesAction;
import fr.soleil.bensikin.actions.context.MatchPossibleContextAttributesAction;
import fr.soleil.bensikin.actions.context.NewContextAction;
import fr.soleil.bensikin.actions.context.RegisterContextAction;
import fr.soleil.bensikin.actions.context.RemoveSelectedContextAttributesAction;
import fr.soleil.bensikin.actions.context.ValidateAlternateSelectionAction;
import fr.soleil.bensikin.actions.snapshot.EditCommentAction;
import fr.soleil.bensikin.actions.snapshot.OpenEditCommentAction;
import fr.soleil.bensikin.actions.snapshot.SelectAllOrNoneAction;
import fr.soleil.bensikin.actions.snapshot.SetEquipmentsAction;
import fr.soleil.bensikin.actions.snapshot.SnapSpectrumSetAction;
import fr.soleil.bensikin.actions.snapshot.SnapSpectrumSetAllAction;

/**
 * @author CLAISSE
 */
public class ReadOnlyRightsManager implements IRightsManager {

    public ReadOnlyRightsManager() {
        super();
    }

    @Override
    public boolean isGrantedToOperator(Action action) {
        boolean result;
        if (action instanceof AddSelectedContextAttributesAction) {
            result = false;
        } else if (action instanceof LaunchSnapshotAction) {
            result = false;
        } else if (action instanceof MatchContextAttributesAction) {
            result = false;
        } else if (action instanceof MatchPossibleContextAttributesAction) {
            result = false;
        } else if (action instanceof NewContextAction) {
            result = false;
        } else if (action instanceof RegisterContextAction) {
            result = false;
        } else if (action instanceof RemoveSelectedContextAttributesAction) {
            result = false;
        } else if (action instanceof ValidateAlternateSelectionAction) {
            result = false;
        } else if (action instanceof EditCommentAction) {
            result = false;
        } else if (action instanceof OpenEditCommentAction) {
            result = false;
        } else if (action instanceof SelectAllOrNoneAction) {
            result = false;
        } else if (action instanceof SetEquipmentsAction) {
            result = false;
        } else if (action instanceof SnapSpectrumSetAction) {
            result = false;
        } else if (action instanceof SnapSpectrumSetAllAction) {
            result = false;
        } else {
            result = true;
        }
        return result;
    }

    @Override
    public void disableUselessFields() {
        // nothing to do
    }

}
