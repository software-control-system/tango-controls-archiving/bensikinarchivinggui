package fr.soleil.bensikin.actions.snapshot;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;

import fr.soleil.bensikin.Bensikin;
import fr.soleil.bensikin.components.snapshot.SnapshotTextFileFilter;
import fr.soleil.bensikin.containers.snapshot.SnapshotDetailTabbedPane;
import fr.soleil.bensikin.containers.snapshot.SnapshotDetailTabbedPaneContent;
import fr.soleil.bensikin.tools.Messages;

public class SnapshotDetailExportAsTextAction extends AbstractSaveToFileAction {

    private static final long serialVersionUID = 447072265712573505L;

    private ImageIcon exportIcon = new ImageIcon(Bensikin.class.getResource("/com/famfamfam/silk/page_go.png"));

    public SnapshotDetailExportAsTextAction() {
	super(Messages.getMessage("SNAPSHOT_DETAIL_EXPORT_AS_TEXT_TOOLTIP"));
	putValue(SMALL_ICON, exportIcon);
	putValue(SHORT_DESCRIPTION, getValue(NAME));
    }

    @Override
    protected JFileChooser initFileChooser() {
	JFileChooser chooser = new JFileChooser();
	String title = Messages.getMessage("DIALOGS_FILE_CHOOSER_SNAPSHOT_EXPORT_AS_TEXT_TITLE");
	chooser.setDialogTitle(title);
	SnapshotTextFileFilter snapshotTextFilter = new SnapshotTextFileFilter();
	chooser.addChoosableFileFilter(snapshotTextFilter);
	return chooser;
    }

    @Override
    protected void save(String path) {
	SnapshotDetailTabbedPane tabbedPane = SnapshotDetailTabbedPane.getInstance();
	((SnapshotDetailTabbedPaneContent) tabbedPane.getSelectedComponent()).exportSnapshotContentAsText(path);
    }

}
