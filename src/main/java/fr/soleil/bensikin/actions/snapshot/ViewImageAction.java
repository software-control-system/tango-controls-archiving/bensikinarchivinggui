package fr.soleil.bensikin.actions.snapshot;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import fr.soleil.bensikin.containers.sub.dialogs.ViewImageDialog;
import fr.soleil.bensikin.tools.Messages;

public class ViewImageAction extends AbstractAction {

    private static final long serialVersionUID = 1613980108125044471L;
    private ViewImageDialog   dialog;
    private String            name;
    private Object            value;

    public ViewImageAction(String name, Object value) {
        super();
        this.putValue(Action.NAME, Messages
                .getMessage("DIALOGS_IMAGE_ATTRIBUTE_VIEW_IMAGE"));
        this.name = name;
        this.value = value;
        dialog = null;

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (dialog == null) {
	    dialog = new ViewImageDialog(name, value);
        }
        dialog.updateContent();
        dialog.setVisible(true);
    }
}
