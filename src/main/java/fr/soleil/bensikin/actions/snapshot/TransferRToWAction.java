package fr.soleil.bensikin.actions.snapshot;

import java.awt.event.ActionEvent;

import javax.swing.Action;

import fr.soleil.bensikin.actions.BensikinAction;
import fr.soleil.bensikin.components.snapshot.detail.SnapshotDetailTable;

public class TransferRToWAction extends BensikinAction {

    private static final long serialVersionUID = -4391057980050018895L;

    private SnapshotDetailTable table;

    public TransferRToWAction(String name, SnapshotDetailTable table) {
        super(name);
        this.putValue(Action.NAME, name);
        this.table = table;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        // be sure to stop editing
        this.table.applyChange();

        // then, edit
        this.table.transferSelectedReadToWrite();
    }

}
