package fr.soleil.bensikin.actions.snapshot;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JTable;

import fr.soleil.bensikin.containers.sub.dialogs.ImageWriteAttibuteDialog;

public class SnapImageSetAction extends AbstractAction {

    private static final long serialVersionUID = 7981493176311957846L;

    private JTable table;
    private int row;
    private ImageWriteAttibuteDialog dialog;

    public SnapImageSetAction(String name, JTable table, ImageWriteAttibuteDialog dialog, int row) {
        this.putValue(Action.NAME, name);
        this.dialog = dialog;
        this.row = row;
        this.table = table;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        table.getModel().setValueAt(dialog.getValue(), row, 1);
        dialog.setVisible(false);
    }
}
