package fr.soleil.bensikin.actions.snapshot;

import java.awt.event.ActionEvent;
import java.io.File;
import java.lang.ref.SoftReference;

import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import fr.soleil.bensikin.actions.BensikinAction;
import fr.soleil.bensikin.components.DataFileFilter;
import fr.soleil.bensikin.containers.BensikinFrame;
import fr.soleil.bensikin.tools.Messages;

public abstract class AbstractSaveToFileAction extends BensikinAction {

    private static final long serialVersionUID = -4266099659795740947L;

    protected SoftReference<JFileChooser> softChooser;

    /**
     * Create the file chooser
     * 
     * @return the file chooser. It should not be null.
     */
    abstract protected JFileChooser initFileChooser();

    /**
     * The save action to be executed
     */
    abstract protected void save(String path);

    public AbstractSaveToFileAction(String _name) {
        super(_name);
        this.putValue(Action.NAME, _name);

        softChooser = new SoftReference<JFileChooser>(null);
    }

    /**
     * Get or create the file chooser. Chooser may have been cleared due to
     * memory needs.
     * 
     * @return the file chooser to use
     */
    private JFileChooser getFileChooser() {
        JFileChooser chooser = softChooser.get();
        if (chooser == null) {
            chooser = initFileChooser();
            softChooser = new SoftReference<JFileChooser>(chooser);
        }
        return chooser;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        // get the file chooser
        JFileChooser chooser = getFileChooser();
        int returnVal = chooser.showSaveDialog(BensikinFrame.getInstance());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = chooser.getSelectedFile();
            String path = f.getAbsolutePath();

            // if a filename is specified
            if (f != null) {
                FileFilter fileFilter = chooser.getFileFilter();
                String expectedExtension = null;
                if (fileFilter instanceof DataFileFilter) {
                    expectedExtension = ((DataFileFilter) fileFilter).getExtension();
                }
                String extension = DataFileFilter.getExtension(f);
                // add expected extension if not found
                if ((expectedExtension != null)
                        && ((extension == null) || (!extension.equalsIgnoreCase(expectedExtension)))) {
                    path += "." + expectedExtension;
                    f = new File(path);
                    chooser.setSelectedFile(f);
                }
                int choice = JOptionPane.YES_OPTION;
                // ask for overwriting if file already exists
                if (f.exists()) {
                    choice = JOptionPane.showConfirmDialog(BensikinFrame.getInstance(),
                            Messages.getMessage("DIALOGS_FILE_CHOOSER_FILE_EXISTS"),
                            Messages.getMessage("DIALOGS_FILE_CHOOSER_FILE_EXISTS_TITLE"), JOptionPane.YES_NO_OPTION,
                            JOptionPane.WARNING_MESSAGE);
                }
                // finally save the file
                if (choice == JOptionPane.YES_OPTION) {
                    save(path);
                }
            }
        } // end if (returnVal == JFileChooser.APPROVE_OPTION)
    }

}
