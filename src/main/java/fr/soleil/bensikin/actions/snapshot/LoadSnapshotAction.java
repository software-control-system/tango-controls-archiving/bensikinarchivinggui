// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/bensikin/bensikin/actions/snapshot/LoadSnapshotAction.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class LoadACAction.
// (Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.3 $
//
// $Log: LoadSnapshotAction.java,v $
// Revision 1.3 2006/04/10 08:47:14 ounsy
// Bensikin action now all inherit from BensikinAction for easy rights
// management
//
// Revision 1.2 2005/12/14 16:09:11 ounsy
// new Word-like file management
//
// Revision 1.1 2005/12/14 14:07:18 ounsy
// first commit including the new "tools,xml,lifecycle,profile" sub-directories
// under "bensikin.bensikin" and removing the same from their former locations
//
// Revision 1.1.2.2 2005/09/14 15:41:20 chinkumo
// Second commit !
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.bensikin.actions.snapshot;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;

import javax.swing.Action;
import javax.swing.JFileChooser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.bensikin.actions.BensikinAction;
import fr.soleil.bensikin.components.DataFileFilter;
import fr.soleil.bensikin.components.snapshot.SnapshotFileFilter;
import fr.soleil.bensikin.containers.BensikinFrame;
import fr.soleil.bensikin.containers.snapshot.SnapshotDetailTabbedPane;
import fr.soleil.bensikin.data.snapshot.Snapshot;
import fr.soleil.bensikin.data.snapshot.manager.ISnapshotManager;
import fr.soleil.bensikin.data.snapshot.manager.SnapshotManagerFactory;
import fr.soleil.bensikin.tools.Messages;

/**
 * Loads a snapshot from a file. This action can be of 2 type: default (quick
 * load from the default directory and file), or non-default (load from the
 * user-defined directory and file, with prepositionning on the default
 * directory).
 * <UL>
 * <LI>if the load isn't a quick load,opens a file chooser dialog specialised in snapshot loading, gets the path to load
 * from from it.
 * <LI>uses the application's ISnapshotManager to load the snapshot.
 * <LI>displays the loaded snapshot.
 * <LI>logs the action's success or failure
 * </UL>
 * 
 * @author CLAISSE
 */
public class LoadSnapshotAction extends BensikinAction {

    private static final long serialVersionUID = -6068527695400810022L;

    private static final Logger LOGGER = LoggerFactory.getLogger(LoadSnapshotAction.class);
    private final boolean isDefault;

    /**
     * Standard action constructor that sets the action's name, plus sets the
     * isDefault attribute.
     * 
     * @param name
     *            The action name
     * @param _isDefault
     */
    public LoadSnapshotAction(final String name, final boolean _isDefault) {
        super.putValue(Action.NAME, name);
        super.putValue(Action.SHORT_DESCRIPTION, name);

        isDefault = _isDefault;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(final ActionEvent actionEvent) {
        final ISnapshotManager manager = SnapshotManagerFactory.getCurrentImpl();
        boolean load = true;
        if (!isDefault) {
            // open file chooser
            final JFileChooser chooser = new JFileChooser();
            final String location = manager.getSaveLocation();
            if ((location == null) || location.trim().isEmpty()) {
                chooser.setCurrentDirectory(new File(manager.getDefaultSaveLocation()));
            } else {
                File file = new File(location);
                if (file.isFile()) {
                    file = file.getParentFile();
                }
                chooser.setCurrentDirectory(file);
            }
            final SnapshotFileFilter filter = new SnapshotFileFilter();
            chooser.addChoosableFileFilter(filter);
            final String title = Messages.getMessage("DIALOGS_FILE_CHOOSER_LOAD_SNAPSHOT_TITLE");
            chooser.setDialogTitle(title);
            final int returnVal = chooser.showOpenDialog(BensikinFrame.getInstance());
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File f = chooser.getSelectedFile();
                String path = f.getAbsolutePath();
                if (f != null) {
                    final String extension = DataFileFilter.getExtension(f);
                    final String expectedExtension = filter.getExtension();
                    if ((extension == null) || (!extension.equalsIgnoreCase(expectedExtension))) {
                        path += "." + expectedExtension;
                        f = new File(path);
                        chooser.setSelectedFile(f);
                    }
                }
                manager.setNonDefaultSaveLocation(path);
            } else {
                load = false;
            }
        } else {
            manager.setNonDefaultSaveLocation(null);
        }
        if (load) {
            try {
                final Snapshot selectedSnapshot = manager.loadSnapshot();
                selectedSnapshot.setLoadable(true);
                final SnapshotDetailTabbedPane tabbedPane = SnapshotDetailTabbedPane.getInstance();
                final String fileName = manager.getFileName();
                tabbedPane.addFileSnapshotDetail(selectedSnapshot, fileName);
                final String msg = Messages.getLogMessage("LOAD_SNAPSHOT_ACTION_OK");
                LOGGER.debug(msg);
            } catch (final FileNotFoundException fnfe) {
                final String msg = Messages.getLogMessage("LOAD_SNAPSHOT_ACTION_WARNING");
                LOGGER.warn(msg, fnfe);
            } catch (final Exception e) {
                final String msg = Messages.getLogMessage("LOAD_SNAPSHOT_ACTION_KO");
                LOGGER.error(msg, e);
            }
        }
    }
}
