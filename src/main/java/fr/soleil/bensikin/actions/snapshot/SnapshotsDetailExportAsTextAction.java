package fr.soleil.bensikin.actions.snapshot;

import java.io.FileWriter;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.snap.api.tools.SnapshotingException;
import fr.soleil.bensikin.Bensikin;
import fr.soleil.bensikin.components.snapshot.SnapshotTextFileFilter;
import fr.soleil.bensikin.components.snapshot.list.SnapshotListTable;
import fr.soleil.bensikin.containers.BensikinFrame;
import fr.soleil.bensikin.data.snapshot.Snapshot;
import fr.soleil.bensikin.data.snapshot.SnapshotData;
import fr.soleil.bensikin.models.SnapshotListTableModel;
import fr.soleil.bensikin.tools.Messages;

public class SnapshotsDetailExportAsTextAction extends AbstractSaveToFileAction {

    private static final long serialVersionUID = -4425260260497139221L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SnapshotsDetailExportAsTextAction.class);
    private ImageIcon exportIcon = new ImageIcon(Bensikin.class.getResource("/com/famfamfam/silk/page_go.png"));

    public SnapshotsDetailExportAsTextAction() {
        super(Messages.getMessage("SNAPSHOTS_DETAIL_EXPORT_AS_TEXT"));
        putValue(SMALL_ICON, exportIcon);
        putValue(SHORT_DESCRIPTION, Messages.getMessage("SNAPSHOTS_DETAIL_EXPORT_AS_TEXT_TOOLTIP"));
    }

    @Override
    protected JFileChooser initFileChooser() {
        JFileChooser chooser = new JFileChooser();
        String title = Messages.getMessage("DIALOGS_FILE_CHOOSER_SNAPSHOTS_EXPORT_AS_TEXT_TITLE");
        chooser.setDialogTitle(title);
        SnapshotTextFileFilter snapshotTextFilter = new SnapshotTextFileFilter();
        chooser.addChoosableFileFilter(snapshotTextFilter);
        return chooser;
    }

    @Override
    protected void save(String path) {
        SnapshotListTable snapshotListTable = SnapshotListTable.getInstance();

        StringBuilder sb = new StringBuilder();

        int[] selectedRows = snapshotListTable.getSelectedRows();
        SnapshotListTableModel sourceModel = (SnapshotListTableModel) snapshotListTable.getModel();
        for (int i = 0; i < selectedRows.length; i++) {
            SnapshotData selectedSnapshotData = sourceModel.getSnapshotDataAtRow(selectedRows[i]);
            Snapshot selectedSnapshot = new Snapshot(selectedSnapshotData);
            selectedSnapshotData.setSnapshot(selectedSnapshot);
            try {
                selectedSnapshot.loadAttributes();
            } catch (SnapshotingException e) {
                final String msg = Messages.getLogMessage("LOAD_SNAPSHOT_ATTRIBUTES_KO");
                LOGGER.warn(msg, e);
            }
            String snapshotDetails = selectedSnapshot.snapshotToUserFriendlyString();

            if (i > 0) {
                sb.append("\n\n#############################################\n\n");
            }
            sb.append(snapshotDetails);
        }

        String snapshotContent = sb.toString();

        FileWriter fw = null;
        try {
            fw = new FileWriter(path);
            fw.write(snapshotContent);
            fw.close();

            final String msg = Messages.getLogMessage("EXPORT_SNAPSHOTS_DETAILS_AS_TEXT_ACTION_OK");
            LOGGER.debug(msg);
        } catch (IOException e) {
            final String msg = Messages.getLogMessage("EXPORT_SNAPSHOTS_DETAILS_AS_TEXT_ACTION_ERROR");
            LOGGER.error(msg, e);
            JOptionPane.showMessageDialog(BensikinFrame.getInstance(), msg + "\n\n" + e.getMessage(),
                    Messages.getMessage("SNAPSHOTS_DETAIL_EXPORT_AS_TEXT_ERROR_TITLE"), JOptionPane.ERROR_MESSAGE);
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

    }

}
