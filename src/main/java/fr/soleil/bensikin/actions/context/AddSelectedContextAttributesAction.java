// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/bensikin/bensikin/actions/context/AddSelectedContextAttributesAction.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class
// AddSelectedContextAttributesAction.
// (Claisse Laurent) - 16 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.5 $
//
// $Log: AddSelectedContextAttributesAction.java,v $
// Revision 1.5 2007/08/24 14:04:53 ounsy
// bug correction with context printing as text
//
// Revision 1.4 2007/08/23 12:57:22 ounsy
// minor changes
//
// Revision 1.3 2006/04/10 08:46:54 ounsy
// Bensikin action now all inherit from BensikinAction for easy rights
// management
//
// Revision 1.2 2005/12/14 16:01:27 ounsy
// attributes added to the tree are automatically added to the attributes select
// table
//
// Revision 1.1 2005/12/14 14:07:17 ounsy
// first commit including the new "tools,xml,lifecycle,profile" sub-directories
// under "bensikin.bensikin" and removing the same from their former locations
//
// Revision 1.1.1.2 2005/08/22 11:58:33 chinkumo
// First commit
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.bensikin.actions.context;

import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.Action;
import javax.swing.tree.TreePath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.bensikin.actions.BensikinAction;
import fr.soleil.bensikin.components.context.detail.ContextAttributesTree;
import fr.soleil.bensikin.components.context.detail.PossibleAttributesTree;
import fr.soleil.bensikin.containers.context.ContextActionPanel;
import fr.soleil.bensikin.containers.context.ContextDataPanel;
import fr.soleil.bensikin.models.ContextAttributesTreeModel;
import fr.soleil.bensikin.tools.Messages;

/**
 * Adds the selected attributes (or attributes that are under selected nodes) to
 * the current context.
 * <UL>
 * <LI>Gets the list of attributes that are under one of the selected tree
 * nodes; if that list is empty, do nothing.
 * <LI>Adds those attributes to the current ContextAttributesTreeModel instance.
 * <LI>Logs the action's success or failure.
 * </UL>
 * 
 * @author CLAISSE
 */
public class AddSelectedContextAttributesAction extends BensikinAction {

    private static final long serialVersionUID = -7536776068615735616L;

    private static final Logger LOGGER = LoggerFactory.getLogger(AddSelectedContextAttributesAction.class);

    /**
     * Standard action constructor that sets the action's name.
     * 
     * @param name
     *            The action name
     */
    public AddSelectedContextAttributesAction(final String name) {
        putValue(Action.NAME, name);
    }

    @Override
    public void actionPerformed(final ActionEvent evt) {
        final PossibleAttributesTree leftTree = PossibleAttributesTree.getInstance();
        final List<TreePath> listToAdd = leftTree.getListOfAttributesTreePathUnderSelectedNodes(false);

        if (listToAdd.size() != 0) {
            final ContextAttributesTreeModel model = ContextAttributesTreeModel.getInstance(false);

            try {
                model.addSelectedAttributes(listToAdd, true);
                model.reload();
                if (ContextAttributesTree.getInstance() != null) {
                    ContextAttributesTree.getInstance().expandAll(true);
                }
                ContextActionPanel.getInstance().updateRegisterButton();
                if (model.isAttributeListChanged()) {
                    RegisterContextAction register = RegisterContextAction.getInstance();
                    if (register != null) {
                        register.setEnabled(true);
                    }
                    ContextDataPanel.getInstance().resetDateAndIDField();
                }
                final String msg = Messages.getLogMessage("ADD_SELECTED_CONTEXT_ATTRIBUTES_ACTION_OK");
                LOGGER.debug(msg);

            } catch (final Exception e) {
                final String msg = Messages.getLogMessage("ADD_SELECTED_CONTEXT_ATTRIBUTES_ACTION_KO");
                LOGGER.error(msg, e);
                return;
            }
            ContextActionPanel.getInstance().allowPrint(false);
        }
    }
}
