// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/bensikin/bensikin/actions/context/AbsRefreshAction.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class AbsRefreshAction.
// (Pigeon Xavier) - 03 octobre 2007
//
// $Author: soleilarc $
//
// $Revision: 1.1 $
//
// $Log: AbsRefreshAction.java,v $
// Revision 1.1 2007/10/03 16:01:07 soleilarc
// Author: XP
// Mantis bug ID: 6594
// Comment: Add this abstract class between the BensikinAction class and the
// FilterSnapshotsAction and LaunchSnapshotAction classes, to be inherited by
// the 2 last ones. Thanks to AbsRefreshAction, it is possible to refresh the
// snapshot list.
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================

package fr.soleil.bensikin.actions.context;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.Condition;
import fr.soleil.archiving.common.api.tools.Criterions;
import fr.soleil.archiving.common.api.utils.DateUtil;
import fr.soleil.archiving.gui.exceptions.FieldFormatException;
import fr.soleil.archiving.gui.tools.DateUtils;
import fr.soleil.archiving.snap.api.extractor.tools.Tools;
import fr.soleil.archiving.snap.api.tools.SnapConst;
import fr.soleil.archiving.snap.api.tools.SnapshotingException;
import fr.soleil.bensikin.actions.BensikinAction;
import fr.soleil.bensikin.components.OperatorsList;
import fr.soleil.bensikin.components.snapshot.list.SnapshotListTable;
import fr.soleil.bensikin.containers.BensikinFrame;
import fr.soleil.bensikin.containers.snapshot.SnapshotFilterPanel;
import fr.soleil.bensikin.data.context.Context;
import fr.soleil.bensikin.models.SnapshotListTableModel;
import fr.soleil.bensikin.tools.Messages;

public abstract class AbsRefreshAction extends BensikinAction {

    private static final long serialVersionUID = -771297074690105053L;

    private static final Logger LOGGER = LoggerFactory.getLogger(AbsRefreshAction.class);

    /**
     * Verifies the fields validity.
     * 
     * @param source The dialog to get the fields values from
     */
    private void verifyFields(final SnapshotFilterPanel source) throws FieldFormatException {
        final String id = source.getTextId().getText();
        if (id != null && !id.trim().isEmpty()) {
            try {
                // int i =
                Integer.parseInt(id);
            } catch (final NumberFormatException e) {
                throw new FieldFormatException(FieldFormatException.FILTER_SNAPSHOTS_ID);
            }
        }

        final String startTime = source.getTextStartTime().getText();
        if (startTime != null && !startTime.trim().isEmpty()) {
            DateUtils.stringToTimestamp(startTime, true, FieldFormatException.FILTER_SNAPSHOTS_START_TIME);
        }

        final String endTime = source.getTextEndTime().getText();
        if (endTime != null && !endTime.trim().isEmpty()) {
            DateUtils.stringToTimestamp(endTime, true, FieldFormatException.FILTER_SNAPSHOTS_START_TIME);
        }

    }

    /**
     * If both operator and threshold value are filled, builds a Condition from
     * them. Otherwise returns null.
     * 
     * @param selectedItem The operator
     * @param text The value
     * @param id_field_key2 The field's id
     * @return The resulting Condition or null
     */
    public Condition getCondition(final Object selectedItem, final String text, final String id_field_key2) {
        final String _selectedItem = (String) selectedItem;
        String _text = text;
        // Date Formating
        if (SnapConst.TAB_SNAP[2].equals(id_field_key2)) {
            if (!_text.isEmpty()) {
                try {
                    final long milli = DateUtil.stringToMilli(_text);
                    final String date = Tools.formatDate(milli);
                    _text = date;
                } catch (final ArchivingException e) {
                    e.printStackTrace();
                }
            }
        }
        boolean isACriterion = true;

        if (selectedItem == null || _selectedItem.equals(OperatorsList.NO_SELECTION)) {
            isACriterion = false;
        }

        if (_text == null || _text.trim().isEmpty()) {
            isACriterion = false;
        }

        if (isACriterion) {
            return new Condition(id_field_key2, _selectedItem, _text.trim());
        }

        return null;
    }

    /**
     * Returns a Criterions object built from the current values of fields.
     * 
     * @return The Criterions object built from the current values of fields
     */
    private Criterions getSnapshotsSearchCriterions() {
        final SnapshotFilterPanel source = SnapshotFilterPanel.getInstance();
        // Context selectedContext = Context.getSelectedContext();
        // ContextData selectedContextData = selectedContext.getContextData();

        try {
            verifyFields(source);
        } catch (final FieldFormatException e) {
            String msg = "ERROR";
            final String title = Messages.getLogMessage("FILTER_SNAPSHOTS_FIELD_ERROR");

            switch (e.getCode()) {
                case FieldFormatException.FILTER_SNAPSHOTS_ID:
                    msg = Messages.getLogMessage("FILTER_SNAPSHOTS_FIELD_ERROR_ID");
                    break;

                case FieldFormatException.FILTER_SNAPSHOTS_START_TIME:
                    msg = Messages.getLogMessage("FILTER_SNAPSHOTS_FIELD_ERROR_START_TIME");
                    break;

                case FieldFormatException.FILTER_SNAPSHOTS_END_TIME:
                    msg = Messages.getLogMessage("FILTER_SNAPSHOTS_FIELD_ERROR_END_TIME");
                    break;
            }

            JOptionPane.showMessageDialog(BensikinFrame.getInstance(), msg, title, JOptionPane.ERROR_MESSAGE);
            return null;
        }

        final Criterions ret = new Criterions();
        Condition cond;

        cond = getCondition(source.getSelectId().getSelectedItem(), source.getTextId().getText(), SnapConst.ID_SNAP);
        ret.addCondition(cond);

        cond = getCondition(source.getSelectStartTime().getSelectedItem(), source.getTextStartTime().getText(),
                SnapConst.time);
        ret.addCondition(cond);

        cond = getCondition(source.getSelectEndTime().getSelectedItem(), source.getTextEndTime().getText(),
                SnapConst.time);
        ret.addCondition(cond);

        cond = getCondition(source.getSelectComment().getSelectedItem(), source.getTextComment().getText(),
                SnapConst.snap_comment);
        ret.addCondition(cond);

        return ret;
    }

    public void refreshSnapList() {
        final Context selectedContext = Context.getSelectedContext();
        final Criterions searchCriterions = getSnapshotsSearchCriterions();
        if (searchCriterions != null) {
            final SnapshotListTableModel modelToUpdate = (SnapshotListTableModel) SnapshotListTable.getInstance()
                    .getModel();
            if (selectedContext != null) {
                try {
                    selectedContext.loadSnapshots(searchCriterions);
                    modelToUpdate.updateList(selectedContext.getSnapshots());
                    final String msg = Messages.getLogMessage("FILTER_SNAPSHOTS_ACTION_OK");
                    LOGGER.debug(msg);
                } catch (final SnapshotingException e) {
                    String msg;
                    if (e.computeIsDueToATimeOut()) {
                        msg = Messages.getLogMessage("FILTER_SNAPSHOTS_ACTION_TIMEOUT");
                    } else {
                        msg = Messages.getLogMessage("FILTER_SNAPSHOTS_ACTION_KO");
                    }
                    LOGGER.error(msg, e);
                } catch (final Exception e) {
                    final String msg = Messages.getLogMessage("FILTER_SNAPSHOTS_ACTION_KO");
                    LOGGER.error(msg, e);
                }
            }
        }

    }
}
